package org.ej4tezos.org.ej4tezos.testing;

import org.ej4tezos.testing.FlextesaEnvironment;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.Rule;

import static org.junit.Assert.*;

public class FlextesaEnvironmentTest {
    @ClassRule
    public static FlextesaEnvironment fteClass = new FlextesaEnvironment();

    @Rule
    public FlextesaEnvironment fteTest = new FlextesaEnvironment();

    @Test
    public void testFlextesaEnvironmentForTest() {
        assertNotNull(fteTest.getUrl());
    }

    @Test
    public void testFlextesaEnvironmentForClass() {
        assertNotNull(fteClass.getUrl());
    }
}
