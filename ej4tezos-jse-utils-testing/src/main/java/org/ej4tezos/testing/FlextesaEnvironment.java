package org.ej4tezos.testing;

import org.apache.commons.io.IOUtils;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.core5.http.Header;
import org.apache.hc.core5.http.HttpStatus;
import org.ej4tezos.model.TezosIdentity;
import org.ej4tezos.model.exception.TezosModelException;
import org.ej4tezos.utils.io.IOToolbox;
import org.json.JSONObject;
import org.junit.rules.ExternalResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.*;

import static java.lang.String.format;

/**
 * Sets up Flextesa sandbox using docker
 */
public class FlextesaEnvironment extends ExternalResource {
    public static final int FLEXTESA_PORT = 20000;
    public static final int MAX_STARTUP_RETRIES = 60;
    public static final int STARTUP_RETRY_DELAY_MS = 500;
    public static final String GENESIS_BLOCK = "Ps9mPmXaRzmzk35gbAYNCAw6UXdE2qoABTHbN2oEEc1qM7CwT9P";
    public static final String DEFAULT_TEST_BOX_NAME = System.getenv().getOrDefault("FLEXTESA_BOX_NAME",
            "delphibox");
    public static final String DEFAULT_IMAGE = "tqtezos/flextesa:20201214";
    public static final String[] DEFAULT_IDENTITY_NAMES = {"alice", "bob"};

    private static final Logger LOG = LoggerFactory.getLogger(FlextesaEnvironment.class);

    private String dockerNetName;
    private String imageName;
    private String testBoxName;
    private String containerName;
    private String containerID;
    private String containerIP;
    private String[] identityNames;
    private Map<String, TezosIdentity> identities = new HashMap<>();
    private int maxStartupRetries;

    public FlextesaEnvironment() {
        this(DEFAULT_IMAGE, DEFAULT_TEST_BOX_NAME, DEFAULT_IDENTITY_NAMES, MAX_STARTUP_RETRIES);
    }

    public FlextesaEnvironment(String imageName, String testBoxName, String[] identityNames, int maxStartupRetries) {
        this.imageName = imageName;
        this.testBoxName = testBoxName;
        this.identityNames = identityNames;
        this.maxStartupRetries = maxStartupRetries;
    }

    public String getUrl() {
        if (containerName == null) {
            throw new RuntimeException("unavailable while a test isn't running");
        }
        return format("http://%s:%d", containerIP, FLEXTESA_PORT);
    }

    public TezosIdentity getIdentity(String name) {
        return identities.get(name);
    }

    @Override
    protected void before() throws Throwable {
        long ts = System.currentTimeMillis();
        containerName = "tezos-sandbox" + ts;
        dockerNetName = System.getenv().get("DOCKER_NET");
        List<String> cmds = new ArrayList<>(Arrays.asList("docker", "run", "--rm", "--name", containerName,
                "--detach"));
        if (dockerNetName != null) {
            cmds.add("--network");
            cmds.add(dockerNetName);
        }
        cmds.add(imageName);
        cmds.add(testBoxName);
        cmds.add("start");

        // Start Flextesa container on an ephemeral port
        LOG.info("Launching Flextesa as {}: {}", containerName,
                Arrays.toString(cmds.toArray()));
        runAndDrain(cmds);
        containerID = runAndDrain(Arrays.asList("docker", "inspect", "--format", "{{.Id}}",
                containerName));
        if(containerID.length() == 0) {
            throw new RuntimeException("Unable to determine Flextesa container ID");
        }
        LOG.info("Launched Flextesa {} as {}", containerName, containerID);

        containerIP = runAndDrain(Arrays.asList("docker", "inspect", "--format",
                "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}",
                containerName));
        if(containerIP.length() == 0) {
            throw new RuntimeException("Unable to determine Flextesa container IP");
        }
        LOG.info("Launched Flextesa {} has IP {}", containerName, containerIP);

        waitTillReady();
        LOG.info("Flextesa {}/{}/{} is ready!", containerName, containerID, containerIP);

        for (String identityName : identityNames) {
            TezosIdentity tid = tezosIdentity(identityName);
            identities.put(identityName, tid);
            LOG.info("Key: {} = {}", identityName, tid);
        }
    }

    @Override
    protected void after() {
        String[] cmds = {"docker", "kill", containerName};
        try {
            new ProcessBuilder(cmds).start().waitFor();
            LOG.info("Flextesa {} is stopped!", containerName);
        } catch (Exception e) {
            LOG.error("Failed to stop {} via {}", containerName, Arrays.toString(cmds), e);
        } finally {
            containerName = null;
            identities.clear();
        }
    }

    private TezosIdentity tezosIdentity(String name) throws IOException, InterruptedException, TezosModelException {
        String[] split = runAndDrain(Arrays.asList("docker", "exec", containerName, "flextesa", "key", name)).
                split(",");
        String pubK = split[1];
        String pubAddr = split[2];
        String pK = split[3].substring("unencrypted:".length());
        return TezosIdentity.toTezosIdentity(pK, pubK, pubAddr);
    }

    private String runAndDrain(List<String> cmds) throws IOException, InterruptedException {
        ProcessBuilder pb = new ProcessBuilder(cmds);
        pb.redirectErrorStream(true);
        Process p = pb.start();
        String out = IOToolbox.drainStream(p.getInputStream());
        p.waitFor();
        if (p.exitValue() != 0) {
            throw new IOException(format("Failed to launch %s: exit code %d, output %s",
                    Arrays.toString(cmds.toArray()), p.exitValue(), out));
        }
        return out;
    }

    private void waitTillReady() throws Throwable {
        Exception lastE = null;
        for (int i = 0; i < maxStartupRetries; i++) {
            try {
                JSONObject jo = executeJsonGet(new URL(getUrl() + "/version"));
                LOG.info("Version is: {}.{}.{}", jo.getJSONObject("version").get("major"),
                        jo.getJSONObject("version").get("minor"),
                        jo.getJSONObject("version").get("additional_info"));

                jo = executeJsonGet(new URL(getUrl() + "/chains/main/blocks/head/protocols"));
                String protocol = jo.getString("protocol");
                if(protocol.equals(GENESIS_BLOCK)) {
                    throw new RuntimeException("Protocols are still reporting genesis block " + protocol);
                }
                LOG.info("Protocol is: {}", protocol);

                jo = executeJsonGet(new URL(getUrl() + "/monitor/bootstrapped"));
                LOG.info("Head block is: {}", jo.getString("block"));
                return;
            } catch (Exception e) {
                LOG.debug("Failed detecting bootstrapped Flextesa: {}", e.toString());
                LOG.info("Not ready, waiting...");
                lastE = e;
                Thread.sleep(STARTUP_RETRY_DELAY_MS);
            }
        }

        LOG.error("Failed detecting bootstrapped Flextesa!", lastE);
        throw lastE;
    }

    private String executeGet(URL url, Header... headers) throws IOException, URISyntaxException {

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpGet get = new HttpGet(url.toURI());
            if (headers.length != 0) {
                get.setHeaders(headers);
            }
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                int statusCode = response.getCode();
                if (statusCode != HttpStatus.SC_OK) {
                    String message = MessageFormat.format("Cannot execute the HTTP GET: {0}! (code = {1})",
                            url, statusCode);
                    throw new IOException(message);
                }
                return IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);
            }
        }
    }

    private JSONObject executeJsonGet(URL url, Header... headers) throws IOException, URISyntaxException {
        String body = executeGet(url, headers);

        return new JSONObject(body);
    }
}
