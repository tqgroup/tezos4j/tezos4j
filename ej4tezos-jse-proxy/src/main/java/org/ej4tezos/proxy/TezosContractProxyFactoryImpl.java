// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.proxy;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.text.MessageFormat;

import java.lang.reflect.Proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.api.TezosContract;
import org.ej4tezos.api.TezosContractStorage;
import org.ej4tezos.api.TezosContractStorageData;

import org.ej4tezos.api.exception.TezosException;

import org.ej4tezos.papi.TezosContractProxy;
import org.ej4tezos.papi.TezosContractProxyFactory;
import org.ej4tezos.papi.TezosContractProxyFactoryContext;

import org.ej4tezos.model.TezosPrivateKey;
import org.ej4tezos.model.TezosContractAddress;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosContractProxyFactoryImpl<C extends TezosContract, S extends TezosContractStorage, D extends TezosContractStorageData> implements TezosContractProxyFactory<C,S,D> {

    private static final Logger LOG = LoggerFactory.getLogger(TezosContractProxyFactoryImpl.class);

    public C createProxy (TezosContractProxyFactoryContext context, Class<C> tezosContractClass, TezosContractAddress tezosContractAddress, TezosPrivateKey adminPrivateKey) throws TezosException {
        return createProxy(context, tezosContractClass, tezosContractAddress, adminPrivateKey, TezosContractProxy.Mode.SYNCHRONOUS);
    }

    public C createProxy (TezosContractProxyFactoryContext context, Class<C> tezosContractClass, TezosContractAddress tezosContractAddress, TezosPrivateKey adminPrivateKey, TezosContractProxy.Mode mode) throws TezosException {

        // Log the call:
        LOG.debug("createProxy (context = {}, tezosContractClass = {}, tezosContractAddress = {}, adminPrivateKey = {})",
                                context, tezosContractClass, tezosContractAddress, adminPrivateKey);

        // Sanity check:
        checkNotNull(context, "context should not be null", TezosException.class);
        checkNotNull(adminPrivateKey, "adminPrivateKey should not be null", TezosException.class);
        checkNotNull(tezosContractClass, "tezosContractClass should not be null", TezosException.class);
        checkNotNull(tezosContractAddress, "tezosContractAddress should not be null", TezosException.class);

        try {

            // Create the handler:
            TezosContractInvocationHandler handler
                = new TezosContractInvocationHandler(context, tezosContractAddress, adminPrivateKey, mode);

            // Initialize the handler:
            handler.init();

            // Create the proxy:
            C result
                = (C) Proxy
                    .newProxyInstance(TezosContractProxyFactoryImpl.class.getClassLoader(),
                                      new Class[]{tezosContractClass},
                                      handler);

            // Log and return the result:
            LOG.debug("result = {}", result);
            return result;
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while creating a proxy: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    public S createProxy (TezosContractProxyFactoryContext context, Class<S> tezosStorageClass, Class<D> tezosStorageDataClass, TezosContractAddress tezosContractAddress) throws TezosException {
        return createProxy (context, tezosStorageClass, tezosStorageDataClass, tezosContractAddress, TezosContractStorage.Mode.CACHED);
    }

    public S createProxy (TezosContractProxyFactoryContext context, Class<S> tezosStorageClass, Class<D> tezosStorageDataClass, TezosContractAddress tezosContractAddress, TezosContractStorage.Mode mode) throws TezosException {

        // Log the call:
        LOG.debug("createProxy (context = {}, tezosStorageClass = {}, tezosStorageDataClass = {}, tezosContractAddress = {})",
                                context, tezosStorageClass, tezosStorageDataClass, tezosContractAddress);

        // Sanity check:
        checkNotNull(context, "context should not be null", TezosException.class);
        checkNotNull(tezosStorageClass, "tezosStorageClass should not be null", TezosException.class);
        checkNotNull(tezosContractAddress, "tezosContractAddress should not be null", TezosException.class);
        checkNotNull(tezosStorageDataClass, "tezosStorageDataClass should not be null", TezosException.class);

        try {

            // Create the handler:
            TezosStorageInvocationHandler handler
                = new TezosStorageInvocationHandler(tezosStorageDataClass, context, tezosContractAddress);


            // Create the proxy:
            S result
                = (S) Proxy
                    .newProxyInstance(TezosContractProxyFactoryImpl.class.getClassLoader(),
                                      new Class[]{tezosStorageClass},
                                      handler);

            //
            result.setMode(mode);

            // Log and return the result:
            LOG.debug("result = {}", result);
            return result;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while creating a proxy: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public String toString () {
        return "TezosContractProxyFactoryImpl";
    }
}