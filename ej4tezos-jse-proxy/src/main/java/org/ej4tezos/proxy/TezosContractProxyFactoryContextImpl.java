// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.proxy;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.text.MessageFormat;

import java.security.SecureRandom;

import org.ej4tezos.api.TezosFeeService;
import org.ej4tezos.api.TezosKeyService;
import org.ej4tezos.api.TezosCoreService;

import org.ej4tezos.api.exception.TezosException;

import org.ej4tezos.impl.*;
import org.ej4tezos.papi.*;

import org.ej4tezos.crypto.impl.TezosKeyServiceImpl;
import org.ej4tezos.crypto.impl.TezosCryptoProviderImpl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosContractProxyFactoryContextImpl implements TezosContractProxyFactoryContext {

    public final static String DEFAULT_NODE = "https://mainnet.smartpy.io/";

    private String nodeUrl = null;
    private TezosKeyService tezosKeyService = null;
    private TezosFeeService tezosFeeService = null;
    private TezosCoreService tezosCoreService = null;
    private TezosConnectivity tezosConnectivity = null;
    private TezosCryptoProvider tezosCryptoProvider = null;
    private TezosInvocationBuilder tezosInvocationBuilder = null;
    private TezosResourceLoader tezosResourceLoader = null;

    public TezosContractProxyFactoryContextImpl () {
        this(DEFAULT_NODE);
    }

    public TezosContractProxyFactoryContextImpl (String nodeUrl) {
        super();
        this.nodeUrl = nodeUrl;
    }

    @Override
    public synchronized TezosConnectivity getTezosConnectivity () throws TezosException {

        try {

            if (tezosConnectivity == null) {
                tezosConnectivity = new TezosConnectivityImpl();
                ((TezosConnectivityImpl) tezosConnectivity).setNodeUrl(nodeUrl);
                ((TezosConnectivityImpl) tezosConnectivity).init();
            }

            return tezosConnectivity;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving TezosConnectivity: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public synchronized TezosKeyService getTezosKeyService () throws TezosException {

        try {

            if(tezosKeyService == null) {
                tezosKeyService = new TezosKeyServiceImpl();
                ((TezosKeyServiceImpl)tezosKeyService).setTezosCryptoProvider(getTezosCryptoProvider());
                ((TezosKeyServiceImpl)tezosKeyService).init();
            }

            return tezosKeyService;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving TezosKeyService: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public synchronized TezosFeeService getTezosFeeService () throws TezosException {

        try {

            if(tezosFeeService == null) {
                tezosFeeService = new TezosFeeServiceImpl();
                ((TezosFeeServiceImpl)tezosFeeService).init();
            }

            return tezosFeeService;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving TezosFeeService: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public synchronized TezosCoreService getTezosCoreService () throws TezosException {

        try {

            if(tezosCoreService == null) {
                tezosCoreService = new TezosCoreServiceImpl();
                ((TezosCoreServiceImpl)tezosCoreService).setTezosConnectivity(getTezosConnectivity());
                ((TezosCoreServiceImpl)tezosCoreService).init();
            }

            return tezosCoreService;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving TezosCoreService: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public synchronized TezosCryptoProvider getTezosCryptoProvider () throws TezosException {

        try {

            if(tezosCryptoProvider == null) {
                tezosCryptoProvider = new TezosCryptoProviderImpl();
                ((TezosCryptoProviderImpl)tezosCryptoProvider).setSecureRandom(new SecureRandom());
                ((TezosCryptoProviderImpl)tezosCryptoProvider).init();
            }

            return tezosCryptoProvider;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving TezosCryptoProvider: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public synchronized TezosResourceLoader getTezosResourceLoader() throws TezosException {

        try {

            if(tezosResourceLoader == null) {
                tezosResourceLoader = new SimpleTezosResourceLoader();
            }

            return tezosResourceLoader;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving TezosResourceLoader: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public synchronized TezosInvocationBuilder getTezosInvocationBuilder () throws TezosException {

        try {

            if(tezosInvocationBuilder == null) {
                tezosInvocationBuilder = new TezosInvocationBuilderImpl();
            }

            return tezosInvocationBuilder;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving TezosInvocationBuilder: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public String toString () {
        return "TezosContractProxyFactoryContextImpl";
    }
}