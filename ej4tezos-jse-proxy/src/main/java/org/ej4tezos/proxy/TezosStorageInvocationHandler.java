// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.proxy;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

import org.ej4tezos.api.TezosContractStorage;
import org.ej4tezos.api.TezosContractStorageData;
import org.ej4tezos.api.TezosCoreService;
import org.ej4tezos.api.annotation.TezosNativeStorage;
import org.ej4tezos.api.exception.*;
import org.ej4tezos.api.model.TezosBigMapStorage;
import org.ej4tezos.api.model.TezosPackedData;
import org.ej4tezos.cgp.api.TezosContractDefinitionGenerator;
import org.ej4tezos.cgp.api.model.TezosContractDefinitionGeneratorContext;
import org.ej4tezos.impl.TezosPackedDataImpl;
import org.ej4tezos.model.TezosBigMap;
import org.ej4tezos.model.TezosContractAddress;
import org.ej4tezos.papi.TezosConnectivity;
import org.ej4tezos.papi.TezosContractProxyFactoryContext;
import org.ej4tezos.parser.MichelineTezosContractDefinitionGenerator;
import org.ej4tezos.utils.micheline.StorageElement;
import org.ej4tezos.utils.micheline.StorageFlattener;
import org.ej4tezos.utils.micheline.StorageMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.MessageFormat;
import java.util.*;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;
import static org.ej4tezos.utils.asserts.Asserts.checkState;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosStorageInvocationHandler <D extends TezosContractStorageData> implements InvocationHandler {

    private static final Logger LOG = LoggerFactory.getLogger(TezosStorageInvocationHandler.class);

    // Proxy fields:
    private long timestamp = 0;
    private TezosContractStorage.Mode mode = TezosContractStorage.Mode.CACHED;

    // Internal technical fields:
    private D storageData = null;
    private Class<D> storageDataClass;
    private List<String> cachedStorageFieldNameList;
    private TezosContractAddress tezosContractAddress;
    private TezosContractProxyFactoryContext tezosContractProxyFactoryContext;

    public TezosStorageInvocationHandler (Class<D> storageDataClass, TezosContractProxyFactoryContext tezosContractProxyFactoryContext, TezosContractAddress tezosContractAddress) {
        LOG.info("A new instance of 'TezosStorageInvocationHandler' got created; logging on {}...", LOG.getName());
        this.storageDataClass = storageDataClass;
        this.tezosContractAddress = tezosContractAddress;
        this.tezosContractProxyFactoryContext = tezosContractProxyFactoryContext;
    }

    @Override
    public Object invoke (Object proxy, Method method, Object[] args) throws Throwable {

        // Log the call:
        LOG.debug("invoke (proxy = XXX, method = {}, args = {})", method, args);

        try {
            //
            if(method.getAnnotation(TezosNativeStorage.class) != null) {
                return invokeTezosNativeStorageAccessor(proxy, method.getName(), args);
            }
            else {
                return invokeNonTezosNativeStorageAccessor(proxy, method, args);
            }
        }
        catch (TezosStorageException e) {
            throw e;
        }
        catch (Exception ex) {
            String exceptionMessage = MessageFormat.format("An exception occurred while executing 'invoke': {0}", ex.getMessage());
            throw new TezosException(exceptionMessage, ex);
        }
    }

    private Object getBigMap(TezosBigMap bigMap, Object... key)
            throws TezosException {
        TezosCoreService tezosCoreService = tezosContractProxyFactoryContext.getTezosCoreService();
        String packedDataRaw = tezosCoreService.packData(key);
        TezosPackedData packedData = new TezosPackedDataImpl(tezosContractProxyFactoryContext.getTezosCryptoProvider(),
                packedDataRaw);
        return tezosCoreService.getBigMap(bigMap, packedData);
    }

    private Object invokeTezosNativeStorageAccessor (Object proxy, String method, Object[] args) throws Throwable {

        // Log the call:
        LOG.debug("invokeTezosNativeStorageAccessor (proxy = XXX, method = {}, args = {})", method, args);

        // Sanity checks:
        checkNotNull(proxy, "proxy should not be null", TezosInvocationException.class);
        checkNotNull(method, "proxy should not be null", TezosInvocationException.class);

        try {

            // Check if we should load the storage:
            if(storageData == null || mode == TezosContractStorage.Mode.REAL_TIME) {
                storageData = loadStorage();
            }

            // Check the method name:
            if(!method.startsWith("get")) {
                String message = MessageFormat.format("Unsupported method {0}", method);
                throw new TezosException(message);
            }

            // Build the field name:
            String fieldName = method.substring(3, 4).toLowerCase() + method.substring(4);
            LOG.debug("fieldName = {}", fieldName);

            // Retrieve the field value:
            Field field = storageData.getClass().getField(fieldName);
            Object fieldValue = field.get(storageData);

            // Log and return the field value:
            LOG.debug("fieldValue = {}", fieldValue);

            if(fieldValue instanceof TezosBigMap && (args != null && args.length > 0)) {
                TezosBigMap bigMap = (TezosBigMap) fieldValue;
                TezosBigMapStorage bigMapStorage = null;
                TezosBigMapStorage.TezosBigMapStorageKey key = null;
                if(mode == TezosContractStorage.Mode.CACHED) {
                    key = new TezosBigMapStorage.TezosBigMapStorageKey(args);
                    Field bigMapStorageField = storageData.getClass().getField(fieldName + "Accessor");
                    bigMapStorage = (TezosBigMapStorage) bigMapStorageField.get(storageData);
                    fieldValue = bigMapStorage.get(key);
                    if(fieldValue != null) {
                        return fieldValue;
                    }
                }
                try {
                    fieldValue = getBigMap(bigMap, args);
                }
                catch(TezosResourceNotFoundException e) {
                    throw new TezosValueNotFoundException(
                            String.format("can't find %s in big_map %s", Arrays.toString(args), fieldName), e);
                }
                if(mode == TezosContractStorage.Mode.CACHED) {
                    fieldValue = bigMapStorage.putIfAbsent(key, fieldValue);
                }
            }
            return fieldValue;
        }
        catch (TezosStorageException e) {
            throw e;
        }
        catch (Exception ex) {
            String exceptionMessage = MessageFormat.format("An exception occurred while executing 'invokeTezosNativeStorage': {0}", ex.getMessage());
            throw new TezosInvocationException(exceptionMessage, ex);
        }
    }

    private Object invokeNonTezosNativeStorageAccessor (Object proxy, Method method, Object[] args) throws Throwable {

        //
        Object result = null;

        // Log the call:
        LOG.debug("invokeNonTezosNativeStorageAccessor (proxy = XXX, method = {}, args = {})", method, args);

        try {

            switch (method.getName()) {
                case "load":
                    load();
                    break;
                case "setMode":
                    setMode(args);
                    break;
                case "getMode":
                    result = mode;
                    break;
                case "getTimestamp":
                    result = timestamp;
                    break;
                case "getData":
                    result = storageData;
                    break;
                case "getDataClass":
                    result = storageDataClass;
                    break;
                case "toString":
                    result = MessageFormat.format("TezosStorageInvocationHandler[contractAddress={0}]", getTezosContractAddress());
                    break;
                default:
                    String exceptionMessage = MessageFormat.format("No administrative method called {0} exists!", method.getName());
                    throw new RuntimeException(exceptionMessage);
            }

            //
            LOG.debug("result = {}", result);
            return result;
        }

        catch (TezosStorageException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String exceptionMessage = MessageFormat.format("An exception occurred while executing 'invokeNonTezosNativeStorage': {0}", ex.getMessage());
            throw new TezosException(exceptionMessage, ex);
        }
    }

    private void load () throws TezosStorageException {

        // Log the call:
        LOG.debug("load ()");

        //
        try {

            storageData = loadStorage();

        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while loading the storage: {0}", ex.getMessage());
            throw new TezosStorageException(message, ex);
        }
    }

    private void setMode (Object [] args) throws TezosException {

        //
        LOG.debug("setMode (args = {})", args);

        //
        checkNotNull(args, "args should not be null", TezosException.class);
        checkState(args.length == 1, "args length should be one", TezosException.class);
        checkNotNull(args[0], "mode should not be null", TezosException.class);

        //
        if(args[0] instanceof TezosContractStorage.Mode) {
            mode = (TezosContractStorage.Mode) args[0];
        }
        else {
            String message = MessageFormat.format("Unexpected mode class: {0}", args[0].getClass());
            throw new TezosException(message);
        }
    }

    private D loadStorage () throws TezosStorageException {

        D result = null;

        //
        LOG.debug("loadStorage ()");

        try {

            //
            TezosConnectivity connectivity
                = tezosContractProxyFactoryContext.getTezosConnectivity();

            //
            String rawTargetUrl
                = MessageFormat
                    .format("/chains/main/blocks/head/context/contracts/{0}/storage",
                            tezosContractAddress.getAddress());

            URL targetUrl = connectivity.toURL(rawTargetUrl);

            //
            JSONObject storage = connectivity.executeJsonGet(targetUrl);

            //
            List<StorageElement> storageElementList = StorageFlattener.flatten(storage);

            //
            List<String> storageFieldNameList = getStorageFieldNameList();

            //
            Map<String,StorageElement> storageElementMap = new HashMap<>();
            for(int i = 0; i < storageFieldNameList.size(); i++) {
                storageElementMap.put(storageFieldNameList.get(i), storageElementList.get(i));
            }

            //
            result = storageDataClass.newInstance();
            StorageMapper.map(result, storageElementMap);

            // Update the timestamp:
            timestamp = System.currentTimeMillis();
            LOG.debug("timestamp = {}", timestamp);

            //
            if(LOG.isDebugEnabled()) { LOG.debug("result = {}", result); }
            return result;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while loading the storage: {0}", ex.getMessage());
            throw new TezosStorageException(message, ex);
        }
    }

    private synchronized List<String> getStorageFieldNameList () throws TezosStorageException {

        //
        LOG.debug("getStorageFieldNameList ()");

        try {

            //
            if(cachedStorageFieldNameList != null) {
                LOG.debug("Storage field name list has already been populated; return the cached content!");
                return cachedStorageFieldNameList;
            }

            //
            TezosConnectivity connectivity
                = tezosContractProxyFactoryContext.getTezosConnectivity();

            //
            String rawTargetUrl
                = MessageFormat
                    .format("/chains/main/blocks/head/context/contracts/{0}/script",
                            tezosContractAddress.getAddress());

            URL targetUrl = connectivity.toURL(rawTargetUrl);

            //
            JSONObject script = connectivity.executeJsonGet(targetUrl);

            //
            TezosContractDefinitionGenerator tcdGenerator
                = new MichelineTezosContractDefinitionGenerator();

            TezosContractDefinitionGeneratorContext context = new TezosContractDefinitionGeneratorContext() {
                public String getName () {
                    return "anonymous";
                }
            };

            //
            cachedStorageFieldNameList = new ArrayList<>();
            JSONObject tezosContractDefinition = tcdGenerator.generate(context, script);
            JSONArray storageDefinitionArray = tezosContractDefinition.getJSONArray("storage");
            for(int i = 0; i < storageDefinitionArray.length(); i++) {
                cachedStorageFieldNameList.add(storageDefinitionArray.getJSONObject(i).getString("name"));
            }

            //
            LOG.debug("cachedStorageFieldNameList = {}", cachedStorageFieldNameList);
            return cachedStorageFieldNameList;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while loading the storage: {0}", ex.getMessage());
            throw new TezosStorageException(message, ex);
        }
    }

    public void setTezosContractProxyFactoryContext (TezosContractProxyFactoryContext tezosContractProxyFactoryContext) {
        this.tezosContractProxyFactoryContext = tezosContractProxyFactoryContext;
    }

    public TezosContractProxyFactoryContext getTezosContractProxyFactoryContext () {
        return tezosContractProxyFactoryContext;
    }

    public void setTezosContractAddress (TezosContractAddress tezosContractAddress) {
        this.tezosContractAddress = tezosContractAddress;
    }

    public TezosContractAddress getTezosContractAddress () {
        return tezosContractAddress;
    }

    @Override
    public String toString () {
        return "TezosStorageInvocationHandler";
    }
}