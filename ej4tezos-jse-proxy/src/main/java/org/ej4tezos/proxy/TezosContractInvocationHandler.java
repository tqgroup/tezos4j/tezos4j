// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.proxy;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

import org.ej4tezos.api.annotation.TezosDefaultFunction;
import org.ej4tezos.api.annotation.TezosNativeFunction;
import org.ej4tezos.api.exception.TezosException;
import org.ej4tezos.api.exception.TezosInvocationException;
import org.ej4tezos.impl.TezosAbstractService;
import org.ej4tezos.impl.model.InvokeOperationResult;
import org.ej4tezos.model.TezosContractAddress;
import org.ej4tezos.model.TezosIdentity;
import org.ej4tezos.model.TezosPrivateKey;
import org.ej4tezos.papi.TezosContractProxy;
import org.ej4tezos.papi.TezosContractProxyFactoryContext;
import org.ej4tezos.papi.TezosInvocationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.text.MessageFormat;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosContractInvocationHandler extends TezosAbstractService implements InvocationHandler {

    private static final Logger LOG = LoggerFactory.getLogger(TezosContractInvocationHandler.class);

    private TezosContractProxy.Mode mode;
    private TezosInvocationBuilder tezosInvocationBuilder;

    private String genericRunOperationTemplate;
    private String genericForgeOperationTemplate;
    private String genericPreApplyOperationTemplate;

    public TezosContractInvocationHandler(TezosContractProxyFactoryContext context,
                                          TezosContractAddress tezosContractAddress,
                                          TezosIdentity adminIdentity,
                                          TezosContractProxy.Mode mode) throws TezosException {
        this(context, tezosContractAddress, adminIdentity.getPrivateKey(), mode);
        setAdminIdentity(adminIdentity);
    }

    public TezosContractInvocationHandler(TezosContractProxyFactoryContext context,
                                          TezosContractAddress tezosContractAddress,
                                          TezosPrivateKey adminPrivateKey,
                                          TezosContractProxy.Mode mode) throws TezosException {

        LOG.info("A new instance of 'TezosContractInvocationHandler' got created; logging on {}...", LOG.getName());

        this.mode = mode;

        setAdminPrivateKey(adminPrivateKey);
        setTezosContractAddress(tezosContractAddress);

        setTezosKeyService(context.getTezosKeyService());
        setTezosFeeService(context.getTezosFeeService());
        setTezosCoreService(context.getTezosCoreService());
        setTezosConnectivity(context.getTezosConnectivity());
        setTezosCryptoProvider(context.getTezosCryptoProvider());
        setTezosInvocationBuilder(context.getTezosInvocationBuilder());
        setTezosResourceLoader(context.getTezosResourceLoader());
    }

    @PostConstruct
    public void init () throws Exception {

        LOG.debug("init()");

        // Load templates:
        genericRunOperationTemplate = loadTemplate("genericRunOperation.template");
        genericForgeOperationTemplate = loadTemplate("genericForgeOperation.template");
        genericPreApplyOperationTemplate = loadTemplate("genericPreApplyOperation.template");

        super.init();

    }

    @Override
    public Object invoke (Object proxy, Method method, Object[] args) throws Throwable {

        // Log the call:
        LOG.debug("invoke (proxy = XXX, method = {}, args = {})", method, args);

        try {

            //
            if(method.getAnnotation(TezosNativeFunction.class) != null) {
                String methodName = method.getAnnotation(TezosDefaultFunction.class) != null ? "default" : method.getName();
                return invokeTezosNativeFunction(proxy, methodName, args);
            }
            else {
                return invokeNonTezosNativeFunction(proxy, method, args);
            }
        }

        catch (Exception ex) {
            String exceptionMessage = MessageFormat.format("An exception occurred while executing 'invoke': {0}", ex.getMessage());
            throw new TezosException(exceptionMessage, ex);
        }
    }

    private Object invokeTezosNativeFunction (Object proxy, String method, Object[] args) throws Throwable {

        // Log the call:
        LOG.debug("invokeTezosNativeFunction (proxy = XXX, method = {}, args = {})", method, args);

        //
        checkNotNull(proxy, "proxy should not be null", TezosInvocationException.class);
        checkNotNull(method, "method should not be null", TezosInvocationException.class);
        checkNotNull(tezosInvocationBuilder, "tezosInvocationBuilder should not be null", TezosInvocationException.class);

        try {

            // Retrieve the parameters:
            String parameters =
                tezosInvocationBuilder
                    .buildInvocationParameters(method, args).toString();

            LOG.debug("args = {}", args);

            // Inject the parameters in the various templates:
            String invocationRunOperationTemplate
                = genericRunOperationTemplate.replace("${invocationParameters}", parameters);

            String invocationForgeOperationTemplate
                = genericForgeOperationTemplate.replace("${invocationParameters}", parameters);

            String invocationPreApplyOperationTemplate
                = genericPreApplyOperationTemplate.replace("${invocationParameters}", parameters);

            // Proceed with the function invocation:
            InvokeOperationResult invokeOperationResult
                = invoke(invocationRunOperationTemplate, invocationForgeOperationTemplate, invocationPreApplyOperationTemplate);

            LOG.debug("invokeOperationResult = {}", invokeOperationResult);

            // If we are in synchronous mode, we wait for the transaction to be
            // included on the chain:
            if(mode == TezosContractProxy.Mode.SYNCHRONOUS) {
                LOG.debug("Waiting for transaction (hash={})...", invokeOperationResult.getTransactionHash());
                getTezosCoreService().waitForTransaction(invokeOperationResult.getBlockHash(),
                        invokeOperationResult.getTransactionHash());
            }

            // Returns the transaction hash:
            return invokeOperationResult.getTransactionHash();
        }

        catch (Exception ex) {
            String exceptionMessage = MessageFormat.format("An exception occurred while executing 'invokeTezosNativeFunction': {0}", ex.getMessage());
            throw new TezosException(exceptionMessage, ex);
        }
    }

    private Object invokeNonTezosNativeFunction (Object proxy, Method method, Object[] args) throws Throwable {

        //
        Object result = null;

        // Log the call:
        LOG.debug("invokeNonTezosNativeFunction (proxy = XXX, method = {}, args = {})", method, args);

        try {

            switch (method.getName()) {
                case "getContractAddress":
                    result = getTezosContractAddress();
                    break;
                case "isSynchronous":
                    result = (mode == TezosContractProxy.Mode.SYNCHRONOUS);
                    break;
                case "getMode":
                    result = mode;
                    break;
                case "toString":
                    result = MessageFormat.format("TezosContractInvocationHandler[contractAddress={0}]", getTezosContractAddress());
                    break;
                default:
                    //
                    String exceptionMessage = MessageFormat.format("No administrative method called {0} exists!", method.getName());
                    throw new RuntimeException(exceptionMessage);
            }

            //
            LOG.debug("result = {}", result);
            return result;
        }

        catch (Exception ex) {
            String exceptionMessage = MessageFormat.format("An exception occurred while executing 'invokeNonTezosNativeFunction': {0}", ex.getMessage());
            throw new TezosException(exceptionMessage, ex);
        }
    }

    public void setTezosInvocationBuilder (TezosInvocationBuilder tezosInvocationBuilder) {
        this.tezosInvocationBuilder = tezosInvocationBuilder;
    }

    public TezosInvocationBuilder getTezosInvocationBuilder () {
        return tezosInvocationBuilder;
    }

    @Override
    public String toString () {
        return "TezosContractInvocationHandler";
    }
}