// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.papi;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.net.URL;
import java.net.MalformedURLException;

import org.json.JSONArray;
import org.json.JSONObject;

import org.apache.hc.core5.http.Header;
import org.apache.hc.core5.http.HttpHeaders;
import org.apache.hc.core5.http.message.BasicHeader;

import org.ej4tezos.api.exception.TezosException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public interface TezosConnectivity {

    Header CONTENT_APPLICATION_JSON = new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json");
    Header CONTENT_APPLICATION_TEXT = new BasicHeader(HttpHeaders.CONTENT_TYPE, "text/plain");

    URL toURL (String rawUrl) throws MalformedURLException, TezosException;

    String executeGet (URL url, Header ... headers) throws TezosException;

    JSONObject executeJsonGet (URL url, Header ... headers) throws TezosException;

    String executePost (URL url, JSONObject jsonObject, Header ... headers) throws TezosException;

    JSONObject executeJsonPost (URL url, JSONObject jsonObject, Header ... headers) throws TezosException;

    JSONObject executeJsonPost (URL url, JSONArray jsonArray, Header ... headers) throws TezosException;

    JSONArray executeJsonArrayPost (URL url, JSONArray jsonArray, Header ... headers) throws TezosException;

    String executePost (URL url, String data, Header ... headers) throws TezosException;

    Header [] addHeader (Header [] headers, Header header);

}