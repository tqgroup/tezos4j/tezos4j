// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.papi;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

import org.ej4tezos.api.TezosFeeService;
import org.ej4tezos.api.TezosKeyService;
import org.ej4tezos.api.TezosCoreService;

import org.ej4tezos.api.exception.TezosException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public interface TezosContractProxyFactoryContext {

    TezosKeyService getTezosKeyService () throws TezosException;

    TezosFeeService getTezosFeeService () throws TezosException;

    TezosCoreService getTezosCoreService () throws TezosException;

    TezosConnectivity getTezosConnectivity () throws TezosException;

    TezosCryptoProvider getTezosCryptoProvider () throws TezosException;

    TezosResourceLoader getTezosResourceLoader() throws TezosException;

    TezosInvocationBuilder getTezosInvocationBuilder () throws TezosException;

}