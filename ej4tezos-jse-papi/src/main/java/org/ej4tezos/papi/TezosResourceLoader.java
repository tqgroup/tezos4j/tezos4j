package org.ej4tezos.papi;

import java.io.InputStream;

public interface TezosResourceLoader {
    /**
     * Returns an input stream for reading the specified resource.
     *
     * @param name The resource name
     * @return An input stream for reading the resource, or null if the resource could not be found
     */
    InputStream getResourceAsStream(String name);
}
