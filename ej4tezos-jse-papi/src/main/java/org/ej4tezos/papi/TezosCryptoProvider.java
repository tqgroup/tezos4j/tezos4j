// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.papi;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.security.MessageDigest;

import org.ej4tezos.model.TezosIdentity;
import org.ej4tezos.model.TezosPublicKey;
import org.ej4tezos.model.TezosPrivateKey;

import org.ej4tezos.papi.model.Ed25519KeyPair;

import org.ej4tezos.api.exception.TezosException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public interface TezosCryptoProvider {

    enum EncoderType { HEX, BASE58 }

    MessageDigest getSha256digester ();

    MessageDigest getBlake2B160Digester ();

    MessageDigest getBlake2B256Digester ();

    Ed25519KeyPair generateEd25519KeyPair () throws TezosException;

    String encode (byte [] payload, EncoderType encoderType) throws TezosException;

    byte [] decode (String payload, EncoderType encoderType) throws TezosException;

    byte [] createSignature (byte [] payload, TezosPrivateKey privateKey) throws TezosException;

    byte [] extractRawPublicKey (TezosIdentity tezosIdentity) throws TezosException;

    byte [] extractRawPublicKey (TezosPublicKey tezosPublicKey) throws TezosException;

    byte [] extractRawPublicKey (TezosPrivateKey tezosPrivateKey) throws TezosException;

    byte [] extractRawPrivateKey (TezosIdentity tezosIdentity) throws TezosException;

    byte [] extractRawPrivateKey (TezosPrivateKey tezosPrivateKey) throws TezosException;

    byte [] computeDoubleCheckSum (byte [] payload) throws TezosException;

    String getDescription ();

}