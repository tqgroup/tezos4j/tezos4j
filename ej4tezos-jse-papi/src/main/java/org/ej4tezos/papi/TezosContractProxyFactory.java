// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.papi;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.api.TezosContract;
import org.ej4tezos.api.TezosContractStorage;
import org.ej4tezos.api.TezosContractStorageData;

import org.ej4tezos.api.exception.TezosException;

import org.ej4tezos.papi.TezosContractProxy.Mode;

import org.ej4tezos.model.TezosPrivateKey;
import org.ej4tezos.model.TezosContractAddress;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public interface TezosContractProxyFactory <C extends TezosContract, S extends TezosContractStorage, D extends TezosContractStorageData> {

    C createProxy (TezosContractProxyFactoryContext context, Class<C> tezosContractClass, TezosContractAddress tezosContractAddress, TezosPrivateKey adminPrivateKey) throws TezosException;

    C createProxy (TezosContractProxyFactoryContext context, Class<C> tezosContractClass, TezosContractAddress tezosContractAddress, TezosPrivateKey adminPrivateKey, Mode mode) throws TezosException;

    S createProxy (TezosContractProxyFactoryContext context, Class<S> tezosStorageClass, Class<D> tezosStorageDataClass, TezosContractAddress tezosContractAddress) throws TezosException;

    S createProxy (TezosContractProxyFactoryContext context, Class<S> tezosStorageClass, Class<D> tezosStorageDataClass, TezosContractAddress tezosContractAddress, TezosContractStorage.Mode mode) throws TezosException;

}