// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.papi.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public interface MichelineConstants {

    String ENTRY_POINTS         = "entrypoints";
    String ENTRY_POINT          = "entrypoint";
    String UNIT                 = "Unit";
    String PAIR                 = "Pair";
    String VALUE                = "value";
    String INT                  = "int";
    String BYTES                = "bytes";
    String STRING               = "string";
    String HOST                 = "host";
    String USER                 = "user";
    String TIMESTAMP            = "timestamp";
    String NAME                 = "name";
    String DEPLOYMENT_ADDRESS   = "deploymentAddress";
    String SOURCE               = "source";
    String PARAMETERS           = "parameters";
    String PARAMETER            = "parameter";
    String CONTRACT             = "contract";
    String OR                   = "or";
    String LIST                 = "list";
    String MAP                  = "map";
    String PRIM                 = "prim";
    String ANNOTS               = "annots";
    String OPTION               = "option";
    String ARGS                 = "args";
    String CODE                 = "code";
    String TYPE                 = "type";
    String OPTIONAL             = "optional";
    String KEY                  = "key";
    String ADDRESS              = "address";
    String DETAILS              = "details";
    String DEFAULT              = "default";
    String SOME                 = "Some";
    String NONE                 = "None";
    String STORAGE              = "storage";
    String BIG_MAP              = "big_map";
    String ELT                  = "Elt";
    String BOOL                 = "bool";
    String NAT                  = "nat";
    String MUTEZ                = "mutez";
    String UNIT_TYPE            = "unit";
    String SIGNATURE            = "signature";
    String SET                  = "set";
    String PAIR_TYPE            = "pair";
}