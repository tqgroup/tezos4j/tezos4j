// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.papi.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class Ed25519KeyPair {

    private byte [] publicKey;
    private byte [] privateKey;

    private Ed25519KeyPair () {
        super();
    }

    public static Ed25519KeyPair toEd25519KeyPair (byte [] publicKey, byte [] privateKey) {
        Ed25519KeyPair keyPair = new Ed25519KeyPair();
        keyPair.publicKey = publicKey;
        keyPair.privateKey = privateKey;
        return keyPair;
    }

    public byte [] getPublicKey () {
        return publicKey;
    }

    public byte [] getPrivateKey () {
        return privateKey;
    }
}
