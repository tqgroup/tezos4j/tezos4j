// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.papi;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public interface TezosContractProxy {

    enum Mode { SYNCHRONOUS, ASYNCHRONOUS }

    boolean isSynchronous ();

    Mode getMode ();

    void setMode (Mode mode);

}