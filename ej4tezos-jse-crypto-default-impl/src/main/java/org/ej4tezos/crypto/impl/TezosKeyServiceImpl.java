// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.crypto.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.text.MessageFormat;

import javax.inject.Inject;

import javax.annotation.PreDestroy;
import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.ej4tezos.utils.asserts.Asserts.*;

import static org.ej4tezos.utils.bytes.ByteToolbox.*;

import org.ej4tezos.api.TezosKeyService;

import org.ej4tezos.api.exception.TezosException;

import org.ej4tezos.model.TezosIdentity;
import org.ej4tezos.model.TezosPrivateKey;
import org.ej4tezos.model.TezosPublicAddress;

import org.ej4tezos.papi.TezosCryptoProvider;

import org.ej4tezos.papi.model.Ed25519KeyPair;

import static org.ej4tezos.model.TezosConstants.TZ1_PREFIX;
import static org.ej4tezos.model.TezosConstants.EDPK_PREFIX;
import static org.ej4tezos.model.TezosConstants.EDSK_PREFIX;

import static org.ej4tezos.papi.TezosCryptoProvider.EncoderType.HEX;
import static org.ej4tezos.papi.TezosCryptoProvider.EncoderType.BASE58;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosKeyServiceImpl implements TezosKeyService {

    private static final Logger LOG = LoggerFactory.getLogger(TezosKeyServiceImpl.class);

    @Inject
    private TezosCryptoProvider tezosCryptoProvider;

    public TezosKeyServiceImpl () {
        LOG.info("A new instance of 'TezosKeyServiceImpl' got created; logging on {}...", LOG.getName());
    }

    @PostConstruct
    public void init () throws Exception {

        // Log the call:
        LOG.info("init()");

        // Sanity check:
        checkNotNull(tezosCryptoProvider, "tezosCryptoProvider is not set", Exception.class);

    }

    @PreDestroy
    public void destroy () throws Exception {
        LOG.info("destroy()");
    }

    @Override
    public TezosIdentity generateIdentity () throws TezosException {

        // Log the call:
        LOG.debug("generateIdentity ()");

        try {

            // Generate the key pair:
            Ed25519KeyPair keyPair = tezosCryptoProvider.generateEd25519KeyPair();

            // Log the keys:
            LOG.debug("publicKey.getEncoded() = 0x{}", tezosCryptoProvider.encode(keyPair.getPublicKey(), HEX));
            LOG.debug("privateKey.getEncoded() = 0x{}", tezosCryptoProvider.encode(keyPair.getPrivateKey(), HEX));

            // Format the public key:
            byte[] tezosPublicKeyRaw = join(EDPK_PREFIX, keyPair.getPublicKey());
            byte[] tezosPublicKeyRawChecksum = tezosCryptoProvider.computeDoubleCheckSum(tezosPublicKeyRaw);
            byte[] tezosCheckedPublicKeyRaw = join(tezosPublicKeyRaw, tezosPublicKeyRawChecksum);
            String tezosPublicKey = tezosCryptoProvider.encode(tezosCheckedPublicKeyRaw, BASE58);

            // Format the private key:
            byte[] tezosPrivateKeyRaw = join(EDSK_PREFIX, keyPair.getPrivateKey(), keyPair.getPublicKey());
            byte[] tezosPrivateKeyRawChecksum = tezosCryptoProvider.computeDoubleCheckSum(tezosPrivateKeyRaw);
            byte[] tezosCheckedPrivateKeyRaw = join(tezosPrivateKeyRaw, tezosPrivateKeyRawChecksum);
            String tezosPrivateKey = tezosCryptoProvider.encode(tezosCheckedPrivateKeyRaw, BASE58);

            // Format the public address:
            byte[] digestedPublicKey = tezosCryptoProvider.getBlake2B160Digester().digest(keyPair.getPublicKey());
            byte[] tezosPublicAddressRaw = join(TZ1_PREFIX, digestedPublicKey);
            byte[] tezosPublicAddressRawChecksum = tezosCryptoProvider.computeDoubleCheckSum(tezosPublicAddressRaw);
            String tezosPublicAddress = tezosCryptoProvider.encode(join(tezosPublicAddressRaw, tezosPublicAddressRawChecksum), BASE58);

            // Instantiate the TezosIdentity with the right formatted keys and address:
            TezosIdentity result = TezosIdentity.toTezosIdentity(tezosPrivateKey, tezosPublicKey, tezosPublicAddress);

            // Log and return the result:
            LOG.debug("result = {}", result);
            return result;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while generating identity: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public TezosPublicAddress getPublicAddress (TezosPrivateKey privateKey) throws TezosException {

        // Log the call:
        LOG.debug("getPublicAddress (privateKey = {})", privateKey);

        try {

            // Extract the raw public key:
            byte [] rawPublicKey = tezosCryptoProvider.extractRawPublicKey(privateKey);

            // Format the public address:
            byte[] digestedPublicKey = tezosCryptoProvider.getBlake2B160Digester().digest(rawPublicKey);
            byte[] tezosPublicAddressRaw = join(TZ1_PREFIX, digestedPublicKey);
            byte[] tezosPublicAddressRawChecksum = tezosCryptoProvider.computeDoubleCheckSum(tezosPublicAddressRaw);
            String tezosPublicAddress = tezosCryptoProvider.encode(join(tezosPublicAddressRaw, tezosPublicAddressRawChecksum), BASE58);

            // Instantiate the TezosPublicAddress with the right formatted keys and address:
            TezosPublicAddress result = TezosPublicAddress.toTezosPublicAddress(tezosPublicAddress);

            // Log and return the result:
            LOG.debug("result = {}", result);
            return result;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while generating identity: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    public void setTezosCryptoProvider (TezosCryptoProvider tezosCryptoProvider) {
        LOG.info("setTezosCryptoProvider (tezosCryptoProvider = {})", tezosCryptoProvider);
        this.tezosCryptoProvider = tezosCryptoProvider;
    }

    @Override
    public String toString () {
        return "TezosKeyServiceImpl";
    }
}