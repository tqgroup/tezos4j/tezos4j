// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.crypto.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.text.MessageFormat;

import java.security.SecureRandom;
import java.security.MessageDigest;

import javax.inject.Inject;

import javax.annotation.PreDestroy;
import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.bitcoinj.core.Base58;

import org.bouncycastle.crypto.Signer;
import org.bouncycastle.crypto.signers.Ed25519Signer;

import org.bouncycastle.util.encoders.Hex;

import org.bouncycastle.jcajce.provider.digest.Blake2b;

import org.bouncycastle.crypto.AsymmetricCipherKeyPair;

import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519KeyGenerationParameters;

import org.bouncycastle.crypto.generators.Ed25519KeyPairGenerator;

import static org.ej4tezos.utils.asserts.Asserts.checkState;
import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

import static org.ej4tezos.utils.bytes.ByteToolbox.*;

import org.ej4tezos.api.exception.TezosException;

import org.ej4tezos.papi.TezosCryptoProvider;

import org.ej4tezos.papi.model.Ed25519KeyPair;

import org.ej4tezos.model.TezosIdentity;
import org.ej4tezos.model.TezosPublicKey;
import org.ej4tezos.model.TezosPrivateKey;

import static org.ej4tezos.model.TezosConstants.EDPK_PREFIX;
import static org.ej4tezos.model.TezosConstants.EDSK_PREFIX;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosCryptoProviderImpl implements TezosCryptoProvider {

    private static final Logger LOG = LoggerFactory.getLogger(TezosCryptoProviderImpl.class);

    private static final int DOUBLE_CHECKSUM_SIZE = 4;

    @Inject
    private SecureRandom secureRandom;

    private Signer signer;
    private MessageDigest sha256digester;
    private MessageDigest blake2b160digester;
    private MessageDigest blake2b256digester;
    private Ed25519KeyPairGenerator keyPairGenerator;

    public TezosCryptoProviderImpl() {
        LOG.info("A new instance of 'TezosCryptoProviderImpl' got created; logging on {}...", LOG.getName());
    }

    @PostConstruct
    public void init () throws Exception {

        // Log the call:
        LOG.info("init()");

        // Sanity check:
        checkNotNull(secureRandom, "SecureRandom is not set", Exception.class);

        // Prepare the key generator:
        keyPairGenerator = new Ed25519KeyPairGenerator();
        keyPairGenerator.init(new Ed25519KeyGenerationParameters(secureRandom));

        // Prepare the digesters:
        blake2b160digester = new Blake2b.Blake2b160();
        blake2b256digester = new Blake2b.Blake2b256();
        sha256digester = MessageDigest.getInstance("SHA-256");

        // Prepare the signer:
        signer = new Ed25519Signer();
    }

    @PreDestroy
    public void destroy () throws Exception {
        LOG.info("destroy()");
    }

    @Override
    public Ed25519KeyPair generateEd25519KeyPair () throws TezosException {

        // Log the call:
        LOG.debug("generateIdentity ()");

        // Generate the key pair:
        AsymmetricCipherKeyPair asymmetricCipherKeyPair = keyPairGenerator.generateKeyPair();

        try {

            // Extract the private and public key:
            Ed25519PublicKeyParameters publicKey = (Ed25519PublicKeyParameters) asymmetricCipherKeyPair.getPublic();
            Ed25519PrivateKeyParameters privateKey = (Ed25519PrivateKeyParameters) asymmetricCipherKeyPair.getPrivate();

            // Instantiate the TezosIdentity with the right formatted keys and address:
            Ed25519KeyPair result = Ed25519KeyPair.toEd25519KeyPair(publicKey.getEncoded(), privateKey.getEncoded());

            // Log and return the result:
            LOG.debug("result = {}", result);
            return result;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while generating an ED25519 key pair: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public String encode (byte [] payload, EncoderType encoderType) throws TezosException {

        //
        LOG.debug("encode (payload = {}, encoderType = {})", payload, encoderType);

        //
        checkNotNull(payload, "payload should not be null", TezosException.class);
        checkNotNull(encoderType, "encoderType should not be null", TezosException.class);
        checkState(payload.length > 0, "Payload should not be empty", TezosException.class);

        try {

            switch (encoderType) {

                case HEX:
                    return Hex.toHexString(payload);

                case BASE58:
                    return Base58.encode(payload);

                default:
                    String message = MessageFormat.format("Unexpected encoding type: {0}", encoderType);
                    throw new TezosException(message);
            }
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while encoding to {0}: {1}", encoderType, ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public byte [] decode (String payload, EncoderType encoderType) throws TezosException {

        //
        LOG.debug("decode (payload = {}, encoderType = {})", payload, encoderType);

        //
        checkNotNull(payload, "payload should not be null", TezosException.class);
        checkNotNull(encoderType, "encoderType should not be null", TezosException.class);
        checkState(!payload.isEmpty(), "Payload should not be empty", TezosException.class);

        try {

            switch (encoderType) {

                case HEX:
                    return Hex.decode(payload);

                case BASE58:
                    return Base58.decode(payload);

                default:
                    String message = MessageFormat.format("Unexpected decoding type: {0}", encoderType);
                    throw new TezosException(message);
            }
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while decoding from {0}: {1}", encoderType, ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public byte [] createSignature (byte [] payload, TezosPrivateKey privateKey) throws TezosException {

        // Log the call:
        LOG.debug("createSignature (payload = {}, privateKey = {})", payload, privateKey);

        // Sanity check:
        checkNotNull(payload, "payload should not be null", TezosException.class);
        checkNotNull(privateKey, "privateKey should not be null", TezosException.class);
        checkState(privateKey.isValid(), "privateKey should be valid", TezosException.class);

        try {

            // Extract the private key from the complex private key:
            byte[] rawPrivateKey = extractRawPrivateKey(privateKey);

            // Prepare the ED25591 parameters:
            Ed25519PrivateKeyParameters parameters
                = new Ed25519PrivateKeyParameters(rawPrivateKey, 0);

            // Generate the signature:
            byte[] signature;
            synchronized (signer) {
                signer.init(true, parameters);
                signer.reset();
                signer.update(payload, 0, payload.length);
                signature = signer.generateSignature();
            }

            //
            LOG.debug("signature.length = {}", signature.length);
            return signature;
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while creating a signature: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public byte [] extractRawPublicKey (TezosIdentity tezosIdentity) throws TezosException {

        // Log the call:
        LOG.debug("extractRawPublicKey (tezosIdentity = {})", tezosIdentity);

        // Sanity check:
        checkNotNull(tezosIdentity, "tezosIdentity should not be null", TezosException.class);
        checkNotNull(tezosIdentity.getPublicKey(), "tezosIdentity.publicKey should not be null", TezosException.class);

        //
        byte [] result = extractRawPublicKey(tezosIdentity.getPublicKey());

        // Log and return the result:
        LOG.debug("result = {}", result);
        return result;
    }

    @Override
    public byte [] extractRawPublicKey (TezosPublicKey tezosPublicKey) throws TezosException {

        // Log the call:
        LOG.debug("extractRawPublicKey (tezosPublicKey = {})", tezosPublicKey);

        // Sanity check:
        checkNotNull(tezosPublicKey, "tezosPublicKey should not be null", TezosException.class);
        checkNotNull(tezosPublicKey.getValue(), "tezosPublicKey.value should not be null", TezosException.class);
        checkState(tezosPublicKey.isValid(), "tezosPublicKey is not valid", TezosException.class);

        try {

            // Decode the public key:
            byte [] decodedPublicKey = Base58.decode(tezosPublicKey.getValue().trim());

            // Extract the public key (32 bit after the prefix):
            byte [] rawPublicKey = mid(decodedPublicKey, EDPK_PREFIX.length, 32);

            // Log and return the value:
            LOG.debug("rawPublicKey = {}", "0x" + Hex.toHexString(rawPublicKey));
            return rawPublicKey;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while extracting the raw public key: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public byte [] extractRawPublicKey (TezosPrivateKey tezosPrivateKey) throws TezosException {

        // Log the call:
        LOG.debug("extractRawPublicKey (tezosPublicKey = {})", tezosPrivateKey);

        // Sanity check:
        checkNotNull(tezosPrivateKey, "tezosPrivateKey should not be null", TezosException.class);
        checkNotNull(tezosPrivateKey.getValue(), "tezosPrivateKey.privateKey should not be null", TezosException.class);
        checkState(tezosPrivateKey.isValid(), "tezosPrivateKey is not valid", TezosException.class);

        try {

            // Decode the private key:
            byte [] decodePrivateKey = Base58.decode(tezosPrivateKey.getValue().trim());

            // Extract the private key (32 bit after the prefix and the private key):
            byte [] rawPublicKey = mid(decodePrivateKey, EDSK_PREFIX.length + 32, 32);

            // Log and return the value:
            LOG.debug("rawPublicKey = {}", "0x" + Hex.toHexString(rawPublicKey));
            return rawPublicKey;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while extracting the raw private key: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public byte [] extractRawPrivateKey (TezosIdentity tezosIdentity) throws TezosException {

        // Log the call:
        LOG.debug("extractRawPrivateKey (tezosIdentity = {})", tezosIdentity);

        // Sanity check:
        checkNotNull(tezosIdentity, "tezosIdentity should not be null", TezosException.class);
        checkNotNull(tezosIdentity.getPrivateKey(), "tezosIdentity.privateKey should not be null", TezosException.class);
        checkState(tezosIdentity.getPrivateKey().isValid(), "tezosIdentity.privateKey is not valid", TezosException.class);

        //
        byte [] result = extractRawPrivateKey(tezosIdentity.getPrivateKey());

        // Log and return the result:
        LOG.debug("result = {}", result);
        return result;
    }

    @Override
    public byte [] extractRawPrivateKey (TezosPrivateKey tezosPrivateKey) throws TezosException {

        // Log the call:
        LOG.debug("extractRawPrivateKey (tezosPrivateKey = {})", tezosPrivateKey);

        // Sanity check:
        checkNotNull(tezosPrivateKey, "tezosPrivateKey should not be null", TezosException.class);
        checkNotNull(tezosPrivateKey.getValue(), "tezosPrivateKey.privateKey should not be null", TezosException.class);
        checkState(tezosPrivateKey.isValid(), "tezosPrivateKey is not valid", TezosException.class);

        try {

            // Decode the private key:
            byte [] decodePrivateKey = Base58.decode(tezosPrivateKey.getValue().trim());

            // Extract the private key (32 bit after the prefix):
            byte [] rawPrivateKey = mid(decodePrivateKey, EDSK_PREFIX.length, 32);

            // Log and return the value:
            LOG.debug("rawPrivateKey = {}", "0x" + Hex.toHexString(rawPrivateKey));
            return rawPrivateKey;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while extracting the raw private key: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public byte [] computeDoubleCheckSum (byte [] payload) throws TezosException {

        // Log the call:
        LOG.debug("computeCheckSum (payload = {})", payload == null ? "null": Hex.toHexString(payload));

        // Sanity check:
        checkNotNull(payload, "payload should not be null", TezosException.class);
        checkState(payload.length > 0, "payload should not be empty", TezosException.class);

        try {

            byte[] checksum = first(sha256digester.digest(sha256digester.digest(payload)), DOUBLE_CHECKSUM_SIZE);

            LOG.debug("checksum = {}", Hex.toHexString(checksum));
            return checksum;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("Cannot compute check sum: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public String getDescription () {
        return this.toString();
    }

    @Override
    public MessageDigest getSha256digester () {
        return sha256digester;
    }

    @Override
    public MessageDigest getBlake2B160Digester () {
        return blake2b160digester;
    }

    @Override
    public MessageDigest getBlake2B256Digester () {
        return blake2b256digester;
    }

    public void setSecureRandom (SecureRandom secureRandom) {
        LOG.debug("setSecureRandom (secureRandom = {})", secureRandom);
        this.secureRandom = secureRandom;
    }

    @Override
    public String toString () {
        return "TezosCryptoProviderImpl";
    }
}