// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.crypto.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.junit.Test;
import org.junit.After;
import org.junit.Before;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.ej4tezos.api.TezosKeyService;

import org.ej4tezos.model.TezosIdentity;

import org.ej4tezos.model.TezosPrivateKey;
import org.ej4tezos.model.TezosPublicAddress;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosKeyServiceImplTest {

    private static final Logger LOG = LoggerFactory.getLogger(TezosKeyServiceImplTest.class);

    private TezosKeyService tezosKeyService;

    @Before
    public void setup () throws Exception {

        TezosCryptoProviderImpl tezosCryptoProvider = new TezosCryptoProviderImpl();

        tezosCryptoProvider.setSecureRandom(new MySecureRandom(
            new int [] {
                    135, 233,  35, 106,
                     34, 131, 105, 112,
                     16, 180,  42,   0,
                    181, 106, 247, 150,
                    209, 248, 169, 174,
                      5, 255,   9, 173,
                     46, 128, 233, 144,
                     61,  85, 190,   4
            }
        ));

        tezosCryptoProvider.init();

        tezosKeyService = new TezosKeyServiceImpl();
        ((TezosKeyServiceImpl) tezosKeyService).setTezosCryptoProvider(tezosCryptoProvider);
        ((TezosKeyServiceImpl) tezosKeyService).init();
    }

    @After
    public void destroy () throws Exception {
        ((TezosKeyServiceImpl) tezosKeyService).destroy();
    }

    @Test
    public void generateKeysTest () throws Exception {

        TezosIdentity identity = tezosKeyService.generateIdentity();

        LOG.info("identity = {}", identity);

        assertThat("Unexpected private key",    identity.getPrivateKey().getValue(),    is("edskRuo5FyLqVmHKSYYfQGGKF2cyMYty9L8h77S1gjUsdXC7RGARKbB9Sec849EmMQWKYE79oSr6eBi12jMqVQVxvAaHuKutN8"));
        assertThat("Unexpected public key",     identity.getPublicKey().getValue(),     is("edpkv3FRyy4WUc2bynqDiEgQUm4fgt3GjhuCpcLfoSycNqq1H8uTVZ"));
        assertThat("Unexpected public address", identity.getPublicAddress().getValue(), is("tz1Nz79TgyfZvCSViQ3Di6Km2kUMMNqTzF6m"));

    }

    @Test
    public void getPublicAddressTest () throws Exception {

        TezosPrivateKey privateKey = TezosPrivateKey.toTezosPrivateKey("edskRuo5FyLqVmHKSYYfQGGKF2cyMYty9L8h77S1gjUsdXC7RGARKbB9Sec849EmMQWKYE79oSr6eBi12jMqVQVxvAaHuKutN8");

        LOG.info("privateKey = {}", privateKey);

        TezosPublicAddress publicAddress = tezosKeyService.getPublicAddress(privateKey);

        assertThat("Unexpected public address", publicAddress.getValue(), is("tz1Nz79TgyfZvCSViQ3Di6Km2kUMMNqTzF6m"));

    }

    @Override
    public String toString () {
        return "TezosKeyServiceImplTest";
    }
}