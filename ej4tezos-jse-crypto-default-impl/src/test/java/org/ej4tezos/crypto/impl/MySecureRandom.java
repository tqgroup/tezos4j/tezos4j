// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.crypto.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.security.SecureRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.bouncycastle.util.encoders.Hex;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class MySecureRandom extends SecureRandom {

    private static final Logger LOG = LoggerFactory.getLogger(MySecureRandom.class);

    private byte [] source;

    public MySecureRandom () {
        super();
    }

    public MySecureRandom (byte [] source) {

        this();

        this.source = new byte[source.length];

        for(int i = 0; i < source.length; i++) {
            this.source[i] = source[i];
        }
    }

    public MySecureRandom (int [] source) {

        this();

        this.source = new byte[source.length];

        for(int i = 0; i < source.length; i++) {
            this.source[i] = (byte)source[i];
        }
    }

    @Override
    public void nextBytes (byte [] bytes) {

        LOG.info("nextBytes (bytes.length = {})", bytes.length);

        if(source == null) {
            for(int i = 0; i < bytes.length; i++) {
                bytes[i] = (byte) i;
            }
        }
        else {
            for(int i = 0; i < bytes.length; i++) {
                bytes[i] = source[i % source.length];
            }
        }

        LOG.info("Source: {}", Hex.toHexString(bytes));
    }

    @Override
    public String toString () {
        return "MySecureRandom";
    }
}