// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.crypto.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.junit.Test;
import org.junit.Before;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.Matchers.is;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

import org.bouncycastle.util.encoders.Hex;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosCryptoProviderImplTest {

    private static final Logger LOG = LoggerFactory.getLogger(TezosCryptoProviderImplTest.class);

    private TezosCryptoProviderImpl provider;

    @Before
    public void init () throws Exception {
        provider = new TezosCryptoProviderImpl();
        provider.setSecureRandom(new MySecureRandom());
        provider.init();
    }
}