// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class UnknownTezosTypeException extends Exception {

    public UnknownTezosTypeException () {
        super();
    }

    public UnknownTezosTypeException (String message) {
        super(message);
    }

    public UnknownTezosTypeException (String message, Throwable cause) {
        super(message, cause);
    }
}