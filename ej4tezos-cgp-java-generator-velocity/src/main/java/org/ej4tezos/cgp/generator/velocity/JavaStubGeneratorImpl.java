// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.io.Writer;
import java.io.IOException;
import java.io.StringReader;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import java.text.MessageFormat;

import org.json.JSONArray;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.apache.velocity.util.StringBuilderWriter;

import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_LOADERS;

import org.ej4tezos.cgp.api.JavaStubGenerator;

import org.ej4tezos.cgp.api.model.JavaStubGeneratorContext;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

import static org.ej4tezos.cgp.generator.velocity.MethodDetails.toMethodDetails;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class JavaStubGeneratorImpl implements JavaStubGenerator {

    private static final Logger LOG = LoggerFactory.getLogger(JavaStubGeneratorImpl.class);

    private String [] templateNameArray = { "Interface.vm", "StorageData.vm", "Helper.vm", "Storage.vm"};
    private String [] recordTemplateNameArray = { "Record.vm" };

    private List<TezosRecordType> tezosRecordList = null;

    public JavaStubGeneratorImpl () {
        LOG.info("New instance of JavaStubGeneratorImpl got created; logging on {}", LOG.getName());
    }

    @Override
    public void generate (JavaStubGeneratorContext generatorContext, JSONObject tezosContractDefinition) throws TezosGeneratorException {

        generateFromTemplate(generatorContext, tezosContractDefinition);
        generateFromRecordTemplate(generatorContext, tezosContractDefinition);

    }

    private void generateFromTemplate (JavaStubGeneratorContext generatorContext, JSONObject tezosContractDefinition) throws TezosGeneratorException {

        // Log the call:
        LOG.debug("generateFromTemplate (generatorContext = {}, tezosContractDefinition = {})", generatorContext, tezosContractDefinition);

        // Sanity check:
        checkNotNull(generatorContext, "generatorContext should not be null", TezosGeneratorException.class);
        checkNotNull(tezosContractDefinition, "tezosContractDefinition should not be null", TezosGeneratorException.class);

        //
        VelocityEngine templateEngine = getTemplateEngine(generatorContext);

        //
        Context context = toContext(tezosContractDefinition, generatorContext);

        //
        for(String templateName : templateNameArray) {

            //
            LOG.debug("Loading template {}...", templateName);
            Template template = templateEngine.getTemplate(templateName);

            //
            StringBuilder builder = new StringBuilder();
            try (Writer writer = new StringBuilderWriter(builder)) {

                //
                template.merge(context, writer);

                //
                if (!context.containsKey("outputFileName")) {
                    throw new TezosGeneratorException("Template does not set the 'outputFileName' variable!");
                }

                //
                String outputFileName = context.get("outputFileName").toString().trim();
                LOG.debug("outputFileName: {}", outputFileName);

                //
                generatorContext.persist(outputFileName, new StringReader(builder.toString()));

                //
                cleanContext(context);
            }

            catch (IOException ex) {
                LOG.warn("An exception occurred while processing template {}: {}", templateName, ex.getMessage());
            }
        }
    }

    private void generateFromRecordTemplate (JavaStubGeneratorContext generatorContext, JSONObject tezosContractDefinition) throws TezosGeneratorException {

        // Log the call:
        LOG.debug("generateFromRecordTemplate (generatorContext = {}, tezosContractDefinition = {})", generatorContext, tezosContractDefinition);

        // Sanity check:
        checkNotNull(generatorContext, "generatorContext should not be null", TezosGeneratorException.class);
        checkNotNull(tezosContractDefinition, "tezosContractDefinition should not be null", TezosGeneratorException.class);

        //
        VelocityEngine templateEngine = getTemplateEngine(generatorContext);

        //
        LOG.debug("tezosRecordList.size = {}", tezosRecordList.size());

        //
        for(TezosRecordType record : tezosRecordList) {

            //
            for (String templateName : recordTemplateNameArray) {

                //
                LOG.debug("Loading template {}...", templateName);
                Template template = templateEngine.getTemplate(templateName);

                //
                Context context = toRecordContext(generatorContext, record);

                //
                StringBuilder builder = new StringBuilder();
                try (Writer writer = new StringBuilderWriter(builder)) {

                    //
                    template.merge(context, writer);

                    //
                    if (!context.containsKey("outputFileName")) {

                        String message
                            = MessageFormat
                                .format("Template {0} does not set the 'outputFileName' variable!",
                                        templateName);

                        throw new TezosGeneratorException(message);
                    }

                    //
                    String outputFileName = context.get("outputFileName").toString().trim();
                    LOG.debug("outputFileName: {}", outputFileName);

                    //
                    generatorContext.persist(outputFileName, new StringReader(builder.toString()));

                    //
                    cleanContext(context);
                }

                catch (IOException ex) {
                    LOG.warn("An exception occurred while processing template {}: {}", templateName, ex.getMessage());
                }
            }
        }
    }

    private VelocityEngine getTemplateEngine (JavaStubGeneratorContext context) throws TezosGeneratorException {

        // Log the call:
        LOG.debug("getTemplateEngine (context = {})", context);

        // Sanity check:
        checkNotNull(context, "context should not be null", TezosGeneratorException.class);

        //
        VelocityEngine templateEngine = new VelocityEngine();

        //
        templateEngine.setProperty(VelocityEngine.RUNTIME_LOG_NAME, "JavaStubGeneratorImpl");

        templateEngine.setProperty(RESOURCE_LOADERS, "classpathResourceLoader");
        templateEngine.setProperty("classpathResourceLoader.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

        //
        templateEngine.init();

        // Log and return the template engine:
        LOG.debug("templateEngine = {}", templateEngine);
        return templateEngine;
    }

    private Context toContext (JSONObject tezosContractDefinition, JavaStubGeneratorContext sourceContext) throws TezosGeneratorException {

        // Log the call:
        LOG.debug("toContext (tezosContractDefinition = {}, sourceContext = {})", tezosContractDefinition, sourceContext);

        // Sanity check:
        checkNotNull(sourceContext, "sourceContext should not be null", TezosGeneratorException.class);
        checkNotNull(tezosContractDefinition, "tezosContractDefinition should not be null", TezosGeneratorException.class);

        //
        validate(tezosContractDefinition);

        //
        String contractName = tezosContractDefinition.getString("name");

        //
        MethodDetails methodDetails;
        List<MethodDetails> methodDetailsList = new ArrayList<>();
        JSONArray jsonEntrypointDetails = tezosContractDefinition.getJSONArray("entrypoints");

        //
        for(int i = 0; i < jsonEntrypointDetails.length(); i++) {

            JSONObject entrypointDetail = jsonEntrypointDetails.getJSONObject(i);

            methodDetails = toMethodDetails(entrypointDetail.getString("name"));

            JSONArray jsonParameterDetails = entrypointDetail.getJSONArray("parameters");

            methodDetails.add(TezosDataParser.getTezosVariableList(contractName, jsonParameterDetails));

            methodDetailsList.add(methodDetails);
        }

        //
        JSONArray jsonStorageDetails = tezosContractDefinition.getJSONArray("storage");
        List<TezosVariable> storagevariableList = TezosDataParser.getTezosVariableList(contractName + "Storage", jsonStorageDetails);

        //
        Context context = initialContext();

        //
        context.put("methods", methodDetailsList);
        context.put("storage", storagevariableList);
        context.put("packageName", sourceContext.getPackageName());
        context.put("contractName", contractName);

        //
        tezosRecordList = new ArrayList<>();
        populateRecordList(methodDetailsList, storagevariableList);

        // Log and return the context:
        LOG.debug("context = {}", context);
        return context;
    }

    private Context toRecordContext (JavaStubGeneratorContext sourceContext, TezosRecordType record) throws TezosGeneratorException {

        // Log the call:
        LOG.debug("toRecordContext (sourceContext = {}, record = {})", sourceContext, record);

        // Sanity check:
        checkNotNull(record, "record should not be null", TezosGeneratorException.class);
        checkNotNull(sourceContext, "sourceContext should not be null", TezosGeneratorException.class);

        //
        Context context = initialContext();

        //
        context.put("record", record);
        context.put("packageName", sourceContext.getPackageName());

        // Log and return the context:
        LOG.debug("context = {}", context);
        return context;
    }

    private Context initialContext () throws TezosGeneratorException {

        // Log the call:
        LOG.debug("initialContext ()");

        //
        Context context = new VelocityContext();

        //
        Toolbox.singleton().setContext(context);

        //
        context.put("toolbox", Toolbox.singleton());
        context.put("properties", System.getProperties());
        context.put("environment", Collections.emptyMap());     // We will filter this at some point.
        context.put("generationDate", new Date());

        // Log and return the context:
        LOG.debug("context = {}", context);
        return context;
    }

    private void validate (JSONObject tezosContractDefinition) throws TezosGeneratorException {

        // Log the call:
        LOG.debug("toContext (tezosContractDefinition = {})", tezosContractDefinition);

        // Sanity check:
        checkNotNull(tezosContractDefinition, "tezosContractDefinition should not be null", TezosGeneratorException.class);

        // Validation:
        if (!tezosContractDefinition.has("name")) {
            throw new TezosGeneratorException("'name' node missing in the contract definition!");

        }

        if (!tezosContractDefinition.has("entrypoints")) {
            throw new TezosGeneratorException("'entrypoints' node missing in the contract definition!");
        }
    }

    private void populateRecordList (List<MethodDetails> methodDetailsList, List<TezosVariable> tezosVariableList) throws TezosGeneratorException {

        // Go through all the methods:
        for(MethodDetails methodDetails : methodDetailsList) {
            populateRecordList(methodDetails.getParameters());
        }

        // Go through all the variables:
        populateRecordList(tezosVariableList);

    }

    private void populateRecordList (List<TezosVariable> tezosVariableList) throws TezosGeneratorException {

        // Go through the types:
        for(TezosVariable tezosVariable : tezosVariableList) {
            populateRecordList(tezosVariable.getType());
        }
    }

    private void populateRecordList (TezosType tezosType) throws TezosGeneratorException {

        TezosType.Style style = tezosType.getStyle();

        switch (style) {

            case PLAIN:
                break;

            case SET:
                TezosNativeSetType setType = (TezosNativeSetType) tezosType;
                populateRecordList(setType.getContainedRecord());
                break;

            case REMOTE_MAP:
            case MAP:
                TezosNativeMapType mapType = (TezosNativeMapType) tezosType;
                populateRecordList(mapType.getKeyType());
                populateRecordList(mapType.getValueType());
                break;

            case RECORD:
                TezosRecordType recordType = (TezosRecordType)tezosType;
                populateRecordList(recordType.fields());
                tezosRecordList.add(recordType);
                break;

            default:
                String message = MessageFormat.format("Unexpected type's style: {0}", style);
                throw new TezosGeneratorException(message);
        }
    }

    private void cleanContext (Context context) throws TezosGeneratorException {

        // Log the call:
        LOG.debug("cleanContext (context = {})", context);

        // Sanity check:
        checkNotNull(context, "context should not be null", TezosGeneratorException.class);

        //
        context.remove("outputFileName");

    }

    @Override
    public String toString () {
        return "JavaStubGeneratorImpl";
    }
}