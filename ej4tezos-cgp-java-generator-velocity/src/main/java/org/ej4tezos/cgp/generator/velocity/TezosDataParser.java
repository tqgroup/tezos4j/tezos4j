// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;
import java.util.ArrayList;

import java.text.MessageFormat;

import org.json.JSONArray;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.ej4tezos.utils.asserts.Asserts.*;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosDataParser {

    private static final Logger LOG = LoggerFactory.getLogger(TezosDataParser.class);

    public static List<TezosVariable> getTezosVariableList (String contextName, JSONArray array) throws TezosGeneratorException {

        //
        List<TezosVariable> result = new ArrayList<>();

        // Log the call:
        LOG.debug("getTezosVariableList (contextName = {}, array = {})", contextName, array);

        //
        for(int i = 0; i < array.length(); i++) {
            JSONObject arrayElement = array.getJSONObject(i);
            result.add(getTezosVariable(contextName, arrayElement));
        }

        //
        LOG.debug("result = {}", result);
        return result;
    }

    public static TezosVariable getTezosVariable (String contextName, JSONObject rawVariable) throws TezosGeneratorException {

        //
        String recordName = "";
        TezosRecordType record = null;

        // Log the call:
        LOG.debug("processTezosVariable (contextName = {}, rawVariable = {})", contextName, rawVariable);

        // Sanity checks:
        checkNotNull(rawVariable, "rawVariable should not be null", TezosGeneratorException.class);
        checkNotNull(contextName, "contextName should not be null", TezosGeneratorException.class);
        checkState(rawVariable.has("name"), "Missing 'name' field!", TezosGeneratorException.class);
        checkState(rawVariable.has("type"), "Missing 'type' field!", TezosGeneratorException.class);

        try {

            // Identify the type:
            TezosType type = TezosType.convert(rawVariable.getString("type"));

            //
            boolean optional = rawVariable.has("optional") ? rawVariable.getBoolean("optional") : false;

            // Create the variable and set its fields:
            TezosVariable result = new TezosVariable();
            String variableName = rawVariable.getString("name");
            result.setName(Toolbox.singleton().addPrefixIfApplicable(variableName));
            result.setOptional(optional);
            result.setType(type);

            // Potentially more processing:
            switch (type.getStyle()) {

                case SET:
                    TezosNativeSetType setType = ((TezosNativeSetType)type);
                    recordName = firstLetterToUpperCase(contextName) + firstLetterToUpperCase(variableName);
                    record = getTezosRecord(recordName, rawVariable.getJSONArray("details"), "Record");
                    setType.setContainedRecord(record);
                    break;

                case REMOTE_MAP:
                    result.setRemoteMapType(true);
                    // intentional fallthrough
                case MAP:
                    recordName = firstLetterToUpperCase(contextName) + firstLetterToUpperCase(variableName);
                    Object rawDetails = rawVariable.get("details");
                    JSONObject details;
                    if(rawDetails instanceof JSONArray) {
                        details = ((JSONArray)rawDetails).getJSONObject(0);
                    }
                    else if (rawDetails instanceof  JSONObject){
                        details = (JSONObject) rawDetails;
                    }
                    else {
                        String message = MessageFormat.format("Unexpected map details type: {0}", rawDetails.getClass());
                        throw new TezosGeneratorException(message);
                    }
                    configureMap(recordName, /*rawVariable.getJSONObject("details")*/ details, (TezosNativeMapType)type);
                    break;

                case RECORD:
                    recordName = firstLetterToUpperCase(contextName) + firstLetterToUpperCase(variableName);
                    /*if(Toolbox.singleton().startsWithDigit(recordName)) {
                        recordName = "_" + recordName;
                    }*/
                    ((TezosRecordType)type).setName(recordName);
                    populateTezosRecord((TezosRecordType) type, rawVariable.getJSONArray("details"), recordName);
                    break;

                case PLAIN:
                    // Nothing to do!
                    break;

                default:
                    String message = MessageFormat.format("Unexpected style: {0}", type.getStyle());
                    throw new TezosGeneratorException(message);
            }

            //
            LOG.debug("result = {}", result);
            return result;
        }

        catch (TezosGeneratorException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while processing a Tezos variable: {0}", ex.getMessage());
            throw new TezosGeneratorException(message, ex);
        }
    }

    public static TezosRecordType getTezosRecord (String recordName, JSONArray details, String suffix) throws TezosGeneratorException {

        // Log the call:
        LOG.debug("getTezosRecord (recordName = {}, details = {}, suffix = {})", recordName, details, suffix);

        // Sanity checks:
        checkNotNull(details, "details should not be null", TezosGeneratorException.class);

        // Create the record:
        TezosRecordType record = new TezosRecordType();
        record.setName(recordName + suffix);

        //
        populateTezosRecord(record, details, recordName);

        //
        LOG.debug("record = {}", record);
        return record;
    }

    public static TezosRecordType populateTezosRecord (TezosRecordType record, JSONArray details, String recordName) throws TezosGeneratorException {

        // Log the call:
        LOG.debug("populateTezosRecord (record = {}, details = {}, recordName = {})", record, details, recordName);

        // Sanity checks:
        checkNotNull(record, "record should not be null", TezosGeneratorException.class);
        checkNotNull(details, "details should not be null", TezosGeneratorException.class);
        checkNotNull(recordName, "recordName should not be null", TezosGeneratorException.class);

        //
        for(int i = 0; i < details.length(); i++) {
            JSONObject rawVariable = details.getJSONObject(i);
            TezosVariable variable = getTezosVariable(recordName, rawVariable);
            LOG.debug("Add variable: {}", variable);
            record.add(variable);
        }

        //
        LOG.debug("record = {}", record);
        return record;
    }

    public static void configureMap (String contextName, JSONObject details, TezosNativeMapType mapType) throws TezosGeneratorException {

        TezosType keyType;
        TezosType valueType;

        // Log the call:
        LOG.debug("decorateMap (contextName = {}, details = {}, mapType = {})", contextName, details, mapType);

        // Sanity checks:
        checkNotNull(details, "details should not be null", TezosGeneratorException.class);
        checkNotNull(mapType, "mapType should not be null", TezosGeneratorException.class);
        checkNotNull(contextName, "contextName should not be null", TezosGeneratorException.class);
        checkState(details.has("key"), "Missing 'key' field!", TezosGeneratorException.class);
        checkState(details.has("value"), "Missing 'value' field!", TezosGeneratorException.class);

        // Identify the key type:
        Object rawKey = details.get("key");
        if(rawKey instanceof String) {
            keyType = TezosType.convert((String)rawKey);
        }
        else if(rawKey instanceof JSONArray) {
            keyType = getTezosRecord(contextName, (JSONArray) rawKey, "Key");
        }
        else {
            String message = MessageFormat.format("Unexpected key type: {0}", rawKey.getClass());
            throw new TezosGeneratorException(message);
        }

        // Identify the value type:
        Object rawValue = details.get("value");
        if(rawValue instanceof String) {
            valueType = TezosType.convert((String)rawValue);
        }
        else if(rawValue instanceof JSONArray) {
            valueType = getTezosRecord(contextName, (JSONArray) rawValue, "Value");
        }
        else {
            String message = MessageFormat.format("Unexpected value type: {0}", rawValue.getClass());
            throw new TezosGeneratorException(message);
        }

        //
        mapType.setKeyType(keyType);
        mapType.setValueType(valueType);
    }

    public static String firstLetterToUpperCase (String string) {

        if(string.length() == 0) {
            return string;
        }
        else {
            return ("" + string.charAt(0)).toUpperCase() + string.substring(1);
        }
    }
}