// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosRecordType extends TezosType {

    private static Logger LOG = LoggerFactory.getLogger(TezosRecordType.class);

    private String name = "anonymous";
    private List<TezosVariable> fields = new ArrayList<>();

    public TezosRecordType () {
        super();
    }

    public Style getStyle () {
        return Style.RECORD;
    }

    public boolean isNative () {
        return false;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getName () {
        return name;
    }

    public void add (TezosVariable field) {
        fields.add(field);
    }

    public List<TezosVariable> fields () {
        return Collections.unmodifiableList(fields);
    }

    @Override
    public String toString () {
        return ReflectionToStringBuilder.toString(this);
    }
}