// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class MethodDetails {

    private String name;
    private List<TezosVariable> parameterDetailsList;

    public MethodDetails () {
        super();
        parameterDetailsList = new ArrayList<>();
    }

    public static MethodDetails toMethodDetails (String name) {
        MethodDetails methodDetails = new MethodDetails();
        methodDetails.setName(name);
        return methodDetails;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getName () {
        return name;
    }

    public void add (TezosVariable parameterDetails) {
        parameterDetailsList.add(parameterDetails);
    }

    public void add (List<TezosVariable> parameterDetailsList) {
        this.parameterDetailsList.addAll(parameterDetailsList);
    }

    public List<TezosVariable> getParameters () {
        return Collections.unmodifiableList(parameterDetailsList);
    }

    @Override
    public String toString () {
        return ReflectionToStringBuilder.toString(this);
    }
}