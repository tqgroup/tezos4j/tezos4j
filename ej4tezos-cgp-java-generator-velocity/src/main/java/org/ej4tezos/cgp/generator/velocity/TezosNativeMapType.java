// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosNativeMapType extends TezosNativeType {

    private TezosType keyType;
    private TezosType valueType;

    public TezosNativeMapType (String tezosRawType) {
        super(tezosRawType);
    }

    public Style getStyle () {
        return Style.MAP;
    }

    public void setKeyType (TezosType keyType) {
        this.keyType = keyType;
    }

    public TezosType getKeyType () {
        return keyType;
    }

    public void setValueType (TezosType valueType) {
        this.valueType = valueType;
    }

    public TezosType getValueType () {
        return valueType;
    }
}