// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.Map;
import java.util.List;
import java.util.HashMap;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.velocity.context.Context;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class Toolbox {

    private static final Logger LOG = LoggerFactory.getLogger(Toolbox.class);

    private static Toolbox singleton;

    private Context context;
    private Map<String,String> typeMapping;

    public synchronized static Toolbox singleton () {

        if(singleton == null) {
            singleton = new Toolbox();
        }

        return singleton;
    }

    private Toolbox () {

        super();

        typeMapping = new HashMap<>();

        typeMapping.put("int", "BigInteger");
        typeMapping.put("nat", "BigInteger");
        typeMapping.put("bool", "Boolean");
        typeMapping.put("unit", "TezosUnit");
        typeMapping.put("bytes", "TezosBytes");
        typeMapping.put("string", "String");
        typeMapping.put("timestamp", "TezosTimestamp");

        typeMapping.put("mutez", "Mutez");
        typeMapping.put("lambda", "TezosLambda");
        typeMapping.put("operation", "TezosOperation");

        typeMapping.put("key", "TezosPublicKey");
        typeMapping.put("address", "TezosAddress");
        typeMapping.put("signature", "TezosSignature");

        typeMapping.put("set", "Set");
        typeMapping.put("list", "List");

        typeMapping.put("map", "Map");
        typeMapping.put("big_map", "TezosBigMap");

    }

    public boolean exists (String tezosType) {

        // Log the call:
        LOG.debug("exists (tezosType = {})", tezosType);

        //
        return typeMapping.containsKey(tezosType);
    }

    public String getJavaTypeString (String tezosType) throws TezosGeneratorException {

        String result = null;

        LOG.debug("tezosType = {}", tezosType);

        if(tezosType == null) {
            throw new TezosGeneratorException("Tezos type should not be null");
        }
        else {

            //
            tezosType = tezosType.trim();

            if(typeMapping.containsKey(tezosType)) {
                result = typeMapping.get(tezosType);
            }
            else {
                String message = MessageFormat.format("Unexpected Tezos type {0}", tezosType);
                throw new TezosGeneratorException(message);
            }
        }

        //
        LOG.debug("result = {}", result);
        return result;
    }

    public String formatField (String fieldName) {
        return ("" + fieldName.charAt(0)).toUpperCase() + fieldName.substring(1);
    }

    public boolean startsWithDigit (String value) {

        //
        LOG.debug("startsWithDigit (value = {})", value);

        //
        if(value == null || value.length() == 0) {
            return false;
        }

        //
        boolean result = Character.isDigit(value.charAt(0));

        //
        LOG.debug("result = {}", result);
        return result;
    }

    public String toParametersLine (List<TezosVariable> parameterList) throws TezosGeneratorException {
        LOG.debug("toParametersLine (parameterList = {})", parameterList);
        TezosVariable[] target = new TezosVariable[parameterList.size()];
        return toParametersLine(parameterList.toArray(target));
    }

    public String toParametersLine (TezosVariable ... parameters) throws TezosGeneratorException {

        LOG.debug("toParametersLine (parameters = {})", parameters);

        //
        StringBuilder builder = new StringBuilder();

        // Loop through the variables:
        for(TezosVariable variable : parameters) {
            LOG.debug("Processing variable {}", variable);
            builder.append(toParameter(variable));
            builder.append(", ");
        }

        // Remove last comma:
        if(builder.length() > 0) {
            int length = builder.length();
            builder.delete(length - 2, builder.length());
        }

        // Compute, long and return the result:
        String result = builder.toString();
        LOG.debug("result = {}", result);
        return result;
    }

    public String toReturnType (TezosVariable parameter) throws TezosGeneratorException {

        //
        LOG.debug("toReturnType (parameter = {})", parameter);

        //
        String result
            = MessageFormat
                .format("{0}", toParameterType(parameter.getType(), parameter.isOptional()));

        //
        LOG.debug("result = {}", result);
        return result;
    }

    public String toParameter (TezosVariable parameter) throws TezosGeneratorException {

        //
        LOG.debug("toParameter (parameter = {})", parameter);

        //
        String result
            = MessageFormat
                .format("{0} {1}",
                        toParameterType(parameter.getType(), parameter.isOptional()),
                        addPrefixIfApplicable(parameter.getName()));

        //
        LOG.debug("result = {}", result);
        return result;
    }

    public String toRemoteMapType (TezosVariable parameter) throws TezosGeneratorException {
        //
        LOG.debug("toRemoteMapType (parameter = {})", parameter);

        //
        switch(parameter.getType().getStyle()) {
            case REMOTE_MAP:
                TezosNativeMapType mapType = (TezosNativeMapType) parameter.getType();

                return MessageFormat
                        .format("{0}<{1}>",
                                "TezosBigMapStorage",
                                toParameterType(mapType.getValueType(), false));
            default:
                String message = MessageFormat.format("Not a remote map parameter: {0}", parameter.getType().getStyle());
                throw new TezosGeneratorException(message);
        }
    }

    public String toMapKeyType (TezosVariable parameter) throws TezosGeneratorException {
        //
        LOG.debug("toMapKeyType (parameter = {})", parameter);

        //
        switch(parameter.getType().getStyle()) {
            case REMOTE_MAP:
            case MAP:

                TezosNativeMapType mapType = (TezosNativeMapType) parameter.getType();

                return toParameterType(mapType.getKeyType(), false);
            default:
                String message = MessageFormat.format("Not a map parameter: {0}", parameter.getType().getStyle());
                throw new TezosGeneratorException(message);
        }
    }

    public String toMapValueType (TezosVariable parameter) throws TezosGeneratorException {
        //
        LOG.debug("toMapValueType (parameter = {})", parameter);

        //
        switch(parameter.getType().getStyle()) {
            case REMOTE_MAP:
            case MAP:

                TezosNativeMapType mapType = (TezosNativeMapType) parameter.getType();

                return toParameterType(mapType.getValueType(), false);
            default:
                String message = MessageFormat.format("Not a map parameter: {0}", parameter.getType().getStyle());
                throw new TezosGeneratorException(message);
        }
    }

    public String toParameterType (TezosType sourceType, boolean optional) throws TezosGeneratorException {

        String result;

        //
        LOG.debug("toParameterType (sourceType = {})", sourceType);

        // Sanity check:
        checkNotNull(sourceType, "sourceType should not be null", TezosGeneratorException.class);
        checkNotNull(sourceType.getStyle(), "sourceType.style should not be null", TezosGeneratorException.class);

        //
        switch(sourceType.getStyle()) {

            case PLAIN:
                TezosNativeType type = (TezosNativeType) sourceType;
                result = getJavaTypeString(type.getTezosRawType());
                break;

            case SET:

                TezosNativeSetType setType = (TezosNativeSetType) sourceType;

                result
                    = MessageFormat
                        .format("{0}<{1}>",
                                getJavaTypeString(setType.getTezosRawType()),
                                toParameterType(setType.getContainedRecord(), false));

                break;

            case RECORD:
                TezosRecordType recordType = (TezosRecordType) sourceType;
                result = recordType.getName();
                break;

            case REMOTE_MAP:
            case MAP:

                TezosNativeMapType mapType = (TezosNativeMapType) sourceType;

                result
                    = MessageFormat
                        .format("{0}<{1},{2}>",
                                getJavaTypeString(mapType.getTezosRawType()),
                                toParameterType(mapType.getKeyType(), false),
                                toParameterType(mapType.getValueType(), false));

                break;

            default:
                String message = MessageFormat.format("Unexpected style of parameter''s type: {0}", sourceType.getStyle());
                throw new TezosGeneratorException(message);
        }

        // Wrap in Optional is applicable:
        if(optional) {
            result = "Optional<" + result + ">";
        }

        //
        LOG.debug("result = {}", result);
        return result;
    }

    public void setContext (Context context) {
        this.context = context;
    }

    public String addPrefixIfApplicable (String variable) {

        if(startsWithDigit(variable)) {
            return "_" + variable;
        }

        return variable;
    }

    @Override
    public String toString () {
        return "Toolbox";
    }
}