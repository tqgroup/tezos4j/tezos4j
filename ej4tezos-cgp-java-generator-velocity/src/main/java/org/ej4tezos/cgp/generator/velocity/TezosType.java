// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.Map;
import java.util.HashMap;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.ej4tezos.utils.asserts.Asserts.*;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public abstract class TezosType {

    enum Style { PLAIN, SET, MAP, REMOTE_MAP, RECORD }

    private static Logger LOG = LoggerFactory.getLogger(TezosType.class);

    private static Map<String,Class<? extends TezosType>> typeMapping;

    static {

        typeMapping = new HashMap<>();

        typeMapping.put("int", TezosNativeType.class);
        typeMapping.put("nat", TezosNativeType.class);
        typeMapping.put("bool", TezosNativeType.class);
        typeMapping.put("unit", TezosNativeType.class);
        typeMapping.put("bytes", TezosNativeType.class);
        typeMapping.put("string", TezosNativeType.class);
        typeMapping.put("timestamp", TezosNativeType.class);
        typeMapping.put("lambda", TezosNativeType.class);
        typeMapping.put("operation", TezosNativeType.class);
        typeMapping.put("key", TezosNativeType.class);
        typeMapping.put("mutez", TezosNativeType.class);
        typeMapping.put("address", TezosNativeType.class);
        typeMapping.put("signature", TezosNativeType.class);

        typeMapping.put("set", TezosNativeSetType.class);
        typeMapping.put("list", TezosNativeSetType.class);

        typeMapping.put("map", TezosNativeMapType.class);
        typeMapping.put("big_map", TezosNativeRemoteMapType.class);

        typeMapping.put("record", TezosRecordType.class);

    }

    public static TezosType convert (String rawType) throws TezosGeneratorException {

        // Sanity check:
        checkNotNull(rawType, "rawType should not be null", TezosGeneratorException.class);

        // Clean-up:
        rawType = rawType.trim();

        //
        if(typeMapping.containsKey(rawType)) {

            try {

                //
                Class<? extends TezosType> typeClass = typeMapping.get(rawType);
                LOG.debug("typeClass = {}", typeClass);

                //
                if(!hasConstructorWithParam(typeClass, String.class)) {
                    LOG.debug("Call empty constructor...");
                    return typeClass.getConstructor().newInstance();
                }
                else {
                    LOG.debug("Call one-param constructor with {}", rawType);
                    return typeClass.getConstructor(String.class).newInstance(rawType);
                }
            }

            catch (Exception ex) {
                String message = MessageFormat.format("An exception occurred while converting {0}: {1}", rawType, ex.getMessage());
                throw new TezosGeneratorException(message, ex);
            }
        }

        //
        String message = MessageFormat.format("Unexpected raw type {0}; cannot convert!", rawType);
        throw new TezosGeneratorException(message);
    }

    public TezosType () {
        super();
    }

    public abstract Style getStyle ();

    public abstract boolean isNative ();

    private static boolean hasConstructorWithParam (Class<?> targetClass, Class<?> ... parameterClass) {

        try {
            targetClass.getConstructor(parameterClass);
            return true;
        }

        catch (NoSuchMethodException ex) {
            return false;
        }
    }

    @Override
    public String toString () {
        return ReflectionToStringBuilder.toString(this);
    }
}