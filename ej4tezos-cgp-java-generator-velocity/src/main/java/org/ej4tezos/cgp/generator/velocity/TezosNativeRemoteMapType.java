// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosNativeRemoteMapType extends TezosNativeMapType {
    public TezosNativeRemoteMapType(String tezosRawType) {
        super(tezosRawType);
    }

    public Style getStyle () {
        return Style.REMOTE_MAP;
    }
}