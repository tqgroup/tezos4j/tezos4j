// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosNativeType extends TezosType {

    private String tezosRawType;

    public TezosNativeType (String tezosRawType) {
        super();
        this.tezosRawType = tezosRawType;
    }

    public String getTezosRawType () {
        return tezosRawType;
    }

    public Style getStyle () {
        return Style.PLAIN;
    }

    public boolean isNative () {
        return true;
    }

    @Override
    public String toString () {
        return ReflectionToStringBuilder.toString(this);
    }
}