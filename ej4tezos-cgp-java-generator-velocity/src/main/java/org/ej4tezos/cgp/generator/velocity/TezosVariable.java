// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosVariable  {

    private String name;
    private TezosType type;
    private boolean optional;
    private boolean remoteMap;

    public TezosVariable () {
        super();
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getName () {
        return name;
    }

    public void setType (TezosType type) {
        this.type = type;
    }

    public TezosType getType () {
        return type;
    }

    public void setOptional (boolean optional) {
        this.optional = optional;
    }

    public boolean isOptional () {
        return optional;
    }

    public boolean isRemoteMapType() { return remoteMap; }

    public void setRemoteMapType (boolean remote) {
        this.remoteMap = remote;
    }

    @Override
    public String toString () {
        return ReflectionToStringBuilder.toString(this);
    }
}