// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosNativeSetType extends TezosNativeType {

    private TezosRecordType containedRecord;

    public TezosNativeSetType (String tezosRawType) {
        super(tezosRawType);
    }

    public Style getStyle () {
        return Style.SET;
    }

    public void setContainedRecord (TezosRecordType containedRecord) {
        this.containedRecord = containedRecord;
    }

    public TezosRecordType getContainedRecord () {
        return containedRecord;
    }
}