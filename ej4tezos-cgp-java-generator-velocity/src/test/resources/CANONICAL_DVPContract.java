package eu.tknext.tezos.contract;

import org.ej4tezos.model.*;

import org.ej4tezos.api.TezosContract;

import org.ej4tezos.api.exception.TezosInvocationException;


public interface EuroTz extends TezosContract {

        void addSuperTransferAgent (TezosAddress addSuperTransferAgent_param_0) throws TezosInvocationException;

        void addTransferAgent (TezosAddress addTransferAgent_param_0) throws TezosInvocationException;

        void approve (Integer amount, TezosAddress f, TezosAddress t) throws TezosInvocationException;

        void approveFeeless (Integer amount, TezosBytes b, TezosAddress f, TezosPublicKey k, Integer nonce, TezosSignature s, TezosAddress t) throws TezosInvocationException;

        void burn (TezosAddress address, Integer amount) throws TezosInvocationException;

        void changeEventSinkContractAddress (TezosAddress changeEventSinkContractAddress_param_0) throws TezosInvocationException;

        void decreaseAllowance (Integer amount, TezosAddress f, TezosAddress t) throws TezosInvocationException;

        void decreaseAllowanceFeeless (Integer amount, TezosBytes b, TezosAddress f, TezosPublicKey k, Integer nonce, TezosSignature s, TezosAddress t) throws TezosInvocationException;

        void deleteSuperTransferAgent (TezosAddress deleteSuperTransferAgent_param_0) throws TezosInvocationException;

        void deleteTransferAgent (TezosAddress deleteTransferAgent_param_0) throws TezosInvocationException;

        void increaseAllowance (Integer amount, TezosAddress f, TezosAddress t) throws TezosInvocationException;

        void increaseAllowanceFeeless (Integer amount, TezosBytes b, TezosAddress f, TezosPublicKey k, Integer nonce, TezosSignature s, TezosAddress t) throws TezosInvocationException;

        void mint (TezosAddress address, Integer amount) throws TezosInvocationException;

        void resetAllAllowances (TezosAddress resetAllAllowances_param_0) throws TezosInvocationException;

        void resetAllowance (TezosAddress address, TezosAddress f) throws TezosInvocationException;

        void setAdministrator (TezosAddress setAdministrator_param_0) throws TezosInvocationException;

        void setPause (Boolean setPause_param_0) throws TezosInvocationException;

        void transfer (Integer amount, TezosAddress f, TezosAddress t) throws TezosInvocationException;

        void transferFeeless (Integer amount, TezosBytes b, TezosAddress f, TezosPublicKey k, Integer nonce, TezosSignature s, TezosAddress t) throws TezosInvocationException;

}
