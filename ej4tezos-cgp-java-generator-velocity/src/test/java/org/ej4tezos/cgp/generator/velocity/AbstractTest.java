// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.json.JSONObject;

import org.apache.commons.io.IOUtils;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public abstract class AbstractTest {

    private Logger LOG = LoggerFactory.getLogger(AbstractTest.class);

    public AbstractTest () {
        super();
    }

    protected String removeJavaComments (String source) throws Exception {
        return source;
    }

    protected JSONObject loadSample (String name) throws Exception {
        return loadSample(name, JSONObject.class);
    }

    protected <T> T loadSample (String name, Class<T> clazz) throws Exception {
        return clazz.getConstructor(String.class).newInstance(IOUtils.toString(AbstractTest.class.getClassLoader().getResourceAsStream(name), Charset.defaultCharset()));
    }

    protected String loadCanonicalFile (String file) throws Exception {
        return IOUtils.toString(AbstractTest.class.getClassLoader().getResourceAsStream("CANONICAL_" + file + ".java"), Charset.defaultCharset());
    }
}