// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.junit.Test;
import org.junit.Before;

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.cgp.api.JavaStubGenerator;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class JavaStubGeneratorImplTest extends AbstractTest {

    private Logger LOG = LoggerFactory.getLogger(JavaStubGeneratorImpl.class);

    private JavaStubGenerator generator;

    @Before
    public void setup () {
        generator = new JavaStubGeneratorImpl();
    }

    @Test
    public void euroTzTest () throws Exception {

        try {
            // Prepare the test:
            JSONObject tezosContractDefinition = loadSample("EuroTz.tcd");
            JavaStubGeneratorTestContext context = new JavaStubGeneratorTestContext();

            // Call the generator:
            generator.generate(context, tezosContractDefinition);

            String generatedSource;

            generatedSource = removeJavaComments(context.getOutputMap().get("FA12Token"));
            LOG.info("Generated interface: \n{}", generatedSource);

            generatedSource = removeJavaComments(context.getOutputMap().get("FA12TokenStorageData"));
            LOG.info("Generated data: \n{}", generatedSource);

            generatedSource = removeJavaComments(context.getOutputMap().get("FA12TokenStorageBalancesValue"));
            LOG.info("Generated record: \n{}", generatedSource);

/*
            generatedSource = removeJavaComments(context.getOutputMap().get("BalancesRecord"));
            LOG.info("Generated record: \n{}", generatedSource);
*/

            // Asserts:
            //assertThat("Unexpected generated result", generatedSource, is("XXX"));
        }

        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}