// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.junit.Test;

import org.json.JSONArray;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hamcrest.Matchers;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class ToolboxTest extends AbstractTest {

    private static final Logger LOG = LoggerFactory.getLogger(ToolboxTest.class);

    private Toolbox toolbox = Toolbox.singleton();

    @Test
    public void startsWithDigitTest () throws Exception {

        boolean result = toolbox.startsWithDigit("0");

        assertThat("Unexpected startsWithDigit result", result, Matchers.is(true));
    }

    @Test
    public void toParametersLine_OneArg_Test () throws Exception {

        JSONObject rawVariable = loadSample("ParameterListOneArg.json");

        TezosVariable variable = TezosDataParser.getTezosVariable("", rawVariable);

        String line = toolbox.toParametersLine(variable);

        assertThat("Unexpected line result", line, Matchers.is("BigInteger param_0"));
    }

    @Test
    public void toParametersLine_OneOptionalArg_Test () throws Exception {

        JSONObject rawVariable = loadSample("ParameterListOneOptionalArg.json");

        TezosVariable variable = TezosDataParser.getTezosVariable("", rawVariable);

        String line = toolbox.toParametersLine(variable);

        assertThat("Unexpected line result", line, Matchers.is("Optional<BigInteger> param_0"));
    }

    @Test
    public void toParametersLine_ThreeArg_Test () throws Exception {

        JSONArray rawVariables = loadSample("ParameterListThreeArg.json", JSONArray.class);

        List<TezosVariable> variables = TezosDataParser.getTezosVariableList("", rawVariables);

        String line = toolbox.toParametersLine(variables);

        assertThat("Unexpected line result", line, Matchers.is("BigInteger param_0, String param_1, TezosAddress param_2"));
    }

    @Test
    public void toParametersLine_FourArgWithMap_Test () throws Exception {

        JSONArray rawVariables = loadSample("ParameterListFourArgWithMap.json", JSONArray.class);

        List<TezosVariable> variables = TezosDataParser.getTezosVariableList("SwordUpgrade", rawVariables);

        String line = toolbox.toParametersLine(variables);

        assertThat("Unexpected line result", line, Matchers.is("BigInteger param_0, String param_1, TezosAddress param_2, TezosBigMap<String,SwordUpgradeParam_3Value> param_3"));
    }

    @Test
    public void toParametersLine_FourArgWithComplexMap_Test () throws Exception {

        JSONArray rawVariables = loadSample("ParameterListFourArgWithComplexMap.json", JSONArray.class);

        List<TezosVariable> variables = TezosDataParser.getTezosVariableList("SwordUpgrade", rawVariables);

        String line = toolbox.toParametersLine(variables);

        assertThat("Unexpected line result", line, Matchers.is("BigInteger param_0, String param_1, TezosAddress param_2, TezosBigMap<SwordUpgradeParam_3Key,SwordUpgradeParam_3Value> param_3"));
    }

    @Test
    public void toParametersLine_FourArgWithSimpleMap_Test () throws Exception {

        JSONArray rawVariables = loadSample("ParameterListFourArgWithSimpleMap.json", JSONArray.class);

        List<TezosVariable> variables = TezosDataParser.getTezosVariableList("Context", rawVariables);

        String line = toolbox.toParametersLine(variables);

        assertThat("Unexpected line result", line, Matchers.is("BigInteger param_0, String param_1, TezosAddress param_2, TezosBigMap<String,TezosAddress> param_3"));
    }
}