// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;

import org.junit.Test;

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.Matchers.*;

import static org.hamcrest.MatcherAssert.assertThat;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosDataParserTest extends AbstractTest {

    private static final Logger LOG = LoggerFactory.getLogger(TezosDataParserTest.class);

    @Test
    public void getTezosVariable_Plain_Test () throws Exception {

        JSONObject rawVariable = loadSample("PlainVariable.json");

        TezosVariable variable = TezosDataParser.getTezosVariable("", rawVariable);

        assertThat("variable should not be null", variable, notNullValue());
        assertThat("variable name should be 'param_0'", variable.getName(), is("param_0"));
        assertThat("variable optional should be 'false'", variable.isOptional(), is(false));
        assertThat("variable.type should not be null", variable.getType(), notNullValue());
        assertThat("variable.type should not be native", variable.getType().isNative(), is(true));
        assertThat("variable.type.style should be 'PLAIN'", variable.getType().getStyle(), is(TezosType.Style.PLAIN));
        assertThat("variable.type.class should 'TezosNativeType'", variable.getType().getClass(), is(TezosNativeType.class));
        assertThat("variable.tezosRawType should be 'int'", ((TezosNativeType)variable.getType()).getTezosRawType(), is("int"));
    }

    @Test
    public void getTezosVariable_ListOfIntVariable_Test () throws Exception {

        JSONObject rawVariable = loadSample("ListOfIntVariable.json");

        TezosVariable variable = TezosDataParser.getTezosVariable("", rawVariable);

        assertThat("variable should not be null", variable, notNullValue());
        assertThat("variable name should be 'myList'", variable.getName(), is("myList"));
        assertThat("variable optional should be 'false'", variable.isOptional(), is(false));
        assertThat("variable.type should not be null", variable.getType(), notNullValue());
        assertThat("variable.type should not be native", variable.getType().isNative(), is(true));
        assertThat("variable.type.style should be 'SET'", variable.getType().getStyle(), is(TezosType.Style.SET));
        assertThat("variable.type.class should 'TezosNativeSetType'", variable.getType().getClass(), is(TezosNativeSetType.class));
        assertThat("variable.tezosRawType should be 'list'", ((TezosNativeType)variable.getType()).getTezosRawType(), is("list"));

        TezosType rawRecord = ((TezosNativeSetType)variable.getType()).getContainedRecord();

        assertThat("record should not be null", rawRecord, notNullValue());
        assertThat("record.class should be 'TezosRecordType'", rawRecord.getClass(), is(TezosRecordType.class));

        TezosRecordType record = (TezosRecordType) rawRecord;

        assertThat("record should not be native", record.isNative(), is(false));
        assertThat("record style should be 'RECORD'", record.getStyle(), is(TezosType.Style.RECORD));

        List<TezosVariable> fields = record.fields();

        assertThat("fields should not be null", fields, notNullValue());
        assertThat("fields should contain one element", fields.size(), is(1));

        variable = fields.get(0);

        assertThat("field[0] should not be null", variable, notNullValue());
        assertThat("field[0] name should be 'param_0'", variable.getName(), is("param_0"));
        assertThat("field[0] optional should be 'false'", variable.isOptional(), is(false));
        assertThat("field[0].type should not be null", variable.getType(), notNullValue());
        assertThat("field[0].type should be native", variable.getType().isNative(), is(true));
        assertThat("field[0].type.style should be 'PLAIN'", variable.getType().getStyle(), is(TezosType.Style.PLAIN));
        assertThat("field[0].type.class should 'TezosNativeType'", variable.getType().getClass(), is(TezosNativeType.class));
        assertThat("field[0].tezosRawType should be 'list'", ((TezosNativeType)variable.getType()).getTezosRawType(), is("int"));

    }

    @Test
    public void getTezosVariable_ListOfIntAndOptionalAddressVariable_Test () throws Exception {

        JSONObject rawVariable = loadSample("ListOfIntAndOptionalAddressVariable.json");

        TezosVariable variable = TezosDataParser.getTezosVariable("", rawVariable);

        assertThat("variable should not be null", variable, notNullValue());
        assertThat("variable name should be 'myList'", variable.getName(), is("myList"));
        assertThat("variable optional should be 'false'", variable.isOptional(), is(false));
        assertThat("variable.type should not be null", variable.getType(), notNullValue());
        assertThat("variable.type should be native", variable.getType().isNative(), is(true));
        assertThat("variable.type.style should be 'SET'", variable.getType().getStyle(), is(TezosType.Style.SET));
        assertThat("variable.type.class should 'TezosNativeSetType'", variable.getType().getClass(), is(TezosNativeSetType.class));
        assertThat("variable.tezosRawType should be 'list'", ((TezosNativeType)variable.getType()).getTezosRawType(), is("list"));

        TezosType rawRecord = ((TezosNativeSetType)variable.getType()).getContainedRecord();

        assertThat("record should not be null", rawRecord, notNullValue());
        assertThat("record.class should be 'TezosRecordType'", rawRecord.getClass(), is(TezosRecordType.class));

        TezosRecordType record = (TezosRecordType) rawRecord;

        assertThat("record should not be native", record.isNative(), is(false));
        assertThat("record style should be 'RECORD'", record.getStyle(), is(TezosType.Style.RECORD));

        List<TezosVariable> fields = record.fields();

        assertThat("fields should not be null", fields, notNullValue());
        assertThat("fields should contain one element", fields.size(), is(2));

        variable = fields.get(0);

        assertThat("field[0] should not be null", variable, notNullValue());
        assertThat("field[0] name should be 'param_0'", variable.getName(), is("param_0"));
        assertThat("field[0] optional should be 'false'", variable.isOptional(), is(false));
        assertThat("field[0].type should not be null", variable.getType(), notNullValue());
        assertThat("field[0].type should be native", variable.getType().isNative(), is(true));
        assertThat("field[0].type.style should be 'PLAIN'", variable.getType().getStyle(), is(TezosType.Style.PLAIN));
        assertThat("field[0].type.class should 'TezosNativeType'", variable.getType().getClass(), is(TezosNativeType.class));
        assertThat("field[0].tezosRawType should be 'int'", ((TezosNativeType)variable.getType()).getTezosRawType(), is("int"));

    }

    @Test
    public void getTezosVariable_ListOfListOfIntVariable_Test () throws Exception {

        JSONObject rawVariable = loadSample("ListOfListOfIntVariable.json");

        TezosVariable variable = TezosDataParser.getTezosVariable("", rawVariable);

        assertThat("variable should not be null", variable, notNullValue());
        assertThat("variable name should be 'myList'", variable.getName(), is("myList"));
        assertThat("variable optional should be 'false'", variable.isOptional(), is(false));
        assertThat("variable.type should not be null", variable.getType(), notNullValue());
        assertThat("variable.type should be native", variable.getType().isNative(), is(true));
        assertThat("variable.type.style should be 'SET'", variable.getType().getStyle(), is(TezosType.Style.SET));
        assertThat("variable.type.class should 'TezosNativeSetType'", variable.getType().getClass(), is(TezosNativeSetType.class));
        assertThat("variable.tezosRawType should be 'list'", ((TezosNativeType)variable.getType()).getTezosRawType(), is("list"));

        // - - -
        TezosType rawRecord = ((TezosNativeSetType)variable.getType()).getContainedRecord();

        assertThat("record should not be null", rawRecord, notNullValue());
        assertThat("record.class should be 'TezosRecordType'", rawRecord.getClass(), is(TezosRecordType.class));

        TezosRecordType record = (TezosRecordType) rawRecord;

        assertThat("record should not be native", record.isNative(), is(false));
        assertThat("record style should be 'RECORD'", record.getStyle(), is(TezosType.Style.RECORD));

        List<TezosVariable> fields = record.fields();

        assertThat("fields should not be null", fields, notNullValue());
        assertThat("fields should contain one element", fields.size(), is(1));

        variable = fields.get(0);

        assertThat("field[0] should not be null", variable, notNullValue());
        assertThat("field[0] name should be 'param_0'", variable.getName(), is("param_0"));
        assertThat("field[0] optional should be 'false'", variable.isOptional(), is(false));
        assertThat("field[0].type should not be null", variable.getType(), notNullValue());
        assertThat("field[0].type should be native", variable.getType().isNative(), is(true));
        assertThat("field[0].type.style should be 'SET'", variable.getType().getStyle(), is(TezosType.Style.SET));
        assertThat("field[0].type.class should 'TezosNativeSetType'", variable.getType().getClass(), is(TezosNativeSetType.class));
        assertThat("field[0].tezosRawType should be 'list'", ((TezosNativeType)variable.getType()).getTezosRawType(), is("list"));

        // - - -
        rawRecord = ((TezosNativeSetType)variable.getType()).getContainedRecord();

        assertThat("record2 should not be null", rawRecord, notNullValue());
        assertThat("record2.class should be 'TezosRecordType'", rawRecord.getClass(), is(TezosRecordType.class));

        record = (TezosRecordType) rawRecord;

        assertThat("record2 should not be native", record.isNative(), is(false));
        assertThat("record2 style should be 'RECORD'", record.getStyle(), is(TezosType.Style.RECORD));

        fields = record.fields();

        assertThat("fields2 should not be null", fields, notNullValue());
        assertThat("fields2 should contain one element", fields.size(), is(1));

        variable = fields.get(0);

        assertThat("field2[0] should not be null", variable, notNullValue());
        assertThat("field2[0] name should be 'param_1'", variable.getName(), is("param_1"));
        assertThat("field2[0] optional should be 'true'", variable.isOptional(), is(true));
        assertThat("field2[0].type should not be null", variable.getType(), notNullValue());
        assertThat("field2[0].type should be native", variable.getType().isNative(), is(true));
        assertThat("field2[0].type.style should be 'PLAIN'", variable.getType().getStyle(), is(TezosType.Style.PLAIN));
        assertThat("field2[0].type.class should 'TezosNativeType'", variable.getType().getClass(), is(TezosNativeType.class));
        assertThat("field2[0].tezosRawType should be 'int'", ((TezosNativeType)variable.getType()).getTezosRawType(), is("int"));

    }

    @Test
    public void getTezosVariable_MapOfStringToAddress_Test () throws Exception {

        JSONObject rawVariable = loadSample("MapOfStringToAddress.json");

        TezosVariable variable = TezosDataParser.getTezosVariable("", rawVariable);

        assertThat("variable should not be null", variable, notNullValue());
        assertThat("variable name should be 'myList'", variable.getName(), is("param_0"));
        assertThat("variable optional should be 'false'", variable.isOptional(), is(false));
        assertThat("variable.type should not be null", variable.getType(), notNullValue());
        assertThat("variable.type should be native", variable.getType().isNative(), is(true));
        assertThat("variable.type.style should be 'MAP'", variable.getType().getStyle(), is(TezosType.Style.REMOTE_MAP));
        assertThat("variable.type.class should 'TezosNativeMapType'", variable.getType().getClass(), is(TezosNativeRemoteMapType.class));
        assertThat("variable.tezosRawType should be 'big_map'", ((TezosNativeType)variable.getType()).getTezosRawType(), is("big_map"));

        TezosNativeMapType map = ((TezosNativeMapType)variable.getType());

        assertThat("map should not be null", map, notNullValue());
        assertThat("map should be native", map.isNative(), is(true));
        assertThat("map.style should be 'MAP'", map.getStyle(), is(TezosType.Style.REMOTE_MAP));

        assertThat("map.key should not be null", map.getKeyType(), notNullValue());
        assertThat("map.key should be native", map.getKeyType().isNative(), is(true));
        assertThat("map.key class should be 'TezosNativeType'", map.getKeyType().getClass(), is(TezosNativeType.class));
        assertThat("map.key.tezosRawType should be 'string'", ((TezosNativeType)map.getKeyType()).getTezosRawType(), is("string"));

        // - - -
        TezosType rawRecord = map.getValueType();

        assertThat("record should not be null", rawRecord, notNullValue());
        assertThat("record.class should be 'TezosRecordType'", rawRecord.getClass(), is(TezosRecordType.class));

        TezosRecordType record = (TezosRecordType) rawRecord;

        assertThat("record should not be native", record.isNative(), is(false));
        assertThat("record style should be 'RECORD'", record.getStyle(), is(TezosType.Style.RECORD));

        List<TezosVariable> fields = record.fields();

        assertThat("fields should not be null", fields, notNullValue());
        assertThat("fields should contain one element", fields.size(), is(1));

        variable = fields.get(0);

        assertThat("field[0] should not be null", variable, notNullValue());
        assertThat("field[0] name should be 'contractAddress'", variable.getName(), is("contractAddress"));
        assertThat("field[0] optional should be 'true'", variable.isOptional(), is(true));
        assertThat("field[0].type should not be null", variable.getType(), notNullValue());
        assertThat("field[0].type should be native", variable.getType().isNative(), is(true));
        assertThat("field[0].type.style should be 'PLAIN'", variable.getType().getStyle(), is(TezosType.Style.PLAIN));
        assertThat("field[0].type.class should 'TezosNativeType'", variable.getType().getClass(), is(TezosNativeType.class));
        assertThat("field[0].tezosRawType should be 'address'", ((TezosNativeType)variable.getType()).getTezosRawType(), is("address"));

    }
}