// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.generator.velocity;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.io.Reader;
import java.io.IOException;
import java.io.FileOutputStream;

import java.nio.charset.Charset;

import java.util.Map;
import java.util.HashMap;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.io.IOUtils;

import org.ej4tezos.cgp.api.model.JavaStubGeneratorContext;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class JavaStubGeneratorTestContext implements JavaStubGeneratorContext {

    private static final Logger LOG = LoggerFactory.getLogger(JavaStubGeneratorTestContext.class);

    private Map<String,String> outputMap = new HashMap<>();

    public String getPackageName () {
        return "eu.tknext.tezos.contract";
    }

    public void persist (String outputFileName, Reader reader) throws TezosGeneratorException {

        LOG.debug("persist (outputFileName = {}, reader = {})", outputFileName, reader);

        try {
            outputMap.put(outputFileName, IOUtils.toString(reader));
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while persisting a file: {0}", ex.getMessage());
            throw new TezosGeneratorException(message, ex);
        }
    }

    public Map<String, String> getOutputMap () {
        return outputMap;
    }

    public void dump () throws IOException {
        for(String key : outputMap.keySet()) {
            IOUtils.write(outputMap.get(key), new FileOutputStream(key + ".java"), Charset.defaultCharset());
        }
    }

    public void dump (Logger logger) {
        for(String key : outputMap.keySet()) {
            logger.info("{} -> \n{}", key, outputMap.get(key));
        }
    }
}