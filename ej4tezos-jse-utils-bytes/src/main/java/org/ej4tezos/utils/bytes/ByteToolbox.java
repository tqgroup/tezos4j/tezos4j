// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.bytes;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import static org.ej4tezos.utils.asserts.Asserts.*;

import org.ej4tezos.api.exception.TezosException;

import java.util.Arrays;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class ByteToolbox {

    public static byte [] last (byte[] source, int length) throws TezosException {

        checkNotNull(source, "source should not be null", TezosException.class);
        checkState(source.length >= length, "source is too small", TezosException.class);

        byte [] result = new byte[length];

        for(int i = 0; i < length; i++) {
            result[i] = source[source.length + i - length];
        }

        return result;
    }

    public static byte [] trim (byte[] source, int length) throws TezosException {

        checkState(source.length >= length, "source is too small", TezosException.class);

        byte [] result = new byte[source.length - length];

        for(int i = 0; i < source.length - length; i++) {
            result[i] = source[i];
        }

        return result;
    }

    public static byte [] first (byte[] source, int length) throws TezosException {

        checkState(source.length >= length, "source is too small", TezosException.class);

        byte [] result = new byte[length];

        for(int i = 0; i < length; i++) {
            result[i] = source[i];
        }

        return result;
    }

    public static byte [] mid (byte[] source, int index, int length) throws TezosException {

        checkState(source.length >= index + length, "source is too small", TezosException.class);

        byte [] result = new byte[length];

        for(int i = 0; i < length; i++) {
            result[i] = source[i + index];
        }

        return result;
    }

    public static byte [] lastFrom (byte[] source, int startIndex) throws TezosException {

        byte [] result = new byte[source.length - startIndex];

        for(int i = startIndex; i < source.length; i++) {
            result[i - startIndex] = source[i];
        }

        return result;
    }

    public static byte [] join (byte [] ... a) throws TezosException {
        int size = 0;

        for(byte [] arr : a) {
            size += arr.length;
        }

        byte [] result = new byte[size];

        int index = 0;
        for(byte [] arr : a) {
            int arr_len = arr.length;
            System.arraycopy(arr, 0, result, index, arr_len);
            index += arr_len;
        }

        return result;
    }

    public static boolean equals (byte[] a, byte[] b) throws TezosException {
        return Arrays.equals(a, b);
    }

    public static boolean startsWith (byte[] source, byte[] prefix) throws TezosException {

        checkState(prefix.length <= source.length, "source is too small", TezosException.class);

        for(int i = 0; i < prefix.length; i++) {
            if(source[i] != prefix[i]) {
                return false;
            }
        }

        return true;
    }

    public static boolean endsWith (byte[] source, byte[] suffix) throws TezosException {

        checkState(suffix.length <= source.length, "source is too small", TezosException.class);

        for(int i = 0; i < suffix.length; i++) {
            if(source[i + (source.length - suffix.length)] != suffix[i]) {
                return false;
            }
        }

        return true;
    }

    public static int toUnsignedInt (byte x) {
        return ((int) x) & 0xff;
    }
}