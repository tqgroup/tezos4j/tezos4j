// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.bytes;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import static org.hamcrest.MatcherAssert.assertThat;

import org.ej4tezos.api.exception.TezosException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class ByteToolboxTest {

    @Test
    public void lastTest () throws Exception {

        byte [] source = new byte [] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

        byte [] result = ByteToolbox.last(source, 4);

        assertThat("result should not be null", result, notNullValue());
        assertThat("unexpected result size", result.length, is(4));
        assertThat("unexpected result[0] value", result[0], is((byte)5));
        assertThat("unexpected result[1] value", result[1], is((byte)6));
        assertThat("unexpected result[2] value", result[2], is((byte)7));
        assertThat("unexpected result[3] value", result[3], is((byte)8));
    }

    @Test
    public void lastTestWithNullValue () throws Exception {

        try {

            byte[] result = ByteToolbox.last(null, 4);

        }

        catch (TezosException ex) {
            assertThat("unexpected exception message", ex.getMessage(), is("source should not be null"));
        }
    }

    @Test
    public void equalsTest () throws Exception {

        byte [] a = new byte [] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
        byte [] b = new byte [] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

        boolean result = ByteToolbox.equals(a, b);

        assertThat("unexpected result", result, is(true));
    }

    @Test
    public void equalsFalseTest () throws Exception {

        byte [] a = new byte [] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
        byte [] b = new byte [] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        boolean result = ByteToolbox.equals(a, b);

        assertThat("unexpected result", result, is(false));
    }

    @Test
    public void startsWithTest () throws Exception {

        byte [] source = new byte [] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

        boolean result = ByteToolbox.startsWith(source, new byte [] { 0, 1, 2 });

        assertThat("unexpected result", result, is(true));
    }

    @Test
    public void startsWithFalseTest () throws Exception {

        byte [] source = new byte [] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

        boolean result = ByteToolbox.startsWith(source, new byte [] { 1, 2 });

        assertThat("unexpected result", result, is(false));
    }

    @Test
    public void endsWithTest () throws Exception {

        byte [] source = new byte [] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

        boolean result = ByteToolbox.endsWith(source, new byte [] { 6, 7, 8 });

        assertThat("unexpected result", result, is(true));
    }

    @Test
    public void endsWithFalseTest () throws Exception {

        byte [] source = new byte [] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

        boolean result = ByteToolbox.endsWith(source, new byte [] { 5, 7, 8 });

        assertThat("unexpected result", result, is(false));
    }

    @Test
    public void trimTest () throws Exception {

        byte [] source = new byte [] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

        byte [] result = ByteToolbox.trim(source, 4);

        assertThat("result should not be null", result, notNullValue());
        assertThat("unexpected result size", result.length, is(5));
        assertThat("unexpected result[0] value", result[0], is((byte)0));
        assertThat("unexpected result[1] value", result[1], is((byte)1));
        assertThat("unexpected result[2] value", result[2], is((byte)2));
        assertThat("unexpected result[3] value", result[3], is((byte)3));
    }

    @Test
    public void firstTest () throws Exception {

        byte [] source = new byte [] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

        byte [] result = ByteToolbox.first(source, 4);

        assertThat("result should not be null", result, notNullValue());
        assertThat("unexpected result size", result.length, is(4));
        assertThat("unexpected result[0] value", result[0], is((byte)0));
        assertThat("unexpected result[1] value", result[1], is((byte)1));
        assertThat("unexpected result[2] value", result[2], is((byte)2));
        assertThat("unexpected result[3] value", result[3], is((byte)3));
    }

    @Test
    public void midTest () throws Exception {

        byte [] source = new byte [] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

        byte [] result = ByteToolbox.mid(source, 2, 5);

        assertThat("result should not be null", result, notNullValue());
        assertThat("unexpected result size", result.length, is(5));
        assertThat("unexpected result[0] value", result[0], is((byte)2));
        assertThat("unexpected result[1] value", result[1], is((byte)3));
        assertThat("unexpected result[2] value", result[2], is((byte)4));
        assertThat("unexpected result[3] value", result[3], is((byte)5));
        assertThat("unexpected result[3] value", result[4], is((byte)6));
    }

    @Test
    public void lastFromTest () throws Exception {

        byte [] source = new byte [] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

        byte [] result = ByteToolbox.lastFrom(source, 5);

        assertThat("result should not be null", result, notNullValue());
        assertThat("unexpected result size", result.length, is(4));
        assertThat("unexpected result[0] value", result[0], is((byte)5));
        assertThat("unexpected result[1] value", result[1], is((byte)6));
        assertThat("unexpected result[2] value", result[2], is((byte)7));
        assertThat("unexpected result[3] value", result[3], is((byte)8));
    }

    @Test
    public void joinTest () throws Exception {

        byte [] result = ByteToolbox.join(new byte [] { 5 }, new byte [] { 6, 7 }, new byte [] { 8 });

        assertThat("result should not be null", result, notNullValue());
        assertThat("unexpected result size", result.length, is(4));
        assertThat("unexpected result[0] value", result[0], is((byte)5));
        assertThat("unexpected result[1] value", result[1], is((byte)6));
        assertThat("unexpected result[2] value", result[2], is((byte)7));
        assertThat("unexpected result[3] value", result[3], is((byte)8));
    }
}