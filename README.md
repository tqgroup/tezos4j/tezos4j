# Tezos 4 Java (tezos4j)

## How to Build

## How to Use

### Maven Repository

Add the following stanza to your Maven project (`pom.xml`): 
```xml
    <repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/22933515/packages/maven</url>
        </repository>
    </repositories>
```

or the following stanza to your Maven settings:

```xml
<settings>
    ...
    <profiles>
        ...
        <profile>
            ...
            <repositories>
                <repository>
                    <id>gitlab-maven</id>
                    <url>https://gitlab.com/api/v4/projects/22933515/packages/maven</url>
                </repository>
            </repositories>
            ...
        </profile>
        ...
    </profiles>
    ...
</settings>
```

For more information about configuring repositories, please see 
[Maven Guide to Multiple Repositories](https://maven.apache.org/guides/mini/guide-multiple-repositories.html).

### Using Maven Code Generation Plugin

Using Maven Code Generator Plugin requires several steps:

1. Configuring Maven Plugin Code Generator.
2. Configuring Maven helper to add newly generated sources to your Maven project.

For full example please refer to [Big Maps Test](ej4tezos-cgp-big-maps-test/pom.xml)

```xml
<project>    
    <properties>
        <tezos.url/>
        <tezos.contract.address/>
    ..
    </properties>
    <build>
        ...
        <plugins>
            ...
            <plugin>
                <groupId>org.ej4tezos</groupId>
                <artifactId>cgp-maven-plugin</artifactId>
                <version>1.0.0.0-SNAPSHOT</version>
                <executions>
                    <execution>
                        <id>script</id>
                        <goals>
                            <goal>script</goal>
                        </goals>
                        <configuration>
                            <url>${tezos.url}</url>
                            <address>${tezos.contract.address}</address>
                        </configuration>
                    </execution>
                    <execution>
                        <id>micheline</id>
                        <goals>
                            <goal>micheline</goal>
                        </goals>
                        <configuration>
                            <name>BigMaps</name>
                        </configuration>
                    </execution>
                    <execution>
                        <id>java</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>java</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <version>1.7</version>
                <executions>
                    <execution>
                        <id>add-source</id>
                        <phase>process-sources</phase>
                        <goals>
                            <goal>add-source</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>target/generated-sources/ej4tezos</source>
                            </sources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            ...
        </plugins>
        ...
    </build>
</project>
```

Once the plugin is configured per above run a maven build:

```bash
mvn install -D"tezos.url=<Tezos Node RPC API URL>" -D"tezos.contract.address=<CONTRACT ADDRESS>"
```

The contract will be downloaded, analyzed, and the stubs will be generated in project's `target/generated-sources/`
directory.
