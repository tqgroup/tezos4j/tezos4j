// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.net.URI;
import java.util.Optional;

import org.ej4tezos.model.TezosContractAddress;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public abstract class TezosContractDefinitionGeneratorContext {

    public abstract String getName ();

    public Optional<TezosContractAddress> getDeploymentAddress () {
        return Optional.empty();
    }

    public Optional<URI> getSource () {
        return Optional.empty();
    }

    public Optional<String> getUser () {
        return Optional.empty();
    }

    public Optional<String> getHost () {
        return Optional.empty();
    }

    public Optional<String> getTimestamp () {
        return Optional.empty();
    }
}