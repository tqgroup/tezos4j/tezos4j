// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.io.Reader;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public interface JavaStubGeneratorContext {

    String getPackageName ();

    void persist (String outputFileName, Reader reader) throws TezosGeneratorException;

}