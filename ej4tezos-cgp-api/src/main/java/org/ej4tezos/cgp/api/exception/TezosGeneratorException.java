// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.api.exception;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosGeneratorException extends Exception {

    public TezosGeneratorException () {
        super();
    }

    public TezosGeneratorException (String message) {
        super(message);
    }

    public TezosGeneratorException (String message, Throwable cause) {
        super(message, cause);
    }
}