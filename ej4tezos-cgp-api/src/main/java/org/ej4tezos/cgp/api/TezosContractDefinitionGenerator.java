// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.api;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.io.InputStream;

import org.json.JSONObject;

import org.ej4tezos.cgp.api.model.TezosContractDefinitionGeneratorContext;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public interface TezosContractDefinitionGenerator {

    JSONObject generate (TezosContractDefinitionGeneratorContext context, JSONObject script) throws TezosGeneratorException;

    JSONObject generate (TezosContractDefinitionGeneratorContext context, InputStream inputStream) throws TezosGeneratorException;

}