// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.api;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.json.JSONObject;

import org.ej4tezos.cgp.api.model.JavaStubGeneratorContext;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public interface JavaStubGenerator {

    void generate (JavaStubGeneratorContext context, JSONObject tezosContractDefinition) throws TezosGeneratorException;

}