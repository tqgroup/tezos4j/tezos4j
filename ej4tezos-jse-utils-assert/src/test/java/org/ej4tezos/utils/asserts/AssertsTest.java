// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.asserts;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.security.InvalidKeyException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.junit.Test;

import static org.junit.Assert.fail;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class AssertsTest {

    private static final Logger LOG = LoggerFactory.getLogger(AssertsTest.class);

    /**
     * Tests if the method checkNotNull is not doing anything when the object is not null.
     * @throws Exception
     */
    @Test
    public void checkNotNullWithNotNullObjectTest () throws Exception {

        Asserts.checkNotNull("not null", "It should be ok", RuntimeException.class);

    }

    /**
     * Tests if the method checkNotNull is throwing the specified exception when the object is null.
     * @throws Exception
     */
    @Test
    public void checkNotNullWithNullObjectTest () throws Exception {

        try {
            Asserts.checkNotNull(null, "It should not be ok", InvalidKeyException.class);
            fail("An exception should have occurred");
        }

        catch (InvalidKeyException ex) {
            assertThat("Unexpected exception message", ex.getMessage(), is("It should not be ok"));
        }
    }
}