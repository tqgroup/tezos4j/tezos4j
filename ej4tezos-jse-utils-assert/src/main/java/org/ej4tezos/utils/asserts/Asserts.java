// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.asserts;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.text.MessageFormat;

import java.lang.reflect.InvocationTargetException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class Asserts {

    private static final Logger LOG = LoggerFactory.getLogger(Asserts.class);

    public static <T extends Throwable> void checkNotNull (Object object, String message, Class<T> clazz) throws T {

        try {

            if (object == null) {

                if(message == null) {
                    message = "object is null";
                }

                T exception = clazz.getConstructor(String.class).newInstance(message);
                LOG.debug("exception = {}", exception.getMessage());
                throw  exception;
            }
        }

        catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {

            String exceptionMessage
                = MessageFormat
                    .format("An exception occurred while executing checkNotNull: {0}",
                            ex.getMessage());

            throw new RuntimeException(exceptionMessage, ex);
        }
    }

    public static <T extends Throwable> void checkState (boolean condition, String message, Class<T> clazz) throws T {

        try {

            if (!condition) {

                if(message == null) {
                    message = "condition is not fulfilled";
                }

                T exception = clazz.getConstructor(String.class).newInstance(message);
                LOG.debug("exception = {}", exception.getMessage());
                throw  exception;
            }
        }

        catch (NoSuchMethodException | InstantiationException |IllegalAccessException | InvocationTargetException ex) {

            String exceptionMessage
                = MessageFormat
                    .format("An exception occurred while executing checkState: {0}",
                            ex.getMessage());

            throw new RuntimeException(exceptionMessage, ex);
        }
    }
}