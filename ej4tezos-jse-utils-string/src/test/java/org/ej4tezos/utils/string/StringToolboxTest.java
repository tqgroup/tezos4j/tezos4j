// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.string;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.junit.Test;

import static org.junit.Assert.assertEquals;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class StringToolboxTest {

    @Test
    public void maskWithSimpleString () throws Exception  {

        String result = StringToolbox.maskWith("Coucou", '*');

        assertEquals("Unexpected result", result, "******");
    }

    @Test
    public void maskWithNullString () throws Exception  {

        String result = StringToolbox.maskWith(null, '*');

        assertEquals("Unexpected result", result, null);
    }

    @Test
    public void maskWithEmptyString () throws Exception  {

        String result = StringToolbox.maskWith("", '*');

        assertEquals("Unexpected result", result, "");
    }
}