// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.string;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class StringToolbox {

    public static final char DEFAULT_MASK_CHARACTER = '*';

    private final static Logger LOG = LoggerFactory.getLogger(StringToolbox.class);

    public static String maskWith (String value, char maskCharacter) {

        if(value == null) {
            return null;
        }

        char [] result = new char[value.length()];

        for(int i = 0; i < result.length; i++) {
            result[i] = maskCharacter;
        }

        return new String(result);
    }

    public static String maskWith (Object value, char maskCharacter) {
        return maskWith(value == null ? null : value.toString(), maskCharacter);
    }

    public static String mask (Object value) {
        return maskWith(value == null ? null : value.toString(), DEFAULT_MASK_CHARACTER);
    }

    public static String mask (String value) {
        return maskWith(value, DEFAULT_MASK_CHARACTER);
    }
}