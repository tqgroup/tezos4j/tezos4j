package org.ej4tezos.impl;

import org.ej4tezos.papi.TezosResourceLoader;

import java.io.InputStream;

public class SimpleTezosResourceLoader implements TezosResourceLoader {
    @Override
    public InputStream getResourceAsStream(String name) {
        InputStream result = null;
        ClassLoader tccl = Thread.currentThread().getContextClassLoader();
        if (tccl != null) {
            result = tccl.getResourceAsStream(name);
        }
        if (result == null) {
            result = SimpleTezosResourceLoader.class.getResourceAsStream(name);
        }
        return result;
    }
}
