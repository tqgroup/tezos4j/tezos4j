// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

import org.ej4tezos.api.TezosContractOriginator;
import org.ej4tezos.api.exception.TezosException;
import org.ej4tezos.api.model.TezosOriginationResult;
import org.ej4tezos.impl.model.InjectOperationResult;
import org.ej4tezos.impl.model.OriginationOperationResult;
import org.ej4tezos.impl.model.PreApplyOperationResult;
import org.ej4tezos.model.TezosIdentity;
import org.ej4tezos.model.TezosPrivateKey;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.Map;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosContractOriginatorImpl extends TezosAbstractService implements TezosContractOriginator {

    private static final Logger LOG = LoggerFactory.getLogger(TezosContractOriginatorImpl.class);

    private static final String JSON_CODE_FIELD = "code";
    private static final String JSON_STORAGE_FIELD = "storage";

    private static final String ORIGINATION_PARAMETERS = "${originationParameters}";

    private String genericRunOriginationTemplate;
    private String genericForgeOriginationTemplate;
    private String genericPreApplyOriginationTemplate;

    public TezosContractOriginatorImpl(TezosOriginationContext context, TezosIdentity adminIdentity) throws TezosException {
        this(context, adminIdentity.getPrivateKey());
        setAdminIdentity(adminIdentity);
    }

    public TezosContractOriginatorImpl(TezosOriginationContext context, TezosPrivateKey adminPrivateKey) throws TezosException {

        LOG.info("A new instance of 'TezosContractOriginatorImpl' got created; logging on {}...", LOG.getName());

        setAdminPrivateKey(adminPrivateKey);

        setTezosKeyService(context.getTezosKeyService());
        setTezosFeeService(context.getTezosFeeService());
        setTezosCoreService(context.getTezosCoreService());
        setTezosConnectivity(context.getTezosConnectivity());
        setTezosCryptoProvider(context.getTezosCryptoProvider());
        setTezosResourceLoader(context.getTezosResourceLoader());
    }

    @Override
    @PostConstruct
    public void init () throws Exception {
        super.init();

        LOG.debug("init ()");

        // Load templates:
        genericRunOriginationTemplate = loadTemplate("genericRunOrigination.template");
        genericForgeOriginationTemplate = loadTemplate("genericForgeOrigination.template");
        genericPreApplyOriginationTemplate = loadTemplate("genericPreApplyOrigination.template");
    }

    protected OriginationOperationResult originate (String runTemplate, String forgeTemplate, String preApplyTemplate) throws TezosException {

        // Log the call:
        LOG.debug("originate (runTemplate = {}, forgeTemplate = {}, preApplyTemplate = {})",
                              runTemplate, forgeTemplate, preApplyTemplate);

        // Delegate the call:
        return originate(runTemplate, forgeTemplate, preApplyTemplate, Collections.emptyMap());
    }

    protected OriginationOperationResult originate (String runTemplate, String forgeTemplate, String preApplyTemplate, Map<String, Object> parameters) throws TezosException {

        JSONObject processOperationResult = processOperation (runTemplate, forgeTemplate, preApplyTemplate, parameters, "origination");

        InjectOperationResult injectOperationResult = (InjectOperationResult) processOperationResult.get("injectOperationResult");
        PreApplyOperationResult preApplyOperationResult = (PreApplyOperationResult) processOperationResult.get("preApplyOperationResult");


        OriginationOperationResult originationOperationResult = OriginationOperationResult.toInjectOperationResult(injectOperationResult, preApplyOperationResult);
        LOG.debug("originationOperationResult = {}", originationOperationResult);

        // Confirmation
        getTezosCoreService().waitForTransaction(originationOperationResult.getOriginatingBlockHash(),
                originationOperationResult.getTransactionHash());
        //

        return originationOperationResult;
    }

    public TezosOriginationResult originate (JSONObject storage, JSONArray code) throws TezosException {

        JSONObject script = new JSONObject();

        script.put(JSON_CODE_FIELD, code);
        script.put(JSON_STORAGE_FIELD, storage);

        //
        String originationRunOperationTemplate
            = genericRunOriginationTemplate.replace(ORIGINATION_PARAMETERS, script.toString());

        String originationForgeOperationTemplate
            = genericForgeOriginationTemplate.replace(ORIGINATION_PARAMETERS, script.toString());

        String originationPreApplyOperationTemplate
            = genericPreApplyOriginationTemplate.replace(ORIGINATION_PARAMETERS, script.toString());

        //
        OriginationOperationResult originationOperationResult
            = originate(originationRunOperationTemplate, originationForgeOperationTemplate, originationPreApplyOperationTemplate);

        //
        TezosOriginationResult originationResult
            = TezosOriginationResult
                .toTezosOriginationResult(originationOperationResult.getOriginatedContractAddress(),
                                          originationOperationResult.getTransactionHash());

        //
        LOG.debug("originationResult = {}", originationResult);
        return originationResult;
    }

    @Override
    public String toString () {
        return "TezosContractOriginatorImpl";
    }
}