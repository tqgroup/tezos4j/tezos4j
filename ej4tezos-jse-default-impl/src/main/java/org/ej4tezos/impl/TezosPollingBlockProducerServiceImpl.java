// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.api.model.TezosBlock;
import org.ej4tezos.api.model.TezosLevel;

import org.ej4tezos.api.exception.TezosException;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosPollingBlockProducerServiceImpl extends TezosAbstractBlockProducerService implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(TezosPollingBlockProducerServiceImpl.class);

    private TezosLevel lastLevel;

    private Thread thread = null;
    private long sleepingDelay = 60000;
    private boolean shouldStop = false;

    public TezosPollingBlockProducerServiceImpl () {
        super();
    }

    public void init () throws TezosException {
        checkNotNull(tezosCoreService, "tezosCoreService should not be null", TezosException.class);
    }

    public void start () throws TezosException {

        LOG.debug("start ()");

        //
        super.start();

        if(thread == null) {
            String name = "tezos-polling-block-producer-thread-" + System.currentTimeMillis();
            LOG.info("Starting thread... (name = {})", name);
            shouldStop = false;
            thread = new Thread(this, name);
            thread.start();
        }
        else {
            LOG.warn("A thread has already been started!");
        }
    }

    public void stop () throws TezosException {

        LOG.debug("stop ()");

        try {

            if(thread != null) {

                // Signal the thread to stop:
                LOG.info("Stoping thread...");
                shouldStop = true;

                // Wait for the thread to join:
                LOG.info("Waiting for thread to join...");
                thread.join();
                LOG.info("Thread joined!");

                // Clean-up the thread:
                thread = null;
            }
            else {
                LOG.warn("No thread is running!");
            }

            //
            super.stop();
        }

        catch (InterruptedException ex) {
            LOG.warn("We got interrupted while waiting for the thread to stop: {}", ex.getMessage());
        }
    }

    @Override
    public void run () {

        //
        LOG.debug("run ()");

        try {

            //
            LOG.debug("Entering main loop...");
            while (!shouldStop && !Thread.currentThread().isInterrupted()) {

                if(hasListener()) {

                    //
                    if(lastLevel == null) {
                        lastLevel = TezosLevel.toTezosLevel(tezosCoreService.getLevel().getValue().subtract(BigInteger.ONE));
                    }

                    //
                    TezosLevel currentLevel = tezosCoreService.getLevel();

                    //
                    for (long value = lastLevel.getValue().longValue() + 1; value <= currentLevel.getValue().longValue(); value++) {

                        try {
                            // Compute the target level:
                            TezosLevel target = TezosLevel.toTezosLevel(value);

                            // Retrieve the target block:
                            TezosBlock block = tezosCoreService.getBlock(target);

                            // Propagate the block:
                            propagate(block);

                            LOG.debug("Set lastLevel to {}...", value);
                            lastLevel = TezosLevel.toTezosLevel(value);
                        }

                        catch (TezosException ex) {
                            LOG.warn("An exception occurred while attempting to propagate block {}: {}", value, ex.getMessage());
                            break;
                        }
                    }
                }
                else {
                    lastLevel = null;
                }

                //
                LOG.debug("Waiting... (delay = {}ms)", sleepingDelay);
                Thread.sleep(sleepingDelay);

            }

            //
            lastLevel = null;
        }

        catch (Exception ex) {
            LOG.warn("An exception occurred while in the main loop: {}", ex.getMessage(), ex);
        }

        LOG.info("Exiting main loop...");

    }

    @Override
    public String toString () {
        return "TezosPollingBlockProducerServiceImpl";
    }
}