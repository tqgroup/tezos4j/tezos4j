// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.text.MessageFormat;

import java.security.SecureRandom;

import org.ej4tezos.api.TezosFeeService;
import org.ej4tezos.api.TezosKeyService;
import org.ej4tezos.api.TezosCoreService;

import org.ej4tezos.api.exception.TezosException;

import org.ej4tezos.papi.TezosConnectivity;
import org.ej4tezos.papi.TezosCryptoProvider;

import org.ej4tezos.crypto.impl.TezosKeyServiceImpl;
import org.ej4tezos.crypto.impl.TezosCryptoProviderImpl;
import org.ej4tezos.papi.TezosResourceLoader;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public class TezosOriginationContext {

    public static final String DEFAULT_NODE = "https://mainnet.smartpy.io/";

    private String nodeUrl;
    private TezosKeyService tezosKeyService = null;
    private TezosFeeService tezosFeeService = null;
    private TezosCoreService tezosCoreService = null;
    private TezosConnectivity tezosConnectivity = null;
    private TezosCryptoProvider tezosCryptoProvider = null;
    private TezosResourceLoader tezosResourceLoader = null;

    public TezosOriginationContext () {
        this(DEFAULT_NODE);
    }

    public TezosOriginationContext (String nodeUrl) {
        super();
        this.nodeUrl = nodeUrl;
    }

    public synchronized TezosConnectivity getTezosConnectivity () throws TezosException {

        try {

            if (tezosConnectivity == null) {
                tezosConnectivity = new TezosConnectivityImpl();
                ((TezosConnectivityImpl) tezosConnectivity).setNodeUrl(nodeUrl);
                ((TezosConnectivityImpl) tezosConnectivity).init();
            }

            return tezosConnectivity;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving TezosConnectivity: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    public synchronized TezosKeyService getTezosKeyService () throws TezosException {

        try {

            if(tezosKeyService == null) {
                tezosKeyService = new TezosKeyServiceImpl();
                ((TezosKeyServiceImpl)tezosKeyService).setTezosCryptoProvider(getTezosCryptoProvider());
                ((TezosKeyServiceImpl)tezosKeyService).init();
            }

            return tezosKeyService;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving TezosKeyService: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    public synchronized TezosFeeService getTezosFeeService () throws TezosException {

        try {

            if(tezosFeeService == null) {
                tezosFeeService = new TezosFeeServiceImpl();
                ((TezosFeeServiceImpl)tezosFeeService).init();
            }

            return tezosFeeService;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving TezosFeeService: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    public synchronized TezosCoreService getTezosCoreService () throws TezosException {

        try {

            if(tezosCoreService == null) {
                tezosCoreService = new TezosCoreServiceImpl();
                ((TezosCoreServiceImpl)tezosCoreService).setTezosConnectivity(getTezosConnectivity());
                ((TezosCoreServiceImpl)tezosCoreService).init();
            }

            return tezosCoreService;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving TezosCoreService: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    public synchronized TezosCryptoProvider getTezosCryptoProvider () throws TezosException {

        try {

            if(tezosCryptoProvider == null) {
                tezosCryptoProvider = new TezosCryptoProviderImpl();
                ((TezosCryptoProviderImpl)tezosCryptoProvider).setSecureRandom(new SecureRandom());
                ((TezosCryptoProviderImpl)tezosCryptoProvider).init();
            }

            return tezosCryptoProvider;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving TezosCryptoProvider: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    public synchronized TezosResourceLoader getTezosResourceLoader () throws TezosException {

        try {
            if(tezosResourceLoader == null) {
                tezosResourceLoader = new SimpleTezosResourceLoader();
            }

            return tezosResourceLoader;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving TezosResourceLoader: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public String toString () {
        return "TezosContractProxyFactoryContextImpl";
    }
}
