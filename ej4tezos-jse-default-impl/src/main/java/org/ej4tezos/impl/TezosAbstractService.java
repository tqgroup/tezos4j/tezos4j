// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.net.URL;

import java.util.Map;
import java.util.Collections;

import java.io.InputStream;

import java.text.MessageFormat;

import java.nio.charset.Charset;

import javax.inject.Inject;

import javax.annotation.PreDestroy;
import javax.annotation.PostConstruct;

import org.ej4tezos.model.TezosIdentity;
import org.ej4tezos.papi.TezosResourceLoader;
import org.json.JSONArray;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.io.IOUtils;

import org.apache.commons.lang3.StringUtils;

import org.ej4tezos.api.model.*;

import org.ej4tezos.impl.model.*;

import org.ej4tezos.model.TezosContractAddress;

import org.ej4tezos.model.TezosPrivateKey;
import org.ej4tezos.model.TezosPublicAddress;

import static org.ej4tezos.papi.TezosCryptoProvider.EncoderType.HEX;
import static org.ej4tezos.papi.TezosCryptoProvider.EncoderType.BASE58;

import static org.ej4tezos.utils.asserts.Asserts.checkState;
import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

import org.ej4tezos.api.TezosKeyService;
import org.ej4tezos.api.TezosFeeService;
import org.ej4tezos.api.TezosCoreService;

import org.ej4tezos.api.exception.TezosException;

import org.ej4tezos.papi.TezosConnectivity;
import org.ej4tezos.papi.TezosCryptoProvider;

import static org.ej4tezos.utils.bytes.ByteToolbox.join;
import static org.ej4tezos.utils.bytes.ByteToolbox.toUnsignedInt;

import static org.ej4tezos.utils.string.StringToolbox.*;

import static org.ej4tezos.model.TezosConstants.EDSIG_PREFIX;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public abstract class TezosAbstractService {

    private static final Logger LOG = LoggerFactory.getLogger(TezosAbstractService.class);

    private static final String INVOCATION = "invocation";
    private static final String ORIGINATION = "origination";
    private static final String TRANSFER = "transfer";
    private static final String DELEGATION = "delegation";
    private static final String BALLOT = "ballot";

    private static final String INJECT_OPERATION_RESULT = "injectOperationResult";
    private static final String PRE_APPLY_OPERATION_RESULT = "preApplyOperationResult";

    private static final String FEE = "${fee}";
    private static final String SOURCE = "${source}";
    private static final String COUNTER = "${counter}";
    private static final String CHAIN_ID = "${chainId}";
    private static final String GAS_LIMIT = "${gasLimit}";
    private static final String SIGNATURE = "${signature}";
    private static final String BLOCK_HASH = "${blockHash}";
    private static final String TEZOS_PROTOCOL = "${protocol}";
    private static final String STORAGE_LIMIT = "${storageLimit}";
    private static final String TEZOS_CONTRACT_ADDRESS = "${tezosContractAddress}";

    private static final String DUMMY_SIGNATURE = "edsigtkKBqNqYu4cuzJGKknmQ94t61Mp3QvZmhvU5e9wtWi8yWX9ZRJgTEcnbaUfi6BBnZuVuM4haVtKaHNTJLmduJA9CFjpq4e";

    @Inject
    private TezosIdentity adminIdentity;
    @Inject
    private TezosPrivateKey adminPrivateKey;
    @Inject
    private TezosKeyService tezosKeyService;
    @Inject
    private TezosFeeService tezosFeeService;
    @Inject
    private TezosCoreService tezosCoreService;
    @Inject
    private TezosConnectivity tezosConnectivity;
    @Inject
    private TezosCryptoProvider tezosCryptoProvider;
    @Inject
    public TezosContractAddress tezosContractAddress;
    @Inject
    private TezosResourceLoader tezosResourceLoader;

    private TezosChainId chainId;
    private TezosProtocol protocol;
    private TezosPublicAddress adminPublicAddress;

    public TezosAbstractService() {
        LOG.info("A new instance of 'TezosAbstractService' got created; logging on {}...", LOG.getName());
    }

    @PostConstruct
    public void init () throws Exception {

        LOG.info("init ()");

        // Assert the parameters:
        checkNotNull(adminPrivateKey != null ? adminPrivateKey : adminIdentity,
                "adminPrivateKey or adminIdentity should not be null", Exception.class);
        checkNotNull(tezosKeyService, "tezosFeeService should not be null", Exception.class);
        checkNotNull(tezosFeeService, "tezosFeeService should not be null", Exception.class);
        checkNotNull(tezosCoreService, "tezosCoreService should not be null", Exception.class);
        checkNotNull(tezosConnectivity, "tezosConnectivity should not be null", Exception.class);
        checkNotNull(tezosCryptoProvider, "tezosCryptoProvider should not be null", Exception.class);
        checkNotNull(tezosResourceLoader, "tezosResourceLoader should not be null", Exception.class);

        // Retrieve some chain details:
        chainId = tezosCoreService.getChainId();
        protocol = tezosCoreService.getProtocols().getCurrent();

        // Retrieve the admin public address:
        if(adminIdentity != null) {
            adminPublicAddress = adminIdentity.getPublicAddress();
        }
        else {
            adminPublicAddress = tezosKeyService.getPublicAddress(adminPrivateKey);
        }

        // Display configuration:
        LOG.info("chainId = {}", chainId);
        LOG.info("protocol = {}", protocol);
        LOG.info("adminPrivateKey = {}", mask(adminPrivateKey));
        LOG.info("tezosKeyService = {}", tezosKeyService);
        LOG.info("tezosFeeService = {}", tezosFeeService);
        LOG.info("tezosCoreService = {}", tezosCoreService);
        LOG.info("tezosConnectivity = {}", tezosConnectivity);
        LOG.info("tezosCryptoProvider = {}", tezosCryptoProvider);

    }

    @PreDestroy
    public void destroy () throws Exception {

        LOG.info("destroy ()");

    }

    protected InvokeOperationResult invoke (String runTemplate, String forgeTemplate, String preApplyTemplate) throws TezosException {

        // Log the call:
        LOG.debug("invoke (runTemplate = {}, forgeTemplate = {}, preApplyTemplate = {})",
                runTemplate, forgeTemplate, preApplyTemplate);

        // Delegate the call:
        return invoke(runTemplate, forgeTemplate, preApplyTemplate, Collections.emptyMap());
    }

    protected InvokeOperationResult invoke (String runTemplate, String forgeTemplate, String preApplyTemplate, Map<String,Object> parameters) throws TezosException {

        JSONObject processOperationResult = processOperation(runTemplate, forgeTemplate, preApplyTemplate, parameters, INVOCATION);

        InjectOperationResult injectOperationResult = (InjectOperationResult) processOperationResult.get(INJECT_OPERATION_RESULT);
        PreApplyOperationResult preApplyOperationResult = (PreApplyOperationResult) processOperationResult.get("preApplyOperationResult");

        InvokeOperationResult invokeOperationResult = InvokeOperationResult.toInjectOperationResult(injectOperationResult, preApplyOperationResult);
        LOG.debug("invokeOperationResult = {}", invokeOperationResult);

        return invokeOperationResult;
    }

    protected JSONObject processOperation (String runTemplate, String forgeTemplate, String preApplyTemplate, Map<String, Object> parameters, String type) throws TezosException {

        // Sanity check:
        checkNotNull(parameters, "parameters should not be null", TezosException.class);
        checkNotNull(runTemplate, "runTemplate should not be null", TezosException.class);
        checkNotNull(forgeTemplate, "forgeTemplate should not be null", TezosException.class);
        checkNotNull(preApplyTemplate, "preApplyTemplate should not be null", TezosException.class);

        try {

            JSONObject processingResult = new JSONObject();

            //
            RunOperationDetails runOperationDetails
                = RunOperationDetails
                    .toRunOperationDetails();

            addParameters(runOperationDetails, parameters);

            //
            LOG.debug("process runOperation...");
            RunOperationResult runOperationResult = runOperation(runTemplate, runOperationDetails, type);
            LOG.debug("runOperationResult = {}", runOperationResult);

            //
            if (runOperationResult.getStatus() != RunOperationResult.Status.APPLIED) {
                String message = MessageFormat.format("Operation cannot be ran (status = {0})", runOperationResult.getStatus());
                throw new TezosException(message);
            }

            //
            LOG.debug("process forgeOperation...");
            ForgeOperationDetails forgeOperationDetails
                = ForgeOperationDetails
                    .toForgeOperationDetails(runOperationResult.getCounter(),
                                             runOperationResult.getBlockHash(),
                                             runOperationResult.getConsumedGas() + 20000,
                                             runOperationResult.getStorageSize() + 500,
                                             runOperationResult.getConsumedGas());

            addParameters(forgeOperationDetails, parameters);

            //
            ForgeOperationResult forgeOperationResult = forgeOperation(forgeTemplate, forgeOperationDetails, type);
            LOG.debug("forgeOperationResult = {}", forgeOperationResult);

            //
            LOG.debug("process signOperation...");
            SignOperationDetails signOperationDetails
                = SignOperationDetails
                    .toSignOperationDetails(forgeOperationResult.getForgedData(), adminPrivateKey);

            //
            SignOperationResult signOperationResult = signOperation(signOperationDetails);
            LOG.debug("signOperationResult = {}", signOperationResult);

            //
            PreApplyOperationDetails preApplyOperationDetails
                = PreApplyOperationDetails
                    .toPreApplyOperationDetails(getProtocol(),
                                                runOperationResult.getCounter(),
                                                runOperationResult.getBlockHash(),
                                                forgeOperationResult.getFee(),
                                                runOperationResult.getConsumedGas() + 20000,
                                                runOperationResult.getStorageSize() + 500,
                                                signOperationResult.getB58EncodedSignature());

            addParameters(preApplyOperationDetails, parameters);

            //
            PreApplyOperationResult preApplyOperationResult = preApplyOperation(preApplyTemplate, preApplyOperationDetails, type);
            LOG.debug("preApplyOperationResult = {}", preApplyOperationResult);

            //
            InjectOperationDetails injectOperationDetails
                = InjectOperationDetails
                    .toInjectOperationDetails(forgeOperationResult.getForgedData(),
                                              signOperationResult.getSignatureAsHex());

            //
            InjectOperationResult injectOperationResult = injectOperation(injectOperationDetails);
            LOG.debug("injectOperationResult = {}", injectOperationResult);

            processingResult.put(INJECT_OPERATION_RESULT, injectOperationResult);
            processingResult.put(PRE_APPLY_OPERATION_RESULT, preApplyOperationResult);

            return processingResult;
        }

        catch (Exception ex) {
            String exceptionMessage = MessageFormat.format("An exception occurred while executing 'processOperation': {0}", ex.getMessage());
            throw new TezosException(exceptionMessage, ex);
        }
    }

    private RunOperationResult runOperation (String template, RunOperationDetails details, String type) throws TezosException {

        //
        RunOperationResult result = null;

        // Log the call:
        LOG.debug("runOperation (template = {}, details = {})", template, details);

        // Sanity check:
        checkNotNull(details, "details should not have been null", TezosException.class);
        checkNotNull(template, "template should not have been null", TezosException.class);

        try {

            //
            TezosCounter counter
                = tezosCoreService
                    .getCounter(adminPublicAddress).increment();

            //
            TezosBlockHash blockHash
                = tezosCoreService
                    .getBlockHash();

            // Prepare the body to post:
            String bodyToPost = runOperationBodyToPost(template, counter, blockHash, type);

            //
            LOG.debug("bodyToPost = {}", bodyToPost);

            //
            for (String parameter : details.getParameters()) {
                LOG.debug("parameter = {}", parameter);
                bodyToPost = bodyToPost.replace("${" + parameter + "}", details.getParameter(parameter).toString());
            }

            //
            URL url = tezosConnectivity.toURL("/chains/main/blocks/head/helpers/scripts/run_operation");
            JSONObject postResult = tezosConnectivity.executeJsonPost(url, new JSONObject(bodyToPost));
            LOG.debug("postResult = {}", postResult);

            JSONObject operationResult
                = postResult
                    .getJSONArray("contents")
                    .getJSONObject(0)
                    .getJSONObject("metadata")
                    .getJSONObject("operation_result");

            //
            String statusAsString
                = operationResult
                    .getString("status");

            if (statusAsString.equals("applied")) {

                String consumedGasAsString
                    = operationResult
                        .getString("consumed_gas");

                String storageSizeAsString
                    = operationResult
                        .getString("storage_size");

                // Prepare the result:
                result
                    = RunOperationResult
                        .toRunOperationResult(counter, blockHash, statusAsString, consumedGasAsString, storageSizeAsString);
            }
            else {

                String errorMessage
                    = operationResult
                        .getJSONArray("errors").getJSONObject(1).getJSONObject("with").getString("string");

                // Prepare the result:
                result
                    = RunOperationResult
                        .toRunOperationResult(counter, blockHash, statusAsString, errorMessage);
            }

            // Log and return the result:
            LOG.debug("result = {}", result);
            return result;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while executing runOperation: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    private ForgeOperationResult forgeOperation (String template, ForgeOperationDetails details, String type) throws TezosException {

        // Log the call:
        LOG.debug("forgeOperation (template = {}, details = {}, type = {})", template, details, type);

        // Sanity check:
        checkNotNull(details, "details should not be null", TezosException.class);

        try {

            // Prepare the intermediate template:
            String intermediateTemplate = forgeOperationBodyToPost(template, details.getCounter(), details.getBlockHash(), details.getGasLimit(), details.getStorageLimit(), type);

            //
            for (String parameter : details.getParameters()) {
                LOG.debug("parameter = {}", parameter);
                intermediateTemplate = intermediateTemplate.replace("${" + parameter + "}", details.getParameter(parameter).toString());
            }

            // Prepare the first body to post:
            String bodyToPost
                = intermediateTemplate
                    .replace(FEE, "0");

            //
            LOG.debug("bodyToPost = {}", bodyToPost);

            //
            URL url = tezosConnectivity.toURL("/chains/main/blocks/head/helpers/forge/operations");

            //
            String postResult = tezosConnectivity.executePost(url, new JSONObject(bodyToPost));
            postResult = StringUtils.removeIgnoreCase(postResult, "\"");
            LOG.debug("postResult(1) = {}", postResult);

            //
            TezosFee tezosFee
                = tezosFeeService.computeFee(postResult.length() / 2, details.getConsumedGas());

            // Prepare the second body to post:
            bodyToPost
                = intermediateTemplate
                    .replace(FEE, tezosFee.toString())
            ;

            //
            postResult = tezosConnectivity.executePost(url, new JSONObject(bodyToPost));
            postResult = StringUtils.removeIgnoreCase(postResult, "\"");
            postResult = "0x03" + postResult;
            LOG.debug("postResult(2) = {}", postResult);

            //
            ForgeOperationResult result
                = ForgeOperationResult.toForgeOperationResult(postResult, tezosFee);

            // Log and return the result:
            LOG.debug("result = {}", result);
            return result;
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while executing forgeOperation: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    private SignOperationResult signOperation (SignOperationDetails details) throws TezosException {

        // Log the call:
        LOG.debug("signOperation (details = {})", details);

        // Sanity check:
        checkNotNull(details, "details should not be null", TezosException.class);
        checkNotNull(details.getData(), "details.data should not be null", TezosException.class);
        checkNotNull(details.getSignerPrivateKey(), "details signer private key should not be null", TezosException.class);

        // More sanity check:
        checkState(details.getSignerPrivateKey().isValid(), "details private key is not valid", TezosException.class);
        checkState(details.getData().toLowerCase().startsWith("0x03"), "Invalid data prefix", TezosException.class);

        try {

            // Extract the forged data:
            byte[] rawForgedData = tezosCryptoProvider.decode(details.getData().substring(2), HEX);
            LOG.debug("rawForgedData = {}", toString(rawForgedData));

            // Compute the forged data hash:
            byte[] rawForgedDataHash = tezosCryptoProvider.getBlake2B256Digester().digest(rawForgedData);
            LOG.debug("rawForgedDataHash = {} (HEX={})", toString(rawForgedDataHash), "0x" + tezosCryptoProvider.encode(rawForgedDataHash, HEX));

            // Generate the signature:
            byte[] signature;
            synchronized (tezosCryptoProvider) {
                signature = tezosCryptoProvider.createSignature(rawForgedDataHash, details.getSignerPrivateKey());
            }

            //
            LOG.debug("signature = {}", toString(signature));
            LOG.debug("signature.length = {}", signature.length);

            //
            String rawSignatureAsHex = "0x" + tezosCryptoProvider.encode(signature, HEX);
            LOG.debug("signature = {}", rawSignatureAsHex);

            // We group the signature with its prefix and compute the double checksum:
            byte[] signatureWithPrefix = join(EDSIG_PREFIX, signature);
            byte[] signatureWithPrefixChecksum = tezosCryptoProvider.computeDoubleCheckSum(signatureWithPrefix);

            // We join the prefixed signature and the checksum:
            byte[] signatureWithPrefixAndChecksum = join(signatureWithPrefix, signatureWithPrefixChecksum);

            // We encode the all stuff with B58:
            String encodedSignature = tezosCryptoProvider.encode(signatureWithPrefixAndChecksum, BASE58);
            LOG.debug("encodedSignature = {}", encodedSignature);

            //
            SignOperationResult result
                = SignOperationResult.toSignOperationResult(encodedSignature, rawSignatureAsHex);

            // Log and return the result:
            LOG.debug("result = {}", result);
            return result;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while executing signOperation: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    private PreApplyOperationResult preApplyOperation (String template, PreApplyOperationDetails details, String type) throws TezosException {

        // Log the call:
        LOG.debug("preApplyOperation (template = {}, details = {}, type = {})", template, details, type);

        // Sanity check:
        checkNotNull(details, "details should not be null", TezosException.class);
        // ... more checks

        try {

            // Prepare the body to post:
            String bodyToPost = preApplyOperationBodyToPost(template, details.getCounter(), details.getBlockHash(), details.getGasLimit(), details.getStorageLimit(), details.getSignature(), details.getFee(), details.getProtocol(), type);

            for (String parameter : details.getParameters()) {
                LOG.debug("parameter = {}", parameter);
                bodyToPost = bodyToPost.replace("${" + parameter + "}", details.getParameter(parameter).toString());
            }

            //
            LOG.debug("bodyToPost = {}", bodyToPost);

            //
            URL url = tezosConnectivity.toURL("/chains/main/blocks/head/helpers/preapply/operations");

            //
            JSONArray postResult = tezosConnectivity.executeJsonArrayPost(url, new JSONArray(bodyToPost));
            LOG.debug("postResult = {}", postResult);

            //
            PreApplyOperationResult result
                = PreApplyOperationResult
                    .toPreApplyOperationResult(
                        postResult
                            .getJSONObject(0)
                            .getJSONArray("contents")
                            .getJSONObject(0)
                            .getJSONObject("metadata")
                            .getJSONObject("operation_result")
                            .put("blockHash", details.getBlockHash())
                    );

            // Log and return the result:
            LOG.debug("result = {}", result);
            LOG.debug("result.status = {}", result.getStatus());
            return result;
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while executing preApplyOperation: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    private InjectOperationResult injectOperation (InjectOperationDetails details) throws TezosException {

        // Log the call:
        LOG.debug("injectOperation (details = {})", details);

        // Sanity check:
        checkNotNull(details, "details should not be null", TezosException.class);
        checkNotNull(details.getData(), "details data should not be null", TezosException.class);
        // ... more checks

        try {

            //
            URL url = tezosConnectivity.toURL("/injection/operation?chain=main");

            //
            String postResult = tezosConnectivity.executePost(url, "\"" + details.getData() + "\"", TezosConnectivity.CONTENT_APPLICATION_JSON);
            LOG.debug("postResult = {}", postResult);

            // Remove double quotes:
            postResult = postResult.substring(1, postResult.length() - 2);

            //
            InjectOperationResult result
                    = InjectOperationResult.toInjectOperationResult(TezosTransactionHash.toTezosTransactionHash(postResult));

            // Log and return the result:
            LOG.debug("result = {}", result);
            return result;
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while executing injectOperation: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    private String toString (byte[] data) {

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < data.length; i++) {
            builder.append(toUnsignedInt(data[i]));
            builder.append(", ");
        }

        if (builder.length() > 2) {
            builder.deleteCharAt(builder.length() - 1);
            builder.deleteCharAt(builder.length() - 1);
        }

        return builder.toString();
    }

    protected String loadTemplate (String path) throws TezosException {

        //
        LOG.debug("loadTemplate (path = {})", path);

        // Sanity check:
        checkNotNull(path, "path should not be null", TezosException.class);

        // Load the template:
        try (InputStream inputStream = this.tezosResourceLoader.getResourceAsStream(path)) {

            if (inputStream == null) {
                String message = MessageFormat.format("No resource found at this location: {0}", path);
                throw new TezosException(message);
            }

            // Load the template
            String template = IOUtils.toString(inputStream, Charset.defaultCharset());

            //
            LOG.debug("template = {}", template);
            return template;
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while loading a template: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    private String runOperationBodyToPost (String template, TezosCounter counter, TezosBlockHash blockHash, String type) throws TezosException {

        String bodyToPost
            = template
                .replace(SOURCE, adminPublicAddress.toString())
                .replace(CHAIN_ID, chainId.getValue())
                .replace(COUNTER, counter.toString())
                .replace(BLOCK_HASH, blockHash.getValue())
                // In the case of the run operation, the signature is required but not validated so we put anything that
                // is syntactically correct:
                .replace(SIGNATURE, DUMMY_SIGNATURE);

        switch (type) {

            case INVOCATION:

                bodyToPost
                    = bodyToPost
                        .replace(TEZOS_CONTRACT_ADDRESS, tezosContractAddress.toString());

                LOG.debug("bodyToPost = {}", bodyToPost);

                return bodyToPost;

            //
            case ORIGINATION:

                LOG.debug("bodyToPost = {}", bodyToPost);
                return bodyToPost;

            // case TRANSFER:
            // case BALLOT:
            // case DELEGATION:

            default:
                String message = MessageFormat.format("Unexpected type encountered in 'runOperationBodyToPost': {0}", type);
                throw new TezosException(message);
        }

    }

    private String forgeOperationBodyToPost (String template, TezosCounter counter, TezosBlockHash blockHash, long gasLimit, long storageLimit, String type) throws TezosException {

        String bodyToPost
            = template
                .replace(BLOCK_HASH, blockHash.toString())
                .replace(SOURCE, adminPublicAddress.toString())
                .replace(COUNTER, counter.toString())
                .replace(GAS_LIMIT, Long.toString(gasLimit))
                .replace(STORAGE_LIMIT, Long.toString(storageLimit));

        switch (type) {

            case INVOCATION:

                bodyToPost
                    = bodyToPost
                        .replace(TEZOS_CONTRACT_ADDRESS, tezosContractAddress.toString());

                LOG.debug("bodyToPost = {}", bodyToPost);

                return bodyToPost;

            //
            case ORIGINATION:

                LOG.debug("bodyToPost = {}", bodyToPost);

                return bodyToPost;

            // case TRANSFER:
            // case BALLOT:
            // case DELEGATION:

            default:
                String message = MessageFormat.format("Unexpected type encountered in 'forgeOperationBodyToPost': {0}", type);
                throw new TezosException(message);
        }

    }

    private String preApplyOperationBodyToPost (String template, TezosCounter counter, TezosBlockHash blockHash, long gasLimit, long storageLimit, String signature, TezosFee fee, TezosProtocol protocol, String type) throws TezosException {

        String bodyToPost
            = template
                .replace(FEE, fee.toString())
                .replace(TEZOS_PROTOCOL, protocol.toString())
                .replace(BLOCK_HASH, blockHash.toString())
                .replace(SOURCE, adminPublicAddress.toString())
                .replace(COUNTER, counter.toString())
                .replace(GAS_LIMIT, Long.toString(gasLimit))
                .replace(STORAGE_LIMIT, Long.toString(storageLimit))
                .replace(SIGNATURE, signature);

        switch (type) {

            case INVOCATION:

                bodyToPost
                    = bodyToPost
                        .replace(TEZOS_CONTRACT_ADDRESS, tezosContractAddress.toString());

                LOG.debug("bodyToPost = {}", bodyToPost);

                return bodyToPost;

            //
            case ORIGINATION:

                LOG.debug("bodyToPost = {}", bodyToPost);

                return bodyToPost;

            // case TRANSFER:
            // case BALLOT:
            // case DELEGATION:

            default:
                String message = MessageFormat.format("Unexpected type encountered in 'preApplyOperationBodyToPost': {}", type);
                throw new TezosException(message);
        }
    }

    protected TezosProtocol getProtocol () {
        return protocol;
    }

    private void addParameters (AbstractOperationDataWithParameters target, Map<String, Object> parameters) throws TezosException {

        for (String key : parameters.keySet()) {
            target.setParameter(key, parameters.get(key));
        }
    }

    public void setAdminPrivateKey (TezosPrivateKey adminPrivateKey) {
        LOG.info("setAdminPrivateKey (adminPrivateKey = {})", adminPrivateKey);
        this.adminPrivateKey = adminPrivateKey;
    }

    public void setAdminIdentity (TezosIdentity adminIdentity) {
        LOG.info("setAdminIdentity (adminIdentity = {})", adminIdentity);
        this.adminIdentity = adminIdentity;
    }

    public void setTezosFeeService (TezosFeeService tezosFeeService) {
        LOG.info("setTezosFeeService (tezosFeeService = {})", tezosFeeService);
        this.tezosFeeService = tezosFeeService;
    }

    public void setTezosKeyService (TezosKeyService tezosKeyService) {
        LOG.info("setTezosKeyService (tezosKeyService = {})", tezosKeyService);
        this.tezosKeyService = tezosKeyService;
    }

    public void setTezosCoreService (TezosCoreService tezosCoreService) {
        LOG.info("setTezosCoreService (tezosCoreService = {})", tezosCoreService);
        this.tezosCoreService = tezosCoreService;
    }

    public TezosCoreService getTezosCoreService () {
        return tezosCoreService;
    }

    public void setTezosConnectivity (TezosConnectivity tezosConnectivity) {
        LOG.info("setTezosConnectivity (tezosConnectivity = {})", tezosConnectivity);
        this.tezosConnectivity = tezosConnectivity;
    }

    public void setTezosCryptoProvider (TezosCryptoProvider tezosCryptoProvider) {
        LOG.info("setTezosCryptoProvider (tezosCryptoProvider = {})", tezosCryptoProvider);
        this.tezosCryptoProvider = tezosCryptoProvider;
    }

    public TezosContractAddress getTezosContractAddress () {
        return tezosContractAddress;
    }

    public void setTezosContractAddress (TezosContractAddress tezosContractAddress) {
        LOG.info("setTezosContractAddress (tezosContractAddress = {})", tezosContractAddress);
        this.tezosContractAddress = tezosContractAddress;
    }

    public TezosResourceLoader getTezosResourceLoader () {
        return tezosResourceLoader;
    }

    public void setTezosResourceLoader (TezosResourceLoader tezosResourceLoader) {
        LOG.info("setTezosResourceLoader (tezosResourceLoader = {})", tezosResourceLoader);
        this.tezosResourceLoader = tezosResourceLoader;
    }

    @Override
    public String toString () {
        return "TezosAbstractService";
    }
}