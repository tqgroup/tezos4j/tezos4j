// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.api.exception.TezosException;

import org.ej4tezos.papi.TezosInvocationBuilder;

import static org.ej4tezos.papi.model.MichelineConstants.*;

import static org.ej4tezos.utils.micheline.MichelineParser.*;
import static org.ej4tezos.utils.micheline.MichelineScanner.*;
import static org.ej4tezos.utils.micheline.MichelsonMessageBuilder.*;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public class TezosInvocationBuilderImpl implements TezosInvocationBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(TezosInvocationBuilderImpl.class);

    private static final String UNEXPECTED_TOKEN = "Unexpected Token";

    public TezosInvocationBuilderImpl () {
        LOG.info("A new instance of TezosInvocationBuilderImpl got created; logging on {}", LOG.getName());
    }

    public static Object buildMichelineExpression (Object ... expression) throws TezosException {
        // Convert expression to a List
        List<Object> expressionList = new ArrayList<>(Arrays.asList(expression));

        String michelsonMessage;
        Object value;

        // Handle entryPoints with no parameter
        if (expressionList.isEmpty()) {
            // michelsonMessage is always 'Unit' for such entryPoints
            michelsonMessage = UNIT;
            value = getNoParamEntryPointDefinition(michelsonMessage);
        }
        // Handle entryPoints with a single parameter
        else if (expressionList.size() == 1) {
            michelsonMessage = buildMichelsonInvocationSingleParam(expressionList);
            // Parse Michelson Message
            List<JSONObject> parsedSRC = scan(michelsonMessage);
            // build Micheline Message
            value = parseExpr(parsedSRC, parsedSRC.get(0));
        }
        // Handle entryPoints with multiple parameters
        else {
            michelsonMessage = buildMichelsonInvocationMessage(null, expressionList);
            // Parse Michelson Message
            List<JSONObject> parsedSRC = scan(michelsonMessage);
            // build Micheline Message
            value = parseExpr(parsedSRC, parsedSRC.get(0));
        }
        return value;
    }

    @Override
    public JSONObject buildInvocationParameters (String entryPointName, Object ... invocationValues) throws TezosException {
        // Convert parameters to a List
        List<Object> invocationValuesList = new ArrayList<>(Arrays.asList(invocationValues));

        String michelsonMessage;
        Object value;

        // Handle entryPoints with no parameter
        if (invocationValuesList.isEmpty()) {
            // michelsonMessage is always 'Unit' for such entryPoints
            michelsonMessage = UNIT;
            value = getNoParamEntryPointDefinition(michelsonMessage);
        }
        // Handle entryPoints with a single parameter
        else if (invocationValuesList.size() == 1) {
            michelsonMessage = buildMichelsonInvocationSingleParam(invocationValuesList);
            // Parse Michelson Message
            List<JSONObject> parsedSRC = scan(michelsonMessage);
            // build Micheline Message
            value = parseExpr(parsedSRC, parsedSRC.get(0));
        }
        // Handle entryPoints with multiple parameters
        else {
            michelsonMessage = buildMichelsonInvocationMessage(null, invocationValuesList);
            // Parse Michelson Message
            List<JSONObject> parsedSRC = scan(michelsonMessage);
            // build Micheline Message
            value = parseExpr(parsedSRC, parsedSRC.get(0));
        }

        JSONObject parameters = new JSONObject();

        parameters.put(ENTRY_POINT, entryPointName);
        parameters.put(VALUE, value);

        // Log & return
        LOG.debug("michelsonMessage = {}", michelsonMessage);
        LOG.debug("parameters = {}", parameters);
        return parameters;
    }

    private static JSONObject getNoParamEntryPointDefinition (String michelsonMessage) {
        return new JSONObject().put(PRIM, michelsonMessage);
    }
}