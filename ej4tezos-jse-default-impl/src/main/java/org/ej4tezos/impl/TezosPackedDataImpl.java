package org.ej4tezos.impl;

import org.ej4tezos.api.exception.TezosException;
import org.ej4tezos.api.model.TezosPackedData;
import org.ej4tezos.papi.TezosCryptoProvider;
import org.ej4tezos.utils.bytes.ByteToolbox;

import java.util.Arrays;

public class TezosPackedDataImpl implements TezosPackedData {
    private static final byte[] PREFIX = {13, 44, 64, 27};

    private final TezosCryptoProvider tezosCryptoProvider;
    private final byte[] rawPackedData;

    private byte[] rawExpressionIdHash;
    private String ledgerBlake2Hash;
    private String expressionIdHash;
    private byte[] rawSha256Hash;
    private Integer hashCode;

    public TezosPackedDataImpl(TezosCryptoProvider tezosCryptoProvider, String packedData) throws TezosException {
        this.tezosCryptoProvider = tezosCryptoProvider;
        rawPackedData = tezosCryptoProvider.decode(packedData, TezosCryptoProvider.EncoderType.HEX);
    }

    @Override
    public byte[] getRawPackedData() {
        return rawPackedData;
    }

    @Override
    public String getExpressionIdHash() throws TezosException {
        String expressionIdHash = this.expressionIdHash;
        if (expressionIdHash == null) {
            byte[] rawExpressionId = getRawExpressionIdHash();
            byte[] prefixedRawExpressionId = ByteToolbox.join(PREFIX, rawExpressionId);
            byte[] expressionId = ByteToolbox.join(prefixedRawExpressionId, getChecksum(prefixedRawExpressionId));
            expressionIdHash = tezosCryptoProvider.encode(expressionId, TezosCryptoProvider.EncoderType.BASE58);
            this.expressionIdHash = expressionIdHash;
        }
        return expressionIdHash;
    }

    public byte[] getRawExpressionIdHash() throws TezosException {
        byte[] rawExpressionIdHash = this.rawExpressionIdHash;
        if (rawExpressionIdHash == null) {
            rawExpressionIdHash = tezosCryptoProvider.getBlake2B256Digester().digest(rawPackedData);
            this.rawExpressionIdHash = rawExpressionIdHash;
        }
        return rawExpressionIdHash;
    }

    @Override
    public String getLedgerBlake2bHash() throws TezosException {
        String ledgerBlake2Hash = this.ledgerBlake2Hash;
        if (ledgerBlake2Hash == null) {
            byte[] rawBlake2bHash = getRawExpressionIdHash();
            ledgerBlake2Hash = tezosCryptoProvider.encode(rawBlake2bHash, TezosCryptoProvider.EncoderType.BASE58);
            this.ledgerBlake2Hash = ledgerBlake2Hash;
        }
        return ledgerBlake2Hash;
    }

    @Override
    public byte[] getRawSha256Hash() throws TezosException {
        byte[] rawSha256Hash = this.rawSha256Hash;
        if (rawSha256Hash == null) {
            rawSha256Hash = tezosCryptoProvider.getSha256digester().digest(getRawPackedData());
            this.rawSha256Hash = rawSha256Hash;
        }
        return rawSha256Hash;
    }

    @Override
    public byte[] getRawSha512Hash() throws TezosException {
        throw new UnsupportedOperationException();
    }

    private byte[] getChecksum(byte[] arr) throws TezosException {
        // sha256(sha256(v).digest()).digest()[:4]
        byte[] doubleSha256 = tezosCryptoProvider.getSha256digester().digest(
                tezosCryptoProvider.getSha256digester().digest(arr));
        return Arrays.copyOfRange(doubleSha256, 0, 4);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TezosPackedData)) return false;
        TezosPackedData that = (TezosPackedData) o;
        return Arrays.equals(rawPackedData, that.getRawPackedData());
    }

    @Override
    public int hashCode() {
        Integer hashCode = this.hashCode;
        if (hashCode == null) {
            hashCode = Arrays.hashCode(rawPackedData);
            this.hashCode = hashCode;
        }
        return hashCode;
    }
}
