// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.api.model.TezosBlockHash;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.model.TezosContractAddress;
import static org.ej4tezos.model.TezosContractAddress.toTezosContractAddress;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class PreApplyOperationResult extends AbstractOperationData {

    private static Logger LOG = LoggerFactory.getLogger(PreApplyOperationResult.class);

    enum Status { APPLIED, UNKNOWN }

    private JSONObject operationResult;

    public PreApplyOperationResult () {
        super();
    }

    public Status getStatus () {

        // Log the call:
        LOG.debug("getStatus ()");

        try {
            String status = operationResult.getString("status");
            LOG.debug("status = {}", status);
            return Status.valueOf(status.trim().toUpperCase());
        }

        catch (Exception ex) {
            LOG.warn("Cannot determine the status of the pre-apply result: {}", ex.getMessage());
            return Status.UNKNOWN;
        }
    }

    public TezosBlockHash getBlockHash () {
        // Log the call:
        LOG.debug("getBlockHash ()");
        TezosBlockHash blockHash = (TezosBlockHash) operationResult.get("blockHash");
        LOG.debug("blockHash = {}", blockHash);
        return blockHash;
    }

    public TezosContractAddress getOriginatedContractAddress () {

        // Log the call:
        LOG.debug("getOriginatedContractAddress ()");

        try {
            TezosContractAddress originatedContractAddress = toTezosContractAddress(operationResult.getJSONArray("originated_contracts").getString(0));
            LOG.debug("originatedContractAddress = {}", originatedContractAddress);
            return originatedContractAddress;
        }

        catch (Exception ex) {
            LOG.warn("Cannot determine the address of the originated contract: {}", ex.getMessage());
            return null;
        }
    }

    public static PreApplyOperationResult toPreApplyOperationResult(JSONObject operationResult) {
        PreApplyOperationResult preApplyOperationResult = new PreApplyOperationResult();
        preApplyOperationResult.operationResult = operationResult;
        return preApplyOperationResult;
    }
}