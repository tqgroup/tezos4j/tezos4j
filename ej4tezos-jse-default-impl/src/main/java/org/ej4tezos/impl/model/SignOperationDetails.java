// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

import org.ej4tezos.model.TezosIdentity;
import org.ej4tezos.model.TezosPrivateKey;

import org.ej4tezos.api.exception.TezosException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class SignOperationDetails extends AbstractOperationData {

    private String data;
    private TezosPrivateKey signerPrivateKey;

    public SignOperationDetails () {
        super();
    }

    public String getData () {
        return data;
    }

    public TezosPrivateKey getSignerPrivateKey () {
        return signerPrivateKey;
    }

    public static SignOperationDetails toSignOperationDetails (String data, TezosIdentity signerIdentity) throws TezosException {

        // Sanity check:
        checkNotNull(data, "data should not be null", TezosException.class);
        checkNotNull(signerIdentity, "signerIdentity should not be null", TezosException.class);

        // Delegate call:
        return toSignOperationDetails(data, signerIdentity.getPrivateKey());
    }

    public static SignOperationDetails toSignOperationDetails (String data, TezosPrivateKey signerPrivateKey) throws TezosException {

        // Sanity check:
        checkNotNull(data, "data should not be null", TezosException.class);
        checkNotNull(signerPrivateKey, "signerPrivateKey should not be null", TezosException.class);

        // Proceed with the conversion:
        SignOperationDetails signOperationDetails = new SignOperationDetails();
        signOperationDetails.data = data;
        signOperationDetails.signerPrivateKey = signerPrivateKey;
        return signOperationDetails;
    }
}