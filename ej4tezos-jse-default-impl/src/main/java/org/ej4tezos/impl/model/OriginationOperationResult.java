// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.api.model.TezosBlockHash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.model.TezosContractAddress;
import org.ej4tezos.api.model.TezosTransactionHash;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public class OriginationOperationResult extends AbstractOperationData {

    private static Logger LOG = LoggerFactory.getLogger(OriginationOperationResult.class);

    private TezosTransactionHash transactionHash;
    private TezosContractAddress originatedContractAddress;
    private TezosBlockHash originatingBlockHash;

    public OriginationOperationResult () {
        super();
    }

    public TezosTransactionHash getTransactionHash () {
        return transactionHash;
    }

    public TezosContractAddress getOriginatedContractAddress () {
        return originatedContractAddress;
    }

    public TezosBlockHash getOriginatingBlockHash() { return originatingBlockHash; }

    public static OriginationOperationResult toInjectOperationResult (InjectOperationResult injectOperationResult, PreApplyOperationResult preApplyOperationResult) {

        LOG.debug("toInjectOperationResult (injectOperationResult = {})", injectOperationResult);

        OriginationOperationResult originationOperationResult = new OriginationOperationResult();
        originationOperationResult.transactionHash = injectOperationResult.getTransactionHash();
        originationOperationResult.originatedContractAddress = preApplyOperationResult.getOriginatedContractAddress();
        originationOperationResult.originatingBlockHash = preApplyOperationResult.getBlockHash();

        return originationOperationResult;
    }
}