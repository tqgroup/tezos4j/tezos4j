// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.api.model.TezosFee;

import org.ej4tezos.api.exception.TezosException;

import static org.ej4tezos.utils.asserts.Asserts.*;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class ForgeOperationResult extends AbstractOperationData {

    private TezosFee fee;
    private String forgedData;

    public ForgeOperationResult () {
        super();
    }

    public static ForgeOperationResult toForgeOperationResult (String forgedData, TezosFee fee) throws TezosException  {

        checkNotNull(fee, "fee should not be null", TezosException.class);
        checkNotNull(forgedData, "forgedData should not be null", TezosException.class);
        checkState(forgedData.length() > 2, "forgedData should not be empty", TezosException.class);

        ForgeOperationResult result = new ForgeOperationResult();
        result.fee = fee;
        result.forgedData = forgedData.trim();
        return result;
    }

    public TezosFee getFee () {
        return fee;
    }

    public String getForgedData () {
        return forgedData;
    }

}