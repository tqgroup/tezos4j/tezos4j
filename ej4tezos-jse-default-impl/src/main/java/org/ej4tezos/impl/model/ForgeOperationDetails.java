// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.api.model.TezosCounter;
import org.ej4tezos.api.model.TezosBlockHash;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class ForgeOperationDetails extends AbstractOperationDataWithParameters<ForgeOperationDetails> {


    protected long gasLimit;
    protected long consumedGas;
    protected long storageLimit;
    protected TezosCounter counter;
    protected TezosBlockHash blockHash;

    public ForgeOperationDetails () {
        super();
    }

    public long getGasLimit () {
        return gasLimit;
    }

    public TezosCounter getCounter () {
        return counter;
    }

    public long getStorageLimit () {
        return storageLimit;
    }

    public TezosBlockHash getBlockHash () {
        return blockHash;
    }

    public long getConsumedGas () {
        return consumedGas;
    }

    public static ForgeOperationDetails toForgeOperationDetails (TezosCounter counter, TezosBlockHash blockHash, long gasLimit, long storageLimit, long consumedGas) {

        ForgeOperationDetails forgeOperationDetails = new ForgeOperationDetails();

        forgeOperationDetails.counter = counter;
        forgeOperationDetails.gasLimit = gasLimit;
        forgeOperationDetails.blockHash = blockHash;
        forgeOperationDetails.consumedGas = consumedGas;
        forgeOperationDetails.storageLimit = storageLimit;

        return forgeOperationDetails;
    }
}