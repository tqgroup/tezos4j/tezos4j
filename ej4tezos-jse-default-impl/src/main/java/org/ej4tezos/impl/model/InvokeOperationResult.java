// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.api.model.TezosBlockHash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.api.model.TezosTransactionHash;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric HUBIN (frederic.hubin@gmail.com)
 */
public class InvokeOperationResult extends AbstractOperationData {

    private static Logger LOG = LoggerFactory.getLogger(InvokeOperationResult.class);

    private TezosTransactionHash transactionHash;
    private TezosBlockHash blockHash;

    public InvokeOperationResult () {
        super();
    }

    public TezosTransactionHash getTransactionHash () {
        return transactionHash;
    }

    public TezosBlockHash getBlockHash () {
        return blockHash;
    }

    public static InvokeOperationResult toInjectOperationResult (InjectOperationResult injectOperationResult,
                                                                 PreApplyOperationResult preApplyOperationResult) {

        LOG.debug("toInjectOperationResult (injectOperationResult = {})", injectOperationResult);

        InvokeOperationResult invokeOperationResult = new InvokeOperationResult();
        invokeOperationResult.transactionHash = injectOperationResult.getTransactionHash();
        invokeOperationResult.blockHash = preApplyOperationResult.getBlockHash();

        return invokeOperationResult;
    }
}