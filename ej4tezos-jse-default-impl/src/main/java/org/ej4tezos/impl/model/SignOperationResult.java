// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.api.exception.TezosException;

import static org.ej4tezos.utils.asserts.Asserts.*;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class SignOperationResult extends AbstractOperationData {

    private String signatureAsHex;
    private String b58EncodedSignature;

    public SignOperationResult () {
        super();
    }

    public String getB58EncodedSignature () {
        return b58EncodedSignature;
    }

    public String getSignatureAsHex () {
        return signatureAsHex;
    }

    public static SignOperationResult toSignOperationResult (String b58EncodedSignature, String signatureAsHex) throws TezosException {

        // Sanity check:
        checkNotNull(signatureAsHex, "signatureAsHex should not be null", TezosException.class);
        checkNotNull(b58EncodedSignature, "b58EncodedSignature should not be null", TezosException.class);

        // Proceed with the conversion:
        SignOperationResult result = new SignOperationResult();
        result.signatureAsHex = signatureAsHex;
        result.b58EncodedSignature = b58EncodedSignature;
        return result;
    }
}