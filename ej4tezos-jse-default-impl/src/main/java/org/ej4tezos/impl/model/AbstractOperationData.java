// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric HUBIN (frederic.hubin@gmail.com)
 */
public abstract class AbstractOperationData {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractOperationData.class);

    public AbstractOperationData () {
        super();
    }

    @Override
    public String toString () {
        return ReflectionToStringBuilder.toString(this);
    }
}
