// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.api.exception.TezosException;

import static org.ej4tezos.utils.asserts.Asserts.*;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric HUBIN (frederic.hubin@gmail.com)
 */
public abstract class AbstractOperationDataWithParameters<C> extends AbstractOperationData {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractOperationDataWithParameters.class);

    private Map<String,Object> parameters;

    public AbstractOperationDataWithParameters () {
        super();
        parameters = new HashMap<>();
    }

    public Set<String> getParameters () {
        return Collections.unmodifiableSet(parameters.keySet());
    }

    public C setParameter (String key, Object value) throws TezosException {

        //
        LOG.debug("setParameter (key = {}, value = {})", key, value);

        //
        checkNotNull(key, "key should not be null", TezosException.class);

        // Set the values:
        parameters.put(key, value);

        //
        return (C)this;
    }

    public Object getParameter (String key) throws TezosException {

        //
        LOG.debug("getParameter (key = {})", key);

        //
        checkNotNull(key, "key should not be null", TezosException.class);

        //
        Object value = parameters.get(key);
        LOG.debug("value = {}", value);
        return value;
    }

    public <T> T getParameter (String key, Class<T> targetClass) throws TezosException {

        //
        LOG.debug("getParameter (key = {}, targetClass = {})", key, targetClass);

        //
        checkNotNull(key, "key should not be null", TezosException.class);
        checkNotNull(targetClass, "targetClass should not be null", TezosException.class);

        //
        Object value = parameters.get(key);

        //
        if(value != null && !targetClass.isAssignableFrom(value.getClass())) {
            String message = MessageFormat.format("Class {0} is not compatible with class {1}!", value.getClass(), targetClass);
            throw new TezosException(message);
        }

        //
        LOG.debug("value = {}", value);
        return (T)value;
    }

    public boolean hasParameter (String key) throws TezosException {
        //
        LOG.debug("hasParameter (key = {})", key);

        //
        checkNotNull(key, "key should not be null", TezosException.class);

        //
        boolean value = parameters.containsKey(key);
        LOG.debug("value = {}", value);
        return value;
    }
}
