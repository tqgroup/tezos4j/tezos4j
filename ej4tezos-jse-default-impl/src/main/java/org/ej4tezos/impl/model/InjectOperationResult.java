// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.api.model.TezosTransactionHash;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric HUBIN (frederic.hubin@gmail.com)
 */
public class InjectOperationResult extends AbstractOperationData {

    private static Logger LOG = LoggerFactory.getLogger(InjectOperationResult.class);

    private TezosTransactionHash transactionHash;

    public InjectOperationResult() {
        super();
    }

    public TezosTransactionHash getTransactionHash () {
        return transactionHash;
    }

    public static InjectOperationResult toInjectOperationResult (TezosTransactionHash transactionHash) {

        InjectOperationResult injectOperationResult = new InjectOperationResult();
        injectOperationResult.transactionHash = transactionHash;
        return injectOperationResult;
    }
}