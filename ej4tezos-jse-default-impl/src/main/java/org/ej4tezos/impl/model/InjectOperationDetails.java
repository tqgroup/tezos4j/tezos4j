// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.ej4tezos.utils.asserts.Asserts.*;

import org.ej4tezos.api.exception.TezosException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class InjectOperationDetails extends AbstractOperationData {

    private static final Logger LOG = LoggerFactory.getLogger(InjectOperationDetails.class);

    private String data;

    public InjectOperationDetails() {
        super();
    }

    public void setData (String data) {
        this.data = data;
    }

    public String getData () {
        return data;
    }

    public static InjectOperationDetails toInjectOperationDetails (String forgedData, String signature) throws TezosException {

        // Log the call:
        LOG.debug("toInjectOperationDetails (forgedData = {}, signature = {})", forgedData, signature);

        // Sanity checks:
        checkNotNull(signature, "signature should not be null", TezosException.class);
        checkNotNull(forgedData, "forgedData should not be null", TezosException.class);
        checkState(signature.startsWith("0x"), "signature should start with 0x", TezosException.class);
        checkState(forgedData.startsWith("0x03"), "forgedData should start with 0x03", TezosException.class);

        //
        InjectOperationDetails injectOperationDetails = new InjectOperationDetails();
        injectOperationDetails.data = forgedData.substring(4).concat(signature.substring(2));

        // Log and return the result:
        LOG.debug("injectOperationDetails = {}", injectOperationDetails);
        return injectOperationDetails;
    }
}