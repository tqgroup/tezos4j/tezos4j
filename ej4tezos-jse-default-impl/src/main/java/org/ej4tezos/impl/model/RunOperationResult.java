// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.lang3.StringUtils;

import static org.ej4tezos.utils.asserts.Asserts.*;

import org.ej4tezos.api.model.TezosCounter;
import org.ej4tezos.api.model.TezosBlockHash;

import org.ej4tezos.api.exception.TezosException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class RunOperationResult extends AbstractOperationData {

    private static final Logger LOG = LoggerFactory.getLogger(RunOperationResult.class);

    public enum Status { FAILED, APPLIED }

    private Status status;
    private long consumedGas;
    private long storageSize;
    private String errorMessage;
    private TezosCounter counter;
    private TezosBlockHash blockHash;

    public RunOperationResult () {
        super();
    }

    public void setStatus (Status status) {
        this.status = status;
    }

    public Status getStatus () {
        return status;
    }

    public void setConsumedGas (long consumedGas) {
        this.consumedGas = consumedGas;
    }

    public long getConsumedGas () {
        return consumedGas;
    }

    public void setStorageSize () {
        this.storageSize = storageSize;
    }

    public long getStorageSize () {
        return storageSize;
    }

    public TezosCounter getCounter () {
        return counter;
    }

    public TezosBlockHash getBlockHash () {
        return blockHash;
    }

    public String getErrorMessage () {
        return errorMessage;
    }

    public static RunOperationResult toRunOperationResult (TezosCounter counter, TezosBlockHash blockHash, String statusAsString, String errorMessage) throws TezosException {

        // Log the call:
        LOG.debug("toRunOperationResult (counter = {}, blockHash = {}, statusAsString = {}, errorMessage = {})",
                                         counter, blockHash, statusAsString, errorMessage);

        // Sanity checks:
        checkNotNull(counter, "counter should not have been null", TezosException.class);
        checkNotNull(blockHash, "blockHash should not have been null", TezosException.class);
        checkNotNull(errorMessage, "errorMessage should not have been null", TezosException.class);
        checkNotNull(statusAsString, "statusAsString should not have been null", TezosException.class);

        try {

            RunOperationResult runOperationResult = new RunOperationResult();

            runOperationResult.counter = counter;
            runOperationResult.blockHash = blockHash;
            runOperationResult.errorMessage = errorMessage;
            runOperationResult.status = Status.valueOf(statusAsString.trim().toUpperCase());

            return runOperationResult;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while converting to RunOperationResult: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    public static RunOperationResult toRunOperationResult (TezosCounter counter, TezosBlockHash blockHash, String statusAsString, String consumedGasAsString, String storageSizeAsString) throws TezosException {

        // Log the call:
        LOG.debug("toRunOperationResult (counter = {}, blockHash = {}, statusAsString = {}, consumedGasAsString = {}, storageSizeAsString = {})",
                                         counter, blockHash, statusAsString, consumedGasAsString, storageSizeAsString);

        // Sanity checks:
        checkNotNull(counter, "counter should not have been null", TezosException.class);
        checkNotNull(blockHash, "blockHash should not have been null", TezosException.class);
        checkNotNull(statusAsString, "statusAsString should not have been null", TezosException.class);
        checkNotNull(consumedGasAsString, "consumedGasAsString should not have been null", TezosException.class);
        checkNotNull(storageSizeAsString, "storageSizeAsString should not have been null", TezosException.class);
        checkState(StringUtils.isNumeric(consumedGasAsString),"consumedGasAsString should be numeric", TezosException.class);
        checkState(StringUtils.isNumeric(storageSizeAsString),"storageSizeAsString should be numeric", TezosException.class);

        try {

            RunOperationResult runOperationResult = new RunOperationResult();

            runOperationResult.counter = counter;
            runOperationResult.blockHash = blockHash;
            runOperationResult.consumedGas = Long.valueOf(consumedGasAsString.trim());
            runOperationResult.storageSize = Long.valueOf(storageSizeAsString.trim());
            runOperationResult.status = Status.valueOf(statusAsString.trim().toUpperCase());

            return runOperationResult;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while converting to RunOperationResult: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }
}