// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.api.exception.TezosException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/*
 * @author frederic.hubin@gmail.com
 */
public class RunOperationDetails extends AbstractOperationDataWithParameters<RunOperationDetails> {

    private static final Logger LOG = LoggerFactory.getLogger(RunOperationDetails.class);

    private RunOperationDetails () {
        super();
    }

    public static RunOperationDetails toRunOperationDetails () throws TezosException {
        return new RunOperationDetails();
    }

    public static RunOperationDetails toRunOperationDetails (String key, Object value) throws TezosException {
        RunOperationDetails runOperationDetails = new RunOperationDetails();
        runOperationDetails.setParameter(key, value);
        return runOperationDetails;
    }
}