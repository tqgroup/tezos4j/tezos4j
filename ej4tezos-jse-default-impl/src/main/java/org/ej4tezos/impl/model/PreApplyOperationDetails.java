// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.api.model.TezosCounter;
import org.ej4tezos.api.model.TezosFee;
import org.ej4tezos.api.model.TezosProtocol;
import org.ej4tezos.api.model.TezosBlockHash;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class PreApplyOperationDetails extends AbstractOperationDataWithParameters<PreApplyOperationDetails> {

    protected long gasLimit;
    protected String signature;
    protected long storageLimit;

    protected TezosFee fee;
    protected TezosCounter counter;
    protected TezosProtocol protocol;
    protected TezosBlockHash blockHash;

    public PreApplyOperationDetails() {
        super();
    }

    public TezosFee getFee () {
        return fee;
    }

    public long getGasLimit () {
        return gasLimit;
    }

    public TezosCounter getCounter () {
        return counter;
    }

    public String getSignature () {
        return signature;
    }

    public TezosProtocol getProtocol () {
        return protocol;
    }

    public long getStorageLimit () {
        return storageLimit;
    }

    public TezosBlockHash getBlockHash () {
        return blockHash;
    }

    public static PreApplyOperationDetails toPreApplyOperationDetails (TezosProtocol protocol, TezosCounter counter, TezosBlockHash blockHash, TezosFee fee, long gasLimit, long storageLimit, String signature) {

        PreApplyOperationDetails preApplyOperationDetails = new PreApplyOperationDetails();

        preApplyOperationDetails.fee = fee;
        preApplyOperationDetails.counter = counter;
        preApplyOperationDetails.gasLimit = gasLimit;
        preApplyOperationDetails.protocol = protocol;
        preApplyOperationDetails.signature = signature;
        preApplyOperationDetails.blockHash = blockHash;
        preApplyOperationDetails.storageLimit = storageLimit;

        return preApplyOperationDetails;
    }
}