// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

import org.apache.commons.lang3.StringUtils;
import org.ej4tezos.api.TezosCoreService;
import org.ej4tezos.api.exception.TezosException;
import org.ej4tezos.api.exception.TezosTransactionLostException;
import org.ej4tezos.api.model.*;
import org.ej4tezos.model.TezosAddress;
import org.ej4tezos.model.TezosBigMap;
import org.ej4tezos.model.TezosPublicAddress;
import org.ej4tezos.papi.TezosConnectivity;
import org.ej4tezos.utils.micheline.ExpressionBuilder;
import org.ej4tezos.utils.micheline.MichelineNode;
import org.ej4tezos.utils.micheline.StorageFlattener;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static org.ej4tezos.impl.Converter.*;
import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;
import static org.ej4tezos.utils.asserts.Asserts.checkState;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosCoreServiceImpl implements TezosCoreService {

    private static final Logger LOG = LoggerFactory.getLogger(TezosCoreServiceImpl.class);

    private final static int BLOCK_COUNT_BEFORE_LOST = 5;

    @Inject
    private TezosConnectivity tezosConnectivity;

    public TezosCoreServiceImpl () {
        LOG.info("A new instance of 'TezosCoreServiceImpl' got created; logging on {}...", LOG.getName());
    }

    @PostConstruct
    public void init () throws Exception {

        LOG.debug("init()");

        // Assert the parameters:
        checkNotNull(tezosConnectivity, "tezosConnectivity should not be null", Exception.class);

    }

    @PreDestroy
    public void destroy () throws Exception {

        LOG.debug("destroy()");

    }

    @Override
    public TezosCounter getCounter (TezosPublicAddress publicAddress) throws TezosException {

        //
        LOG.debug("getCounter (publicAddress = {})", publicAddress);

        try {

            // Proceed with the REST call:
            String body = tezosConnectivity.executeGet(tezosConnectivity.toURL(format("/chains/main/blocks/head/context/contracts/%s/counter", publicAddress)));
            LOG.debug("body = {}", body);

            // Clean-up and remove double quotes:
            body = body.trim();
            body = StringUtils.removeIgnoreCase(body, "\"");

            // Convert the result:
            TezosCounter counter = toCounter(body);
            LOG.debug("counter = {}", counter);
            return counter;
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = format("An exception occurred while retrieving the counter for %s: %s", publicAddress, ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public TezosProtocols getProtocols () throws TezosException {

        //
        LOG.debug("getProtocols ()");

        try {

            // Proceed with the REST call:
            JSONObject object = tezosConnectivity.executeJsonGet(tezosConnectivity.toURL("/chains/main/blocks/head/protocols"));
            LOG.debug("object = {}", object);

            // Convert the result:
            TezosProtocols protocol = toProtocols(object);
            LOG.debug("protocol = {}", protocol);
            return protocol;
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = format("An exception occurred while retrieving the protocol: %s", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public TezosBlockHash getBlockHash () throws TezosException {

        //
        LOG.debug("getBlockHash ()");

        try {

            // Proceed with the REST call:
            String body = tezosConnectivity.executeGet(tezosConnectivity.toURL("/chains/main/blocks/head/hash"));
            LOG.debug("body = {}", body);

            // Clean-up and reemove double quotes:
            body = body.trim();
            body = StringUtils.removeIgnoreCase(body, "\"");

            // Convert the result:
            TezosBlockHash blockHash = toBlockHash(body);
            LOG.debug("blockHash = {}", blockHash);
            return blockHash;
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = format("An exception occurred while retrieving the block hash: %s", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public TezosChainId getChainId () throws TezosException {

        //
        LOG.debug("getChainId ()");

        try {

            // Proceed with the REST call:
            String body = tezosConnectivity.executeGet(tezosConnectivity.toURL("/chains/main/chain_id"));
            LOG.debug("body = {}", body);

            // Clean-up and remove double quotes:
            body = body.trim();
            body = StringUtils.removeIgnoreCase(body, "\"");

            // Convert the result:
            TezosChainId chainId = toChainId(body);
            LOG.debug("chainId = {}", chainId);
            return chainId;
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = format("An exception occurred while retrieving the chain id: %s", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public TezosLevel getLevel (TezosBlockHash blockHash) throws TezosException {
        return getLevel(blockHash.getValue());
    }

    @Override
    public TezosLevel getLevel () throws TezosException {
        return getLevel("head");
    }

    protected TezosLevel getLevel (String blockHash) throws TezosException {

        //
        LOG.debug("getLevel ()");

        try {

            // Proceed with the REST call:
            JSONObject object = tezosConnectivity.executeJsonGet(
                    tezosConnectivity.toURL(String.format("/chains/main/blocks/%s", blockHash)));
            LOG.debug("object = {}", object);

            // Convert, log and return the result:
            BigInteger result = object.getJSONObject("header").getBigInteger("level");
            LOG.debug("result = {}", result);
            return TezosLevel.toTezosLevel(result);
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = format("An exception occurred while retrieving the block number: %s", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public TezosBlock getBlock (TezosLevel level) throws TezosException {

        //
        LOG.debug("getBlock (level = {})", level);

        //
        checkNotNull(level, "level should not be null", TezosException.class);

        try {

            //
            String rawUrl = "/chains/main/blocks/" + level.getValue().longValue();
            LOG.debug("rawUrl = {}", rawUrl);

            // Proceed with the REST call:
            JSONObject block = tezosConnectivity.executeJsonGet(tezosConnectivity.toURL(rawUrl));
            LOG.debug("block = {}", block);

            //
            TezosBlock result = new TezosBlock();

            // Set the various fields:
            result.setJson(block);
            result.setHash(TezosBlockHash.toTezosBlockHash(block.getString("hash")));
            result.setLevel(level);
            result.getTransactionList().addAll(getTransactionList(block));

            // Log and return the result:
            LOG.debug("result = {}", result);
            return result;
        }

        catch (Exception ex) {
            String message = format("An exception occurred while retrieving the transaction list: %s", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public List<TezosTransaction> getTransactionList (TezosLevel level) throws TezosException {

        // Log the call:
        LOG.debug("getTransactionList (level = {})", level);

        // Sanity check:
        checkNotNull(level, "level should not be null", TezosException.class);
        checkState(level.isValid(), "level should be valid", TezosException.class);

        try {

            //
            List<TezosTransaction> result = getBlock(level).getTransactionList();

            // Log and return the result:
            LOG.debug("result = {}", result);
            return result;
        }

        catch (Exception ex) {
            String message = format("An exception occurred while retrieving the transaction list: %s", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public List<TezosTransaction> getTransactionList (TezosLevel level, TezosAddress destinationAddress) throws TezosException {

        // Log the call:
        LOG.debug("getTransactionList (level = {}, destinationAddress = {})", level, destinationAddress);

        // Sanity check:
        checkNotNull(level, "level should not be null", TezosException.class);
        checkState(level.isValid(), "level should be valid", TezosException.class);

        checkNotNull(destinationAddress, "destinationAddress should not be null", TezosException.class);
        checkState(destinationAddress.isValid(), "destinationAddress should be valid", TezosException.class);

        //
        List<TezosTransaction> transactionList = getTransactionList(level);
        LOG.debug("transactionList = {}", transactionList);

        //
        List<TezosTransaction> result = new ArrayList<>();
        for(TezosTransaction transaction : transactionList) {

            if(transaction.getDestination() == null) {
                LOG.debug("transaction destination is null; skip it!");
                continue;
            }

            if(transaction.getDestination().equals(destinationAddress)) {
                result.add(transaction);
            }
        }

        // Log and return the result:
        LOG.debug("result = {}", result);
        return result;
    }

    private List<TezosTransaction> getTransactionList (JSONObject block) throws TezosException {

        // Log the call:
        LOG.debug("getTransactionList (block = {})", block);

        // Sanity check:
        checkNotNull(block, "block should not be null", TezosException.class);

        try {

            //
            List<TezosTransaction> result = new ArrayList<>();
            List<JSONObject> operationList = new ArrayList<>();

            //
            TezosLevel level
                = TezosLevel
                    .toTezosLevel(block.getJSONObject("metadata").getJSONObject("level").getBigInteger("level"));

            // Get to the transaction array:
            JSONArray operationsArray = block.getJSONArray("operations");
            LOG.debug("operationsArray = {}", operationsArray);

            // Transaction are located in the third array:
            JSONArray subOperationsArray = operationsArray.getJSONArray(3);

            for(int j = 0; j < subOperationsArray.length(); j++) {

                operationList.add(subOperationsArray.getJSONObject(j));

            }

            // Loop through the operations:
            for(JSONObject operation : operationList) {

                LOG.debug("operation = {}", operation);

                JSONArray contents = operation.getJSONArray("contents");

                // Loop through the contents:
                for(int j = 0; j < contents.length(); j++) {

                    JSONObject content = contents.getJSONObject(j);
                    LOG.debug("content = {}", content);

                    String kind = content.getString("kind");
                    if(!kind.equals("transaction")) {
                        LOG.debug("This is not a transaction; skip it (kind = {})", kind);
                        continue;
                    }

                    // Create the transaction:
                    TezosTransaction transaction = new TezosTransaction();

                    if(content.has("parameters")) {
                        transaction.setEntrypoint(content.getJSONObject("parameters").getString("entrypoint"));
                    }

                    transaction.setLevel(level);
                    transaction.setJson(content);
                    transaction.setDestination(TezosAddress.toTezosAddress(content.getString("destination")));
                    transaction.setTransactionHash(TezosTransactionHash.toTezosTransactionHash(operation.getString("hash")));
                    transaction.setSource(TezosAddress.toTezosAddress(content.getString("source")));

                    LOG.debug("transaction = {}", transaction);

                    result.add(transaction);
                }
            }

            // Log and return the result:
            LOG.debug("result = {}", result);
            return result;
        }

        catch (Exception ex) {
            String message = format("An exception occurred while retrieving the transaction list: %s", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    public List<TezosMempoolTransaction> getMempoolTransaction (MemoryPoolStatus status) throws TezosException {

        //
        LOG.debug("getMempoolTransaction (status = {})", status);

        try{

            // Proceed with the REST call:
            JSONObject object = tezosConnectivity.executeJsonGet(tezosConnectivity.toURL("/chains/main/mempool/pending_operations"));

            // Convert the result:
            JSONArray tezosMempoolTransactionsJSONArray = object.getJSONArray(status.toString().toLowerCase());
            List<TezosMempoolTransaction> tezosMempoolTransactionList = new ArrayList<>();

            for (int i = 0; i < tezosMempoolTransactionsJSONArray.length(); i++) {

                // In status branch_delayed  and refused the form of data is different: array of array
                if(tezosMempoolTransactionsJSONArray.get(i) instanceof JSONObject){

                    JSONObject content = tezosMempoolTransactionsJSONArray.getJSONObject(i);
                    TezosMempoolTransaction tezosMempoolTransaction = new TezosMempoolTransaction();

                    tezosMempoolTransaction.setHash(TezosTransactionHash.toTezosTransactionHash(content.getString("hash")));
                    tezosMempoolTransaction.setJson(content);
                    tezosMempoolTransaction.setSignature(TezosSignature.toTezosSignature(content.getString("signature")));
                    tezosMempoolTransaction.setBranch(TezosBranch.toTezosBranch(content.getString("branch")));

                    tezosMempoolTransactionList.add( tezosMempoolTransaction );
                }
                else {

                    org.json.JSONArray content = tezosMempoolTransactionsJSONArray.getJSONArray(i);
                    TezosMempoolTransaction tezosMempoolTransaction = new TezosMempoolTransaction();

                    tezosMempoolTransaction.setHash(TezosTransactionHash.toTezosTransactionHash(content.get(0).toString()));


                    JSONObject json =  new JSONObject();
                    json.put("contents",content.get(1));

                    tezosMempoolTransaction.setJson(json);
                    tezosMempoolTransaction.setSignature(TezosSignature.toTezosSignature(content.getJSONObject(1).getString("signature")));
                    tezosMempoolTransaction.setBranch(TezosBranch.toTezosBranch(content.getJSONObject(1).getString("branch")));

                    tezosMempoolTransactionList.add( tezosMempoolTransaction );
                }
            }

            //
            LOG.debug("tezosMempoolTransactionList = {}", tezosMempoolTransactionList);
            return tezosMempoolTransactionList;
        }

        catch (Exception ex) {
            String message = format("An exception occurred while retrieving the transaction list: %s", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public void waitForTransaction (TezosBlockHash startFrom, TezosTransactionHash transactionHash) throws TezosException {

        //
        LOG.debug("waitForTransaction (startFrom = {}, transactionHash = {})", startFrom, transactionHash);

        //
        checkNotNull(transactionHash, "transactionHash should not be null", TezosException.class);

        try {

            // search for the transaction in future blocks
            boolean isInThisBlock = false;

            // initiate to zero to do the first block check
            int diff = 0;
            TezosLevel maxLevel = getLevel();
            TezosLevel level = getLevel(startFrom);

            while (diff <= BLOCK_COUNT_BEFORE_LOST) {
                // While we reached maxLevel we need to wait
                while (level.after(maxLevel)) {
                    maxLevel = getLevel();
                    LOG.debug("level = {}, maxLevel = {}", level, maxLevel);
                    if (level.after(maxLevel)) {
                        Thread.sleep(1000);
                    }
                }

                LOG.debug("checking {} in level {}, maxLevel = {}", transactionHash, level, maxLevel);
                String object = tezosConnectivity.executeGet(
                        tezosConnectivity.toURL(String.format("/chains/main/blocks/%d/operation_hashes/3",
                                level.getValue())));
                JSONArray operationHashes = new JSONArray(object);
                isInThisBlock = operationHashes.toList().contains(transactionHash.toString());
                if (isInThisBlock) {
                    break;
                }

                level = level.next();
                diff++;
            }

            // To prevent infinite loop
            if (!isInThisBlock) {
                throw TezosTransactionLostException.toTezosTransactionLostException("Transaction is lost", transactionHash);
            }

            LOG.debug("Found transaction {} in level = {}, maxLevel = {}", transactionHash, level, maxLevel);
        }

        catch (Exception ex) {
            String message = format("An exception occurred while waiting for the transaction (hash=%s): %s", transactionHash, ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public String packData(Object... expression) throws TezosException {
        LOG.debug("packData ()");

        JSONObject packedData;
        try {
            MichelineNode<?, ?> node = ExpressionBuilder.getExpression(expression);
            JSONObject data = new JSONObject();
            data.put("data", node.getNodeJson());
            data.put("type", node.getNodeTypeJson());

            // Proceed with the REST call:
            packedData = tezosConnectivity.executeJsonPost(
                    tezosConnectivity.toURL("/chains/main/blocks/head/helpers/scripts/pack_data"),
                    data
            );

            return (String) packedData.get("packed");
        } catch (TezosException ex) {
            throw ex;
        } catch (Exception ex) {
            String message = format("An exception occurred while packing data: %s", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    @Override
    public Object getBigMap(TezosBigMap bigMap, TezosPackedData key) throws TezosException {
        LOG.debug("getBigMap ()");

        JSONObject data;
        try {
            // Proceed with the REST call:
            data = tezosConnectivity.executeJsonGet(
                    tezosConnectivity.toURL(String.format("/chains/main/blocks/head/context/big_maps/%s/%s",
                            bigMap.getAddress().toString(), key.getExpressionIdHash())));

            return StorageFlattener.flattenValues(data);
        } catch (TezosException ex) {
            throw ex;
        } catch (Exception ex) {
            String message = format("An exception occurred while retrieving big map data: %s", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    private boolean isFoundInMemPool (List<TezosMempoolTransaction> appliedTezosMempoolTransactionList, TezosTransactionHash transactionHash) throws TezosException {

        //
        LOG.debug("isFoundInMemPool (appliedTezosMempoolTransactionList = {}, transactionHash = {})", appliedTezosMempoolTransactionList, transactionHash);

        //
        checkNotNull(transactionHash, "transactionHash should not be null", TezosException.class);
        checkNotNull(appliedTezosMempoolTransactionList, "appliedTezosMempoolTransactionList should not be null", TezosException.class);

        //
        for(TezosMempoolTransaction tezosMempoolTransaction : appliedTezosMempoolTransactionList){

            if (tezosMempoolTransaction.getHash().equals(transactionHash)){
                return true;
            }
        }

        return false;
    }

    public void setTezosConnectivity (TezosConnectivity tezosConnectivity) {
        LOG.debug("setTezosConnectivity (tezosConnectivity = {})", tezosConnectivity);
        this.tezosConnectivity = tezosConnectivity;
    }

    @Override
    public String toString () {
        return "TezosCoreServiceImpl";
    }
}