// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.net.URL;
import java.net.MalformedURLException;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import javax.inject.Inject;

import javax.annotation.PreDestroy;
import javax.annotation.PostConstruct;

import static java.lang.String.format;

import org.ej4tezos.api.exception.TezosResourceNotFoundException;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.json.JSONObject;

import org.apache.commons.io.IOUtils;

import org.apache.hc.core5.http.Header;
import org.apache.hc.core5.http.HttpStatus;

import org.apache.hc.core5.http.io.entity.StringEntity;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;

import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;

import org.ej4tezos.papi.TezosConnectivity;

import org.ej4tezos.api.exception.TezosException;
import static org.ej4tezos.api.exception.TezosRPCException.toTezosRPCException;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosConnectivityImpl implements TezosConnectivity {

    private static final Logger LOG = LoggerFactory.getLogger(TezosConnectivityImpl.class);

    @Inject
    private String nodeUrl;

    private CloseableHttpClient httpClient;

    public TezosConnectivityImpl () {
        LOG.info("A new instance of 'TezosConnectivityImpl' got created; logging on {}...", LOG.getName());
    }

    @PostConstruct
    public void init () throws Exception {

        LOG.debug("init()");

        // Assert the parameters:
        checkNotNull(nodeUrl, "nodeUrl should not be null", Exception.class);

        // Remove trailing '/' in nodeUrl:
        while(nodeUrl.endsWith("/")) {
            nodeUrl = nodeUrl.substring(0, nodeUrl.length() - 1);
        }

        // Create the http client:
        LOG.info("Create the HTTP client...");
        httpClient = HttpClientBuilder.create().build();
    }

    @PreDestroy
    public void destroy () throws Exception {

        LOG.debug("destroy()");

        if(httpClient != null) {
            LOG.info("Close the HTTP client...");
            httpClient.close();
            httpClient = null;
        }
    }

    @Override
    public URL toURL (String rawUrl) throws MalformedURLException, TezosException {

        // Log the call:
        LOG.debug("toURL (rawUrl = {})", rawUrl);

        // Sanity check:
        checkNotNull(rawUrl , "rawUrl should not be null", TezosException.class);

        // Proceed with the conversion, log and return:
        URL url = new URL(format("%s%s", nodeUrl, rawUrl));
        LOG.debug("url = {}", url);
        return url;
    }

    @Override
    public String executeGet (URL url, Header ... headers) throws TezosException {

        CloseableHttpResponse response = null;

        // Log the call:
        LOG.debug("executeGet (url = {})", url);

        // Sanity check:
        checkNotNull(url, "url should not be null", TezosException.class);

        try {

            // Prepare the get object:
            HttpGet get = new HttpGet(url.toURI());

            // Set the header(s) if applicable:
            if(headers.length != 0) {
                get.setHeaders(headers);
            }

            // Execute the call:
            response = httpClient.execute(get);

            LOG.debug("entity = {}", response.getEntity());

            // Get the status code:
            int statusCode = response.getCode();

            // Throw an exception if it is not 200:
            if(statusCode != HttpStatus.SC_OK) {
                if(statusCode == HttpStatus.SC_NOT_FOUND) {
                    throw new TezosResourceNotFoundException(
                            format("Can't find resource executing the HTTP GET: %s", url));
                }
                String message = format("Cannot execute the HTTP GET: %s! (code = %s)", url, statusCode);
                LOG.warn(message);
                throw new TezosException(message);
            }

            //
            String body = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);
            LOG.debug("body = {}", body);
            return body;
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = format("An exception occurred while executing an HTTP GET: %s (url = %s)", ex.getMessage(), url);
            throw new TezosException(message, ex);
        }

        finally {

            try {
                if(response != null) {
                    response.close();
                }
            }

            catch (Exception ex) {
                String message = format("An exception occurred while trying to close the HTTP response: %s", ex.getMessage());
                throw new TezosException(message, ex);
            }
        }
    }

    @Override
    public JSONObject executeJsonGet (URL url, Header ... headers) throws TezosException {

        // Proceed with the get:
        String body = executeGet(url, headers);
        LOG.debug("body = {}", body);

        //
        JSONObject object = new JSONObject(body);
        LOG.debug("object = {}", object);
        return object;
    }

    @Override
    public String executePost (URL url, JSONObject jsonObject, Header ... headers) throws TezosException {

        // Log the call:
        LOG.debug("executePost (url = {}, jsonObject = {}, headers = {})", url, jsonObject, headers);

        // Sanity check:
        checkNotNull(url, "url should not be null", TezosException.class);
        checkNotNull(headers, "headers should not be null", TezosException.class);
        checkNotNull(jsonObject, "jsonObject should not be null", TezosException.class);

        //
        String body = executePost(url, jsonObject.toString(), addHeader(headers, CONTENT_APPLICATION_JSON));
        LOG.debug("body = {}", body);
        return body;
    }

    @Override
    public JSONObject executeJsonPost (URL url, JSONObject jsonObject, Header ... headers) throws TezosException {

        // Log the call:
        LOG.debug("executeJsonPost (url = {}, jsonObject = {}, headers = {})", url, jsonObject, headers);

        // Sanity check:
        checkNotNull(url, "url should not be null", TezosException.class);
        checkNotNull(headers, "headers should not be null", TezosException.class);
        checkNotNull(jsonObject, "jsonObject should not be null", TezosException.class);

        //
        String body = executePost(url, jsonObject.toString(), addHeader(headers, CONTENT_APPLICATION_JSON));
        LOG.debug("body = {}", body);

        //
        JSONObject object = new JSONObject(body);
        LOG.debug("object = {}", object);
        return object;
    }

    @Override
    public JSONObject executeJsonPost (URL url, JSONArray jsonArray, Header ... headers) throws TezosException {

        // Log the call:
        LOG.debug("executeJsonPost (url = {}, jsonArray = {}, headers = {})", url, jsonArray, headers);

        // Sanity check:
        checkNotNull(url, "url should not be null", TezosException.class);
        checkNotNull(headers, "headers should not be null", TezosException.class);
        checkNotNull(jsonArray, "jsonArray should not be null", TezosException.class);

        //
        String body = executePost(url, jsonArray.toString(), addHeader(headers, CONTENT_APPLICATION_JSON));
        LOG.debug("body = {}", body);

        //
        JSONObject object = new JSONObject(body);
        LOG.debug("object = {}", object);
        return object;
    }

    @Override
    public JSONArray executeJsonArrayPost (URL url, JSONArray jsonArray, Header ... headers) throws TezosException {

        // Log the call:
        LOG.debug("executeJsonPost (url = {}, jsonArray = {}, headers = {})", url, jsonArray, headers);

        // Sanity check:
        checkNotNull(url, "url should not be null", TezosException.class);
        checkNotNull(headers, "headers should not be null", TezosException.class);
        checkNotNull(jsonArray, "jsonArray should not be null", TezosException.class);

        //
        String body = executePost(url, jsonArray.toString(), addHeader(headers, CONTENT_APPLICATION_JSON));
        LOG.debug("body = {}", body);

        //
        JSONArray object = new JSONArray(body);
        LOG.debug("object = {}", object);
        return object;
    }

    @Override
    public String executePost (URL url, String data, Header ... headers) throws TezosException {

        CloseableHttpResponse response = null;

        // Log the call:
        LOG.debug("executePost (url = {}, data = {}, headers = {})", url, data, headers);

        // Sanity check:
        checkNotNull(url, "url should not be null", TezosException.class);
        checkNotNull(data, "data should not be null", TezosException.class);
        checkNotNull(headers, "headers should not be null", TezosException.class);

        try {

            // Prepare the post object:
            HttpPost post = new HttpPost(url.toURI());
            post.setEntity(new StringEntity(data));

            // Set the header(s) if applicable:
            if(headers.length != 0) {
                post.setHeaders(headers);
            }

            // Execute the call:
            response = httpClient.execute(post);

            LOG.debug("response = {}", response);
            LOG.debug("response.entity = {}", response.getEntity());

            // Get the status code:
            int statusCode = response.getCode();

            // Throw an exception if it is not 200:
            if(statusCode != HttpStatus.SC_OK) {
                if(statusCode == HttpStatus.SC_NOT_FOUND) {
                    throw new TezosResourceNotFoundException(
                            format("Can't find resource executing HTTP GET: %s!", url));
                }
                String message = format("Cannot execute the HTTP POST: %s! (code = %s)", url, statusCode);
                LOG.warn(message);

                String content = IOUtils.toString(response.getEntity().getContent(), Charset.defaultCharset());

                LOG.debug("response = {}", response);
                LOG.debug("response.entity = {}", response.getEntity());
                LOG.debug("response.entity.content = {}", content);

                throw toTezosRPCException(message, statusCode, content);
            }

            //
            String body = IOUtils.toString(response.getEntity().getContent(), Charset.defaultCharset());
            LOG.debug("body = {}", body);
            return body;
        }

        catch (TezosException ex) {
            throw ex;
        }

        catch (Exception ex) {
            String message = format("An exception occurred while executing an HTTP GET: %s (url = %s)", ex.getMessage(), url);
            throw new TezosException(message, ex);
        }

        finally {

            try {
                if(response != null) {
                    response.close();
                }
            }

            catch (Exception ex) {
                String message = format("An exception occurred while trying to close the HTTP response: %s", ex.getMessage());
                throw new TezosException(message, ex);
            }
        }
    }

    @Override
    public Header [] addHeader (Header [] headers, Header header) {

        Header [] concatenatedHeaders = new Header[headers.length + 1];

        int i = 0;
        for(; i < headers.length; i++) {
            concatenatedHeaders[i] = headers[i];
        }

        //
        concatenatedHeaders[i] = header;

        //
        return concatenatedHeaders;
    }

    public void setNodeUrl (String nodeUrl) {
        LOG.info("setNodeUrl (nodeUrl = {})", nodeUrl);
        this.nodeUrl = nodeUrl.trim();
    }

    @Override
    public String toString () {
        return "TezosConnectivityImpl";
    }
}