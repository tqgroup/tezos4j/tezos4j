// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.text.MessageFormat;

import javax.annotation.PreDestroy;
import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.api.TezosFeeService;

import org.ej4tezos.api.model.TezosFee;

import org.ej4tezos.api.exception.TezosException;

import static org.ej4tezos.utils.asserts.Asserts.*;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosFeeServiceImpl implements TezosFeeService {

    private static final Logger LOG = LoggerFactory.getLogger(TezosFeeServiceImpl.class);

    private long premium = 5000;

    public TezosFeeServiceImpl () {
        LOG.info("A new instance of 'TezosFeeServiceImpl' got created; logging on {}...", LOG.getName());
    }

    @PostConstruct
    public void init () throws Exception {

        LOG.debug("init()");

        checkState(premium > 0, "Premium should be positive", TezosException.class);

    }

    @PreDestroy
    public void destroy () throws Exception {

        LOG.debug("destroy()");

    }

    @Override
    public TezosFee computeFee (long size, long consumedGas) throws TezosException {

        //
        LOG.debug("computeFee (size = {}, consumedGas = {})", size, consumedGas);

        // Sanity check:
        checkState(size > 0,"Size should be positive", TezosException.class);
        checkState(consumedGas > 0,"consumed gas should be positive", TezosException.class);

        try {

            // long fee = (0.0001 + (0.000001 * size) + (0.0000001 * consumedGas)) * 1000000;
            long fee = size + (consumedGas / 10) + 100 + premium;

            //
            TezosFee result = TezosFee.toTezosFee(fee);

            //
            LOG.debug("result = {}", result);
            return result;

        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while computing the fee: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    public void setPremium (long premium) {
        LOG.debug("setPremium (premium = {})", premium);
        LOG.debug("Previous premium: {}", this.premium);
        this.premium = premium;
    }

    @Override
    public String toString () {
        return "TezosFeeServiceImpl";
    }
}