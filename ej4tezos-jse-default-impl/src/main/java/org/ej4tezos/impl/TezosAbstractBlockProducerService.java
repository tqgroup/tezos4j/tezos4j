// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;
import java.util.ArrayList;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.api.model.TezosBlock;

import org.ej4tezos.api.TezosCoreService;
import org.ej4tezos.api.TezosBlockListener;
import org.ej4tezos.api.TezosBlockProducerService;

import org.ej4tezos.api.exception.TezosException;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public abstract class TezosAbstractBlockProducerService implements TezosBlockProducerService {

    private static final Logger LOG = LoggerFactory.getLogger(TezosAbstractBlockProducerService.class);

    @Inject
    protected TezosCoreService tezosCoreService;

    private List<TezosBlockListener> tezosBlockListenerList;

    public TezosAbstractBlockProducerService () {
        super();
        tezosBlockListenerList = new ArrayList<>();
    }

    public void start () throws TezosException {

        LOG.debug("start ()");
        LOG.debug("tezosBlockListenerList.size = {}", tezosBlockListenerList.size());

    }

    public void stop () throws TezosException {

        LOG.debug("stop ()");

        for(TezosBlockListener listener : tezosBlockListenerList) {

            try {

                if(listener == null) {
                    LOG.warn("Found a null listener; skipping it!");
                    continue;
                }

                listener.onClose();

            }

            catch (Exception ex) {
                LOG.warn("An exception was thrown when invoking onClose on listener {}: {}", listener, ex.getMessage());
            }
        }
    }

    public void register (TezosBlockListener listener) throws TezosException {

        //
        LOG.debug("register (listener = {})", listener);

        //
        checkNotNull(listener, "listener should not be null", TezosException.class);

        //
        synchronized (tezosBlockListenerList) {
            tezosBlockListenerList.add(listener);
        }
    }

    public TezosBlockListener unregister (TezosBlockListener listener) throws TezosException {

        //
        LOG.debug("unregister (listener = {})", listener);

        //
        checkNotNull(listener, "listener should not be null", TezosException.class);

        //
        synchronized (tezosBlockListenerList) {

            int index = tezosBlockListenerList.indexOf(listener);

            if(index == -1) {
                return null;
            }

            tezosBlockListenerList.remove(index);

            return listener;
        }
    }

    protected boolean hasListener () {
        synchronized (tezosBlockListenerList) {
            return tezosBlockListenerList.size() > 0;
        }
    }

    protected int listenerCount () {
        synchronized (tezosBlockListenerList) {
            return tezosBlockListenerList.size();
        }
    }

    protected void propagate (TezosBlock block) throws TezosException {

        //
        LOG.debug("propagate (block = {})", block);

        //
        checkNotNull(block, "block should not be null", TezosException.class);

        //
        synchronized (tezosBlockListenerList) {

            for(TezosBlockListener listener : tezosBlockListenerList) {

                try {

                    listener.onBlock(block);

                }

                catch (Exception ex) {
                    onPropagationException(block, listener, ex);
                }
            }
        }
    }

    protected void onPropagationException (TezosBlock block, TezosBlockListener listener, Exception ex) {

        if(!LOG.isDebugEnabled()) {
            LOG.warn("An exception occurred while propagating block {} to {}: {}", block, listener, ex.getMessage());
        }
        else {
            LOG.warn("An exception occurred while propagating block {} to {}: {}", block, listener, ex.getMessage(), ex);
        }
    }

    public void setTezosCoreService (TezosCoreService tezosCoreService) {
        this.tezosCoreService = tezosCoreService;
    }

    public TezosCoreService getTezosCoreService () {
        return tezosCoreService;
    }

    @Override
    public String toString () {
        return "TezosAbstractService";
    }
}