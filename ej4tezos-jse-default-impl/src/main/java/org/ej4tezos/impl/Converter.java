// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.math.BigInteger;

import java.text.MessageFormat;

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.lang3.StringUtils;

import org.ej4tezos.api.model.TezosChainId;
import org.ej4tezos.api.model.TezosCounter;
import org.ej4tezos.api.model.TezosProtocol;
import org.ej4tezos.api.model.TezosProtocols;
import org.ej4tezos.api.model.TezosBlockHash;

import static org.ej4tezos.utils.asserts.Asserts.*;

import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class Converter {

    private static final Logger LOG = LoggerFactory.getLogger(Converter.class);

    static public TezosCounter toCounter (String body) throws TezosModelException {

        // Log the call:
        LOG.debug("toCounter (body = {})", body);

        // Sanity check:
        checkNotNull(body, "object should not be null", TezosModelException.class);

        //
        body = body.trim();

        // More sanity check:
        checkState(!body.startsWith("-"), "value should not be negative", TezosModelException.class);
        checkState(StringUtils.isNumeric(body), "body should be numeric", TezosModelException.class);

        try {

            //
            TezosCounter counter = TezosCounter.toTezosCounter(new BigInteger(body));

            //
            LOG.debug("counter = {}", counter);
            return counter;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while converting to TezosCounter: {0}", ex.getMessage());
            throw new TezosModelException(message, ex);
        }
    }

    static public TezosProtocols toProtocols (JSONObject object) throws TezosModelException {

        // Log the call:
        LOG.debug("TezosProtocols (object = {})", object);

        // Sanity check:
        checkNotNull(object, "object should not be null", TezosModelException.class);

        try {

            //
            TezosProtocols protocols
                = TezosProtocols
                    .toTezosProtocols(TezosProtocol.toTezosProtocol(object.getString("protocol")),
                                      TezosProtocol.toTezosProtocol(object.getString("next_protocol")));

            //
            LOG.debug("protocols = {}", protocols);
            return protocols;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while converting to TezosProtocols: {0}", ex.getMessage());
            throw new TezosModelException(message, ex);
        }
    }

    static public TezosBlockHash toBlockHash (String body) throws TezosModelException {

        // Log the call:
        LOG.debug("toBlockHash (body = {})", body);

        // Sanity check:
        checkNotNull(body, "body should not be null", TezosModelException.class);

        try {

            //
            TezosBlockHash blockHash = TezosBlockHash.toTezosBlockHash(body);

            //
            LOG.debug("blockHash = {}", blockHash);
            return blockHash;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while converting to TezosBlockHash: {0}", ex.getMessage());
            throw new TezosModelException(message, ex);
        }
    }

    static public TezosChainId toChainId (String body) throws TezosModelException {

        // Log the call:
        LOG.debug("toChainId (body = {})", body);

        // Sanity check:
        checkNotNull(body, "body should not be null", TezosModelException.class);

        try {

            //
            TezosChainId chainId = TezosChainId.toTezosChainId(body);

            //
            LOG.debug("chainId = {}", chainId);
            return chainId;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while converting to ChainId: {0}", ex.getMessage());
            throw new TezosModelException(message, ex);
        }
    }
}