// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.junit.Test;
import org.junit.Before;

import java.io.IOException;
import java.io.InputStream;

import java.util.*;

import java.nio.charset.Charset;

import org.json.JSONObject;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.ej4tezos.model.TezosBytes;
import org.ej4tezos.model.TezosPublicKey;
import org.ej4tezos.model.TezosSignature;
import org.ej4tezos.model.TezosPublicAddress;
import org.ej4tezos.model.exception.TezosModelException;

import org.ej4tezos.api.exception.TezosException;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public class TezosInvocationBuilderImplTest {

    private TezosInvocationBuilderImpl parametersBuilder;

    @Before
    public void setup () {
        parametersBuilder = new TezosInvocationBuilderImpl();
    }

    @Test
    public void parametersBuilderComplexInvocationTest () throws Exception {

        // ContractAddress = "KT1CeQaaNGDcRCXXKh6k4ohHeMu5J5CgE4kx";

        // Load invocation:
        InputStream inputStream = loadSample("MapAndListInvocation2.json");

        JSONObject mapAndListInvocation = new JSONObject(IOUtils.toString(inputStream, Charset.defaultCharset()));

        List<Integer> myList = new ArrayList<>();
        myList.add(1);
        myList.add(2);
        myList.add(3);

        Map<String, Integer> myMap = new HashMap<>();

        myMap.put("firstKey", 1);
        myMap.put("secondKey", 2);

        String myString = "myString";

        String entryPointName = "mapsAndList";

        int myNat = 12;

        int myInt = 440;

        Object result = parametersBuilder.buildInvocationParameters(entryPointName, myInt, myList, myMap, myNat, myString);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode generatedJSON = mapper.readTree(result.toString());
        JsonNode resourcesJSON = mapper.readTree(mapAndListInvocation.toString());

        // Assert result:
        assertThat("Generated Values should not be null", result, notNullValue());

        // Assert result:
        assertThat("Generated Invocation JSON must match the defined one in resources", generatedJSON.equals(resourcesJSON));
    }

    @Test
    public void parametersBuilderWithMapsAndListInvocationTest () throws Exception {

        // ContractAddress = "KT1EA8NYS2yAB49DbeUENhorJcZdnrNrcxL1";

        // Load invocation:
        InputStream inputStream = loadSample("MapAndListInvocation.json");

        JSONObject mapAndListInvocation = new JSONObject(IOUtils.toString(inputStream, Charset.defaultCharset()));

        List<String> myList = new ArrayList<>();
        myList.add("firstItem");
        myList.add("secondItem");
        myList.add("thirdItem");

        Map<String, Integer> myMap = new HashMap<>();

        myMap.put("firstKey", 1);
        myMap.put("secondKey", 2);

        String myString = "myString";

        String entryPointName = "mapsAndList";

        Object result = parametersBuilder.buildInvocationParameters(entryPointName, myList, myMap, myString);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode generatedJSON = mapper.readTree(result.toString());
        JsonNode resourcesJSON = mapper.readTree(mapAndListInvocation.toString());

        // Assert result:
        assertThat("Generated Values should not be null", result, notNullValue());

        // Assert result:
        assertThat("Generated Invocation JSON must match the defined one in resources", generatedJSON.equals(resourcesJSON));
    }

    @Test
    public void parametersBuilderTransferInvocationTest () throws TezosModelException, TezosException, IOException {

        // Load invocation:
        InputStream inputStream = loadSample("TransferInvocation.json");

        JSONObject transferInvocation = new JSONObject(IOUtils.toString(inputStream, Charset.defaultCharset()));

        int amount = 35;
        TezosPublicAddress from = TezosPublicAddress.toTezosPublicAddress("tz1LTLvbWJ85Lsq6zkgcSgisNFfNu7mRNEvg");
        TezosPublicAddress to = TezosPublicAddress.toTezosPublicAddress("tz1XvMBRHwmXtXS2K6XYZdmcc5kdwB9STFJu");

        // Call the builder:
        Object result = parametersBuilder.buildInvocationParameters("transfer", amount, from, to);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode generatedJSON = mapper.readTree(result.toString());
        JsonNode resourcesJSON = mapper.readTree(transferInvocation.toString());

        // Assert result:
        assertThat("Generated Values should not be null", result, notNullValue());

        // Assert result:
        assertThat("Generated Invocation JSON must match the defined one in resources", generatedJSON.equals(resourcesJSON));

    }

    @Test
    public void parametersBuilderTransferFeelessInvocationTest () throws TezosModelException, IOException, TezosException {

        // Load invocation:
        InputStream inputStream = loadSample("TransferFeelessInvocation.json");

        JSONObject transferFeelessInvocation = new JSONObject(IOUtils.toString(inputStream, Charset.defaultCharset()));

        TezosBytes bytes = TezosBytes.toTezosBytes("050707008c110707000107070a000000160000819bdfbed6cbd0c333a4f876a174321675bccc6207070a00000016000097f4d8f0a7fd3965dd642119713088da0ae2973e0a000000160156b71152d92ac24e81ebe1be087c701c585467f500");
        TezosPublicKey publicKey = TezosPublicKey.toTezosPublicKey("edpkur4NRwWj6YJ6tZH52xRD6Av1UCpaSLqsXhy6MMzrrfAAp2C468");
        TezosSignature signature =  TezosSignature.toTezosSignature("edsigtxzZN2eagL7nXZcLLKLBoyypLCKgdjSAMikdec3urVewJz2K7ygPAG9U1xrCF5bdBwnnRmgVD6cERg6drvKw2tiSb6oM3u");

        // Call the builder:
        Object result = parametersBuilder.buildInvocationParameters("transferFeeless", bytes, publicKey, signature);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode generatedJSON = mapper.readTree(result.toString());
        JsonNode resourcesJSON = mapper.readTree(transferFeelessInvocation.toString());

        // Assert result:
        assertThat("Generated Values should not be null", result, notNullValue());

        // Assert result:
        assertThat("Generated Invocation JSON must match the defined one in resources", generatedJSON.equals(resourcesJSON));

    }

    @Test
    public void parametersBuilderOneParamInvocationTest () throws TezosException, IOException {

        // Load invocation:
        InputStream inputStream = loadSample("OneParamInvocation.json");

        JSONObject OneParamInvocation = new JSONObject(IOUtils.toString(inputStream, Charset.defaultCharset()));

        int value = 12;

        // Call the builder:
        Object result = parametersBuilder.buildInvocationParameters("oneParam", value);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode generatedJSON = mapper.readTree(result.toString());
        JsonNode resourcesJSON = mapper.readTree(OneParamInvocation.toString());

        // Assert result:
        assertThat("Generated Values should not be null", result, notNullValue());

        // Assert result:
        assertThat("Generated Invocation JSON must match the defined one in resources", generatedJSON.equals(resourcesJSON));

    }

    @Test
    public void parametersBuilderNoParamInvocationTest () throws TezosException, IOException {

        // Load invocation:
        InputStream inputStream = loadSample("NoParamInvocation.json");

        JSONObject OneParamInvocation = new JSONObject(IOUtils.toString(inputStream, Charset.defaultCharset()));

        // Call the builder:
        Object result = parametersBuilder.buildInvocationParameters("noParam");

        ObjectMapper mapper = new ObjectMapper();
        JsonNode generatedJSON = mapper.readTree(result.toString());
        JsonNode resourcesJSON = mapper.readTree(OneParamInvocation.toString());

        // Assert result:
        assertThat("Generated Values should not be null", result, notNullValue());

        // Assert result:
        assertThat("Generated Invocation JSON must match the defined one in resources", generatedJSON.equals(resourcesJSON));
    }

    @Test
    public void parametersBuilderBuildOptionalInvocationTest () throws TezosException {

        String name = "SmartPy";
        int value = 12;
        Optional<Integer> opt = Optional.of(value);

        // Call the builder:
        Object result = parametersBuilder.buildInvocationParameters("buildOptional", opt, name);

        // Assert result:
        assertThat("Generated Values should not be null", result, notNullValue());
    }

    @Test
    public void parametersBuilderBuildOptionalWithNullInvocationTest () throws TezosException {

        String name = "SmartPy";
        Optional<Integer> opt = Optional.empty();

        // Call the builder:
        Object result = parametersBuilder.buildInvocationParameters("buildOptional", opt, name);

        // Assert result:
        assertThat("Generated Values should not be null", result, notNullValue());
    }

    @Test
    public void parametersBuilderBuildOptionalWithBoolInvocationTest () throws TezosException {

        String name = "SmartPy";
        Optional<Boolean> opt = Optional.of(Boolean.TRUE);

        // Call the builder:
        Object result = parametersBuilder.buildInvocationParameters("buildOptional", opt, name);

        // Assert result:
        assertThat("Generated Values should not be null", result, notNullValue());
    }

    private InputStream loadSample (String name) {
        return TezosInvocationBuilderImplTest.class.getClassLoader().getResourceAsStream(name);
    }
}
