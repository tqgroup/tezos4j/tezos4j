// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

import org.apache.commons.io.IOUtils;
import org.ej4tezos.api.model.*;
import org.ej4tezos.crypto.impl.TezosCryptoProviderImpl;
import org.ej4tezos.model.TezosAddress;
import org.ej4tezos.model.TezosBigMap;
import org.ej4tezos.papi.TezosCryptoProvider;
import org.ej4tezos.testing.FlextesaEnvironment;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.SecureRandom;

import static org.junit.Assert.*;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosCoreServiceImplTest {

    private static final Logger LOG = LoggerFactory.getLogger(TezosCoreServiceImplTest.class);
    @ClassRule
    public static FlextesaEnvironment fte = new FlextesaEnvironment();

    private static TezosOriginationResult originationResult;
    private TezosCoreServiceImpl tezosCoreServiceImpl;
    private TezosCryptoProviderImpl tezosCryptoProvider;

    @BeforeClass
    public static void setupBigMapContract() throws Exception {
        TezosOriginationContext tezosOriginationContext = new TezosOriginationContext(fte.getUrl());
        TezosContractOriginatorImpl tezosContractOriginator = new TezosContractOriginatorImpl(tezosOriginationContext,
                fte.getIdentity("alice"));
        tezosContractOriginator.init();

        InputStream codeInputStream = loadSample("TwoBigMaps/Code.json");
        JSONArray code = new JSONArray(IOUtils.toString(codeInputStream, Charset.defaultCharset()));
        InputStream storageInputStream = loadSample("TwoBigMaps/Storage.json");
        JSONObject storage = new JSONObject(IOUtils.toString(storageInputStream, Charset.defaultCharset()));

        originationResult = tezosContractOriginator.originate(storage, code);
    }

    private static InputStream loadSample(String name) {
        return TezosCoreServiceImplTest.class.getClassLoader().getResourceAsStream(name);
    }

    @Before
    public void setup() throws Exception {

        tezosCryptoProvider = new TezosCryptoProviderImpl();
        tezosCryptoProvider.setSecureRandom(SecureRandom.getInstanceStrong());
        tezosCryptoProvider.init();

        TezosConnectivityImpl tezosConnectivity = new TezosConnectivityImpl();
        tezosConnectivity.setNodeUrl(fte.getUrl());
        tezosConnectivity.init();

        tezosCoreServiceImpl = new TezosCoreServiceImpl();
        tezosCoreServiceImpl.setTezosConnectivity(tezosConnectivity);
        tezosConnectivity.init();
    }

    @Test
    @Ignore
    public void getBlockTest() throws Exception {

        TezosBlock block = tezosCoreServiceImpl.getBlock(TezosLevel.toTezosLevel("707606"));

        LOG.info("block = {}", block);
    }

    @Test
    @Ignore
    public void waitForTransactionTest() throws Exception {
        TezosTransactionHash hash = TezosTransactionHash.toTezosTransactionHash("opCKb1Pcotuc42e4rjFKov7b6NMEpfkFrdFaaNXXhcqpT5McwNp");
        tezosCoreServiceImpl.waitForTransaction(null, hash);
    }

    @Test
    public void testPackedDataImpl() throws Exception {
        String prototypeValue = "05070700020a0000001600004c1c8aba93a04d08d42c2b56b850ef2d16a97ca8";

        TezosPackedDataImpl packedData = new TezosPackedDataImpl(tezosCryptoProvider,
                prototypeValue);

        assertArrayEquals(packedData.getRawPackedData(),
                tezosCryptoProvider.decode(prototypeValue, TezosCryptoProvider.EncoderType.HEX));
        assertEquals(packedData.getLedgerBlake2bHash(), "54yWryT7Tjh9Drj6nJgV17ycoSPybRngkPcaRCEcEAFy");
        assertEquals(packedData.getExpressionIdHash(), "exprtyW2qbaCmgkWJiqpk9rJDwqXXCind9VS8iCSdT4PfDmQV3fSm6");
        assertArrayEquals(packedData.getRawSha256Hash(),
                tezosCryptoProvider.decode("898624826f505821aa8f4e6a7e2239688d91e2c7968de7af4a5c26618a262a87",
                        TezosCryptoProvider.EncoderType.HEX));
    }

    @Test
    public void testPackDataOneArg() throws Exception {
        TezosAddress ta = TezosAddress.toTezosAddress("tz1bwsEWCwSEXdRvnJxvegQZKeX5dj6oKEys");
        tezosCoreServiceImpl.packData(ta);
    }

    @Test
    public void testPackDataTwoArgs() throws Exception {
        assertEquals(
                tezosCoreServiceImpl.packData(2,
                        TezosAddress.toTezosAddress("tz1SaUEpoM8ME55RdMbNifypEqETodYKyNBD")),
                "05070700020a0000001600004c1c8aba93a04d08d42c2b56b850ef2d16a97ca8"
        );
    }

    @Test
    public void testPackDataThreeArgs() throws Exception {
        TezosAddress ta1 = TezosAddress.toTezosAddress("tz1bwsEWCwSEXdRvnJxvegQZKeX5dj6oKEys");
        TezosAddress ta2 = TezosAddress.toTezosAddress("tz1bwsEWCwSEXdRvnJxvegQZKeX5dj6oKEys");
        TezosAddress ta3 = TezosAddress.toTezosAddress("tz1bwsEWCwSEXdRvnJxvegQZKeX5dj6oKEys");

        tezosCoreServiceImpl.packData(ta1, ta2, ta3);
    }

    @Test
    public void testPackDataFourArgs() throws Exception {
        TezosAddress ta1 = TezosAddress.toTezosAddress("tz1bwsEWCwSEXdRvnJxvegQZKeX5dj6oKEys");
        TezosAddress ta2 = TezosAddress.toTezosAddress("tz1bwsEWCwSEXdRvnJxvegQZKeX5dj6oKEys");
        TezosAddress ta3 = TezosAddress.toTezosAddress("tz1bwsEWCwSEXdRvnJxvegQZKeX5dj6oKEys");
        TezosAddress ta4 = TezosAddress.toTezosAddress("tz1bwsEWCwSEXdRvnJxvegQZKeX5dj6oKEys");

        tezosCoreServiceImpl.packData(ta1, ta2, ta3, ta4);
    }

    @Test
    public void testBigMap() throws Exception {
        BigInteger bigMapAddr = BigInteger.valueOf(0);
        TezosBigMap bigMap = new TezosBigMap(bigMapAddr);
        String rawPackedKey = tezosCoreServiceImpl.packData(
                TezosAddress.toTezosAddress("tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"));
        TezosPackedData packedKey = new TezosPackedDataImpl(tezosCryptoProvider, rawPackedKey);
        Object value = tezosCoreServiceImpl.getBigMap(bigMap, packedKey);
        assertTrue(value instanceof BigInteger);
        assertEquals(value, BigInteger.valueOf(5));
    }
}
