// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.junit.Test;
import org.junit.Ignore;
import org.junit.Before;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;

import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

import org.ej4tezos.model.TezosPrivateKey;

import org.ej4tezos.api.model.TezosOriginationResult;

import static org.ej4tezos.model.TezosPrivateKey.toTezosPrivateKey;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public class TezosContractOriginatorTest {

    private TezosContractOriginatorImpl tezosContractOriginator;

    @Before
    public void setup () throws Exception {

        TezosPrivateKey originatorPrivateKey = toTezosPrivateKey("edskRioMPrRYbibwR7EqrokJtq3f8a66LeUvm1Sxya3pTgvWz7pTdghfNAvahAHNf3iox3j7P9nkYrKeCiaHVtiMu7yZraNYRe");

        TezosOriginationContext tezosOriginationContext = new TezosOriginationContext("https://testnet-tezos.giganode.io/");

        tezosContractOriginator = new TezosContractOriginatorImpl(tezosOriginationContext, originatorPrivateKey);

        // init should be called manually here
        tezosContractOriginator.init();
    }

    // The test is removed for the moment because we don't have a transaction confirmation system and that will cause
    // his launch 2 times simultaneously...
    // (FH) and it should not be here anyway.
    // @TezosPollingBlockProducerServiceImplTest
    @Test
    @Ignore
    public void originateTest () throws Exception {

        InputStream codeInputStream = loadSample("StoreValueContract/Code.json");

        JSONArray code = new JSONArray(IOUtils.toString(codeInputStream, Charset.defaultCharset()));

        InputStream storageInputStream = loadSample("StoreValueContract/Storage.json");

        JSONObject storage = new JSONObject(IOUtils.toString(storageInputStream, Charset.defaultCharset()));

        TezosOriginationResult result = tezosContractOriginator.originate(storage, code);

        // Assert result:
        assertThat("Result should not be null", result, notNullValue());

    }

    private InputStream loadSample (String name) {
        return TezosInvocationBuilderImplTest.class.getClassLoader().getResourceAsStream(name);
    }
}

