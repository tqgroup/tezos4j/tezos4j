// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.apache.commons.io.IOUtils;
import org.ej4tezos.api.model.TezosBlockHash;
import org.ej4tezos.api.model.TezosOriginationResult;

import org.ej4tezos.model.TezosPrivateKey;
import org.json.JSONArray;
import org.json.JSONObject;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.InputStream;
import java.nio.charset.Charset;

import static org.ej4tezos.model.TezosPrivateKey.toTezosPrivateKey;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Safwen Trabelsi (safwentrabelsi95@gmail.com)
 */
public class TezosTransactionConfirmationTest {

    private TezosCoreServiceImpl tezosCoreServiceImpl;
    private TezosContractOriginatorImpl tezosContractOriginator;

    @Before
    public void setup () throws Exception {

        TezosConnectivityImpl tezosConnectivity = new TezosConnectivityImpl();
        tezosConnectivity.setNodeUrl("https://testnet-tezos.giganode.io/");
        tezosConnectivity.init();
        TezosPrivateKey originatorPrivateKey = toTezosPrivateKey("edskRioMPrRYbibwR7EqrokJtq3f8a66LeUvm1Sxya3pTgvWz7pTdghfNAvahAHNf3iox3j7P9nkYrKeCiaHVtiMu7yZraNYRe");

        TezosOriginationContext tezosOriginationContext = new TezosOriginationContext("https://testnet-tezos.giganode.io/");

        tezosContractOriginator = new TezosContractOriginatorImpl(tezosOriginationContext, originatorPrivateKey);

        tezosCoreServiceImpl = new TezosCoreServiceImpl();
        tezosCoreServiceImpl.setTezosConnectivity(tezosConnectivity);


        // init should be called manually here
        tezosCoreServiceImpl.init();
        tezosContractOriginator.init();

    }

    @Test
    @Ignore
    public void confirmationTest () throws Exception {

        InputStream codeInputStream = loadSample("StoreValueContract/Code.json");

        JSONArray code = new JSONArray(IOUtils.toString(codeInputStream, Charset.defaultCharset()));

        InputStream storageInputStream = loadSample("StoreValueContract/Storage.json");

        JSONObject storage = new JSONObject(IOUtils.toString(storageInputStream, Charset.defaultCharset()));

        TezosBlockHash blockHash = tezosCoreServiceImpl.getBlockHash();
        TezosOriginationResult result = tezosContractOriginator.originate(storage, code);

        tezosCoreServiceImpl.waitForTransaction(blockHash, result.getTransactionHash());

        TezosOriginationResult resultAfterConfirm = tezosContractOriginator.originate(storage, code);

         // Assert result:
        assertThat("Result should not be null", resultAfterConfirm, notNullValue());

    }

    private InputStream loadSample (String name) {
        return TezosInvocationBuilderImplTest.class.getClassLoader().getResourceAsStream(name);
    }
}

