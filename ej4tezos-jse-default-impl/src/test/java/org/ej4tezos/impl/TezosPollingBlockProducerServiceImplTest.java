// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.impl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.io.InputStream;

import org.junit.Test;
import org.junit.Before;
import org.junit.Ignore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.api.model.TezosBlock;

import org.ej4tezos.api.TezosBlockListener;
import org.ej4tezos.api.TezosBlockProducerService;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosPollingBlockProducerServiceImplTest {

    private static final Logger LOG = LoggerFactory.getLogger(TezosPollingBlockProducerServiceImplTest.class);

    private TezosBlockProducerService tezosBlockProducerService;

    @Before
    public void setup () throws Exception {

        TezosConnectivityImpl tezosConnectivity = new TezosConnectivityImpl();
        tezosConnectivity.setNodeUrl("https://carthagenet.smartpy.io");
        tezosConnectivity.init();

        TezosCoreServiceImpl tezosCoreServiceImpl = new TezosCoreServiceImpl();
        tezosCoreServiceImpl.setTezosConnectivity(tezosConnectivity);
        tezosConnectivity.init();

        tezosBlockProducerService = new TezosPollingBlockProducerServiceImpl();
        ((TezosPollingBlockProducerServiceImpl) tezosBlockProducerService).setTezosCoreService(tezosCoreServiceImpl);
        tezosConnectivity.init();

    }

    @Test
    @Ignore
    public void genericTest () throws Exception {

        // Start the producer:
        tezosBlockProducerService.start();

        //
        Thread.sleep(90000);

        //
        tezosBlockProducerService
            .register(
                new TezosBlockListener () {

                   @Override
                   public void onBlock (TezosBlock block) {
                       LOG.info("A Block: {}", block);
                   }

                   @Override
                   public void onClose () {
                       LOG.info("Block producer is closed!");
                   }
               });

        //
        Thread.sleep(90000);

        // Stop the producer:
        tezosBlockProducerService.stop();

        //
        Thread.sleep(90000);

        //
        tezosBlockProducerService
            .register(
                new TezosBlockListener () {

                    @Override
                    public void onBlock (TezosBlock block) {
                        LOG.info("B Block: {}", block);
                    }

                    @Override
                    public void onClose () {
                        LOG.info("Block producer is closed!");
                    }
                });

        // Start the producer:
        tezosBlockProducerService.start();

        //
        Thread.sleep(90000);

        // Stop the producer:
        tezosBlockProducerService.stop();
    }

    private InputStream loadSample (String name) {
        return TezosPollingBlockProducerServiceImplTest.class.getClassLoader().getResourceAsStream(name);
    }
}
