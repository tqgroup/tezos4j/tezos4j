package org.ej4tezos.impl;

import org.ej4tezos.api.exception.TezosException;
import org.ej4tezos.model.TezosIdentity;
import org.ej4tezos.testing.FlextesaEnvironment;
import org.junit.Before;
import org.junit.ClassRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TezosAbstractServiceTest {
    @ClassRule
    public static FlextesaEnvironment fte = new FlextesaEnvironment();

    private TezosOriginationContext tezosOriginationContext;
    private TezosAbstractServiceImpl tezosAbstractService;

    @Before
    public void setup() throws Exception {
        tezosOriginationContext = new TezosOriginationContext(fte.getUrl());
        tezosAbstractService = new TezosAbstractServiceImpl(tezosOriginationContext,
                fte.getIdentity("alice"));

        // init should be called manually here
        tezosAbstractService.init();
    }

    public static class TezosAbstractServiceImpl extends TezosAbstractService {

        private static final Logger LOG = LoggerFactory.getLogger(TezosContractOriginatorImpl.class);

        public TezosAbstractServiceImpl(TezosOriginationContext context, TezosIdentity tezosIdentity)
                throws TezosException {

            LOG.info("A new instance of 'TezosAbstractServiceImpl' got created; logging on {}...", LOG.getName());

            setAdminIdentity(tezosIdentity);
            setAdminPrivateKey(tezosIdentity.getPrivateKey());

            setTezosKeyService(context.getTezosKeyService());
            setTezosFeeService(context.getTezosFeeService());
            setTezosCoreService(context.getTezosCoreService());
            setTezosConnectivity(context.getTezosConnectivity());
            setTezosCryptoProvider(context.getTezosCryptoProvider());
            setTezosResourceLoader(context.getTezosResourceLoader());
        }
    }
}

