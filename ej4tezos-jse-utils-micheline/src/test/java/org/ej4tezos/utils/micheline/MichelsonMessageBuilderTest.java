// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.micheline;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.junit.Test;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.model.TezosPublicAddress;
import org.ej4tezos.model.exception.TezosModelException;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public class MichelsonMessageBuilderTest {

    private static final Logger LOG = LoggerFactory.getLogger(MichelsonMessageBuilderTest.class);

    @Test
    public void buildMichelsonInvocationMessageSingleBoolParamTest () {


        // boolean myString = true;

        List<Object> invocationValuesList = new ArrayList<>();

        invocationValuesList.add(false);

        String result = MichelsonMessageBuilder.buildMichelsonInvocationSingleParam(invocationValuesList);

        LOG.info("michelsonMessage = {}", result);

        // Assert result:
        assertThat("Generated michelsonMessage should not be null", result, notNullValue());

        // Assert result:
        // assertThat("Generated michelsonMessage should be equal to src", result.equals(src));

    }

    @Test
    public void buildMichelsonInvocationMessageSingleParamTest () {

        String src = "\"myString\"";

        String myString = "myString";

        List<Object> invocationValuesList = new ArrayList<>();

        invocationValuesList.add(myString);

        String result = MichelsonMessageBuilder.buildMichelsonInvocationMessage(null, invocationValuesList);

        LOG.info("michelsonMessage = {}", result);

        // Assert result:
        assertThat("Generated michelsonMessage should not be null", result, notNullValue());

        // Assert result:
        assertThat("Generated michelsonMessage should be equal to src", result.equals(src));

    }

    @Test
    public void buildMichelsonInvocationMessageTwoParamsTest () throws TezosModelException {

        String src = "(Pair \"tz1SVqTz7entj982jDSKcTQNgT7f2cg7C8dk\" 100)";

        int amount = 100;
        TezosPublicAddress receiverAddress = TezosPublicAddress.toTezosPublicAddress("tz1SVqTz7entj982jDSKcTQNgT7f2cg7C8dk");

        List<Object> invocationValuesList = new ArrayList<>();

        invocationValuesList.add(receiverAddress);
        invocationValuesList.add(amount);

        String result = MichelsonMessageBuilder.buildMichelsonInvocationMessage(null, invocationValuesList);

        LOG.info("michelsonMessage = {}", result);

        // Assert result:
        assertThat("Generated michelsonMessage should not be null", result, notNullValue());

        // Assert result:
        assertThat("Generated michelsonMessage should be equal to src", result.equals(src));

    }

    @Test
    public void buildMichelsonInvocationMessageThreeParamsTest () throws TezosModelException {

        String src = "(Pair 100 (Pair \"tz1SVqTz7entj982jDSKcTQNgT7f2cg7C8dk\" \"tz1GVGz2YwuscuN1MEtocf45Su4xomQj1K8z\"))";

        int amount = 100;
        TezosPublicAddress senderAddress = TezosPublicAddress.toTezosPublicAddress("tz1SVqTz7entj982jDSKcTQNgT7f2cg7C8dk");
        TezosPublicAddress receiverAddress = TezosPublicAddress.toTezosPublicAddress("tz1GVGz2YwuscuN1MEtocf45Su4xomQj1K8z");

        List<Object> invocationValuesList = new ArrayList<>();

        invocationValuesList.add(amount);
        invocationValuesList.add(senderAddress);
        invocationValuesList.add(receiverAddress);

        String result = MichelsonMessageBuilder.buildMichelsonInvocationMessage(null, invocationValuesList);

        LOG.info("michelsonMessage = {}", result);

        // Assert result:
        assertThat("Generated michelsonMessage should not be null", result, notNullValue());

        // Assert result:
        assertThat("Generated michelsonMessage should be equal to src", result.equals(src));

    }

    @Test
    public void buildMichelsonInvocationMessageFourParamsTest () {

        String src = "(Pair (Pair 1 4) (Pair 2 3))";

        int first = 1;
        int second = 2;
        int third = 3;
        int fourth = 4;

        List<Object> invocationValuesList = new ArrayList<>();

        invocationValuesList.add(first);
        invocationValuesList.add(fourth);
        invocationValuesList.add(second);
        invocationValuesList.add(third);

        String result = MichelsonMessageBuilder.buildMichelsonInvocationMessage(null, invocationValuesList);

        LOG.info("michelsonMessage = {}", result);

        // Assert result:
        assertThat("Generated michelsonMessage should not be null", result, notNullValue());

        // Assert result:
        assertThat("Generated michelsonMessage should be equal to src", result.equals(src));

    }

    @Test
    public void buildMichelsonInvocationMessageFiveSimpleParamsTest () {

        String src = "(Pair (Pair 5 1) (Pair 4 (Pair 2 3)))";

        int first = 1;
        int second = 2;
        int third = 3;
        int fourth = 4;
        int fifth = 5;

        List<Object> invocationValuesList = new ArrayList<>();

        invocationValuesList.add(fifth);
        invocationValuesList.add(first);
        invocationValuesList.add(fourth);
        invocationValuesList.add(second);
        invocationValuesList.add(third);


        String result = MichelsonMessageBuilder.buildMichelsonInvocationMessage(null, invocationValuesList);

        LOG.info("michelsonMessage = {}", result);

        // Assert result:
        assertThat("Generated michelsonMessage should not be null", result, notNullValue());

        // Assert result:
        assertThat("Generated michelsonMessage should be equal to src", result.equals(src));

    }

    @Test
    public void buildMichelsonInvocationMessageFiveComplexParamsTest () {

        String src = "(Pair (Pair 440 { 1 ; 2 ; 3 }) (Pair { Elt \"firstKey\" 1 ; Elt \"secondKey\" 2 } (Pair 12 \"myString\")))";

        List<Integer> myList = new ArrayList<>();
        myList.add(1);
        myList.add(2);
        myList.add(3);

        Map<String, Integer> myMap = new HashMap<>();
        myMap.put("firstKey", 1);
        myMap.put("secondKey", 2);

        String myString = "myString";

        int myNat = 12;

        int myInt = 440;

        List<Object> invocationValuesList = new ArrayList<>();

        invocationValuesList.add(myInt);
        invocationValuesList.add(myList);
        invocationValuesList.add(myMap);
        invocationValuesList.add(myNat);
        invocationValuesList.add(myString);


        String result = MichelsonMessageBuilder.buildMichelsonInvocationMessage(null, invocationValuesList);

        LOG.info("michelsonMessage = {}", result);

        // Assert result:
        assertThat("Generated michelsonMessage should not be null", result, notNullValue());

        // Assert result:
        assertThat("Generated michelsonMessage should be equal to src", result.trim().equals(src.trim()));

    }
}