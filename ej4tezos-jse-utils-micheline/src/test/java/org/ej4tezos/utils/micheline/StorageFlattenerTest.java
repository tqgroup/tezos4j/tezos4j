// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.micheline;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;
import java.util.ArrayList;

import java.io.InputStream;
import java.io.StringWriter;

import java.nio.charset.Charset;

import org.junit.Test;

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.io.IOUtils;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.ej4tezos.api.exception.TezosException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class StorageFlattenerTest {

    private static final Logger LOG = LoggerFactory.getLogger(StorageFlattenerTest.class);

    @Test
    public void flattenEquisafeTest () throws Exception {

        List<StorageElement> result;

        result = StorageFlattener.flatten(loadFile("EquisafeStorage"));

        for(StorageElement element : result) {
            LOG.info("> {}", element);
        }
    }

    @Test
    public void flattenEuroTzTest () throws Exception {

        List<StorageElement> result;

        result = StorageFlattener.flatten(loadFile("EuroTzStorage"));

        for(StorageElement element : result) {
            LOG.info("> {}", element);
        }
    }

    private JSONObject loadFile (String resource) throws Exception {

        InputStream inputStream = StorageFlattenerTest.class.getClassLoader().getResourceAsStream(resource + ".json");

        StringWriter stringWriter = new StringWriter();
        IOUtils.copy(inputStream, stringWriter, Charset.defaultCharset());

        String content = stringWriter.getBuffer().toString();
        LOG.debug("content =\n{}", content);

        return new JSONObject(content);
    }
}