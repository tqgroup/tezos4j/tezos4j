// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.micheline;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;

import org.junit.Test;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.model.TezosPublicAddress;
import org.ej4tezos.model.exception.TezosModelException;

import org.ej4tezos.api.exception.TezosException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public class MichelineScannerTest {

    private static final Logger LOG = LoggerFactory.getLogger(MichelineScannerTest.class);

    @Test
    public void scanParamTest () throws TezosException {

        List<JSONObject> result = MichelineScanner.scan("Pair False 12");

        LOG.info("michelineMessage = {}", result);

        // Assert result:
        assertThat("Generated michelineMessage should not be null", result, notNullValue());

    }
}