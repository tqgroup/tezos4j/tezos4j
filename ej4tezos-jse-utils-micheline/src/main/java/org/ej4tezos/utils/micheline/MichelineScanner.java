// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.micheline;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Pattern;

import org.json.JSONObject;

import org.ej4tezos.api.exception.TezosException;

import static org.ej4tezos.papi.model.MichelineConstants.*;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public class MichelineScanner {

    private MichelineScanner () {
        throw new IllegalStateException("MichelineScanner is an utility class");
    }

    private enum Literal {
        NUMBER,
        STRING,
        BYTES,
        IDENT,
    }

    static Pattern isSpace = Pattern.compile("\\s");
    static Pattern isIdentStart = Pattern.compile("[:@%_A-Za-z]");
    static Pattern isIdent = Pattern.compile("[@%_\\.A-Za-z0-9]");
    static Pattern isDigit = Pattern.compile("[0-9]");
    static Pattern isHex = Pattern.compile("[0-9a-fA-F]");

    public static List<JSONObject> scan (String src) throws TezosException {

        int i = 0;

        List<JSONObject> result = new ArrayList<>();

        while (i < src.length()) {
            // Skip space
            while (i < src.length() && isSpace.matcher(Character.toString(src.charAt(i))).find()) {
                i++;
            }
            if (i == src.length()) {
                return result;
            }

            String s = Character.toString(src.charAt(i));
            int start = i;

            if (isIdentStart.matcher(s).find()) {
                // Identifier
                i++;
                while (i < src.length() && isIdent.matcher(Character.toString(src.charAt(i))).find()) {
                    i++;
                }
                String ss = src.substring(start, i);
                JSONObject entry = new JSONObject();
                entry.put(TYPE, Literal.IDENT);
                entry.put(VALUE, ss);
                result.add(entry);
            } else if (src.length() - i > 1 && src.startsWith("0x", i)) {
                // Bytes
                i += 2;
                while (i < src.length() && isHex.matcher(Character.toString(src.charAt(i))).find()) {
                    i++;
                }
                if (i - start == 2) {
                    throw new TezosException("Bytes literal is too short");
                } else if (((i - start) & 1) != 0) {
                    throw new TezosException("Bytes literal length is expected to be power of two");
                }
                String ss = src.substring(start, i);
                JSONObject entry = new JSONObject();
                entry.put(TYPE, Literal.BYTES);
                entry.put(VALUE, ss);
                result.add(entry);
            } else if (isDigit.matcher(s).find() || s.equals("-")) {
                // Number
                if (s.equals("-")) {
                    i++;
                }
                int ii = i;
                while (i < src.length() && isDigit.matcher(Character.toString(src.charAt(i))).find()) {
                    i++;
                }
                if (ii == i) {
                    throw new TezosException("Number literal is too short");
                }
                String ss = src.substring(start, i);
                JSONObject entry = new JSONObject();
                entry.put(TYPE, Literal.NUMBER);
                entry.put(VALUE, ss);
                result.add(entry);
            } else if (s.equals("\"")) {
                // String
                i++;
                boolean esc = false;
                for (; i < src.length() && (esc || !Character.toString(src.charAt(i)).equals("\"")); i++) {
                    if (!esc && Character.toString(src.charAt(i)).equals("\\")) {
                        esc = true;
                    } else {
                        esc = false;
                    }
                }
                if (i == src.length()) {
                    throw new TezosException("Unterminated string literal");
                }
                i++;
                String ss = src.substring(start + 1, i - 1);
                JSONObject entry = new JSONObject();
                entry.put(TYPE, Literal.STRING);
                entry.put(VALUE, ss);
                result.add(entry);
            } else if (s.equals("(")  || s.equals(")") || s.equals("{") || s.equals("}") || s.equals(";")) {
                i++;
                JSONObject entry = new JSONObject();
                entry.put(TYPE, s);
                entry.put(VALUE, s);
                result.add(entry);
            } else {
                throw new TezosException("Invalid character found");
            }
        }

        return result;
    }
}