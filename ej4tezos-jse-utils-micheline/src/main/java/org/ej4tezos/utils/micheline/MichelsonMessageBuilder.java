// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.micheline;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.*;

import static org.ej4tezos.papi.model.MichelineConstants.*;

import static org.apache.commons.lang3.StringUtils.isNumeric;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public class MichelsonMessageBuilder {

    private MichelsonMessageBuilder () {
        throw new IllegalStateException("MichelsonMessageBuilder is an utility class");
    }

    public static String buildMichelsonInvocationMessage (String builtMessage, List<Object> invocationValuesList) {
        if (builtMessage == null) {
            builtMessage = "";
        }

        if (invocationValuesList.size() == 1) {
            return format(invocationValuesList.get(0));
        } else if (invocationValuesList.size() == 2) {
            return "(" + PAIR + " " + format(invocationValuesList.get(0)) + " " + format(invocationValuesList.get(1)) + ")";
        } else if (invocationValuesList.size() >= 3) {
            builtMessage += "(" + PAIR + " " + buildMichelsonInvocationMessage(builtMessage, invocationValuesList.subList(0, (Math.abs(invocationValuesList.size() / 2)))) + " " + buildMichelsonInvocationMessage(builtMessage, invocationValuesList.subList((Math.abs(invocationValuesList.size() / 2)), invocationValuesList.size())) + ")";
        }

        return builtMessage;
    }

    public static String buildMichelsonInvocationSingleParam (List<Object> invocationValuesList) {
        return format(invocationValuesList.get(0));
    }

    private static String format (Object param) {
        if (param instanceof Optional) {
            if (((Optional) param).isPresent()) {
                return formatOptional(((Optional) param).get().toString());
            } else {
                return formatOptional(null);
            }
        } else if (param instanceof ArrayList) {
            return formatList((List) param);
        } else if (param instanceof Map) {
            return formatMap((HashMap) param);
        } else {
            return formatString(param.toString());
        }
    }

    // TODO add types verification based on TezosBytes/TezosSignature ....
    private static String formatString (String param) {
        if (!isNumeric(param) && !param.startsWith("0x")) {
            if (Boolean.TRUE.equals(isBoolean(param.toLowerCase()))) {
                // Case sensitive: Boolean param must be True or False
                return param.substring(0, 1).toUpperCase() + param.substring(1).toLowerCase();
            }
            return "\"" + param + "\"";
        }
        return param;
    }

    private static String formatOptional (String param) {
        if (param == null) {
            return  NONE;
        } else if (!isNumeric(param) && !param.startsWith("0x")) {
            if (Boolean.TRUE.equals(isBoolean(param.toLowerCase()))) {
                // Case sensitive: Boolean param must be True or False
                return "("+ SOME + " \"" + param.substring(0, 1).toUpperCase() + param.substring(1).toLowerCase() + "\")";
            }
            return "("+ SOME + " \"" + param + "\")";
        }
        return "(" + SOME + " " + param + ")";
    }

    private static String formatList (List<Object> param) {

        StringBuilder listParam = new StringBuilder();
        listParam.append("{ ");
        for (int i = 0; i < param.size(); i++) {
            listParam.append(format(param.get(i)));

            if (i < param.size() - 1) {
                listParam.append(" ; ");
            }

            if (i == param.size() - 1) {
                listParam.append(" }");
            }
        }
        return listParam.toString();
    }

    private static String formatMap (HashMap<Object, Object> param) {

        StringBuilder mapParam = new StringBuilder();
        mapParam.append("{");

        Iterator<Map.Entry<Object, Object>> itr = param.entrySet().iterator();

        while(itr.hasNext())
        {
            Map.Entry<Object, Object> entry = itr.next();
            mapParam.append(" " + ELT + " ").append(format(entry.getKey())).append(" ").append(format(entry.getValue()));

            if (itr.hasNext()) {
                mapParam.append(" ;");
            }

            if (!itr.hasNext()) {
                mapParam.append(" }");
            }
        }
        return mapParam.toString();
    }

    public static Boolean isBoolean (String input) {
        switch(input.toLowerCase(Locale.ENGLISH).trim()) {
            case "true":
            case "false":
                return true;
            default:
                return false;
        }
    }
}