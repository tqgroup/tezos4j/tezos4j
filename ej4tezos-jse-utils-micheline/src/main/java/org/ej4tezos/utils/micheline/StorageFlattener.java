// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.micheline;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

import org.ej4tezos.api.exception.TezosException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;
import static org.ej4tezos.utils.asserts.Asserts.checkState;
import static org.ej4tezos.utils.micheline.StorageElement.Type.*;
import static org.ej4tezos.utils.micheline.StorageElementArray.toStorageElementArray;
import static org.ej4tezos.utils.micheline.StorageMapElement.toStorageMapElement;
import static org.ej4tezos.utils.micheline.StorageSimpleElement.toStorageSimpleElement;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class StorageFlattener {

    private static final Logger LOG = LoggerFactory.getLogger(StorageFlattener.class);

    public static List<StorageElement> flatten (String rawStorage) throws TezosException {

        // Log the call:
        LOG.debug("flatten (rawStorage = {})", rawStorage);

        // Sanity check:
        checkNotNull(rawStorage, "rawStorage should not be null", TezosException.class);

        try {

            // Convert to Json object:
            JSONObject storage = new JSONObject(rawStorage);

            // Delegate the call:
            return flatten(storage);
        }

        catch (Exception ex) {
            String message = MessageFormat.format("", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    public static Object flattenValues(JSONObject storage) throws TezosException {
        List<Object> els = flatten(storage).parallelStream().map(StorageFlattener::flattenValue).
                collect(Collectors.toList());
        if (els.size() == 1) {
            return els.get(0);
        }
        return els;
    }

    private static Object flattenValue(StorageElement el) {
        if (el instanceof StorageSimpleElement) {
            return ((StorageSimpleElement) el).getValue();
        }
        if (el instanceof StorageElementArray) {
            return ((StorageElementArray) el).getValue().parallelStream().
                    map(StorageFlattener::flattenValue).collect(Collectors.toList());
        }
        if (el instanceof StorageMapElement) {
            return Arrays.asList(((StorageMapElement) el).getKey(), ((StorageMapElement) el).getValue());
        }
        throw new UnsupportedOperationException("unknown storage element " + el);
    }

    public static List<StorageElement> flatten (JSONObject storage) throws TezosException {

        List<StorageElement> result = new ArrayList<>();

        // Log the call:
        LOG.debug("flatten (storage = {})", storage);

        // Sanity check:
        checkNotNull(storage, "storage should not be null", TezosException.class);

        //
        parseSingleElement(storage, result);

        // Log and return the result:
        LOG.debug("result = {}", result);
        return result;
    }

    private static void parsePair (JSONObject element, List<StorageElement> result) throws TezosException {

        //
        LOG.debug("parsePair (element = {}, result = {})", element, result == null ? "null" : result.size());

        //
        checkNotNull(result, "result should not be null", TezosException.class);
        checkNotNull(element, "element should not be null", TezosException.class);
        checkState(element.has("args"),"Pair should have an args field!", TezosException.class);

        //
        Object rawArgs = element.get("args");
        checkState(rawArgs instanceof JSONArray,"args should be a JSONArray!", TezosException.class);

        //
        JSONArray args = (JSONArray) rawArgs;

        if(args.length() != 2) {
            String message = MessageFormat.format("Unexpected args size: {0} (expected was 2)", args.length());
            throw new TezosException(message);
        }

        //
        parsePairArgsElement(args.get(0), result);
        parsePairArgsElement(args.get(1), result);
    }

    private static void parseElt (JSONObject element, List<StorageElement> result) throws TezosException {

        List<StorageElement> temporaryResult = null;

        //
        LOG.debug("parseElt (element = {}, result = {})", element, result == null ? "null" : result.size());

        //
        checkNotNull(result, "result should not be null", TezosException.class);
        checkNotNull(element, "element should not be null", TezosException.class);
        checkState(element.has("args"),"Pair should have an args field!", TezosException.class);

        //
        Object rawArgs = element.get("args");
        checkState(rawArgs instanceof JSONArray,"args should be a JSONArray!", TezosException.class);

        //
        JSONArray args = (JSONArray) rawArgs;

        if(args.length() != 2) {
            String message = MessageFormat.format("Unexpected args size: {0} (expected was 2)", args.length());
            throw new TezosException(message);
        }

        //
        temporaryResult = new ArrayList<>();
        parsePairArgsElement(args.get(0), temporaryResult);
        StorageElement key = toStorageElementArray(temporaryResult);

        //
        temporaryResult = new ArrayList<>();
        parsePairArgsElement(args.get(1), temporaryResult);
        StorageElement value = toStorageElementArray(temporaryResult);

        //
        result.add(toStorageMapElement(key, value));
    }

    private static void parsePairArgsElement (Object element, List<StorageElement> result) throws TezosException {

        //
        LOG.debug("parsePairArgsElement (element = {}, result = {})", element, result == null ? "null" : result.size());

        //
        checkNotNull(result, "result should not be null", TezosException.class);
        checkNotNull(element, "element should not be null", TezosException.class);

        // Check if the element has a "prim" field:
        if(element instanceof JSONArray) {
            parseElementArray((JSONArray)element, result);
        }
        else if(element instanceof JSONObject) {
            parseSingleElement((JSONObject)element, result);
        }
        else {
            String message = MessageFormat.format("Unexpected element type: {0}", element.getClass());
            throw new TezosException(message);
        }
    }

    private static void parseElementArray (JSONArray array, List<StorageElement> result) throws TezosException {

        //
        LOG.debug("parseElementArray (array = {}, result = {})",
                                      array == null ? "null" : array.length(),
                                      result == null ? "null" : result.size());

        //
        checkNotNull(array, "array should not be null", TezosException.class);
        checkNotNull(result, "result should not be null", TezosException.class);

        //
        for(int i = 0; i < array.length(); i++) {
            LOG.debug("index = {}", i);
            parseSingleElement(array.getJSONObject(i), result);
        }
    }

    private static void parseSingleElement (JSONObject element, List<StorageElement> result) throws TezosException {

        //
        LOG.debug("parseSingleElement (element = {}, result = {})", element, result == null ? "null" : result.size());

        //
        checkNotNull(result, "result should not be null", TezosException.class);
        checkNotNull(element, "element should not be null", TezosException.class);

        // Check if the element has a "prim" field:
        if(element.has("prim")) {

            LOG.debug("Element is a Prim...");

            String value = element.getString("prim");

            switch (value) {

                case "Pair":
                    parsePair(element, result);
                    break;

                case "False":
                    result.add(toStorageSimpleElement(BOOLEAN, Boolean.FALSE));
                    break;

                case "True":
                    result.add(toStorageSimpleElement(BOOLEAN, Boolean.TRUE));
                    break;

                case "Elt":
                    parseElt(element, result);
                    break;

                default:
                    String message = MessageFormat.format("Unexpected value ", value);
                    throw new TezosException(message);
            }
        }
        else if(element.has("string")) {
            LOG.debug("Element is a String...");
            String value = element.getString("string");
            LOG.debug("value = {}", value);
            result.add(toStorageSimpleElement(STRING, value));
        }
        else if(element.has("int")) {
            LOG.debug("Element is a Integer...");
            String value = element.getString("int");
            LOG.debug("value = {}", value);
            result.add(toStorageSimpleElement(INT, new BigInteger(value)));
        }
        else if(element.has("bytes")) {
            LOG.debug("Element is an array of byte...");
            String value = element.getString("bytes");
            LOG.debug("value = {}", value);
            result.add(toStorageSimpleElement(BYTES, value));
        }
        else {
            String message = MessageFormat.format("Unexpected key(s) in the element: {0}", element.keySet());
            throw new TezosException(message);
        }
    }
}