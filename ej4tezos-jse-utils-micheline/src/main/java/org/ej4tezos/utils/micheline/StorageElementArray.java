// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.micheline;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class StorageElementArray extends StorageElement {

    private List<StorageElement> value;

    public static StorageElementArray toStorageElementArray (List<StorageElement> value) {
        StorageElementArray element = new StorageElementArray();
        element.setType(Type.ARRAY);
        element.value = value;
        return element;
    }

    protected StorageElementArray () {
        super();
    }

    public List<StorageElement> getValue () {
        return value;
    }
}