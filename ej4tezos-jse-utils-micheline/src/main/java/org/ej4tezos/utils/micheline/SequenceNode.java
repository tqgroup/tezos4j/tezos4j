package org.ej4tezos.utils.micheline;

import org.ej4tezos.papi.model.MichelineConstants;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SequenceNode implements MichelineNode<Collection<?>, JSONArray> {
    private final Collection<MichelineNode<?, ?>> value;
    private final String type;
    private JSONArray ja;
    private JSONObject typeJo;

    public SequenceNode(Set<?> value) {
        this(value, MichelineConstants.SET);
    }

    public SequenceNode(List<?> value) {
        this(value, MichelineConstants.LIST);
    }

    SequenceNode(Collection<?> value, String type) {
        this.value = PrimitiveApplicationNode.toMichelineNodes(value);
        this.type = type;
    }

    @Override
    public JSONArray getNodeJson() {
        JSONArray ja = this.ja;
        if (ja == null) {
            ja = new JSONArray(value.stream().map(MichelineNode::getNodeJson).collect(Collectors.toList()));
            this.ja = ja;
        }
        return ja;
    }

    @Override
    public JSONObject getNodeTypeJson() {
        JSONObject jo = this.typeJo;
        if (jo == null) {
            if (value.isEmpty()) {
                throw new RuntimeException("type information not available");
            }
            MichelineNode<?, ?> node = value.iterator().next();
            jo = new JSONObject();
            jo.put(MichelineConstants.PRIM, type);
            jo.put(MichelineConstants.ARGS, new JSONObject[]{node.getNodeTypeJson()});
            this.typeJo = jo;
        }
        return jo;
    }

    @Override
    public Collection<?> getValue() {
        return this.value;
    }
}
