// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.micheline;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class StorageElement {

    public enum Type { ARRAY, ELT, INT, STRING, BOOLEAN, BYTES }

    private Type type;

    protected StorageElement () {
        super();
    }

    public void setType (Type type) {
        this.type = type;
    }

    public Type getType () {
        return type;
    }

    @Override
    public String toString () {
        return ReflectionToStringBuilder.toString(this);
    }
}