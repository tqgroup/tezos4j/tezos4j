package org.ej4tezos.utils.micheline;

import org.ej4tezos.papi.model.MichelineConstants;
import org.json.JSONObject;

public class StringNode implements MichelineNode<String, JSONObject> {
    private final String value;
    private final String michelson;
    private final String type;
    private JSONObject jo;
    private JSONObject typeJo;

    public StringNode(String value) {
        this(value, MichelineConstants.STRING);
    }

    public StringNode(String value, String type) {
        this.value = value;
        this.michelson = value;
        this.type = validateValue(value, type);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public JSONObject getNodeJson() {
        JSONObject jo = this.jo;
        if (jo == null) {
            jo = new JSONObject();
            jo.put(MichelineConstants.STRING, michelson);
            this.jo = jo;
        }
        return jo;
    }

    @Override
    public JSONObject getNodeTypeJson() {
        JSONObject jo = typeJo;
        if (jo == null) {
            jo = new JSONObject();
            jo.put(MichelineConstants.PRIM, type);
            typeJo = jo;
        }
        return jo;
    }

    private String validateValue(String value, String type) {
        switch (type) {
            case MichelineConstants.ADDRESS:
            case MichelineConstants.TIMESTAMP:
            case MichelineConstants.STRING:
                break;
            default:
                throw new IllegalArgumentException(String.format("'%s' is not an integer type", type));
        }
        return type;
    }
}
