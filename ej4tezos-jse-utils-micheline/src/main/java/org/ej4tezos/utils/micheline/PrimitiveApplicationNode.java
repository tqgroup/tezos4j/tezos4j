package org.ej4tezos.utils.micheline;

import org.ej4tezos.model.*;
import org.ej4tezos.papi.model.MichelineConstants;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

public class PrimitiveApplicationNode implements MichelineNode<PrimitiveApplicationNode, JSONObject> {
    private final String primitive;
    private final List<MichelineNode<?, ?>> args;
    private final List<String> annots;
    private JSONObject jo;
    private JSONObject typeJo;

    public PrimitiveApplicationNode(String primitive) {
        this(primitive, (Object[]) null, null);
    }

    public PrimitiveApplicationNode(String primitive, Object[] args) {
        this(primitive, args != null ? Arrays.asList(args) : null);
    }

    public PrimitiveApplicationNode(String primitive, Object[] args, String[] annots) {
        this(primitive, args != null ? Arrays.asList(args) : null, args != null ? Arrays.asList(annots) : null);
    }

    public PrimitiveApplicationNode(String primitive, List<?> args) {
        this(primitive, args, null);
    }

    public PrimitiveApplicationNode(String primitive, List<?> args, List<String> annots) {
        this.primitive = primitive;
        this.args = toMichelineNodes(args);
        this.annots = null;
    }

    public static MichelineNode<?, ?> toMichelineNode(Object o) {
        if (o instanceof MichelineNode<?, ?>) {
            return (MichelineNode<?, ?>) o;
        } else if (o instanceof Integer || o instanceof Long || o instanceof Short || o instanceof Byte ||
                o instanceof BigInteger) {
            return new IntegerNode(((Number) o).longValue());
        } else if (o instanceof Mutez) {
            return new IntegerNode(((Mutez) o).getValue(), MichelineConstants.MUTEZ);
        } else if (o instanceof Natural) {
            return new IntegerNode(((Natural) o).getValue(), MichelineConstants.NAT);
        } else if (o instanceof String) {
            return new StringNode((String) o);
        } else if (o instanceof Boolean) {
            return new BoolNode((Boolean) o);
        } else if (o instanceof byte[]) {
            return new BytesNode((byte[]) o);
        } else if (o instanceof TezosBytes) {
            return BytesNode.fromTezosBytes((TezosBytes) o);
        } else if (o instanceof TezosAddress) {
            return new StringNode(((TezosAddress) o).getValue(), MichelineConstants.ADDRESS);
        } else if (o instanceof TezosPublicKey) {
            return new StringNode(((TezosPublicKey) o).getValue(), MichelineConstants.KEY);
        } else if (o instanceof TezosSignature) {
            return new StringNode(((TezosSignature) o).getValue(), MichelineConstants.SIGNATURE);
        } else if (o instanceof Optional) {
            throw new UnsupportedOperationException();
        } else if (o instanceof List) {
            return new SequenceNode((List<?>) o);
        } else if (o instanceof Set) {
            return new SequenceNode((Set<?>) o);
        } else if (o instanceof Map) {
            return new MappingNode((Map<?, ?>) o);
        }
        throw new IllegalArgumentException();
    }

    public static List<MichelineNode<?, ?>> toMichelineNodes(Collection<?> objs) {
        return objs.stream().map(PrimitiveApplicationNode::toMichelineNode).collect(Collectors.toList());
    }

    @Override
    public PrimitiveApplicationNode getValue() {
        return this;
    }

    @Override
    public JSONObject getNodeJson() {
        JSONObject jo = this.jo;
        if (jo == null) {
            jo = new JSONObject();
            jo.put(MichelineConstants.PRIM, primitive);
            if (args != null && args.size() > 0) {
                jo.put(MichelineConstants.ARGS, args);
            }
            if (annots != null && annots.size() > 0) {
                jo.put(MichelineConstants.ANNOTS, annots);
            }
            this.jo = jo;
        }
        return jo;
    }

    @Override
    public JSONObject getNodeTypeJson() {
        JSONObject jo = this.typeJo;
        if (jo == null) {
            jo = new JSONObject();
            jo.put(MichelineConstants.PRIM, primitive);
            if (args != null && args.size() > 0) {
                jo.put(MichelineConstants.ARGS,
                        args.stream().map(MichelineNode::getNodeTypeJson).collect(Collectors.toList()));
            }
            this.typeJo = jo;
        }
        return jo;
    }
}
