// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.micheline;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class StorageMapElement extends StorageElement {

    private StorageElement key;
    private StorageElement value;

    public static StorageMapElement toStorageMapElement (StorageElement key, StorageElement value) {
        StorageMapElement element = new StorageMapElement();
        element.setType(Type.ELT);
        element.key = key;
        element.value = value;
        return element;
    }

    protected StorageMapElement () {
        super();
    }

    public StorageElement getKey () {
        return key;
    }

    public StorageElement getValue () {
        return value;
    }
}