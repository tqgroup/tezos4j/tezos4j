// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.micheline;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.Map;

import java.math.BigInteger;

import java.text.MessageFormat;

import java.lang.reflect.Field;

import org.ej4tezos.api.model.TezosBigMapStorage;
import org.ej4tezos.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.api.TezosContractStorageData;

import org.ej4tezos.api.exception.TezosException;

import org.ej4tezos.api.annotation.TezosNativeStorage;

import static org.ej4tezos.utils.asserts.Asserts.checkState;
import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

import static org.ej4tezos.utils.micheline.StorageElement.Type.*;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class StorageMapper {

    private static final Logger LOG = LoggerFactory.getLogger(StorageMapper.class);

    public static void map (TezosContractStorageData storage, Map<String,StorageElement> storageElementMap) throws TezosException {

        // Log the call:
        LOG.debug("map (storage = {}, storageElementMap = {})", storage, storageElementMap);

        // Sanity check:
        checkNotNull(storage, "storage should not be null", TezosException.class);
        checkNotNull(storageElementMap, "storageElementMap should not be null", TezosException.class);

        try {

            LOG.debug("Storage element map keys: {}", storageElementMap.keySet());

            Field[] fields = storage.getClass().getFields();

            for(Field field : fields) {

                // Check if it is a mappable field:
                if(field.getAnnotationsByType(TezosNativeStorage.class).length == 0) {
                    LOG.debug("Field {} is not mappable; skip!", field);
                    continue;
                }

                //
                String fieldName = field.getName();
                LOG.debug("Mapping field {}...", fieldName);

                //
                if(!storageElementMap.containsKey(fieldName)) {
                    LOG.warn("Cannot find field {} in the storage element map!", fieldName);
                    continue;
                }

                // Retrieve the storage element for the specified field name:
                StorageElement storageElement = storageElementMap.get(fieldName);

                // Map a specific field:
                map(storage, field, storageElement);
            }
        }

        catch (Exception ex) {
            String message = MessageFormat.format("", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    public static void map (TezosContractStorageData storage, Field field, StorageElement storageElement) throws TezosException {

        // Log the call:
        LOG.debug("map (storage = {}, field = {}, storageElement = {})", storage, field, storageElement);

        // Sanity check:
        checkNotNull(field, "field should not be null", TezosException.class);
        checkNotNull(storage, "storage should not be null", TezosException.class);
        checkNotNull(storageElement, "storageElement should not be null", TezosException.class);

        // Reject the field if found non mappable:
        if(field.getAnnotationsByType(TezosNativeStorage.class).length == 0 &&
           field.getType() != TezosBigMapStorage.class) {
            String message = MessageFormat.format("Field {0} is not mappable!", field.getName());
            throw new TezosException(message);
        }

        try {

            if(field.getType() == String.class && storageElement.getType() == STRING) {
                field.set(storage, toString(storageElement));
            }
            else if(field.getType() == TezosTimestamp.class && storageElement.getType() == STRING) {
                field.set(storage, TezosTimestamp.toTezosTimestamp(toString(storageElement)));
            }
            else if(field.getType() == TezosAddress.class && storageElement.getType() == STRING) {
                field.set(storage, TezosAddress.toTezosAddress(toString(storageElement)));
            }
            else if(field.getType() == TezosPublicKey.class && storageElement.getType() == STRING) {
                field.set(storage, TezosPublicKey.toTezosPublicKey(toString(storageElement)));
            }
            else if(field.getType() == TezosSignature.class && storageElement.getType() == STRING) {
                field.set(storage, TezosSignature.toTezosSignature(toString(storageElement)));
            }
            else if(field.getType() == TezosBytes.class && storageElement.getType() == BYTES) {
                field.set(storage, TezosBytes.toTezosBytes(toBytes(storageElement)));
            }
            else if(field.getType() == Mutez.class && storageElement.getType() == INT) {
                field.set(storage, Mutez.toMutez(toBigInteger(storageElement)));
            }
            else if(field.getType().equals(BigInteger.class) && storageElement.getType() == INT) {
                field.set(storage, toBigInteger(storageElement));
            }
            else if(field.getType().equals(Boolean.class) && storageElement.getType() == BOOLEAN) {
                field.set(storage, toBoolean(storageElement));
            }
            else if(field.getType().equals(TezosBigMap.class) && storageElement.getType() == INT) {
                TezosBigMap bigMap = new TezosBigMap(toBigInteger(storageElement));
                field.set(storage, bigMap);
                Field accessorField = field.getDeclaringClass().getField(field.getName() + "Accessor");
                accessorField.set(storage, new TezosBigMapStorage<>());
            }
            else {

                String message
                    = MessageFormat
                        .format("Cannot map {0} to {1} (class={2})!",
                                storageElement.getType(),
                                field.getName(),
                                field.getType());

                throw new TezosException(message);
            }
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred when mapping a field: {0}", ex.getMessage());
            throw new TezosException(message, ex);
        }
    }

    public static String toBytes (StorageElement storageElement) throws TezosException {
        checkNotNull(storageElement, "storageElement should not be null", TezosException.class);
        checkState(storageElement.getType() == BYTES, "storageElement should be a bytes", TezosException.class);
        return (String) ((StorageSimpleElement) storageElement).getValue();
    }

    public static String toString (StorageElement storageElement) throws TezosException {
        checkNotNull(storageElement, "storageElement should not be null", TezosException.class);
        checkState(storageElement.getType() == STRING, "storageElement should be a String", TezosException.class);
        return (String) ((StorageSimpleElement) storageElement).getValue();
    }

    public static long toLong (StorageElement storageElement) throws TezosException {
        checkNotNull(storageElement, "storageElement should not be null", TezosException.class);
        checkState(storageElement.getType() == INT, "storageElement should be a Integer", TezosException.class);
        return ((BigInteger) ((StorageSimpleElement) storageElement).getValue()).longValue();
    }

    public static BigInteger toBigInteger (StorageElement storageElement) throws TezosException {
        checkNotNull(storageElement, "storageElement should not be null", TezosException.class);
        checkState(storageElement.getType() == INT, "storageElement should be a Integer", TezosException.class);
        return (BigInteger) ((StorageSimpleElement) storageElement).getValue();
    }

    public static Boolean toBoolean (StorageElement storageElement) throws TezosException {
        checkNotNull(storageElement, "storageElement should not be null", TezosException.class);
        checkState(storageElement.getType() == BOOLEAN, "storageElement should be a Boolean", TezosException.class);
        return (Boolean) ((StorageSimpleElement) storageElement).getValue();
    }
}