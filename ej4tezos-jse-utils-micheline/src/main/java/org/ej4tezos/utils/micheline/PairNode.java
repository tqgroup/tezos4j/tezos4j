package org.ej4tezos.utils.micheline;

import org.ej4tezos.papi.model.MichelineConstants;
import org.json.JSONObject;

public class PairNode implements MichelineNode<PairNode, JSONObject> {
    private final MichelineNode<?, ?> left;
    private final MichelineNode<?, ?> right;
    private JSONObject jo;
    private JSONObject typeJo;

    public PairNode(Object left, Object right) {
        this.left = PrimitiveApplicationNode.toMichelineNode(left);
        this.right = PrimitiveApplicationNode.toMichelineNode(right);
    }

    @Override
    public PairNode getValue() {
        return this;
    }

    @Override
    public JSONObject getNodeJson() {
        JSONObject jo = this.jo;
        if (jo == null) {
            jo = new JSONObject();
            jo.put(MichelineConstants.PRIM, MichelineConstants.PAIR);
            jo.put(MichelineConstants.ARGS, new Object[]{left.getNodeJson(), right.getNodeJson()});
            this.jo = jo;
        }
        return jo;
    }

    @Override
    public JSONObject getNodeTypeJson() {
        JSONObject jo = this.typeJo;
        if (jo == null) {
            jo = new JSONObject();
            jo.put(MichelineConstants.PRIM, MichelineConstants.PAIR_TYPE);
            jo.put(MichelineConstants.ARGS, new JSONObject[]{left.getNodeTypeJson(), right.getNodeTypeJson()});
            this.typeJo = jo;
        }
        return jo;
    }
}
