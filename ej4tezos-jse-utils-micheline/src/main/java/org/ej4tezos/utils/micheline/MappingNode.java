package org.ej4tezos.utils.micheline;

import org.ej4tezos.papi.model.MichelineConstants;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class MappingNode implements MichelineNode<Map<MichelineNode<?, ?>, MichelineNode<?, ?>>, JSONArray> {
    private final Map<MichelineNode<?, ?>, MichelineNode<?, ?>> value;
    private JSONArray ja;
    private JSONObject typeJo;

    MappingNode(Map<?, ?> value) {
        this.value = new HashMap<>(value.size());
        for (Map.Entry<?, ?> e : value.entrySet()) {
            this.value.put(PrimitiveApplicationNode.toMichelineNode(e.getKey()),
                    PrimitiveApplicationNode.toMichelineNode(e.getValue()));
        }
    }

    @Override
    public JSONArray getNodeJson() {
        JSONArray ja = this.ja;
        if (ja == null) {
            ja = new JSONArray();
            for (Map.Entry<MichelineNode<?, ?>, MichelineNode<?, ?>> entry : this.value.entrySet()) {
                JSONObject jo = new JSONObject();
                jo.put(MichelineConstants.PRIM, MichelineConstants.ELT);
                jo.put(MichelineConstants.ARGS, new Object[]{
                        entry.getKey().getNodeJson(),
                        entry.getValue().getNodeJson()
                });
                ja.put(jo);
            }
            this.ja = ja;
        }
        return ja;
    }

    @Override
    public JSONObject getNodeTypeJson() {
        JSONObject jo = this.typeJo;
        if (jo == null) {
            if (value.isEmpty()) {
                throw new RuntimeException("type information not available");
            }
            Map.Entry<MichelineNode<?, ?>, MichelineNode<?, ?>> entry = value.entrySet().iterator().next();
            jo = new JSONObject();
            jo.put(MichelineConstants.PRIM, MichelineConstants.MAP);
            jo.put(MichelineConstants.ARGS, new JSONObject[]{
                    entry.getKey().getNodeTypeJson(),
                    entry.getValue().getNodeTypeJson()
            });
        }
        this.typeJo = jo;

        return jo;
    }

    @Override
    public Map<MichelineNode<?, ?>, MichelineNode<?, ?>> getValue() {
        return this.value;
    }
}
