package org.ej4tezos.utils.micheline;

import org.ej4tezos.papi.model.MichelineConstants;
import org.json.JSONObject;

public abstract class PrimitiveNode<V> implements MichelineNode<V, JSONObject> {
    private final String michelson;
    private final String type;
    private JSONObject jo;
    private JSONObject typeJo;

    public PrimitiveNode(String michelson, String type) {
        this.michelson = michelson;
        this.type = type;
    }

    @Override
    public JSONObject getNodeJson() {
        JSONObject jo = this.jo;
        if (jo == null) {
            jo = new JSONObject();
            jo.put(MichelineConstants.PRIM, michelson);
            this.jo = jo;
        }
        return jo;
    }

    @Override
    public JSONObject getNodeTypeJson() {
        JSONObject jo = typeJo;
        if (jo == null) {
            jo = new JSONObject();
            jo.put(MichelineConstants.PRIM, type);
            typeJo = jo;
        }
        return jo;
    }
}
