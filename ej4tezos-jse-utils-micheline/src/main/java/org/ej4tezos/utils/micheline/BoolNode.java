package org.ej4tezos.utils.micheline;

import org.ej4tezos.papi.model.MichelineConstants;

public class BoolNode extends PrimitiveNode<Boolean> {
    private final Boolean value;

    public BoolNode(Boolean value) {
        super(value ? "True" : "False", MichelineConstants.BOOL);
        this.value = value;
    }

    @Override
    public Boolean getValue() {
        return this.value;
    }
}
