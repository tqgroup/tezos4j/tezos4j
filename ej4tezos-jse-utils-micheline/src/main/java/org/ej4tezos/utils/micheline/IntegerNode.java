package org.ej4tezos.utils.micheline;

import org.ej4tezos.papi.model.MichelineConstants;
import org.json.JSONObject;

import java.math.BigInteger;

public class IntegerNode implements MichelineNode<BigInteger, JSONObject> {
    private final BigInteger value;
    private final String michelson;
    private final String type;
    private JSONObject jo;
    private JSONObject typeJo;

    public IntegerNode(BigInteger value) {
        this(value, MichelineConstants.INT);
    }

    public IntegerNode(int value) {
        this(value, MichelineConstants.INT);
    }

    public IntegerNode(int value, String type) {
        this(BigInteger.valueOf(value), type);
    }

    public IntegerNode(long value) {
        this(value, MichelineConstants.INT);
    }

    public IntegerNode(long value, String type) {
        this(BigInteger.valueOf(value), type);
    }

    public IntegerNode(BigInteger value, String type) {
        this.value = value;
        this.michelson = value.toString();
        this.type = validateValue(value, type);
    }

    @Override
    public BigInteger getValue() {
        return this.value;
    }

    @Override
    public JSONObject getNodeJson() {
        JSONObject jo = this.jo;
        if (jo == null) {
            jo = new JSONObject();
            jo.put(MichelineConstants.INT, michelson);
            this.jo = jo;
        }
        return jo;
    }

    @Override
    public JSONObject getNodeTypeJson() {
        JSONObject jo = typeJo;
        if (jo == null) {
            jo = new JSONObject();
            jo.put(MichelineConstants.PRIM, type);
            typeJo = jo;
        }
        return jo;
    }

    private String validateValue(BigInteger value, String type) {
        switch (type) {
            case MichelineConstants.TIMESTAMP:
            case MichelineConstants.NAT:
                if (value.signum() == -1) {
                    throw new IllegalArgumentException(String.format("%d is not a '%s'", value, type));
                }
                break;
            case MichelineConstants.MUTEZ:
                int bits = value.bitLength() + (value.signum() < 0 ? 1 : 0);
                if (bits > 64) {
                    throw new IllegalArgumentException(String.format("%d is not a '%s'", value, type));
                }
                break;
            case MichelineConstants.INT:
                break;
            default:
                throw new IllegalArgumentException(String.format("'%s' is not an integer type", type));
        }
        return type;
    }
}
