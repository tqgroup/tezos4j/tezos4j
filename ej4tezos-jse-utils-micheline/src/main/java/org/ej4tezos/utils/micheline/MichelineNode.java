package org.ej4tezos.utils.micheline;

import org.json.JSONObject;

/**
 * Encapsulates API of a Micheline `node`, which is either a literal, literal sequence or a primitive application.
 * Additionally, a Micheline node may have to carry a type information in cases where type information is not available.
 * When used as parameter to a contract invocation, type information is encoded in the contract. When hashing the data,
 * however, type information is not available and has to be supplied.
 */
public interface MichelineNode<V,T> {
    /**
     * Returns the value of this node
     *
     * @return node value
     */
    V getValue();

    /**
     * Returns the JSON object or array representing this Micheline node
     *
     * @return json object or array
     */
    T getNodeJson();

    /**
     * Returns the JSON object or array representing type of this node
     *
     * @return json object or array
     */
    JSONObject getNodeTypeJson();
}
