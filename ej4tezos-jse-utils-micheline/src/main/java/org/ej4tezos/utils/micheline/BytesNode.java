package org.ej4tezos.utils.micheline;

import org.ej4tezos.model.TezosBytes;
import org.ej4tezos.papi.model.MichelineConstants;
import org.json.JSONObject;

public class BytesNode implements MichelineNode<byte[], JSONObject> {
    private final byte[] value;
    private final String michelson;
    private JSONObject jo;
    private JSONObject typeJo;

    public BytesNode(byte[] bytes) {
        this.value = bytes;
        this.michelson = toHex(bytes);
    }

    public static BytesNode fromTezosBytes(TezosBytes bytes) {
        String stringBytes = bytes.getValue();

        byte[] val = new byte[stringBytes.length() / 2];
        for (int i = 0; i < val.length; i++) {
            int index = i * 2;
            int j = Integer.parseInt(stringBytes.substring(index, index + 2), 16);
            val[i] = (byte) j;
        }

        return new BytesNode(val);
    }

    @Override
    public byte[] getValue() {
        return this.value;
    }

    @Override
    public JSONObject getNodeJson() {
        JSONObject jo = this.jo;
        if (jo == null) {
            jo = new JSONObject();
            jo.put(MichelineConstants.BYTES, michelson);
            this.jo = jo;
        }
        return jo;
    }

    @Override
    public JSONObject getNodeTypeJson() {
        JSONObject jo = typeJo;
        if (jo == null) {
            jo = new JSONObject();
            jo.put(MichelineConstants.PRIM, MichelineConstants.BYTES);
            typeJo = jo;
        }
        return jo;
    }

    private String toHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for (byte b : bytes) {
            sb.append(Character.forDigit((b >> 4) & 0xF, 16));
            sb.append(Character.forDigit(b & 0xF, 16));
        }
        return sb.toString();
    }
}
