// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.micheline;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class StorageSimpleElement extends StorageElement {

    private Object value;

    public static StorageSimpleElement toStorageSimpleElement (Type type, Object value) {
        StorageSimpleElement element = new StorageSimpleElement();
        element.setType(type);
        element.value = value;
        return element;
    }

    protected StorageSimpleElement () {
        super();
    }

    public Object getValue () {
        return value;
    }

    @Override
    public String toString () {
        return ReflectionToStringBuilder.toString(this);
    }
}