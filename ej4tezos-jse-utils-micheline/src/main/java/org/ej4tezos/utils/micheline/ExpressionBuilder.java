package org.ej4tezos.utils.micheline;

import org.ej4tezos.api.exception.TezosException;

import java.util.Arrays;
import java.util.List;

public class ExpressionBuilder {
    private ExpressionBuilder() {
    }

    public static MichelineNode<?, ?> getExpression(Object... expression) throws TezosException {
        List<Object> expressionList = Arrays.asList(expression);

        if (expressionList.size() == 1) {
            return PrimitiveApplicationNode.toMichelineNode(expressionList.get(0));
        } else if (expressionList.size() == 2) {
            return new PairNode(expressionList.get(0), expressionList.get(1));
        } else {
            return new PairNode(
                    getExpression(expressionList.subList(0, expressionList.size() / 2)),
                    getExpression(expressionList.subList(expressionList.size() / 2, expressionList.size())));
        }
    }
}
