// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.micheline;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.api.exception.TezosException;

import static org.ej4tezos.papi.model.MichelineConstants.*;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class MichelineParser {

    private static final Logger LOG = LoggerFactory.getLogger(MichelineParser.class);

    private static final String UNEXPECTED_TOKEN = "Unexpected Token";

    public static Object parseExpr (List<JSONObject> parsedSRC, JSONObject tok) throws TezosException {

        switch (tok.get("type").toString()) {
            case "IDENT":
                return new JSONObject().put(PRIM, parsedSRC.get(0).get(VALUE));
            case "NUMBER":
                return new JSONObject().put(INT, parsedSRC.get(0).get(VALUE));
            case "STRING":
                return new JSONObject().put(STRING, parsedSRC.get(0).get(VALUE));
            case "BYTES":
                return new JSONObject().put(BYTES, parsedSRC.get(0).getString(VALUE).substring(2));
            case "(":
                return parseList(parsedSRC);
            case "{":
                return parseSequence(parsedSRC, null, true);
            default:
                throw new TezosException(UNEXPECTED_TOKEN);
        }
    }

    private static JSONObject parseList (List<JSONObject> parsedSrc) throws TezosException {
        parsedSrc.remove(0);
        JSONObject ret = new JSONObject();

        ret.put(PRIM, parsedSrc.get(0).getString(VALUE));

        for ( ; ; ) {
            parsedSrc.remove(0);

            if (parsedSrc.get(0).getString(VALUE).equals(")")) {
                break;
            }
            if (!ret.has(ARGS)) {
                JSONArray args = new JSONArray();
                ret.put(ARGS, args);
            }

            Object parsedExpr = parseExpr(parsedSrc, parsedSrc.get(0));

            if (ret.get(ARGS) instanceof JSONObject) {
                ret.getJSONObject(ARGS).put(ARGS, parsedExpr);
            } else if (ret.get(ARGS) instanceof JSONArray) {
                ret.getJSONArray(ARGS).put(parsedExpr);
            }
        }
        return ret;
    }

    private static JSONArray parseSequence (List<JSONObject> parsedSrc, JSONObject initialToken, boolean expectBracket) throws TezosException {
        JSONArray seq = new JSONArray();
        for ( ; ; ) {
            JSONObject tok;
            if (initialToken != null) {
                initialToken = null;
            } else {
                parsedSrc.remove(0);
                if (parsedSrc.isEmpty()) {
                    if (expectBracket) {
                        throw new TezosException("Unexpected EOF");
                    } else {
                        return seq;
                    }
                }
            }
            tok = parsedSrc.get(0);

            if (tok.get(VALUE).toString().equals("}")) {
                if (!expectBracket) {
                    throw new TezosException("Seq: " + UNEXPECTED_TOKEN);
                } else {
                    return seq;
                }
            } else if (tok.get(TYPE).toString().equals("IDENT")) {
                List<Object> p = parseArgs(parsedSrc, tok, expectBracket);
                seq.put(p.get(0));
                if (p.get(1) == Boolean.TRUE) {
                    return seq;
                }
            } else {
                seq.put(parseExpr(parsedSrc, tok));

                parsedSrc.remove(0);

                if (parsedSrc.isEmpty()) {
                    if (expectBracket) {
                        throw new TezosException(UNEXPECTED_TOKEN);
                    } else {
                        return seq;
                    }
                } else if (parsedSrc.get(0).get(TYPE).toString().equals("}")) {
                    if (!expectBracket) {
                        throw new TezosException(UNEXPECTED_TOKEN);
                    } else {
                        return seq;
                    }
                } else if (!parsedSrc.get(0).get(TYPE).toString().equals(";")) {
                    throw new TezosException(UNEXPECTED_TOKEN);
                }
            }
        }
    }

    private static List<Object> parseArgs (List<JSONObject> parsedSrc, JSONObject initialToken, boolean expectBracket) throws TezosException {

        JSONObject p = new JSONObject();

        p.put(PRIM, initialToken.get(VALUE).toString());

        for (; ; ) {
            parsedSrc.remove(0);
            if (parsedSrc.isEmpty()) {
                if (expectBracket) {
                    throw new TezosException("Unexpected errEOF");
                } else {
                    List<Object> res = new ArrayList<>();
                    res.add(p);
                    res.add(true);
                    return res;
                }
            } else if (parsedSrc.get(0).get(TYPE).toString().equals("}")) {
                if (!expectBracket) {
                    throw new TezosException(UNEXPECTED_TOKEN);
                } else {
                    List<Object> res = new ArrayList<>();
                    res.add(p);
                    res.add(true);
                    return res;
                }
            } else if (parsedSrc.get(0).get(TYPE).toString().equals(";")) {
                List<Object> res = new ArrayList<>();
                res.add(p);
                res.add(false);
                return res;
            }

            if (!p.has(ARGS)) {
                JSONArray args = new JSONArray();
                p.put(ARGS, args);
            }

            Object parsedExpr = parseExpr(parsedSrc, parsedSrc.get(0));

            if (p.get(ARGS) instanceof JSONObject) {
                p.getJSONObject(ARGS).put(ARGS, parsedExpr);
            } else if (p.get(ARGS) instanceof JSONArray) {
                p.getJSONArray(ARGS).put(parsedExpr);
            }
        }
    }
}