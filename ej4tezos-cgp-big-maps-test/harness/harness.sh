#!/bin/bash -eEu

docker kill tezos-sandbox 2>/dev/null || true
SANDBOX="$(docker run --rm --name tezos-sandbox --detach --network tezos tqtezos/flextesa:20201214 delphibox start)"
sleep 5
tezos-client -A tezos-sandbox -P 20000 config update
tezos-client bootstrapped
tezos-client import secret key alice unencrypted:edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq
tezos-client import secret key bob unencrypted:edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt

tezos-client originate contract big_maps transferring 10 from alice running big_maps.tz \
             --init '(Pair { Elt "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" 5 ; Elt "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6" 10 }{ Elt "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" True; Elt "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6" False })' \
             --burn-cap 1 --force

CONTRACT_ID="$(tezos-client list known contracts | grep 'big_maps: ' | sed 's/big_maps\: //')"
ALICE_ID="$(tezos-client list known contracts | grep 'alice: ' | sed 's/alice\: //')"

source $1

docker kill $SANDBOX
