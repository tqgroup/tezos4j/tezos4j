// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.big_maps.test;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.Date;

import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.model.TezosPrivateKey;
import org.ej4tezos.model.TezosContractAddress;

import org.ej4tezos.contract.BigMaps;
import org.ej4tezos.contract.BigMapsHelper;
import org.ej4tezos.contract.BigMapsStorage;

import org.ej4tezos.papi.TezosContractProxy;
import org.ej4tezos.papi.TezosContractProxyFactoryContext;

import org.ej4tezos.proxy.TezosContractProxyFactoryContextImpl;

import org.ej4tezos.api.TezosContractStorage;
import org.ej4tezos.api.model.TezosTransactionHash;
import org.ej4tezos.api.exception.TezosStorageException;

import static org.ej4tezos.model.TezosPrivateKey.toTezosPrivateKey;
import static org.ej4tezos.model.TezosPublicAddress.toTezosPublicAddress;
import static org.ej4tezos.model.TezosContractAddress.toTezosContractAddress;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class Main {

    private static Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main (String [] args) {

        TezosContractStorage.Mode storageMode = TezosContractStorage.Mode.CACHED;
        TezosContractProxy.Mode contractMode = TezosContractProxy.Mode.ASYNCHRONOUS;

        try {

            if(args.length < 2) {
                LOG.error("Please specify the URL, contract ID, contract mode (ASYNCHRONOUS|SYNCHRONOUS) and storage mode (CACHED|REAL_TIME)!");
                return;
            }

            //
            TezosContractProxyFactoryContext context
                = new TezosContractProxyFactoryContextImpl(args[0]);

            //
            TezosPrivateKey adminPrivateKey = toTezosPrivateKey(args[1]);
            TezosContractAddress tezosContractAddress = toTezosContractAddress(args[2]);

            // Set the modes if applicable:
            if(args.length > 3) {
                contractMode = TezosContractProxy.Mode.valueOf(args[3].toUpperCase());
            }

            if(args.length > 4) {
                storageMode = TezosContractStorage.Mode.valueOf(args[4].toUpperCase());
            }

            // Create the proxies:
            BigMaps bigMaps = BigMapsHelper.createProxy(context, tezosContractAddress, adminPrivateKey, contractMode);
            BigMapsStorage bigMapsStorage = BigMapsHelper.createStorageProxy(context, tezosContractAddress, storageMode);

            // Display the contract details:
            LOG.info("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
            LOG.info("BigMaps' Contract:");
            LOG.info("address:       {}", bigMaps.getContractAddress());
            LOG.info("contract mode: {}", bigMaps.getMode());
            LOG.info("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
            LOG.info("BigMaps' Storage:");
            Date timestamp = new Date(bigMapsStorage.getTimestamp());
            LOG.info("timestamp:     {} [{}ms]", timestamp, timestamp.getTime());
            LOG.info("mode:          {}", bigMapsStorage.getMode());
            LOG.info("balances:      {}", bigMapsStorage.getBalances());
            LOG.info("authorized:    {}", bigMapsStorage.getAuthorized());
            LOG.info("data:          {}", bigMapsStorage.getData());
            LOG.info("balance alice  {}", bigMapsStorage.getBalances(
                    toTezosPublicAddress("tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb")));
            LOG.info("data:          {}", bigMapsStorage.getData());
            LOG.info("balance alice (cached) {}", bigMapsStorage.getBalances(
                    toTezosPublicAddress("tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb")));
            LOG.info("data:          {}", bigMapsStorage.getData());
            LOG.info("balance bob    {}", bigMapsStorage.getBalances(
                    toTezosPublicAddress("tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6")));
            LOG.info("data:          {}", bigMapsStorage.getData());
            LOG.info("authorized alice {}", bigMapsStorage.getAuthorized(
                    toTezosPublicAddress("tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb")));
            LOG.info("data:          {}", bigMapsStorage.getData());
            LOG.info("authorized bob   {}", bigMapsStorage.getAuthorized(
                    toTezosPublicAddress("tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6")));
            LOG.info("data:          {}", bigMapsStorage.getData());
            LOG.info("authorized bob (cached) {}", bigMapsStorage.getAuthorized(
                    toTezosPublicAddress("tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6")));
            LOG.info("data:          {}", bigMapsStorage.getData());
            try {
                LOG.info("balance unknown {}", bigMapsStorage.getBalances(
                        toTezosPublicAddress("tz1MwD9RhFGD8DN5PmBg3aKJPcVr5AoF8LmX")));
            }
            catch(TezosStorageException e) {
                LOG.error("Failed to retrieve a balance", e);
            }
        }

        catch (Exception ex) {
            LOG.warn("An exception occurred: {}", ex.getMessage(), ex);
        }
    }

    @Override
    public String toString () {
    	return "Main";
    }
}