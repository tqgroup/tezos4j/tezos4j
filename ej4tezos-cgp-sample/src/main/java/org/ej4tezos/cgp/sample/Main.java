// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.sample;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.Date;

import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.model.TezosPrivateKey;
import org.ej4tezos.model.TezosContractAddress;

import org.ej4tezos.contract.EuroTz;
import org.ej4tezos.contract.EuroTzHelper;
import org.ej4tezos.contract.EuroTzStorage;

import org.ej4tezos.papi.TezosContractProxy;
import org.ej4tezos.papi.TezosContractProxyFactoryContext;

import org.ej4tezos.proxy.TezosContractProxyFactoryContextImpl;

import org.ej4tezos.api.TezosContractStorage;

import org.ej4tezos.api.model.TezosTransactionHash;

import static org.ej4tezos.model.TezosPrivateKey.toTezosPrivateKey;
import static org.ej4tezos.model.TezosPublicAddress.toTezosPublicAddress;
import static org.ej4tezos.model.TezosContractAddress.toTezosContractAddress;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class Main {

    private static Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main (String [] args) {

        TezosContractStorage.Mode storageMode = TezosContractStorage.Mode.CACHED;
        TezosContractProxy.Mode contractMode = TezosContractProxy.Mode.ASYNCHRONOUS;

        try {

            if(args.length < 2) {
                LOG.error("Please specify the URL, private key, contract mode (ASYNCHRONOUS|SYNCHRONOUS) and storage mode (CACHED|REAL_TIME)!");
                return;
            }

            //
            TezosContractProxyFactoryContext context
                = new TezosContractProxyFactoryContextImpl(args[0]);

            //
            TezosPrivateKey adminPrivateKey = toTezosPrivateKey(args[1]);
            TezosContractAddress tezosContractAddress = toTezosContractAddress("KT1GVGz2YwuscuN1MEtocf45Su4xomQj1K8z");

            // Set the modes if applicable:
            if(args.length > 2) {
                contractMode = TezosContractProxy.Mode.valueOf(args[2].toUpperCase());
            }

            if(args.length > 3) {
                storageMode = TezosContractStorage.Mode.valueOf(args[3].toUpperCase());
            }

            // Create the proxies:
            EuroTz euroTz = EuroTzHelper.createProxy(context, tezosContractAddress, adminPrivateKey, contractMode);
            EuroTzStorage euroTzStorage = EuroTzHelper.createStorageProxy(context, tezosContractAddress, storageMode);

            // Display the contract details:
            LOG.info("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
            LOG.info("EuroTz's Contract:");
            LOG.info("address:       {}", euroTz.getContractAddress());
            LOG.info("contract mode: {}", euroTz.getMode());
            LOG.info("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
            LOG.info("EuroTz's Storage:");
            Date timestamp = new Date(euroTzStorage.getTimestamp());
            LOG.info("timestamp:     {} [{}ms]", timestamp, timestamp.getTime());
            LOG.info("mode:          {}", euroTzStorage.getMode());
            LOG.info("administrator: {}", euroTzStorage.getAdministrator());
            LOG.info("balances:      {}", euroTzStorage.getBalances());
            LOG.info("bigMapData:    {}", euroTzStorage.getBigMapData());
            LOG.info("totalSupply:   {}", euroTzStorage.getTotalSupply());
            LOG.info("data:          {}", euroTzStorage.getData());

            // Invoke the mint method:
            TezosTransactionHash hash
                = euroTz.mint (
                    toTezosPublicAddress("tz1MwD9RhFGD8DN5PmBg3aKJPcVr5AoF8LmX"),
                    BigInteger.valueOf(10000)
            );

            LOG.info("Transaction hash: {}", hash);
        }

        catch (Exception ex) {
            LOG.warn("An exception occurred: {}", ex.getMessage(), ex);
        }
    }

    @Override
    public String toString () {
    	return "Main";
    }
}