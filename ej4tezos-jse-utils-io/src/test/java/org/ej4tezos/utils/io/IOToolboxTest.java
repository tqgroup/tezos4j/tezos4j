// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.io;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.io.Closeable;
import java.io.IOException;

import org.junit.Test;

import org.mockito.Mockito;

import static org.junit.Assert.fail;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.doThrow;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class IOToolboxTest {

    @Test
    public void silentCloseWithNeutralCloseableTest () throws Exception  {

        // Prepare the mock:
        Closeable neutral = Mockito.mock(Closeable.class);

        try {
            // Call the method to test:
            IOToolbox.silentClose(neutral);
        }

        catch (Exception ex) {
            fail("An exception should not have occurred");
        }

        // Check if 'close' was called:
        verify(neutral, times(1)).close();
    }

    @Test
    public void silentCloseWithCloseableThrowingExceptionTest () throws Exception  {

        // Prepare the mock:
        Closeable closeableThrowingException = Mockito.mock(Closeable.class);
        doThrow(new IOException()).when(closeableThrowingException).close();

        try {
            // Call the method to test:
            IOToolbox.silentClose(closeableThrowingException);
        }

        catch (Exception ex) {
            fail("An exception should not have occurred");
        }

        // Check if 'close' was called:
        verify(closeableThrowingException, times(1)).close();
    }

    @Test
    public void silentCloseWithNullCloseableTest () throws Exception  {

        try {
            IOToolbox.silentClose((Closeable) null);
        }

        catch (Exception ex) {
            fail("An exception should not have occurred");
        }
    }
}