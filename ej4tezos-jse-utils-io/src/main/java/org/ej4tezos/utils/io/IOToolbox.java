// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.utils.io;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

import java.io.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class IOToolbox {

    private final static Logger LOG = LoggerFactory.getLogger(IOToolbox.class);

    /**
     * close the specified closeable (even null) without throwing any exception.
     * It is useful for non trivial cases of try-with-resources.
     * @param closeable
     */
    public static void silentClose (Closeable closeable) {

        try {
            if(closeable != null) {
                closeable.close();
            }
        }

        catch (Exception ex) {
            if(!LOG.isDebugEnabled()) {
                LOG.debug("An exception occurred while closing {}: {}", closeable, ex.getMessage());
            }
            else {
                LOG.debug("An exception occurred while closing {}: {}", closeable, ex.getMessage(), ex);
            }
        }
    }

    public static String drainStream(InputStream is) throws IOException {
        StringBuilder result = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = br.readLine()) != null) {
                result.append(line);
            }
        }
        return result.toString();
    }
}