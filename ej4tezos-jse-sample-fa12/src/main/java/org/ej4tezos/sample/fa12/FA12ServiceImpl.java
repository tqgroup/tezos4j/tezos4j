// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.sample.fa12;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.Map;
import java.util.HashMap;

import java.text.MessageFormat;

import javax.annotation.PreDestroy;
import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.api.exception.TezosException;

import org.ej4tezos.model.TezosPublicAddress;

import org.ej4tezos.impl.TezosAbstractService;

import static org.ej4tezos.utils.asserts.Asserts.*;

import org.ej4tezos.impl.model.InvokeOperationResult;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class FA12ServiceImpl extends TezosAbstractService implements FA12Service {

    private static final Logger LOG = LoggerFactory.getLogger(FA12ServiceImpl.class);

    private String mint_runOperationTemplate;
    private String mint_forgeOperationTemplate;
    private String mint_preApplyOperationTemplate;

    private String burn_runOperationTemplate;
    private String burn_forgeOperationTemplate;
    private String burn_preApplyOperationTemplate;

    public FA12ServiceImpl () {
        //
        LOG.debug("A new instance of 'FA12ServiceImpl' got created; logging on {}...", LOG.getName());
    }

    @PostConstruct
    public void init () throws Exception {

        LOG.debug("init()");

        // Load templates:
        mint_runOperationTemplate = loadTemplate("mint_runOperation.template");
        mint_forgeOperationTemplate = loadTemplate("mint_forgeOperation.template");
        mint_preApplyOperationTemplate = loadTemplate("mint_preApplyOperation.template");

        burn_runOperationTemplate = loadTemplate("burn_runOperation.template");
        burn_forgeOperationTemplate = loadTemplate("burn_forgeOperation.template");
        burn_preApplyOperationTemplate = loadTemplate("burn_preApplyOperation.template");

        super.init();

    }

    @PreDestroy
    public void destroy () throws Exception {

        super.destroy();

        LOG.debug("destroy()");

    }

    @Override
    public void mint (long quantity, TezosPublicAddress creditor) throws TezosException {

        // Log the call:
        LOG.debug("mint (quantity = {}, creditor = {})", quantity, creditor);

        // Sanity check:
        checkNotNull(creditor, "creditor should not be null", TezosException.class);

        checkState(quantity > 0, "quantity should be above zero", TezosException.class);
        checkState(creditor.isValid(), "Invalid creditor", TezosException.class);

        try {

            // Prepare the parameters:
            Map<String,Object> parameters = new HashMap<>();
            parameters.put("quantity", quantity);
            parameters.put("creditor", creditor);

            //
            InvokeOperationResult invokeOperationResult
                = invoke(mint_runOperationTemplate, mint_forgeOperationTemplate, mint_preApplyOperationTemplate, parameters);

            LOG.info("invokeOperationResult = {}", invokeOperationResult);

        }

        catch (Exception ex) {
            String exceptionMessage = MessageFormat.format("An exception occurred while minting: {0}", ex.getMessage());
            throw new TezosException(exceptionMessage, ex);
        }
    }

    @Override
    public void burn (long quantity, TezosPublicAddress debtor) throws TezosException {

        // Log the call:
        LOG.debug("burn (quantity = {}, debtor = {})", quantity, debtor);

        // Sanity check:
        checkNotNull(debtor, "debtor should not be null", TezosException.class);

        checkState(quantity > 0, "quantity should be above zero", TezosException.class);
        checkState(debtor.isValid(), "Invalid debtor", TezosException.class);

        try {

            // Prepare the parameters:
            Map<String,Object> parameters = new HashMap<>();
            parameters.put("quantity", quantity);
            parameters.put("debtor", debtor);

            //
            InvokeOperationResult invokeOperationResult
                = invoke(burn_runOperationTemplate, burn_forgeOperationTemplate, burn_preApplyOperationTemplate, parameters);

            LOG.info("invokeOperationResult = {}", invokeOperationResult);

        }

        catch (Exception ex) {
            String exceptionMessage = MessageFormat.format("An exception occurred while minting: {0}", ex.getMessage());
            throw new TezosException(exceptionMessage, ex);
        }
    }

    @Override
    public String toString () {
        return "FA12ServiceImpl";
    }
}