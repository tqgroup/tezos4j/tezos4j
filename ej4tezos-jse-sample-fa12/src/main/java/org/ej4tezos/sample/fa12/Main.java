// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.sample.fa12;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;
import java.util.Arrays;

import java.security.SecureRandom;

import javax.annotation.PreDestroy;
import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.ej4tezos.api.TezosFeeService;
import org.ej4tezos.api.TezosKeyService;
import org.ej4tezos.api.TezosCoreService;

import org.ej4tezos.crypto.impl.TezosKeyServiceImpl;
import org.ej4tezos.crypto.impl.TezosCryptoProviderImpl;

import org.ej4tezos.impl.TezosFeeServiceImpl;
import org.ej4tezos.impl.TezosCoreServiceImpl;
import org.ej4tezos.impl.TezosConnectivityImpl;

import org.ej4tezos.model.TezosContractAddress;

import org.ej4tezos.papi.TezosConnectivity;
import org.ej4tezos.papi.TezosCryptoProvider;

import static org.ej4tezos.utils.asserts.Asserts.*;

import org.ej4tezos.model.TezosPrivateKey;
import org.ej4tezos.model.TezosPublicAddress;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    enum Operation { burn, mint, help }

    private Operation operation;
    private List<String> parameters;
    private FA12Service fa12Service;

    public Main () {
        //
        LOG.debug("A new instance of 'Main' got created; logging on {}...", LOG.getName());
    }

    public void execute () throws Exception {

        LOG.debug("execute()");

        switch (operation) {

            case help:
                showHelp();
                break;

            case mint:
                checkState(parameters.size() == 5, "Invalid parameter size for mint", RuntimeException.class);
                fa12Service.mint(Long.parseLong(parameters.get(4)),
                                 TezosPublicAddress.toTezosPublicAddress(parameters.get(3)));
                break;

            case burn:
                checkState(parameters.size() == 5, "Invalid parameter size for burn", RuntimeException.class);
                fa12Service.burn(Long.parseLong(parameters.get(4)),
                                 TezosPublicAddress.toTezosPublicAddress(parameters.get(3)));
                break;

            default:
                LOG.error("Unexpected operation {}", operation);
                break;
        }
    }

    @PostConstruct
    private void init () throws Exception {

        LOG.debug("init()");

        if(parameters.isEmpty()) {
            operation = Operation.help;
            return;
        }

        operation = Operation.valueOf(parameters.get(0));

        if(parameters.size() == 1) {
            return;
        }

        //
        TezosPrivateKey adminPrivateKey = TezosPrivateKey.toTezosPrivateKey(parameters.get(2));
        TezosContractAddress fa12ContractAddress = TezosContractAddress.toTezosContractAddress(parameters.get(1));

        //
        TezosCryptoProvider tezosCryptoProvider = new TezosCryptoProviderImpl();
        ((TezosCryptoProviderImpl)tezosCryptoProvider).setSecureRandom(new SecureRandom());
        ((TezosCryptoProviderImpl)tezosCryptoProvider).init();

        //
        TezosConnectivity tezosConnectivity = new TezosConnectivityImpl();
        ((TezosConnectivityImpl)tezosConnectivity).setNodeUrl("https://carthagenet.smartpy.io/");
        ((TezosConnectivityImpl)tezosConnectivity).init();

        //
        TezosKeyService tezosKeyService = new TezosKeyServiceImpl();
        ((TezosKeyServiceImpl)tezosKeyService).setTezosCryptoProvider(tezosCryptoProvider);
        ((TezosKeyServiceImpl)tezosKeyService).init();

        //
        TezosCoreService tezosCoreService = new TezosCoreServiceImpl();
        ((TezosCoreServiceImpl)tezosCoreService).setTezosConnectivity(tezosConnectivity);
        ((TezosCoreServiceImpl)tezosCoreService).init();

        //
        TezosFeeService tezosFeeService = new TezosFeeServiceImpl();
        ((TezosFeeServiceImpl)tezosFeeService).init();

        //
        fa12Service = new FA12ServiceImpl();

        //
        ((FA12ServiceImpl)fa12Service).setAdminPrivateKey(adminPrivateKey);
        ((FA12ServiceImpl)fa12Service).setTezosContractAddress(fa12ContractAddress);

        ((FA12ServiceImpl)fa12Service).setTezosKeyService(tezosKeyService);
        ((FA12ServiceImpl)fa12Service).setTezosFeeService(tezosFeeService);
        ((FA12ServiceImpl)fa12Service).setTezosCoreService(tezosCoreService);
        ((FA12ServiceImpl)fa12Service).setTezosConnectivity(tezosConnectivity);
        ((FA12ServiceImpl)fa12Service).setTezosCryptoProvider(tezosCryptoProvider);

        //
        ((FA12ServiceImpl)fa12Service).init();
    }

    @PreDestroy
    public void destroy () {

        LOG.debug("destroy()");

        try {

            if(fa12Service != null) {
                ((FA12ServiceImpl) fa12Service).destroy();
            }

        }

        catch (Exception ex) {
            LOG.error("An exception occurred during the destruction: {}", ex.getMessage());
        }
    }

    private void showHelp () {
        LOG.info("java -jar fa12.jar [burn|mint] contractAddress adminPrivateKey publicAddress quantity");
    }

    public static void main (String [] args) {

        Main main = null;

        try {

            main = new Main();

            main.parameters = Arrays.asList(args);

            main.init();
            main.execute();

        }

        catch (Exception ex) {
            if(LOG.isDebugEnabled()) {
                LOG.error("An exception occurred: {}", ex.getMessage(), ex);
            }
            else {
                LOG.error("An exception occurred: {}", ex.getMessage());
            }
        }

        finally {
            if(main != null) {
                main.destroy();
            }
        }
    }
}