// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.junit.Test;

import static org.hamcrest.Matchers.is;

import static org.hamcrest.MatcherAssert.assertThat;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosContractAddressTest  {

    @Test
    public void equalsTest () throws Exception {

        TezosContractAddress contractA = TezosContractAddress.toTezosContractAddress("KT1Uz3ZPgVqKYTt8jJpbeRaZkq8b8PqFkRyg");
        TezosContractAddress contractB = TezosContractAddress.toTezosContractAddress("KT1Uz3ZPgVqKYTt8jJpbeRaZkq8b8PqFkRyg");

        assertThat("result should be true", contractA.equals(contractB), is(true));
    }

    @Test
    public void equalsTestWithDifferentValue () throws Exception {

        TezosContractAddress contractA = TezosContractAddress.toTezosContractAddress("KT1Uz3ZPgVqKYTt8jJpbeRaZkq8b8PqFkRyg");
        TezosContractAddress contractB = TezosContractAddress.toTezosContractAddress("KT1Acfs1M5FXHGYQpvdKUwGbZtrUkqrisweJ");

        assertThat("result should be false", contractA.equals(contractB), is(false));
    }

    @Test
    public void equalsTestWithDifferentType () throws Exception {

        TezosPublicAddress address = TezosPublicAddress.toTezosPublicAddress("tz1SVqTz7entj982jDSKcTQNgT7f2cg7C8dk");
        TezosContractAddress contract = TezosContractAddress.toTezosContractAddress("KT1Uz3ZPgVqKYTt8jJpbeRaZkq8b8PqFkRyg");

        assertThat("result should be false", contract.equals(address), is(false));
    }

    @Test
    public void equalsTestWithNull () throws Exception {

        TezosContractAddress contract = TezosContractAddress.toTezosContractAddress("KT1Uz3ZPgVqKYTt8jJpbeRaZkq8b8PqFkRyg");

        assertThat("result should be false", contract.equals(null), is(false));
    }
}