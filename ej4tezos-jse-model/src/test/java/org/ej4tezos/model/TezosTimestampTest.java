// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.junit.Test;
import org.junit.Ignore;

import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.notNullValue;

import static org.hamcrest.MatcherAssert.assertThat;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosTimestampTest {

    @Test
    public void simpleTest () throws Exception {

        TezosTimestamp timestamp = TezosTimestamp.toTezosTimestamp("2019-10-22T16:27:54Z");

        Date date = new Date(timestamp.getValue() * 1000);

        assertThat("date should not be null", date, notNullValue());
        assertThat("date.toString should be equals", date.toString(), startsWith("Tue Oct 22 16:27:54"));
    }

    @Test
    @Ignore  // Ignore until we solve the timezone issue
    public void valueToStringTest () throws Exception {

        TezosTimestamp timestamp = TezosTimestamp.toTezosTimestamp("2019-10-22T16:27:54Z");

        String value = timestamp.getValue().toString();

        assertThat("value should not be null", value, notNullValue());
        assertThat("value should be equals", value, is("1571754474"));
    }

    @Test
    @Ignore  // Ignore until we solve the timezone issue
    public void toStringTest () throws Exception {

        TezosTimestamp timestamp = TezosTimestamp.toTezosTimestamp("2019-10-22T16:27:54Z");

        String value = timestamp.toString();

        assertThat("value should not be null", value, notNullValue());
        assertThat("value should be equals", value, startsWith("1571754474"));
    }
}