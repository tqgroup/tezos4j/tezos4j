// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosPublicKey extends TezosKey {

    public TezosPublicKey () {
        super();
    }

    public static TezosPublicKey toTezosPublicKey (String value) throws TezosModelException {
        return toTezosValueObject(value, TezosPublicKey.class);
    }

    @Override
    public void validate () throws TezosModelException {

        super.validate();

        // @Todo Add more validation
    }

    public String getType () {
        return ("Tezos public key");
    }
}