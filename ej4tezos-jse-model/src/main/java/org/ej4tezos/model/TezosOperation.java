// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosOperation extends TezosValueObject<Void> {

    public static TezosOperation toTezosOperation () throws TezosModelException {
        return toTezosValueObject(null, TezosOperation.class);
    }

    protected TezosOperation () {
        super();
    }

    @Override
    public void validate () throws TezosModelException {
    }
}