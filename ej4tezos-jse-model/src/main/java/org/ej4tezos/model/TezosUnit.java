// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosUnit extends TezosValueObject<Void> {

    public static TezosUnit toTezosUnit () throws TezosModelException {
        return toTezosValueObject(null, TezosUnit.class);
    }

    protected TezosUnit () {
        super();
    }

    @Override
    public void validate () throws TezosModelException {
    }
}