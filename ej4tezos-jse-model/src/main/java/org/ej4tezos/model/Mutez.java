// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.math.BigInteger;

import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class Mutez extends TezosValueObject<BigInteger> {

    public static Mutez toMutez (BigInteger data) throws TezosModelException {
        return toTezosValueObject(data, Mutez.class);
    }

    public static Mutez toMutez (String data) throws TezosModelException {
        return toTezosValueObject(new BigInteger(data), Mutez.class);
    }

    protected Mutez () {
        super();
    }

    @Override
    public void validate () throws TezosModelException {
    }
}