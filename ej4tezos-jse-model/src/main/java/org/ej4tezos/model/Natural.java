// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

import org.ej4tezos.model.exception.TezosModelException;

import java.math.BigInteger;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class Natural extends TezosValueObject<BigInteger> {

    protected Natural() {
        super();
    }

    public static Natural toNatural(BigInteger data) throws TezosModelException {
        if (data.signum() == -1) {
            throw new IllegalArgumentException(String.format("%s is not a natural", data));
        }
        return toTezosValueObject(data, Natural.class);
    }

    public static Natural toNatural(String data) throws TezosModelException {
        BigInteger value = new BigInteger(data);
        if (value.signum() == -1) {
            throw new IllegalArgumentException(String.format("%s is not a natural", value));
        }
        return toTezosValueObject(value, Natural.class);
    }

    @Override
    public void validate() throws TezosModelException {
    }
}