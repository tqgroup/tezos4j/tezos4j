// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosBytes extends TezosValueObject<String> {

    public static TezosBytes toTezosBytes (String data) throws TezosModelException {
        return toTezosValueObject(data, TezosBytes.class);
    }

    protected TezosBytes () {
        super();
    }

    @Override
    public void validate () throws TezosModelException {

        super.validate();

        // @Todo Add more validation
    }

    @Override
    public String toString () {
        //return (value == null || value.length() == 0) ? "" : ("0x" + value.toString().toUpperCase());
        return (value == null || value.length() == 0) ? "" : ("0x" + value.toString().toLowerCase());
    }
}