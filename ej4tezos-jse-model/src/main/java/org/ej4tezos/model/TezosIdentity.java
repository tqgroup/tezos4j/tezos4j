// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosIdentity extends TezosValidatableObject {

    private TezosPublicKey publicKey;
    private TezosPrivateKey privateKey;
    private TezosPublicAddress publicAddress;

    public TezosIdentity () {
        super();
    }

    public static TezosIdentity toTezosIdentity (String privateKey, String publicKey, String publicAddress) throws TezosModelException {

        TezosIdentity tezosIdentity = new TezosIdentity();

        tezosIdentity.setPublicKey(TezosPublicKey.toTezosPublicKey(publicKey));
        tezosIdentity.setPrivateKey(TezosPrivateKey.toTezosPrivateKey(privateKey));
        tezosIdentity.setPublicAddress(TezosPublicAddress.toTezosPublicAddress(publicAddress));

        return tezosIdentity;
    }

    public static TezosIdentity toTezosIdentity (TezosPrivateKey privateKey, TezosPublicKey publicKey, TezosPublicAddress publicAddress) throws TezosModelException {

        // Create a new instance of TezosIdentity:
        TezosIdentity tezosIdentity = new TezosIdentity();

        // Set the identity fields:
        tezosIdentity.setPublicKey(publicKey);
        tezosIdentity.setPrivateKey(privateKey);
        tezosIdentity.setPublicAddress(publicAddress);

        // Validate the newly created identity:
        tezosIdentity.validate();

        // Return the result:
        return tezosIdentity;
    }

    @Override
    public void validate () throws TezosModelException {
        publicKey.validate();
        privateKey.validate();
        publicAddress.validate();
    }

    public void setPrivateKey (TezosPrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public TezosPrivateKey getPrivateKey () {
        return privateKey;
    }

    public void setPublicKey (TezosPublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public TezosPublicKey getPublicKey () {
        return publicKey;
    }

    public void setPublicAddress (TezosPublicAddress publicAddress) {
        this.publicAddress = publicAddress;
    }

    public TezosPublicAddress getPublicAddress () {
        return publicAddress;
    }

    @Override
    public String toString() {
        return "TezosIdentity{" +
                "publicKey=" + publicKey +
                ", publicAddress=" + publicAddress +
                '}';
    }
}