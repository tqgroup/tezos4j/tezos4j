package org.ej4tezos.model;

import java.math.BigInteger;


// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosBigMap<K, V> implements TezosObject {
    private final BigInteger address;

    public TezosBigMap(BigInteger address) {
        this.address = address;
    }

    public BigInteger getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "TezosBigMap{" +
                "address=" + address +
                '}';
    }
}