// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosSignature extends TezosValueObject<String> {

    public static TezosSignature toTezosSignature (String value) throws TezosModelException {
        return toTezosValueObject(value, TezosSignature.class);
    }

    protected TezosSignature() {
        super();
    }

    @Override
    public void validate () throws TezosModelException {

        super.validate();

        // @Todo Add more validation
    }
}