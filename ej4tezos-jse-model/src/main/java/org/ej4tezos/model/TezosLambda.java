// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosLambda extends TezosValueObject<String> {

    public static TezosLambda toTezosLambda (String value) throws TezosModelException {
        return toTezosValueObject(value, TezosLambda.class);
    }

    protected TezosLambda () {
        super();
    }

    @Override
    public void validate () throws TezosModelException {

        super.validate();

        // @Todo Add more validation
    }
}