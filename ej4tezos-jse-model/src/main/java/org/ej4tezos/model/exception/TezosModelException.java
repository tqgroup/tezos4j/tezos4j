// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model.exception;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosModelException extends Exception {

    public TezosModelException () {
        super();
    }

    public TezosModelException (String message) {
        super(message);
    }

    public TezosModelException (String message, Throwable cause) {
        super(message, cause);
    }
}