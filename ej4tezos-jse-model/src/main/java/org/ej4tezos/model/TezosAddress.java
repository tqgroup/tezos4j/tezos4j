// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.text.MessageFormat;

import org.ej4tezos.model.exception.TezosModelException;

import static org.ej4tezos.utils.asserts.Asserts.checkState;
import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public abstract class TezosAddress extends TezosValueObject<String> {

    protected TezosAddress () {
        super();
    }

    public static TezosAddress toTezosAddress (String value) throws TezosModelException {

        // Sanity check:
        checkNotNull(value, "value should not be null", TezosModelException.class);

        //
        value = value.trim();

        // More sanity check:
        checkState(value.length() > 2, "Invalid value size", TezosModelException.class);

        //
        if(value.startsWith("tz")) {
            return TezosPublicAddress.toTezosPublicAddress(value);
        }

        //
        if(value.startsWith("KT")) {
            return TezosContractAddress.toTezosContractAddress(value);
        }

        //
        String message = MessageFormat.format("Invalid address format: {0}", value);
        throw new TezosModelException(message);
    }

    public String getAddress () {
        return getValue();
    }

    public String getType () {
        return ("Tezos address");
    }
}