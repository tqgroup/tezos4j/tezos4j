// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.text.MessageFormat;
import java.util.Objects;

import org.ej4tezos.model.exception.TezosModelException;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public abstract class TezosValueObject<V> extends TezosValidatableObject {

    protected V value;

    protected TezosValueObject () {
        super();
    }

    public static <T extends TezosValueObject, V> T toTezosValueObject (V value, Class<T> targetTezosObjectClass) throws TezosModelException {

        checkNotNull(value, "value should not be null", TezosModelException.class);
        checkNotNull(targetTezosObjectClass, "targetTezosObjectClass should not be null", TezosModelException.class);

        try {
            T result = targetTezosObjectClass.newInstance();
            result.setValue(value);
            result.validate();
            return result;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while converting to {0}: {1}", targetTezosObjectClass.getSimpleName(), ex.getMessage());
            throw new TezosModelException(message, ex);
        }
    }

    @Override
    public void validate () throws TezosModelException {

        checkNotNull(value, getType() + " value should not be null", TezosModelException.class);

    }

    public void setValue (V value) throws TezosModelException {
        this.value = value;
    }

    public V getValue () {
        return value;
    }

    public String getType () {
        return (this.getClass().getSimpleName());
    }

    @Override
    public String toString () {
        return value == null ? "null" : value.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TezosValueObject)) return false;
        if (getClass() != o.getClass()) return false;
        TezosValueObject<?> that = (TezosValueObject<?>) o;
        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}