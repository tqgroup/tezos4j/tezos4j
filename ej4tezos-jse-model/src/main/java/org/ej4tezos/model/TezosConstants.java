// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public interface TezosConstants {

    byte [] EDPK_PREFIX  = new byte [] { (byte)  13, (byte)  15, (byte)  37, (byte) 217 };
    byte [] EDSK_PREFIX  = new byte [] { (byte)  43, (byte) 246, (byte)  78, (byte)   7 };
    byte [] TZ1_PREFIX   = new byte [] { (byte)   6, (byte) 161, (byte) 159 };
    byte [] EDSIG_PREFIX = new byte [] { (byte)   9, (byte) 245, (byte) 205, (byte) 134, (byte)  18 };

}