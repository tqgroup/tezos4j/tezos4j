// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.Date;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;

import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosTimestamp extends TezosValueObject<Long> {

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    public static TezosTimestamp toTezosTimestamp (long value) throws TezosModelException {
        return toTezosValueObject(value, TezosTimestamp.class);
    }

    public static TezosTimestamp toTezosTimestamp (Date value) throws TezosModelException {
        return toTezosTimestamp(value.getTime() / 1000);
    }

    public static TezosTimestamp now () throws TezosModelException {
        return toTezosTimestamp(new Date());
    }

    public static TezosTimestamp toTezosTimestamp (String value) throws TezosModelException {

        try {

            synchronized (simpleDateFormat) {
                return toTezosTimestamp(simpleDateFormat.parse(value));
            }

        }

        catch (Exception ex) {

            String message
                = MessageFormat
                    .format("An exception occurred while converting {0} to a TezosTimestamp: {1}",
                            value,
                            ex.getMessage());

            throw new TezosModelException(message, ex);
        }
    }

    protected TezosTimestamp () {
        super();
    }

    @Override
    public void validate () throws TezosModelException {

        super.validate();

        // @Todo Add more validation
    }
}