// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosPublicAddress extends TezosAddress {

    public TezosPublicAddress () {
        super();
    }

    public static TezosPublicAddress toTezosPublicAddress (String value) throws TezosModelException {
        return toTezosValueObject(value, TezosPublicAddress.class);
    }

    @Override
    public void validate () throws TezosModelException {

        super.validate();

        // @Todo Add more validation
    }

    public String getType () {
        return ("Tezos public address");
    }
}