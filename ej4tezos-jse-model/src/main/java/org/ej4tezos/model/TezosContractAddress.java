// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosContractAddress extends TezosAddress {

    public TezosContractAddress () {
        super();
    }

    public static TezosContractAddress toTezosContractAddress (String value) throws TezosModelException {
        return toTezosValueObject(value, TezosContractAddress.class);
    }

    public String getType () {
        return ("Tezos contract address");
    }
}