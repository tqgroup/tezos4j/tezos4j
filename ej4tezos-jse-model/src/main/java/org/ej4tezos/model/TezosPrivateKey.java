// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class TezosPrivateKey extends TezosKey {

    public TezosPrivateKey () {
        super();
    }

    public static TezosPrivateKey toTezosPrivateKey (String value) throws TezosModelException {
        return toTezosValueObject(value, TezosPrivateKey.class);
    }

    @Override
    public void validate () throws TezosModelException {

        super.validate();

        // @Todo Add more validation
    }

    public String getType () {
        return ("Tezos private key");
    }
}