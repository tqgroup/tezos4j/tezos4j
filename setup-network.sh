#!/bin/bash -eEu

if docker network inspect tezos 2>&1 > /dev/null; then
  DOCKER_TEZOS_NET_ID="$(docker network inspect tezos --format='{{.Id}}')"
  DOCKER_TEZOS_NET_IF=br-"${DOCKER_TEZOS_NET_ID:0:12}"
  docker network rm tezos
  if which 'firewall-cmd' 2>&1 > /dev/null; then
    firewall-cmd --permanent --zone=docker --remove-interface $DOCKER_TEZOS_NET_IF
  fi
fi

docker network create tezos
DOCKER_TEZOS_NET_ID="$(docker network inspect tezos --format='{{.Id}}')"
DOCKER_TEZOS_NET_IF=br-"${DOCKER_TEZOS_NET_ID:0:12}"
# DOCKER_TEZOS_NET_SUBNET="$(docker network inspect tezos --format='{{range .IPAM.Config}}{{.Subnet}}{{end}}')"

if which 'firewall-cmd' 2>&1 > /dev/null; then
  firewall-cmd --permanent --zone=docker --add-interface $DOCKER_TEZOS_NET_IF
  # firewall-cmd --permanent --zone=public --add-rich-rule="rule family=ipv4 source address=$DOCKER_TEZOS_NET_SUBNET accept"
  firewall-cmd --reload
fi
