# EJ4Tezos Maven Plugin:

## Introduction:

The EJ4Tezos Maven Plugin provides various goals allowing the user to simplify the interactions with the Tezos
blockchain.

## Goals:

In the following paragraph, the various goals are described in more details.

We will be using the following contract as an example: KT1PpNh5jGx5DhPkpvVo374B6Bbk3H3aZAtu (on Carthagenet).

### Script:

```
mvn -Dnetwork=carthagenet -Daddress=KT1PpNh5jGx5DhPkpvVo374B6Bbk3H3aZAtu org.ej4tezos:cgp-maven-plugin:1.0.0.0-SNAPSHOT:script
```

or

```
<plugin>
	<groupId>org.ej4tezos</groupId>
	<artifactId>cgp-maven-plugin</artifactId>
	<version>1.0.0.0-SNAPSHOT</version>
	<executions>
		<execution>
			<id>script</id>
			<goals>
				<goal>script</goal>
			</goals>
			<configuration>
				<network>carthagenet</network>
				<address>KT1PpNh5jGx5DhPkpvVo374B6Bbk3H3aZAtu</address>
			</configuration>
		</execution>
	</executions>
</plugin>
```

### Micheline:

```
mvn -Dname=EuroTz org.ej4tezos:cgp-maven-plugin:1.0.0.0-SNAPSHOT:micheline
```

or

```
<plugin>
	<groupId>org.ej4tezos</groupId>
	<artifactId>cgp-maven-plugin</artifactId>
	<version>1.0.0.0-SNAPSHOT</version>
	<executions>
		<execution>
			<id>micheline</id>
			<goals>
				<goal>micheline</goal>
			</goals>
			<configuration>
				<name>EuroTz</name>
			</configuration>
		</execution>
	</executions>
</plugin>
```

### Java goal:

```
mvn org.ej4tezos:cgp-maven-plugin:1.0.0.0-SNAPSHOT:java
```

or

```
<plugin>
	<groupId>org.ej4tezos</groupId>
	<artifactId>cgp-maven-plugin</artifactId>
	<version>1.0.0.0-SNAPSHOT</version>
	<executions>
		<execution>
			<id>java</id>
			<phase>process-resources</phase>
			<goals>
				<goal>java</goal>
			</goals>
		</execution>
	</executions>
</plugin>
```