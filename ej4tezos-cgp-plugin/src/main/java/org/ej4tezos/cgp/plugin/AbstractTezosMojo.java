// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.plugin;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import static java.lang.String.format;

import org.apache.maven.plugin.AbstractMojo;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public abstract class AbstractTezosMojo extends AbstractMojo {

    public AbstractTezosMojo () {
        super();
    }

    protected InputStream getResource (String resourceFileName, String directoryName) throws FileNotFoundException, TezosGeneratorException {
        return getResource(resourceFileName, directoryName, false);
    }

    protected InputStream getResource (String resourceFileName, String directoryName, boolean checkGenerated) throws FileNotFoundException, TezosGeneratorException {

        InputStream resourceAsStream = null;

        //
        getLog().debug(format("getResource (resourceFileName = %s)", resourceFileName));

        // Sanity check:
        checkNotNull(resourceFileName, "resourceFileName should not be null", TezosGeneratorException.class);

        // Check the generated-resources directory if we have to:
        if (checkGenerated) {

            String targetFilePath = format("target%1$s%3$s%1$s%2$s", File.separatorChar, resourceFileName, directoryName);
            getLog().debug(format("Check %s...", targetFilePath));

            if (new File(targetFilePath).exists()) {
                resourceAsStream = new FileInputStream(targetFilePath);
            }
        }

        // If the stream is still null, check the resources directory:
        if (resourceAsStream == null) {

            String targetFilePath = format("src%1$smain%1$sresources%1$s%2$s", File.separatorChar, resourceFileName);
            getLog().debug(format("Check %s...", targetFilePath));

            if (new File(targetFilePath).exists()) {
                resourceAsStream = new FileInputStream(targetFilePath);
            }
        }

        // Check if we found something:
        if (resourceAsStream == null) {
            throw new FileNotFoundException(format("Cannot load %s as a resource!", resourceFileName));
        }

        //
        return resourceAsStream;
    }

    @Override
    public String toString () {
        return "AbstractTezosMojo";
    }
}