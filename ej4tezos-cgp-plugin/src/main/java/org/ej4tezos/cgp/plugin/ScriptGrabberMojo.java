// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.plugin;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.io.*;

import java.net.URL;
import java.net.MalformedURLException;

import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONObject;

import org.apache.commons.io.IOUtils;

import org.apache.http.HttpStatus;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.CloseableHttpResponse;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.CloseableHttpClient;

import org.apache.maven.plugin.MojoExecutionException;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.LifecyclePhase;

import static java.lang.String.format;

import static org.ej4tezos.utils.io.IOToolbox.*;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;
import static org.ej4tezos.utils.asserts.Asserts.checkState;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
@Mojo(name = "script", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class ScriptGrabberMojo extends AbstractTezosMojo {
    private static final String SCRIPT_URL_FORMAT ="%s/chains/main/blocks/head/context/contracts/%s/script";

    @Parameter(property = "url", readonly = true)
    private String url;

    @Parameter(property = "name", readonly = true, defaultValue = "Anonymous")
    private String name;

    @Parameter(property = "address", readonly = true)
    private String address;

    @Parameter(property = "useDeploymentDescriptor", readonly = true, defaultValue = "false")
    private boolean useDeploymentDescriptor;

    @Parameter(property = "useLocalScript", readonly = true, defaultValue = "false")
    private boolean useLocalScript;

    private CloseableHttpClient httpClient;

    public ScriptGrabberMojo() {
        super();
        httpClient = HttpClientBuilder.create().build();
    }

    public void execute() throws MojoExecutionException {

        Writer writer = null;
        URL targetScriptURL = null;
        CloseableHttpResponse response = null;
        InputStream contractScript;

        //
        getLog().debug("execute ()");

        getLog().info(format("useDeploymentDescriptor = %s", useDeploymentDescriptor));

        getLog().info(format("useLocalScript = %s", useLocalScript));

        try {

            //
            checkState(!((useDeploymentDescriptor || useLocalScript)  && (useDeploymentDescriptor == useLocalScript)), "you should select either useDeploymentDescriptor or useLocalScript", MojoExecutionException.class);

            if (!useLocalScript) {

                if (useDeploymentDescriptor) {
                    InputStream deploymentDescriptorInputStream = getResource ("deploymentDescriptor.json", "deploy", true);
                    JSONObject deploymentDescriptorJSON = new JSONObject(IOUtils.toString(deploymentDescriptorInputStream, Charset.defaultCharset()));

                    address = deploymentDescriptorJSON.getString("originatedContractAddress").trim();

                } else {
                    address = address.trim();
                }


                // Sanity check:
                checkNotNull(url, "url should not be null", MojoExecutionException.class);
                checkNotNull(name, "name should not be null", MojoExecutionException.class);
                checkNotNull(address, "address should not be null", MojoExecutionException.class);

                // Clean-up:
                url = url.trim();
                name = name.trim();

                // Prepare the target script URL:
                targetScriptURL = new URL(format(SCRIPT_URL_FORMAT, url, address));
                getLog().info(format("targetScriptURL = %s", targetScriptURL));

                // Prepare the get object:
                HttpGet get = new HttpGet(targetScriptURL.toURI());

                // Execute the call:
                response = httpClient.execute(get);

                // Get the status code:
                int statusCode = response.getStatusLine().getStatusCode();

                getLog().debug(format("targetScriptURL returned status %s", statusCode));

                // Throw an exception if it is not 200:
                if (statusCode != HttpStatus.SC_OK) {
                    String message = format("Cannot execute the HTTP GET: %s! (code = %s)", targetScriptURL, statusCode);
                    getLog().warn(message);
                    throw new MojoExecutionException(message);
                }

                contractScript = response.getEntity().getContent();

            } else {
                // Format the generated script to be identical to RPC response
                JSONObject intermediateContractScript = new JSONObject().put("code", new JSONArray(IOUtils.toString(getResource (name + "_compiled.json", "compile", true), Charset.defaultCharset())));

                contractScript = new ByteArrayInputStream(intermediateContractScript.toString().getBytes());
            }

            //
            String targetDirectory = format("target%1$sgenerated-resources%1$s", File.separatorChar);
            String outputFileWithExtension = format("%s.json", name);
            getLog().debug(format("targetDirectory = %s", targetDirectory));

            //
            File targetDirectoryAsFile = new File(targetDirectory);
            getLog().debug(format("targetDirectoryAsFile = %s", targetDirectoryAsFile));

            //
            targetDirectoryAsFile.mkdirs();
            writer = new FileWriter(new File(targetDirectoryAsFile, outputFileWithExtension));

            //
            IOUtils.copy(contractScript, writer, Charset.defaultCharset());

         } catch (MojoExecutionException ex) {
            throw ex;
        } catch (MalformedURLException ex) {
            String message = format("The URL is malformed: %s", ex.getMessage());
            throw new MojoExecutionException(message, ex);
        } catch (Exception ex) {
            String message = format("An exception occurred while executing an HTTP GET: %s (url = %s)", ex.getMessage(), targetScriptURL);
            throw new MojoExecutionException(message, ex);
        } finally {
            silentClose(writer);
            silentClose(response);
        }
    }

    @Override
    public String toString() {
        return "ScriptGrabberMojo";
    }
}