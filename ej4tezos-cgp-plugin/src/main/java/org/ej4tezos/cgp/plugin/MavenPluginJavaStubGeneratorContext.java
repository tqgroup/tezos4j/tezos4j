// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.plugin;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.io.File;
import java.io.Reader;
import java.io.FileWriter;
import java.io.IOException;

import static java.lang.String.format;

import org.apache.commons.io.IOUtils;

import org.apache.maven.plugin.Mojo;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.ContextEnabled;

import org.ej4tezos.cgp.api.model.JavaStubGeneratorContext;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class MavenPluginJavaStubGeneratorContext implements JavaStubGeneratorContext {

    private Mojo mojo;
    private String packageName;
    private ContextEnabled mojoContext;

    public MavenPluginJavaStubGeneratorContext (String packageName, AbstractMojo abstractMojo) {
        this(packageName, abstractMojo, abstractMojo);
    }

    public MavenPluginJavaStubGeneratorContext (String packageName, Mojo mojo, ContextEnabled mojoContext) {

        super();

        this.mojo = mojo;
        this.mojoContext = mojoContext;
        this.packageName = packageName;
    }

    @Override
    public String getPackageName () {
        return packageName;
    }

    @Override
    public void persist (String outputFileName, Reader reader) throws TezosGeneratorException {

        FileWriter writer = null;

        // Log the call:
        mojo.getLog().debug(format("persist (outputFileName = %s, reader = %s)", outputFileName, reader));

        // Sanity check:
        checkNotNull(reader, "reader should not be null", TezosGeneratorException.class);
        checkNotNull(outputFileName, "outputFileName should not be null", TezosGeneratorException.class);

        try {

            //
            String packageNameAsDirectory = packageName.replace('.', File.separatorChar);

            //
            String targetDirectory = format("target%1$sgenerated-sources%1$sej4tezos%1$s%2$s%1$s", File.separatorChar, packageNameAsDirectory);
            String outputFileWithExtension = format("%s.java", outputFileName);
            mojo.getLog().debug(format("targetDirectory = %s", targetDirectory));

            //
            File targetDirectoryAsFile = new File(targetDirectory);
            mojo.getLog().debug(format("targetDirectoryAsFile = %s", targetDirectoryAsFile));

            //
            targetDirectoryAsFile.mkdirs();
            writer = new FileWriter(new File(targetDirectoryAsFile, outputFileWithExtension));

            //
            IOUtils.copy(reader, writer);
        }

        catch (IOException ex) {
            String message = format("An exception occurred while persisting a file: %s", ex.getMessage());
            throw new TezosGeneratorException(message, ex);
        }

        finally {
            mojo.getLog().debug("Close the files");
            // @todo Refactor to avoid deprecated methods.
            IOUtils.closeQuietly(writer);
            IOUtils.closeQuietly(reader);
        }
    }

    @Override
    public String toString () {
        return "MavenPluginJavaStubGeneratorContext";
    }
}