// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.plugin;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.io.File;
import java.io.InputStream;
import java.io.FileOutputStream;

import java.nio.charset.Charset;

import static java.lang.String.format;

import org.json.JSONObject;

import org.apache.commons.io.IOUtils;

import org.apache.maven.plugin.MojoExecutionException;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.LifecyclePhase;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

import org.ej4tezos.cgp.api.TezosContractDefinitionGenerator;

import org.ej4tezos.cgp.api.model.TezosContractDefinitionGeneratorContext;

import org.ej4tezos.parser.MichelineTezosContractDefinitionGenerator;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
@Mojo( name = "micheline", defaultPhase = LifecyclePhase.PROCESS_RESOURCES )
public class MichelineConverterMojo extends AbstractTezosMojo {

    @Parameter(property = "name", readonly = true, defaultValue = "AnonymousContract")
    private String name;

    @Parameter(property = "contract", readonly = true, defaultValue = "Anonymous")
    private String contract;

    private TezosContractDefinitionGenerator tezosContractDefinitionGenerator;

    public MichelineConverterMojo () throws Exception {

        super();

        tezosContractDefinitionGenerator = new MichelineTezosContractDefinitionGenerator();
    }

    public void execute () throws MojoExecutionException {

        //
        getLog().debug(format("execute (contractFilePath = %s)", contract));

        // Sanity check:
        checkNotNull(contract, "contractFileName should not be null", MojoExecutionException.class);

        //
        try {

            //
            InputStream michelineContractDefinitionStream
                = getResource(contract + ".json", "generated-resources", true);

            //
            TezosContractDefinitionGeneratorContext context
                = new MavenPluginTezosDefinitionContractGeneratorContext(name);

            // Call Micheline2TCD ...
            JSONObject tezosContractDefinition
                = tezosContractDefinitionGenerator.generate(context, michelineContractDefinitionStream);

            //
            String outputDirectory = format("target%1$sgenerated-resources%1$s", File.separatorChar);
            getLog().info(format("outputDirectory = %s)", outputDirectory));
            new File(outputDirectory).mkdirs();

            //
            String outputFileWithExtension = format("%s%s.tcd", outputDirectory, contract);
            getLog().info(format("Writing Tezos Contract Definition to: %s)", outputFileWithExtension));

            // Write the Tezos Contract Definition file:
            IOUtils.write(tezosContractDefinition.toString(), new FileOutputStream(outputFileWithExtension), Charset.defaultCharset());

        }

        catch (Exception ex) {
            String message = format("An exception occurred while processing file %s: %s", contract, ex.getMessage());
            throw new MojoExecutionException(message, ex);
        }
    }

    @Override
    public String toString () {
        return "MichelineConverterMojo";
    }
}