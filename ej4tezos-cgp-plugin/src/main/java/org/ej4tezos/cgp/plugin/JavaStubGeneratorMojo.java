// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.plugin;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.io.InputStream;

import java.nio.charset.Charset;

import static java.lang.String.format;

import org.json.JSONObject;

import org.apache.commons.io.IOUtils;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.LifecyclePhase;

import org.apache.maven.plugin.MojoExecutionException;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

import org.ej4tezos.cgp.api.JavaStubGenerator;

import org.ej4tezos.cgp.api.model.JavaStubGeneratorContext;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

import org.ej4tezos.cgp.generator.velocity.JavaStubGeneratorImpl;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
@Mojo( name = "java", defaultPhase = LifecyclePhase.GENERATE_SOURCES )
public class JavaStubGeneratorMojo extends AbstractTezosMojo {

    private JavaStubGenerator javaStubGenerator;

    @Parameter(alias = "package", property = "package", readonly = true, defaultValue = "org.ej4tezos.contract")
    private String _package;

    @Parameter(property = "definition", readonly = true, defaultValue = "Anonymous")
    private String definition;

    public JavaStubGeneratorMojo () {
        super();
        javaStubGenerator = new JavaStubGeneratorImpl();
    }

    public void execute () throws MojoExecutionException {

        //
        getLog().debug("Execute...");
        process(definition);

    }

    private void process (String contractDefinition) throws MojoExecutionException {

        //
        getLog().debug(format("process (contractDefinition = %s)", contractDefinition));

        // Sanity check:
        checkNotNull(contractDefinition, "contractDefinition should not be null", MojoExecutionException.class);

        //
        try {

            //
            InputStream contractDefinitionStream
                = getResource(contractDefinition + ".tcd", "generated-resources", true);

            //
            JSONObject contractDefinitionDocument
                = prepare(contractDefinitionStream);

            // Prepare the context:
            JavaStubGeneratorContext context
                = new MavenPluginJavaStubGeneratorContext(_package, this);

            //
            javaStubGenerator.generate(context, contractDefinitionDocument);

        }

        catch (Exception ex) {
            String message = format("An exception occurred while processing file %s: %s", contractDefinition, ex.getMessage());
            throw new MojoExecutionException(message, ex);
        }
    }

    /**
     * This method prepares the contract definition. It will load, validate and then
     * return the definition in the TCD format.
     * @param contractDefinitionStream
     * @return
     */
    private JSONObject prepare (InputStream contractDefinitionStream) throws TezosGeneratorException {

        //
        getLog().debug(format("prepare (contractDefinitionStream = %s)", contractDefinitionStream));

        // Sanity check:
        checkNotNull(contractDefinitionStream, "contractDefinitionStream should not be null", TezosGeneratorException.class);

        try {

            // Read the contract:
            JSONObject contract
                = new JSONObject(IOUtils.toString(contractDefinitionStream, Charset.defaultCharset()));

            // Validate the TCD:
            // ...

            // Log and return the result:
            getLog().debug(format("contract = %s", contract));
            return contract;
        }

        catch (Exception ex) {
            String message = format("An exception occurred while preparing the contract definition: %s", ex.getMessage());
            throw new TezosGeneratorException(message, ex);
        }
    }

    @Override
    public String toString () {
        return "JavaStubGeneratorMojo";
    }
}