// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.cgp.plugin;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import org.ej4tezos.cgp.api.model.TezosContractDefinitionGeneratorContext;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public class MavenPluginTezosDefinitionContractGeneratorContext extends TezosContractDefinitionGeneratorContext {

    private String contractName;

    public MavenPluginTezosDefinitionContractGeneratorContext () {
        this.contractName = "Anonymous";
    }

    public MavenPluginTezosDefinitionContractGeneratorContext (String contractName) {
        this.contractName = contractName;
    }

    @Override
    public String getName () {
        return contractName;
    }

    @Override
    public String toString () {
        return ReflectionToStringBuilder.toString(this);
    }
}