// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.parser;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import java.text.MessageFormat;

import org.json.JSONArray;
import org.json.JSONObject;

import static org.ej4tezos.utils.asserts.Asserts.checkState;
import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

import static org.ej4tezos.papi.model.MichelineConstants.*;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public class EntryPointsDefinitionGenerator {

    public List<JSONObject> generateEntryPointsDefinition (JSONArray code) throws TezosGeneratorException {

        try {

            // Sanity check:
            checkNotNull(code, "Code should not be null", TezosGeneratorException.class);

            // EntryPoints Json node will be assigned to:
            List<JSONObject> parsedEntryPointsDetails = new ArrayList<>();

            // Get the array that contains the entryPoints description
            JSONArray args = getArgs(code);

            // Sanity check:
            checkState(args.length() > 0, "Contract script must have parameter object", TezosGeneratorException.class);

            // Check if the contract has Multiple or Single entryPoint
            Boolean isMultipleEntryPoints = isMultipleEntryPoints(args.getJSONObject(0));

            JSONObject entryPointsDetailsObject = Boolean.TRUE.equals(isMultipleEntryPoints) ? convertScriptMultipleEntryPoints(args.getJSONObject(0), null) : convertScriptSingleEntryPoint(args.getJSONObject(0));

            // = = = = = = = = = = = = Get the Parameters object = = = = = = = = = = = = = =

            JSONObject entryPointsDetails = (JSONObject) entryPointsDetailsObject.get(ENTRY_POINTS);

            // = = = = = = = = = = = Iterate on EntryPoints = = = = = = = = = = = = = =
            for (String entryPointName : JSONObject.getNames(entryPointsDetails)) {

                JSONObject entryPoint = (JSONObject) entryPointsDetails.get(entryPointName);
                JSONObject entryPointDetails = new JSONObject();

                entryPointDetails.put(PARAMETERS, Parser.parse(entryPoint, entryPointName, ENTRY_POINT));
                entryPointDetails.put(NAME, entryPointName);

                parsedEntryPointsDetails.add(entryPointDetails);
            }

            // Sort the entryPoints ArrayList
            Collections.sort(parsedEntryPointsDetails, (entryPointA, entryPointB) -> {
                String entryPointAName = (String) entryPointA.get(NAME);
                String entryPointBName = (String) entryPointB.get(NAME);
                return entryPointAName.compareTo(entryPointBName);
            });

            return parsedEntryPointsDetails;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while generating contract definition object: {0}", ex.getMessage());
            throw new TezosGeneratorException(message, ex);
        }
    }

    private JSONObject convertScriptSingleEntryPoint (JSONObject jsonObj) {

        return new JSONObject().put(ENTRY_POINTS, new JSONObject().put(DEFAULT, jsonObj));
    }

    private JSONObject convertScriptMultipleEntryPoints (JSONObject jsonObj, JSONObject builtObject) {

        JSONArray args;
        String entryPointName;

        if (builtObject == null) {
            builtObject = new JSONObject();
        }

        args = (JSONArray) jsonObj.get(ARGS);

        for (int i = 0; i < args.length(); i++) {

            if (args.getJSONObject(i).has(PRIM) && args.getJSONObject(i).get(PRIM).equals(OR)) {

                convertScriptMultipleEntryPoints(args.getJSONObject(i), builtObject);

            }
            else {

                JSONArray annotsArray = (JSONArray) args.getJSONObject(i).get(ANNOTS);

                entryPointName = annotsArray.getString(0).replace("%", "").replace(":", "");

                args.getJSONObject(i).remove(ANNOTS);

                builtObject.put(entryPointName, args.getJSONObject(i));

            }
        }

        JSONObject convertedScript = new JSONObject();

        convertedScript.put(ENTRY_POINTS, builtObject);

        return convertedScript;
    }

    private Boolean isMultipleEntryPoints (JSONObject args) {

        if (args.has(PRIM) && args.get(PRIM).equals(OR)) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    private JSONArray getArgs (JSONArray scriptJSONArray) {

        for (int i = 0; i < scriptJSONArray.length(); i++) {
            if (scriptJSONArray.getJSONObject(i).has(PRIM) && scriptJSONArray.getJSONObject(i).get(PRIM).equals(PARAMETER)) {
                return (JSONArray) scriptJSONArray.getJSONObject(i).get(ARGS);
            }
        }

        return new JSONArray();
    }
}