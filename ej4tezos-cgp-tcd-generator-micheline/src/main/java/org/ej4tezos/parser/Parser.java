// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.parser;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import static org.ej4tezos.papi.model.MichelineConstants.*;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public class Parser {

    // TODO Move these constants to project: papi/model/MichelineConstants.java
    // private static final String VALUE_DETAILS = "valueRecordDetails";
    // private static final String KEY_DETAILS = "keyRecordDetails";
    private static final String PAIR_MIN = "pair";
    private static final String SET = "set";
    private static final String LAMBDA = "lambda";
    private static final String RECORD = "record";

    static List<JSONObject> parse (JSONObject entryPoint, String name, String type) {

        List<JSONObject> parameters = new ArrayList<>();

        JSONArray paramArray = decodeParameters(entryPoint, null);

        for (int i = 0; i < paramArray.length(); i++) {

            JSONObject pair;
            String paramName = null;
            String paramType = null;
            Boolean optional = Boolean.FALSE;
            List<JSONObject> details = new ArrayList<>();
            JSONObject mapDetails = new JSONObject();
            List<Object> pairDescription = new ArrayList<>();

            JSONObject jsonObj = paramArray.getJSONObject(i);

            // retrieve param name (annot) if exists
            if (jsonObj.has(ANNOTS)) {
                JSONArray annotsArray = (JSONArray) jsonObj.get(ANNOTS);
                paramName = annotsArray.getString(0).replace("%", "").replace(":", "");
            }

            // retrieve type
            if (jsonObj.has(PRIM)) {

                String prim = jsonObj.get(PRIM).toString();

                switch (prim) {
                    // currently, we don't need to retrieve the details of the contract that will be called
                    case CONTRACT:
                        paramType = ADDRESS;
                        break;

                    case OPTION:
                        paramType = (String) jsonObj.getJSONArray(ARGS).getJSONObject(0).get(PRIM);
                        if (paramType.equalsIgnoreCase(PAIR)) {

                            details = parse(jsonObj.getJSONArray(ARGS).getJSONObject(0), "", type);

                        } else if (paramType.equalsIgnoreCase(MAP) || paramType.equalsIgnoreCase(BIG_MAP)) {

                            mapDetails = getMapDescription(jsonObj.getJSONArray(ARGS).getJSONObject(0).getJSONArray(ARGS), type);

                        } else if (paramType.equalsIgnoreCase(LIST) || paramType.equalsIgnoreCase(SET)) {

                            details = parse(jsonObj.getJSONArray(ARGS).getJSONObject(0), "",  type);

                        }
                        optional = Boolean.TRUE;
                        break;

                    case PAIR_MIN:
                        paramType = (String) jsonObj.get(PRIM);
                        JSONArray pairDesc = (JSONArray) jsonObj.get(ARGS);

                        for (int j = 0; j < pairDesc.length(); j++) {
                            if (pairDesc.getJSONObject(j).getString(PRIM).equalsIgnoreCase(PAIR_MIN)) {

                                List<JSONObject> intRecordParams = parse(pairDesc.getJSONObject(j), "", type);
                                JSONObject internalRecordDetails = new JSONObject();

                                internalRecordDetails.put(DETAILS, intRecordParams);
                                internalRecordDetails.put(TYPE, RECORD);
                                internalRecordDetails.put(OPTIONAL, Boolean.FALSE);
                                String paramOfField = type.equalsIgnoreCase(ENTRY_POINT) ? "_param_" : "_field_";
                                internalRecordDetails.put(NAME, name + paramOfField + j);

                                pairDescription.add(internalRecordDetails);

                            } else {
                                List<JSONObject> intParam = parse(pairDesc.getJSONObject(j), "", type);
                                pairDescription.add(intParam.size() == 1 ? intParam.get(0) : intParam);
                            }
                        }
                        break;

                    case MAP:
                    case BIG_MAP:
                        paramType = (String) jsonObj.get(PRIM);
                        mapDetails = getMapDescription(jsonObj.getJSONArray(ARGS), type);
                        break;

                    case LIST:
                    case SET:
                        paramType = (String) jsonObj.get(PRIM);
                        details = parse(jsonObj.getJSONArray(ARGS).getJSONObject(0), "", type);
                        break;

                    // currently, we don't retrieve the lambda details (input & output )
                    // case LAMBDA:
                    //    paramType = (String) jsonObj.get(PRIM);
                    //    break;

                    default:
                        paramType = (String) jsonObj.get(PRIM);
                        break;
                }
            }

            // if paramName is empty, we assign it a name based on the entryPoint name and it's order in params list
            if (paramName == null) {
                String paramOfField = type.equalsIgnoreCase(ENTRY_POINT) ? "_param_" : "_field_";
                paramName = name + paramOfField + i;
            }

            pair = new JSONObject();

            if (paramType != null && !paramType.equalsIgnoreCase(UNIT)) {

                pair.put(TYPE, paramType.equals(PAIR_MIN) ? RECORD : paramType);
                pair.put(NAME, paramName);
                pair.put(OPTIONAL, optional);

                if (!pairDescription.isEmpty()) {
                    pair.put(DETAILS, pairDescription);
                }

                if (!details.isEmpty()) {
                    pair.put(DETAILS, details);
                }

                if (!mapDetails.isEmpty()) {
                    pair.put(DETAILS, mapDetails);
                }

                parameters.add(pair);
            }

        }

        return parameters;
    }

    private static JSONObject getMapDescription (JSONArray parameters, String type) {

        JSONObject pair;

        String keyType;
        String valueType;

        // List<JSONObject> intParameters = new ArrayList<>();

        JSONObject key = parameters.getJSONObject(0);
        JSONObject value = parameters.getJSONObject(1);

        keyType = key.get(PRIM).equals(PAIR_MIN) ? RECORD : (String) key.get(PRIM);
        valueType = value.get(PRIM).equals(PAIR_MIN) ? RECORD : (String) value.get(PRIM);

        pair = new JSONObject();

        List<JSONObject> valueDescription;
        List<JSONObject> keyDescription;

        if (keyType.equalsIgnoreCase(RECORD)) {
            keyDescription = parse(key, "", STORAGE);
            // pair.put(KEY_DETAILS, keyDescription);
            pair.put(KEY, keyDescription);
        } else {
            pair.put(KEY, keyType);
        }

        if (valueType.equalsIgnoreCase(RECORD)) {
            valueDescription = parse(value, "", STORAGE);
            // pair.put(VALUE_DETAILS, valueDescription);
            pair.put(VALUE, valueDescription);
        } else {
            pair.put(VALUE, valueType);
        }

        // pair.put(KEY, keyType);
        // pair.put(VALUE, valueType);

        // intParameters.add(pair);

        return pair;
    }

    private static JSONArray decodeParameters (JSONObject jsonObj, JSONArray builtArray) {

        JSONObject left;
        JSONObject right;

        if ((jsonObj.has(ARGS)) || (jsonObj.has(PRIM))) {

            if (builtArray == null) {
                builtArray = new JSONArray();
            }

            // We don't treat the view contracts as the rest as we know that if the are present, we only need an address.
            if (jsonObj.has(ARGS) && !Arrays.asList(CONTRACT, OPTION, LIST, MAP, BIG_MAP, SET, LAMBDA).contains(jsonObj.get(PRIM).toString())) {

                // handle Pairs (Records)
                if (jsonObj.has(ANNOTS) && jsonObj.getString(PRIM).equalsIgnoreCase(PAIR)) {
                    builtArray.put(jsonObj);
                } else {
                    JSONArray myArr = jsonObj.getJSONArray(ARGS);
                    left = myArr.getJSONObject(0);
                    builtArray = decodeParameters(left, builtArray);

                    if (myArr.length() > 1) {
                        right = myArr.getJSONObject(1);
                        builtArray = decodeParameters(right, builtArray);
                    }
                }
            } else {
                builtArray.put(jsonObj);
                return builtArray;
            }
        }

        return builtArray;
    }
}