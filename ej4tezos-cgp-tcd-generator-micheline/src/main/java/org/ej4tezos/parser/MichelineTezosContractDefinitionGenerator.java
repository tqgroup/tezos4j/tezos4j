// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.parser;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;

import java.io.InputStream;
import java.io.FileNotFoundException;

import java.text.MessageFormat;

import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.io.IOUtils;

import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.everit.json.schema.ValidationException;

import org.ej4tezos.cgp.api.TezosContractDefinitionGenerator;

import org.ej4tezos.cgp.api.model.TezosContractDefinitionGeneratorContext;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;

import static org.ej4tezos.papi.model.MichelineConstants.*;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public class MichelineTezosContractDefinitionGenerator implements TezosContractDefinitionGenerator {

    private static final Logger LOG = LoggerFactory.getLogger(MichelineTezosContractDefinitionGenerator.class);

    private Schema schema;
    private StorageDefinitionGenerator storageDefinitionGenerator;
    private EntryPointsDefinitionGenerator entryPointsDefinitionGenerator;

    public MichelineTezosContractDefinitionGenerator () throws TezosGeneratorException {

        LOG.info("A new instance of 'MichelineTezosContractDefinitionGenerator' is getting created; logging on {}", LOG.getName());

        // Get the Schema
        try (InputStream schemaInputStream
                = MichelineTezosContractDefinitionGenerator.class.getResourceAsStream("/contractJSONSchema.json")) {

            // Check the the schema was found:
            if(schemaInputStream == null) {
                throw new FileNotFoundException("No contractJSONSchema.json file found!");
            }

            // Read the input stream:
            JSONObject jsonSchema = new JSONObject(new JSONTokener(schemaInputStream));

            // Load the Schema
            schema = SchemaLoader.load(jsonSchema);

            // Create the generators:
            storageDefinitionGenerator = new StorageDefinitionGenerator();
            entryPointsDefinitionGenerator = new EntryPointsDefinitionGenerator();
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while creating the class instance: {0}", ex.getMessage());
            throw new TezosGeneratorException(message, ex);
        }
    }

    @Override
    public JSONObject generate (TezosContractDefinitionGeneratorContext context, InputStream inputStream) throws TezosGeneratorException {

        //
        LOG.debug("generate (context = {}, inputStream = {})", context, inputStream);

        //
        checkNotNull(context, "context should not be null", TezosGeneratorException.class);
        checkNotNull(inputStream, "inputStream should not be null", TezosGeneratorException.class);

        try {

            //
            JSONObject script = new JSONObject(IOUtils.toString(inputStream, Charset.defaultCharset()));

            //
            return generate(context, script);
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while generating contract definition object: {0}", ex.getMessage());
            throw new TezosGeneratorException(message, ex);
        }
    }

    @Override
    public JSONObject generate (TezosContractDefinitionGeneratorContext context, JSONObject script) throws TezosGeneratorException {

        //
        LOG.debug("generate (context = {}, script = {})", context, script);

        //
        checkNotNull(script, "script should not be null", TezosGeneratorException.class);
        checkNotNull(context, "context should not be null", TezosGeneratorException.class);

        try {

            List<JSONObject> storageDefinition;
            List<JSONObject> entryPointsDefinition;

            // TCD will be stored in:
            JSONObject tezosContractDefinition = new JSONObject();

            // Get code from the script:
            JSONArray code = (JSONArray) script.get(CODE);

            // Validate code format:
            validate(code);

            // Generate EntryPoints Definition
            entryPointsDefinition = entryPointsDefinitionGenerator.generateEntryPointsDefinition(code);

            // Generate Storage Definition:
            storageDefinition = storageDefinitionGenerator.generateStorageDefinition(code, context.getName());

            // = = = = = = = = = = = Build Response = = = = = = = = = = = = = = = = = = = =
            tezosContractDefinition.put(NAME, context.getName());
            tezosContractDefinition.put(ENTRY_POINTS, entryPointsDefinition);
            tezosContractDefinition.put(STORAGE, storageDefinition);

            // Handle optional values
            context.getDeploymentAddress().ifPresent(deploymentAddress -> tezosContractDefinition.put(DEPLOYMENT_ADDRESS, deploymentAddress.toString()));
            context.getSource().ifPresent(source -> tezosContractDefinition.put(SOURCE, source.toString()));
            context.getHost().ifPresent(host -> tezosContractDefinition.put(HOST, host));
            context.getUser().ifPresent(user -> tezosContractDefinition.put(USER, user));
            context.getTimestamp().ifPresent(timestamp -> tezosContractDefinition.put(TIMESTAMP, timestamp));

            // Log and return:
            LOG.debug("tezosContractDefinition = {}", tezosContractDefinition);
            return tezosContractDefinition;
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while generating contract definition object: {0}", ex.getMessage());
            throw new TezosGeneratorException(message, ex);
        }
    }

    private void validate (JSONArray scriptJSONArray) throws TezosGeneratorException {

        //
        LOG.debug("validate (scriptJSONArray = {})", scriptJSONArray);

        //
        checkNotNull(scriptJSONArray, "scriptJSONArray should not be null", TezosGeneratorException.class);

        try {
            schema.validate(scriptJSONArray);
        }

        catch (ValidationException ex) {
            String message = MessageFormat.format("An exception occurred while validating the JSON passed as inputStream: {0}", ex.getMessage());
            throw new TezosGeneratorException(message, ex);
        }
    }
}