// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.parser;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import static org.ej4tezos.papi.model.MichelineConstants.*;

import static org.ej4tezos.utils.asserts.Asserts.checkNotNull;
import static org.ej4tezos.utils.asserts.Asserts.checkState;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public class StorageDefinitionGenerator {

    public List<JSONObject> generateStorageDefinition (JSONArray code, String contractName) throws TezosGeneratorException {

        // Sanity check:
        checkNotNull(code, "Code should not be null", TezosGeneratorException.class);

        // Get Storage args
        JSONObject storageArgs = getStorageArgs(code);

        // Sanity check:
        checkState(storageArgs.length() > 0, "Contract script must have storage object", TezosGeneratorException.class);

        return Parser.parse(storageArgs, contractName, STORAGE);
    }

    private JSONObject getStorageArgs (JSONArray scriptJSONArray) {
        for (int i = 0; i < scriptJSONArray.length(); i++) {
            if (scriptJSONArray.getJSONObject(i).has(PRIM) && scriptJSONArray.getJSONObject(i).get(PRIM).equals(STORAGE)) {
                JSONArray storage =  (JSONArray) scriptJSONArray.getJSONObject(i).get(ARGS);
                return storage.getJSONObject(0);
            }
        }
        return new JSONObject();
    }
}