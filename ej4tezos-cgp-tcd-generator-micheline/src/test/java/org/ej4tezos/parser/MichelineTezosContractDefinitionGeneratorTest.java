// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.parser;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.junit.Test;
import org.junit.Before;

import org.json.JSONObject;
import org.json.JSONTokener;

import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.everit.json.schema.ValidationException;

import java.io.InputStream;

import java.net.URI;
import java.net.InetAddress;

import java.util.Optional;

import java.sql.Timestamp;

import java.text.MessageFormat;

import org.ej4tezos.model.TezosContractAddress;

import org.ej4tezos.cgp.api.exception.TezosGeneratorException;

import org.ej4tezos.cgp.api.model.TezosContractDefinitionGeneratorContext;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import static org.mockito.Mockito.*;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Mohamed Ali Masmoudi (mohammedali.masmoudi@gmail.com)
 */
public class MichelineTezosContractDefinitionGeneratorTest {

    private static final String SOURCE = "https://carthagenet.SmartPy.io";

    private MichelineTezosContractDefinitionGenerator rpcTezosContractDefinitionGenerator;

    // TODO: Implement more tests

    @Before
    public void setup () throws Exception {
        rpcTezosContractDefinitionGenerator = new MichelineTezosContractDefinitionGenerator();
    }

    @Test
    public void generateWithLambdaContractTest () throws Exception {

        String contractAddress = "KT1HVxcwwbsxJeXvZ7VT5negyviF1xcrmD51";

        // Load sample:
        InputStream inputStream = loadSample("WithLambdaScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("WithLambda", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateDexterContractTest () throws Exception {

        String contractAddress = "KT1SZuK6u3QgQFcoYqDwZCd2Xb9pn4HXJW56";

        // Load sample:
        InputStream inputStream = loadSample("DexterContractScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("DexterContract", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateDummyContractTest () throws Exception {

        String contractAddress = "KT1HcKKYbUASeuVyjSANZoLp5mKXXhUZUDFU";

        // Load sample:
        InputStream inputStream = loadSample("DummyContractScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("DummyContract", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateWithOptionalStructuresContractTest () throws Exception {

        String contractAddress = "KT1Da1FrhobjrYBoNph3a11tPBG2PiysZ75A";

        // Load sample:
        InputStream inputStream = loadSample("WithOptionalStructuresScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("WithOptionalStructures", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateEquisafeKYCRegistrarContractTest () throws Exception {

        String contractAddress = "KT1SJdCaQABX3YrfP1kbfs3Vxm2kTTtxg4Hx";

        // Load sample:
        InputStream inputStream = loadSample("EquisafeKYCRegistrarScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("EquisafeKYCRegistrar", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateComplexContractTest () throws Exception {

        // String contractAddress = "KT1PYGmeV69F2brU16dbxr7TeV1X6SsZj3oK";
        String contractAddress = "KT1DMZ5dmrbsqX463NLWirkKZjDBzVAXBdeX";

        // Load sample:
        // InputStream inputStream = loadSample("ComplexContractScript_2.json");
        InputStream inputStream = loadSample("ComplexContractScript.json");

        // Call the generator:
        // JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("ComplexContractScript_2", contractAddress), inputStream);
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("ComplexContract", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateRandomWithRecordInStorageContractTest () throws Exception {

        String contractAddress = "KT1XZ4APZ11kZNTdXoMeSvZErpufG6LFEKPP";

        // Load sample:
        InputStream inputStream = loadSample("RandomWithRecordInStorageContract.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("RandomWithRecordInStorageContract", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateRandomWithRecordInStorageContract_2_Test () throws Exception {

        String contractAddress = "KT19KmNpAFWSGhQ5qXmAcrB97hXk9faprrZe";

        // Load sample:
        InputStream inputStream = loadSample("RandomWithRecordInStorageContract_2.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("RandomWithRecordInStorageContract_2", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateOracleContractTest () throws Exception {

        String contractAddress = "KT1VZprQjiuHG9qz2F7hhiSgzttq9qw6Vvb9";

        // Load sample:
        InputStream inputStream = loadSample("OracleScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("Oracle", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateRandomSampleContractTest () throws Exception {

        String contractAddress = "KT1SSos6qM3idMSy9zrnRPNh5dqihcenyxrY";

        // Load sample:
        InputStream inputStream = loadSample("RandomSampleScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("RandomSample", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateRandomFA12ContractTest () throws Exception {

        String contractAddress = "KT1CwWg1VJ6L9hMd1xwnn3iEFYDXeQmfgt4Z";

        // Load sample:
        InputStream inputStream = loadSample("RandomFA12Script.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("RandomFA12", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateAtomexContractTest () throws Exception {

        String contractAddress = "KT1WpQ6k4njZXyQavXUoh23Up8XP4ysGe6E3";

        // Load sample:
        InputStream inputStream = loadSample("AtomexScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("Atomex", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateEventSinkContractTest () throws Exception {

        String contractAddress = "KT1RzokejsxyURUBeaGoB9DMeqeWz89YJBjy";

        // Load sample:
        InputStream inputStream = loadSample("EventSinkScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("EventSink", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateWithMapAndListAndOptionalScriptContractTest () throws Exception {

        String contractAddress = "KT19yuVzZRcw6FF2EnWujX6boUQAHo3gDjwS";

        // Load sample:
        InputStream inputStream = loadSample("WithMapAndListAndOptionalScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("WithMapAndListAndOptional", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateEuroTzProxyContractTest () throws Exception {

        String contractAddress = "KT1DzuutCr2Mqt2KXhyBkcZJBN2ZJsA7AST6";

        // Load sample:
        InputStream inputStream = loadSample("EuroTzProxyScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("EuroTzProxy", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }


    @Test
    public void generateSimpleContractTest () throws Exception {

        String contractAddress = "KT1BWFSwJwYVGQKtfKWwb9uCuGL13GuqG3B8";

        // Load sample:
        InputStream inputStream = loadSample("SimpleContractScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("SimpleContract", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateStoreValueTest () throws Exception {

        String contractAddress = "KT1Wqqo1AMJZ7iH557rtzQxdTum3Wx41bEwC";

        // Load sample:
        InputStream inputStream = loadSample("StoreValueScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("StoreValue", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateEuroTzTest () throws Exception {

        String contractAddress = "KT1GVGz2YwuscuN1MEtocf45Su4xomQj1K8z";

        // Load sample:
        InputStream inputStream = loadSample("EuroTzScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("EuroTz", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateWithSingleEntryPointTest () throws Exception {

        String contractAddress = "KT1Qni2QpynNToHpSyesbK9LLysckbkv17E5";

        // Load sample:
        InputStream inputStream = loadSample("SingleEntryPointScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("SingleEntryPoint", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateFA12ContractTest () throws Exception {

        String contractAddress = "KT1UbMhCyCduLDw6exdC9kiNTQLKnogAKqPb";

        // Load sample:
        InputStream inputStream = loadSample("FA12ContractScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("FA12Contract", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateWithOptionalValue () throws Exception {

        String contractAddress = "KT1Lwbzc3jxLwBcG1KZRMvK7gDG5oxi3qQaL";

        // Load sample:
        InputStream inputStream = loadSample("WithOptionalValueScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("WithOptionalValue", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    @Test
    public void generateFA2ContractTest () throws Exception {

        String contractAddress = "KT1T7Gir2h4scgcko1uAaX5jMrQnVzU7Xfdt";

        // Load sample:
        InputStream inputStream = loadSample("FA2ContractScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("FA2Contract", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }


    private InputStream loadSample (String name) {
        return MichelineTezosContractDefinitionGeneratorTest.class.getClassLoader().getResourceAsStream(name);
    }

    private void validateGenerated (JSONObject generatedDefinition) throws TezosGeneratorException {

        try {
            // == Get the Schema
            InputStream inputStreamSchema = MichelineTezosContractDefinitionGeneratorTest.class.getResourceAsStream("/generatedJSONSchema.json");
            JSONObject jsonSchema = new JSONObject(new JSONTokener(inputStreamSchema));

            // == Load the Schema
            Schema schema = SchemaLoader.load(jsonSchema);

            // == Validate the input
            schema.validate(generatedDefinition);
        } catch (ValidationException ex) {
            String message = MessageFormat.format("An exception occurred while validating the JSON passed as inputStream: {0}", ex.getMessage());
            throw new TezosGeneratorException(message, ex);
        }
    }

    @Test
    public void generateSwordContractTest () throws Exception {

        String contractAddress = "KT1Uz3ZPgVqKYTt8jJpbeRaZkq8b8PqFkRyg";

        // Load sample:
        InputStream inputStream = loadSample("SwordScript.json");

        // Call the generator:
        JSONObject result = rpcTezosContractDefinitionGenerator.generate(mockContext("Sword", contractAddress), inputStream);

        // Assert result:
        assertThat("Generated template should not be null", result, notNullValue());

        // Assert that the generated JSONObject has the correct format:
        validateGenerated(result);
    }

    private Optional<String> getHostName () throws TezosGeneratorException {
        try {
            return Optional.of(InetAddress.getLocalHost().getHostName());
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving the hostName: {0}", ex.getMessage());
            throw new TezosGeneratorException(message, ex);
        }
    }

    private Optional<String> getUserName () throws TezosGeneratorException {
        try {
            return Optional.of(System.getProperty("user.name"));
        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while retrieving userName: {0}", ex.getMessage());
            throw new TezosGeneratorException(message, ex);
        }
    }

    private Optional<String> getCurrentTimeStamp () {
        // Pass the milliseconds to constructor of Timestamp class
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
        return Optional.of(currentTimestamp.toString());
    }

    private TezosContractDefinitionGeneratorContext mockContext (String name, String address) throws TezosGeneratorException {

        try {
            Optional<TezosContractAddress> contractAddress = Optional.of(TezosContractAddress.toTezosContractAddress(address));
            Optional<URI> source = Optional.of(URI.create(SOURCE));

            TezosContractDefinitionGeneratorContext context = mock(TezosContractDefinitionGeneratorContext.class);

            // == Required values
            doReturn(name).when(context).getName();

            // == Optional values
            doReturn(getCurrentTimeStamp()).when(context).getTimestamp();
            doReturn(contractAddress).when(context).getDeploymentAddress();
            doReturn(getUserName()).when(context).getUser();
            doReturn(getHostName()).when(context).getHost();
            doReturn(source).when(context).getSource();

            return context;

        }

        catch (Exception ex) {
            String message = MessageFormat.format("An exception occurred while mocking the context: {0}", ex.getMessage());
            throw new TezosGeneratorException(message, ex);
        }
    }

}