// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.json.JSONArray;
import org.json.JSONObject;

import org.ej4tezos.api.exception.TezosException;

import org.ej4tezos.api.model.TezosOriginationResult;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public interface TezosContractOriginator {
 
    TezosOriginationResult originate (JSONObject storage, JSONArray code) throws TezosException;

}