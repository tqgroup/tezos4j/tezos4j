// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.exception;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosException extends Exception {

    public TezosException () {
        super();
    }

    public TezosException (String message) {
        super(message);
    }

    public TezosException (String message, Throwable cause) {
        super(message, cause);
    }
}