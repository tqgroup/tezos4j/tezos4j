// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.exception;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosRPCException extends TezosException {

    private int statusCode;
    private String content;

    public static TezosRPCException toTezosRPCException (String message, int statusCode, String content) {

        TezosRPCException exception = new TezosRPCException(message);

        //
        exception.content = content;
        exception.statusCode = statusCode;

        //
        return exception;
    }

    public TezosRPCException () {
        super();
    }

    public TezosRPCException (String message) {
        super(message);
    }

    public TezosRPCException (String message, Throwable cause) {
        super(message, cause);
    }

    public int getStatusCode () {
        return statusCode;
    }

    public String getContent () {
        return content;
    }
}