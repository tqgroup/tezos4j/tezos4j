// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.exception;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosValueNotFoundException extends TezosStorageException {

    public TezosValueNotFoundException() {
        super();
    }

    public TezosValueNotFoundException(String message) {
        super(message);
    }

    public TezosValueNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}