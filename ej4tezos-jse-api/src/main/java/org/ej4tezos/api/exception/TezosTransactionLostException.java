// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.exception;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.api.model.TezosTransactionHash;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosTransactionLostException extends TezosException {

    private TezosTransactionHash transactionHash;

    public static TezosTransactionLostException toTezosTransactionLostException (String message, TezosTransactionHash transactionHash) {
        TezosTransactionLostException ex = new TezosTransactionLostException(message);
        ex.transactionHash = transactionHash;
        return ex;
    }

    public TezosTransactionLostException () {
        super();
    }

    public TezosTransactionLostException (String message) {
        super(message);
    }

    public TezosTransactionLostException (String message, Throwable cause) {
        super(message, cause);
    }

    public TezosTransactionHash getTransactionHash () {
        return transactionHash;
    }
}