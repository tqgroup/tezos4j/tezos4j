// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.exception;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.api.model.TezosTransactionHash;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosRejectedTransactionException extends TezosException {

    private TezosTransactionHash transactionHash;

    public static TezosRejectedTransactionException toTezosRejectedTransactionException (String message, TezosTransactionHash transactionHash) {
        TezosRejectedTransactionException ex = new TezosRejectedTransactionException(message);
        ex.transactionHash = transactionHash;
        return ex;
    }

    public TezosRejectedTransactionException () {
        super();
    }

    public TezosRejectedTransactionException (String message) {
        super(message);
    }

    public TezosRejectedTransactionException (String message, Throwable cause) {
        super(message, cause);
    }

    public TezosTransactionHash getTransactionHash () {
        return transactionHash;
    }
}