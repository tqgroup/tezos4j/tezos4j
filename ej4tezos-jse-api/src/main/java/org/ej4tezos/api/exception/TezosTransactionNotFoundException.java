// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.exception;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.api.model.TezosTransactionHash;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosTransactionNotFoundException extends TezosException {

    private TezosTransactionHash transactionHash;

    public static TezosTransactionNotFoundException toTezosTransactionNotFoundException (String message, TezosTransactionHash transactionHash) {
        TezosTransactionNotFoundException ex = new TezosTransactionNotFoundException(message);
        ex.transactionHash = transactionHash;
        return ex;
    }

    public TezosTransactionNotFoundException () {
        super();
    }

    public TezosTransactionNotFoundException (String message) {
        super(message);
    }

    public TezosTransactionNotFoundException (String message, Throwable cause) {
        super(message, cause);
    }

    public TezosTransactionHash getTransactionHash () {
        return transactionHash;
    }
}