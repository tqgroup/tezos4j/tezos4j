// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.exception;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosStorageException extends TezosException {

    public TezosStorageException () {
        super();
    }

    public TezosStorageException (String message) {
        super(message);
    }

    public TezosStorageException (String message, Throwable cause) {
        super(message, cause);
    }
}