package org.ej4tezos.api.exception;

public class TezosResourceNotFoundException extends TezosException {
    public TezosResourceNotFoundException() {
        super();
    }

    public TezosResourceNotFoundException(String message) {
        super(message);
    }

    public TezosResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
