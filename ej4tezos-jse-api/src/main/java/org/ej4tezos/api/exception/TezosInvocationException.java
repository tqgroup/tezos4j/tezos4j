// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.exception;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frederic HUBIN (frederic.hubin@gmail.com)
 */
public class TezosInvocationException extends TezosException {

    public TezosInvocationException () {
        super();
    }

    public TezosInvocationException (String message) {
        super(message);
    }

    public TezosInvocationException (String message, Throwable cause) {
        super(message, cause);
    }
}