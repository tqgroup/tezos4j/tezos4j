// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.api.exception.TezosException;
import org.ej4tezos.api.exception.TezosStorageException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public interface TezosContractStorage <T extends TezosContractStorageData>{

    enum Mode { CACHED, REAL_TIME }

    /**
     * Sets the mode for the storage.
     * @param mode
     * @throws TezosException if mode is null
     */
    void setMode (Mode mode) throws TezosException;

    /**
     * Returns the storage mode.
     * @return the storage mode
     */
    Mode getMode ();

    /**
     * Loads the storage from the chain.
     * @throws TezosStorageException if the storage cannot be loaded.
     */
    void load () throws TezosStorageException;

    /**
     * Returns the timestamp of the last time the storage was loaded from the chain.
     * @return the time of the last load or zero if nothing has been loaded yet.
     */
    long getTimestamp ();

    /**
     * Returns the instance of the data class.
     * @return
     */
    T getData ();

    /**
     * Returns the data class.
     * @return
     */
    Class<T> getDataClass ();

}