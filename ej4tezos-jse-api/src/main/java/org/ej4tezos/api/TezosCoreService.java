// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;

import org.ej4tezos.api.model.*;

import org.ej4tezos.model.TezosAddress;
import org.ej4tezos.model.TezosBigMap;
import org.ej4tezos.model.TezosPublicAddress;

import org.ej4tezos.api.exception.TezosException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public interface TezosCoreService {

    enum MemoryPoolStatus { APPLIED, REFUSED, BRANCH_REFUSED, BRANCH_DELAYED, UNPROCESSED };

    TezosCounter getCounter (TezosPublicAddress publicAddress) throws TezosException;

    TezosProtocols getProtocols () throws TezosException;

    TezosBlockHash getBlockHash () throws TezosException;

    TezosChainId getChainId () throws TezosException;

    TezosLevel getLevel () throws TezosException;

    TezosLevel getLevel (TezosBlockHash blockHash) throws TezosException;

    TezosBlock getBlock (TezosLevel level) throws TezosException;

    List<TezosTransaction> getTransactionList (TezosLevel level) throws TezosException;

    List<TezosTransaction> getTransactionList (TezosLevel level, TezosAddress destinationAddress) throws TezosException;

    List<TezosMempoolTransaction> getMempoolTransaction (MemoryPoolStatus status) throws TezosException;

    void waitForTransaction (TezosBlockHash startFrom, TezosTransactionHash transactionHash) throws TezosException;

    String packData(Object ... expression) throws TezosException;

    Object getBigMap(TezosBigMap bigMap, TezosPackedData key) throws TezosException;
}
