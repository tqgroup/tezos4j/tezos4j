// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.api.exception.TezosException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author Frédéric Hubin (frederic.hubin@gmail.com)
 */
public interface TezosBlockProducerService {

    void init () throws TezosException;

    void start () throws TezosException;

    void stop () throws TezosException;

    void register (TezosBlockListener listener) throws TezosException;

    TezosBlockListener unregister (TezosBlockListener listener) throws TezosException;

}