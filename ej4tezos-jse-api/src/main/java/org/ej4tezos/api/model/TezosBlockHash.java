// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosBlockHash extends TezosHash {

    public TezosBlockHash () {
        super();
    }

    public static TezosBlockHash toTezosBlockHash (String value) throws TezosModelException {
        return toTezosValueObject(value, TezosBlockHash.class);
    }

    public void validate () throws TezosModelException {

        //
        super.validate();

        // @Todo more checks

    }

    @Override
    public String getType () {
        return "TezosBlockHash";
    }
}