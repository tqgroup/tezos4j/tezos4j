// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.TezosValueObject;
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosBranch extends TezosValueObject<String> {

    public TezosBranch () {
        super();
    }

    public static TezosBranch toTezosBranch (String value) throws TezosModelException {
        return toTezosValueObject(value, TezosBranch.class);
    }

    public void validate () throws TezosModelException {

        //
        super.validate();

        // @Todo more checks

    }
}