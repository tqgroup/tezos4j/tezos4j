package org.ej4tezos.api.model;

import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;

public class TezosBigMapStorage<V> {
    private final ConcurrentHashMap<TezosBigMapStorageKey, V> cache = new ConcurrentHashMap<>();

    public V get(TezosBigMapStorageKey key) {
        return cache.get(key);
    }

    public V putIfAbsent(TezosBigMapStorageKey key, V value) {
        V result = cache.putIfAbsent(key, value);
        return result == null ? value : result;
    }

    @Override
    public String toString() {
        return "TezosBigMapStorage{" +
                "cache=" + cache +
                '}';
    }

    public static class TezosBigMapStorageKey {
        private final Object[] args;
        private final int hashCode;

        public TezosBigMapStorageKey(Object[] args) {
            this.args = args;
            this.hashCode = Arrays.hashCode(args);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TezosBigMapStorageKey that = (TezosBigMapStorageKey) o;
            return Arrays.equals(args, that.args);
        }

        @Override
        public int hashCode() {
            return hashCode;
        }

        @Override
        public String toString() {
            return "{" +
                    Arrays.toString(args) +
                    "}";
        }
    }
}
