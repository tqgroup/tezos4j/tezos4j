// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.math.BigInteger;

import org.ej4tezos.model.TezosValueObject;
import org.ej4tezos.model.exception.TezosModelException;

import static org.ej4tezos.utils.asserts.Asserts.checkState;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosFee extends TezosValueObject<BigInteger> {

    public TezosFee () {
        super();
    }

    public static TezosFee toTezosFee (long fee) throws TezosModelException {
        return toTezosValueObject(BigInteger.valueOf(fee), TezosFee.class);
    }

    public static TezosFee toTezosFee (BigInteger fee) throws TezosModelException {
        return toTezosValueObject(fee, TezosFee.class);
    }

    public void validate () throws TezosModelException {

        //
        super.validate();

        //
        checkState(value.signum() > 0, getType() + " value should be positive", TezosModelException.class);

    }

    @Override
    public String getType () {
        return "TezosFee";
    }

    @Override
    public String toString () {
        return Long.toString(value.longValue());
    }
}