// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import static org.ej4tezos.utils.asserts.Asserts.*;

import org.ej4tezos.model.TezosObject;
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosProtocols implements TezosObject {

    private TezosProtocol next;
    private TezosProtocol current;

    public TezosProtocols () {
        super();
    }

    public static TezosProtocols toTezosProtocols (TezosProtocol current, TezosProtocol next) throws TezosModelException {

        TezosProtocols tezosProtocols = new TezosProtocols();

        tezosProtocols.setNext(next);
        tezosProtocols.setCurrent(current);

        return tezosProtocols;
    }

    public void setNext (TezosProtocol next) throws TezosModelException {
        checkNotNull(next, "next protocol should not be null", TezosModelException.class);
        this.next = next;
    }

    public TezosProtocol getNext () {
        return next;
    }

    public void setCurrent (TezosProtocol current) throws TezosModelException {
        checkNotNull(next, "current protocol should not be null", TezosModelException.class);
        this.current = current;
    }

    public TezosProtocol getCurrent () {
        return current;
    }

    public void validate () throws TezosModelException {
        next.validate();
        current.validate();
    }
}