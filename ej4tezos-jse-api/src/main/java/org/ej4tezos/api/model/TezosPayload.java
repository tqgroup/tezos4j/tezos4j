// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.TezosObject;
import org.ej4tezos.model.TezosAddress;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * @author frederic.hubin@gmail.com
 */
public class TezosPayload implements TezosObject {

    private TezosAddress source;
    private TezosAddress destination;

    public TezosPayload() {
        super();
    }

    public void setSource (TezosAddress source) {
        this.source = source;
    }

    public TezosAddress getSource () {
        return source;
    }

    public void setDestination (TezosAddress destination) {
        this.destination = destination;
    }

    public TezosAddress getDestination () {
        return destination;
    }

    @Override
    public String toString () {
        return ReflectionToStringBuilder.toString(this);
    }
}