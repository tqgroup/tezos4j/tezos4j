// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.json.JSONObject;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public interface JSONContainer {

    boolean hasJson ();

    JSONObject getJson ();

    void setJson (JSONObject data);
}