// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.TezosValueObject;
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosSignature extends TezosValueObject<String> {

    public TezosSignature() {
        super();
    }

    public static TezosSignature toTezosSignature (String value) throws TezosModelException {
        return toTezosValueObject(value, TezosSignature.class);
    }

    public void validate () throws TezosModelException {

        //
        super.validate();

        // @Todo more checks

    }
}