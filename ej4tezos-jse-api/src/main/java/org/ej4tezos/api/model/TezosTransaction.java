// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.json.JSONObject;

import org.ej4tezos.model.TezosObject;
import org.ej4tezos.model.TezosAddress;

import java.text.MessageFormat;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosTransaction implements TezosObject, JSONContainer {

    enum Kind { ENDORSEMENT, TEMPORARY }

    private JSONObject data;
    private String entrypoint;

    private TezosLevel level;
    private TezosAddress source;
    private TezosAddress destination;
    private TezosTransactionHash transactionHash;

    public TezosTransaction () {
        super();
    }

    public void setSource (TezosAddress source) {
        this.source = source;
    }

    public TezosAddress getSource () {
        return source;
    }

    public void setLevel (TezosLevel level) {
        this.level = level;
    }

    public TezosLevel getLevel () {
        return level;
    }

    public void setDestination (TezosAddress destination) {
        this.destination = destination;
    }

    public TezosAddress getDestination () {
        return destination;
    }

    public void setEntrypoint (String entrypoint) {
        this.entrypoint = entrypoint;
    }

    public String getEntrypoint () {
        return entrypoint;
    }

    public void setTransactionHash (TezosTransactionHash transactionHash) {
        this.transactionHash = transactionHash;
    }

    public TezosTransactionHash getTransactionHash () {
        return transactionHash;
    }

    @Override
    public boolean hasJson () {
        return data != null;
    }

    @Override
    public JSONObject getJson () {
        return data;
    }

    @Override
    public void setJson (JSONObject data) {
        this.data = data;
    }

    @Override
    public String toString () {
        return MessageFormat.format("TezosTransaction[hash={0},source={1},destination={2}]", transactionHash, source, destination);
    }
}