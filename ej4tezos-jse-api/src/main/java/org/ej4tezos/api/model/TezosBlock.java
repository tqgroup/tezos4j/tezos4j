// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.util.List;
import java.util.ArrayList;

import java.text.MessageFormat;

import org.json.JSONObject;

import org.ej4tezos.model.TezosObject;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosBlock implements TezosObject, JSONContainer {

    private JSONObject data;
    private TezosLevel level;
    private TezosBlockHash hash;
    private List<TezosTransaction> transactionList = new ArrayList<>();

    public TezosBlock () {
        super();
    }

    public void setLevel (TezosLevel level) {
        this.level = level;
    }

    public TezosLevel getLevel () {
        return level;
    }

    public void setHash (TezosBlockHash hash) {
        this.hash = hash;
    }

    public TezosBlockHash getHash () {
        return hash;
    }

    public List<TezosTransaction> getTransactionList () {
        return transactionList;
    }

    @Override
    public boolean hasJson () {
        return data != null;
    }

    @Override
    public JSONObject getJson () {
        return data;
    }

    @Override
    public void setJson (JSONObject data) {
        this.data = data;
    }

    @Override
    public String toString () {
        return MessageFormat.format("TezosBlock[level={0},transaction.size={1}]", level, transactionList.size());
    }
}