// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.json.JSONObject;

import org.ej4tezos.model.TezosObject;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosMempoolTransaction implements TezosObject, JSONContainer {

    private JSONObject data;

    private TezosLevel level;
    private TezosBranch branch;
    private TezosSignature signature;
    private TezosTransactionHash hash;
    private TezosTransaction.Kind kind;

    public TezosMempoolTransaction () {
        super();
    }

    public void setLevel (TezosLevel level) {
        this.level = level;
    }

    public TezosLevel getLevel () {
        return level;
    }

    public void setBranch (TezosBranch branch) {
        this.branch = branch;
    }

    public TezosBranch getBranch () {
        return branch;
    }

    public void setKind (TezosTransaction.Kind kind) {
        this.kind = kind;
    }

    public TezosTransaction.Kind getKind() {
        return kind;
    }

    public void setSignature (TezosSignature signature) {
        this.signature = signature;
    }

    public TezosSignature getSignature () {
        return signature;
    }

    public void setHash (TezosTransactionHash hash) {
        this.hash = hash;
    }

    public TezosTransactionHash getHash () {
        return hash;
    }

    @Override
    public boolean hasJson () {
        return data != null;
    }

    @Override
    public JSONObject getJson () {
        return data;
    }

    @Override
    public void setJson (JSONObject data) {
        this.data = data;
    }

    @Override
    public String toString () {
        return ReflectionToStringBuilder.toString(this);
    }
}