// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.math.BigInteger;

import org.ej4tezos.model.TezosValueObject;
import org.ej4tezos.model.exception.TezosModelException;

import static org.ej4tezos.utils.asserts.Asserts.checkState;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosLevel extends TezosValueObject<BigInteger> {

    public TezosLevel () {
        super();
    }

    public static TezosLevel toTezosLevel (BigInteger level) throws TezosModelException {
        return toTezosValueObject(level, TezosLevel.class);
    }

    public static TezosLevel toTezosLevel (int level) throws TezosModelException {
        return toTezosValueObject(BigInteger.valueOf(level), TezosLevel.class);
    }

    public static TezosLevel toTezosLevel (long level) throws TezosModelException {
        return toTezosValueObject(BigInteger.valueOf(level), TezosLevel.class);
    }

    public static TezosLevel toTezosLevel (String level) throws TezosModelException {
        return toTezosValueObject(new BigInteger(level), TezosLevel.class);
    }

    public void validate () throws TezosModelException {

        //
        super.validate();

        //
        checkState(value.signum() > 0, getType() + " value should be positive", TezosModelException.class);

    }

    public boolean before(TezosLevel other) {
        return getValue().compareTo(other.getValue()) < 0;
    }

    public boolean after(TezosLevel other) {
        return getValue().compareTo(other.getValue()) > 0;
    }

    public TezosLevel next() throws TezosModelException {
        return toTezosLevel(getValue().add(BigInteger.valueOf(1)));
    }

    @Override
    public String toString () {
        return value.toString();
    }
}