// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosTransactionHash extends TezosHash {

    public TezosTransactionHash () {
        super();
    }

    public static TezosTransactionHash toTezosTransactionHash (String value) throws TezosModelException {
        return toTezosValueObject(value, TezosTransactionHash.class);
    }

    public void validate () throws TezosModelException {

        //
        super.validate();

        // @Todo more checks

    }

    @Override
    public String getType () {
        return "TezosTransactionHash";
    }
}