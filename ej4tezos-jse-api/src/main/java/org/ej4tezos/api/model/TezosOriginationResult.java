// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.TezosObject;
import org.ej4tezos.model.TezosContractAddress;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosOriginationResult implements TezosObject {

    private TezosContractAddress contractAddress;
    private TezosTransactionHash transactionHash;

    public TezosOriginationResult () {
        super();
    }

    public static TezosOriginationResult toTezosOriginationResult (TezosContractAddress contractAddress, TezosTransactionHash transactionHash) {
        TezosOriginationResult result = new TezosOriginationResult();
        result.setContractAddress(contractAddress);
        result.setTransactionHash(transactionHash);
        return result;
    }

    public void setContractAddress (TezosContractAddress contractAddress) {
        this.contractAddress = contractAddress;
    }

    public TezosContractAddress getContractAddress () {
        return contractAddress;
    }

    public void setTransactionHash (TezosTransactionHash transactionHash) {
        this.transactionHash = transactionHash;
    }

    public TezosTransactionHash getTransactionHash () {
        return transactionHash;
    }

    @Override
    public String toString () {
        return ReflectionToStringBuilder.toString(this);
    }
}