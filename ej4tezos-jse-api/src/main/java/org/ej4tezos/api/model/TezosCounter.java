// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import java.math.BigInteger;

import static org.ej4tezos.utils.asserts.Asserts.checkState;

import org.ej4tezos.model.TezosValueObject;

import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosCounter extends TezosValueObject<BigInteger> {

    public TezosCounter () {
        super();
    }

    public static TezosCounter toTezosCounter (BigInteger value) throws TezosModelException {
        return toTezosValueObject(value, TezosCounter.class);
    }

    public TezosCounter increment () throws TezosModelException {
        setValue(getValue().add(BigInteger.ONE));
        return this;
    }

    public void validate () throws TezosModelException {

        //
        super.validate();

        //
        checkState(value.signum() >= 0, getType() + " value should be non-negative", TezosModelException.class);

    }

    @Override
    public String toString () {
        return value.toString();
    }
}