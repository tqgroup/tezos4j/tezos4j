package org.ej4tezos.api.model;

import org.ej4tezos.api.exception.TezosException;

public interface TezosPackedData {
    byte[] getRawPackedData();

    byte[] getRawExpressionIdHash() throws TezosException;

    String getExpressionIdHash() throws TezosException;

    String getLedgerBlake2bHash() throws TezosException;

    byte[] getRawSha256Hash() throws TezosException;

    byte[] getRawSha512Hash() throws TezosException;
}
