// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.TezosValueObject;
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public abstract class TezosHash extends TezosValueObject<String> {

    public TezosHash( ) {
        super();
    }

    public void validate () throws TezosModelException {

        //
        super.validate();

        // @Todo more checks

    }
}