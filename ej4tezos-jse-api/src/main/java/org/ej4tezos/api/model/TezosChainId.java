// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
package org.ej4tezos.api.model;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import org.ej4tezos.model.TezosValueObject;
import org.ej4tezos.model.exception.TezosModelException;

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
public class TezosChainId extends TezosValueObject<String> {

    public TezosChainId () {
        super();
    }

    public static TezosChainId toTezosChainId (String value) throws TezosModelException {
        return toTezosValueObject(value, TezosChainId.class);
    }

    public void validate () throws TezosModelException {

        //
        super.validate();

        // @Todo more checks

    }
}