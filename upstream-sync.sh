#!/bin/bash -eEu

# Requires git filter-repo (`dnf install git-filter-repo` or similar)

CHILD_JSE_REPOS="ej4tezos-jse-api ej4tezos-jse-crypto-default-impl ej4tezos-jse-default-impl ej4tezos-jse-model ej4tezos-jse-papi \
            ej4tezos-jse-proxy ej4tezos-jse-sample-fa12 ej4tezos-jse-utils-assert ej4tezos-jse-utils-bytes \
            ej4tezos-jse-utils-io ej4tezos-jse-utils-micheline ej4tezos-jse-utils-string"
CHILD_MAVEN_REPOS="ej4tezos-cgp-tcd-generator-micheline ej4tezos-cgp-api ej4tezos-cgp-java-generator-velocity \
                   ej4tezos-cgp-plugin ej4tezos-cgp-sample"
CHILD_REPOS="$CHILD_JSE_REPOS $CHILD_MAVEN_REPOS"

function merge_upstream_repo() {
  set -eEu

  local REMOTE=$1
  local REMOTE_URL=$2
  local BRANCH=$3
  local TARGET_BRANCH=$4

  git remote add $REMOTE $REMOTE_URL
  git fetch $REMOTE
  git checkout $REMOTE/$BRANCH

  git filter-repo --force --refs refs/remotes/$REMOTE/$BRANCH --to-subdirectory-filter $REMOTE
  git checkout $TARGET_BRANCH
  git merge --allow-unrelated-histories --no-edit $REMOTE/$BRANCH
  git remote remove $REMOTE
}

set -x

BASE_BRANCH=${BASE_BRANCH:-develop}
git checkout $BASE_BRANCH

if [ -z "${TARGET_BRANCH:-}" ]; then
  TARGET_BRANCH="upstream_sync_$(date -u '+%Y-%m-%dT%H_%M_%SZ')"
  git checkout -b $TARGET_BRANCH
else
  git checkout $TARGET_BRANCH
fi

# Merge repos
for r in $CHILD_JSE_REPOS; do
  merge_upstream_repo $r git@gitlab.com:tezos-paris-hub/ej4tezos/java-se-connectivity/$r.git develop $TARGET_BRANCH
done

for r in $CHILD_MAVEN_REPOS; do
  merge_upstream_repo $r git@gitlab.com:tezos-paris-hub/ej4tezos/maven-code-generator-plugin/$r.git develop $TARGET_BRANCH
done

git add \*
git commit -a -m "Upstream sync as of $(date -u '+%Y-%m-%dT%H:%M:%SZ')"
git gc --aggressive
